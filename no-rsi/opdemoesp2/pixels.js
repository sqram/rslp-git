var pixels = {}


/* Emiles*/
pixels.emiles = function() {
	var img = document.createElement('img')
	img.width = 1
	img.height = 1
	img.setAttribute('src','https://www.e-miles.com/autocredit.do?pc=5248KUXSM2FSK7M&icampaignID=67BB8B')
	document.querySelector("body").appendChild(img);
}


/* Burst Demo */
pixels.burstDemo = function() {
	var script   = document.createElement("script");
	script.type  = "text/javascript";
	script.src   = "http://reporting.burstdirect.com/pixel.js?cid=24605&trid=";
	document.body.appendChild(script);
}


/* RocketFuel (purchase conversion */
pixels.rocketFuelPurchase = function() {
	var cache_buster = parseInt(Math.random()*99999999);
	var img = document.createElement('img')
	img.width = 1
	img.height = 1
	img.setAttribute('src','http://p.rfihub.com/ca.gif?rb=3087&ca=20557137&ra=' + cache_buster)
	document.querySelector("body").appendChild(img);
}


/* Nanigans */
pixels.nanigans = function() {
	var img = document.createElement('img')
	img.width = 1
	img.height = 1
	img.setAttribute('src','//api.nanigans.com/event.php?app_id=18060&type=install&name=reg')
	document.querySelector("body").appendChild(img);
}


/* Comission Junction */
pixels.commissionJunction = function() {
	function GUID () {
	    var S4 = function () {
			return Math.floor(Math.random() * 0x10000 /* 65536 */).toString(16);
		};
		return (S4() + S4() + "-" + S4() + "-" + S4() + "-" +  S4() + "-" + S4() + S4() + S4() );
	}
	var guid = GUID()
	var iframe = document.createElement('iframe')
	iframe.width = 1
	iframe.height = 1
	iframe.setAttribute('src','https://www.emjcd.com/tags/c?containerTagId=1584&AMOUNT=0&CID=1500591&OID='+guid+'&TYPE=355357&CURRENCY=0')
	document.querySelector("body").appendChild(iframe);
}


/* Facebook email */
pixels.facebookEmail = function() {
	var img = document.createElement('img')
	img.width = 1
	img.height = 1
	img.setAttribute('src','https://www.facebook.com/offsite_event.php?id=6013984543448&value=0&currency=USD')
	document.querySelector("body").appendChild(img);
}


/* Google Demo Conversion */
pixels.googleDemoConversion = function() {
	var img = document.createElement('img')
	img.width = 1
	img.height = 1
	img.setAttribute('src','www.googleadservices.com/pagead/conversion/1072735889/?value=0&label=GW2JCL_60AMQkc3C_wM&guid=ON&script=0')
	document.querySelector("body").appendChild(img);
}


/* AMGDT */
pixels.amgdgt = function() {
	// Unfortunatelly gonna  use jQuery for this one.
	$('body').append('<!-- Begin: AMGDGT Tag -->\r\n<script language=\"Javascript\">amgdgt_ctr=\"11984\";amgdgt_t=\"x\";<\/script><script type=\"text\/javascript\" src=\"https:\/\/cdns.amgdgt.com\/base\/js\/v1\/amgdgt.js\"><\/script>\r\n<noscript><iframe src=\"https:\/\/ad.amgdgt.com\/ads\/?f=i&t=x&ctr=11984&rnd='+Math.floor(Math.random()*99999999999)+'\" width=\"1\" height=\"1\" frameborder=\"0\"><\/iframe><\/noscript>\r\n<!-- End: AMGDGT Tag -->');
}

/* Convertro User Tag */
pixels.convertroUserTag = function(email) {
	$CVO = window.$CVO || [];
	$CVO.push([ 'trackEvent', {
		type: 'lead-language-demo',
		id: null,
		amount: '1'
	}]);
	$CVO.push([ 'trackUser', {
		id: email
	}]);
}


/* YieldMo Ad Conversion */
pixels.yieldmoConversion = function() {
	var rs_marketing_src = RSUI.util.getCookie("rs_marketing_src");
	if (rs_marketing_src == 'yieldmoad') {
		var click_id = RSUI.util.getCookie('click_id')
		if(typeof click_id != 'undefined' && click_id !=''){
			var img = document.createElement('img')
			img.width = 1
			img.height = 1
			img.setAttribute('src','http://tkr.yieldmo.com/t_adt/adt?click_id='+clickid+'&conversion_type=lead')
			document.querySelector("body").appendChild(img);
		}
	}
}


