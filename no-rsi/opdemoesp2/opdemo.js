
/*
 * Detects if we are on a mobile device.
 * User-agent sniffing isn't reliable, but Yolo.
 */
isDesktop = (function()  {
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
 		return false
	return true

})()



/*
 * First, if we are on a mobile or tablet device, we
 * don't waste bandwidth with the masthead video.
 * Here we check to see if we're on mobile/tablet,
 * and if so, do not prepend video to body
 */
var video = ''
	+ '<video '
	+ 'preload="auto"'
	+ ' autoplay="true"'
	+ ' loop="loop"'
	+ ' muted="muted"'
	+ ' volume="0"'
	+ ' id="video-masthead"'
	+ ' poster="img/vidbg.jpg">'
	+ '<source src="video-header.webm" type="video/webm">'
	+ '</video>'
	;


if (isDesktop) $('#container-top-wrapper').prepend(video)


// Elements we will target more than once
var $defaultOption 		= $('#default-option')
	, $dropdown 		= $('ul#dropdown')
	, $dropdownLi 		= $('ul#dropdown li')
	, $inputWrapper 	= $('#input-wrapper')
	, $circlesLi 		= $('ul#circles li')
	, $lightboxDemo 	= $('#lightbox-demo')
	, $videoMasthead 	= $('#video-masthead')
	, $form 			= $('form')
	, $postDemo			= $('#offer-postdemo')
	;


// Show/hide languages when dropdown is clicked
$defaultOption.click(function() {
	$dropdown.slideToggle()
})



/*
 * Play audio when circles are hovered.
 * If we're on mobile/tablet, it has to be
 * a click event, not hover.
 */
if (isDesktop) {

	$circlesLi.hover(function() {
		$(this).find('audio')[0].play()
		$(this).find('img').fadeIn()
	}, function () {
		var audio = $(this).find('audio')[0]
		audio.pause()
		audio.currentTime = 0
		$(this).find('img').fadeOut()
	});

} else {

	$circlesLi.click(function() {
		$img = $(this).find('img')
		a = $(this).find('audio')
		a.get(0).play();
		a.get(0).addEventListener('ended', function() {
			$img.fadeOut()
		}, false)
		$img.fadeIn()
	})

}


// When a language is clicked in the dropdown...
$dropdownLi.click(function() {
	$dropdown.slideUp(250, function() {
		$inputWrapper.slideDown()
	})

	// Add a class of 'selected'
	$dropdownLi.removeClass('selected')
	$(this).addClass('selected')

	// Set defaultOption's value to the selected language
	$defaultOption.html( $(this).html() )
})



$form.submit(function(e) {
	e.preventDefault()

	var $inputEmail = $('input[type=text]')
		, $languageError = $('#language-error')
		, error = false
		, email = $('input[type=text]').val()
		, selectedLanguage = $dropdown.find('.selected').data('code') || 'en-es'
		;

	// This should never happen since to see email field, you have to select a language.
	// but add it for good measure anyway.
	if (!$dropdownLi.find('.selected')) {
		error = true
		$languageError.fadeIn()
	} else {
		$languageError.fadeOut()
	}

	// Only validate email if user doesn't have the demotaken cookie
	if (!RSUI.util.getCookie('demotaken')) {
		if (!validateEmail(email)) {
			error = true
			$inputEmail.addClass('input-error')
		} else {
			$inputEmail.removeClass('input-error')
		}

		// Event18 - when start demo button is clicked
		s.events = "event18";s.t();delete s.events;
	}

	if (!error) {

		sendEmailToE(email, selectedLanguage, true)
	}
})


function sendEmailToE(email, lang, open) {
	var email = email ||RSUI.util.getCookie('rsDemoEmail') || grabUrlParam('email')
	var open = (typeof open === undefined ||  open == true) ? true : false // auto open demo if EC stores email




		var map = {
			'hi-fr' : 'fra',
			'hi-de' : 'deu',
			'hi-it' : 'ita',
			'hi-en' : 'eng',
			'hi-es' : 'esp'
		}

		var demoType = isDesktop?'Hispanic_Demo_Desktop':'Hispanic_Demo_Mobile';

		// Send to EC 
		var data = {
			email : email,
			demo_lang : map[lang],
			cis_name : 'US Hispanic Demo Leads',
			website: 'EH_WEBSITE',
			form_type : 'demo',
			demo_type : demoType,
			form_url : window.location.pathname,
			newsletter_type : "Top_Landing_Opdemoesp2",
			cid : _satellite.getVar("mostrecentcampaign")
		}

		 var url = (window.location.href.search(/(\.stg\.)|(\.local)/i)>-1 ?'http://www.stg' :'http://www')
                         + '.rosettastone.com/?p_p_id=rosettaajaxsubmit_WAR_rosettaajaxsubmitportlet&p_p_lifecycle=2&redirect2mobile=no';
      

      var request = $.ajax({              
        url: url,
        type: "POST",
        data: data
      });
      // S-EVENTS
      request.done(function(msg) {
        if (JSON.parse(msg)[0]['cisFlag']  == 'true') { 
				// Event19 - Email collected in EC
				s.events="event19";	s.t(); delete s.events;
				if (!$('#mboxinnner').length) {
					var tgtdiv = document.createElement("div");
					tgtdiv.setAttribute("id", "mboxinner");
					document.querySelector("body").appendChild(tgtdiv);
					mboxDefine('mboxinner','US_RS_DemoLeadConfirm', rs.mbox_env);
					mboxUpdate('US_RS_DemoLeadConfirm',  rs.mbox_env);
				}

				// Cookie that demo is taken so they don't input email again
				RSUI.util.setCookie('demotaken', "1", 1, 30 * 1000 * 60 * 60 * 24);

				hideEmailInput();


				// record the email in a cookie that expires in 30 days
				var days_till_expiration = 30;
				var expiration = new Date(new Date().getTime() + days_till_expiration*24*60*60*1000).toUTCString();
				document.cookie='rsDemoEmail='+email+'; expires='+expiration+'; path=/; domain=.rosettastone.com';

				if (open){
					openDemo(lang);
				}


				//$('input[type=text]').removeClass('input-error').attr('placeholder', 'E-mail')
			} else {
				// Very unlikely we'll reach this. Must've been an error in myaccount
				$('input[type=text]').addClass('input-error').attr('placeholder', 'Sorry, try again!')
			}
		})

	//} // else
}


function openDemo(lang) {
	lang = typeof lang !== 'undefined' ? lang : 'en-es'

	if (!isDesktop) {
		setTimeout(function() {
			window.location.href = 'http://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=' + lang
		}, 2000)

	} else {



		var demoFrame = ""
			+ "<iframe "
			+ " id='demo'"
			+ " src='http://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=" + lang + "'"
			+ " width='100%'"
			+ " height='100%'"
			+ " scrolling='no'"
			+ "></iframe>"
			;

		// Append iframe inside lightbox
		$lightboxDemo.append(demoFrame)

		// Center lightbox horizontally
		var marginLeft = ((window.innerWidth - $lightboxDemo.innerWidth()) / 2) + 'px'
		$lightboxDemo.css({
			'margin-left' :  marginLeft
		})

		// Pause masthead video
		$('#video-masthead')[0].pause()

		// Now it's ready to be shown
		$lightboxDemo.fadeIn()

		// Store lightbox's height. We'll need it when we close postdemo offer
		var originalHeight = {
			'min-height' : $lightboxDemo.css('min-height'),
			'height' : $lightboxDemo.css('height')
		}

		/*
		 * Close event.
		 * First click removes iframe and shows postdemo offer,
		 * second click hides lightbox
		 */
		$lightboxDemo.find('img').click(function() {

			// Remove demo iframe
			$lightboxDemo.find('iframe').fadeOut('fast').remove()

			// Unbind click so we can rebind for postdemo offer
			$(this).unbind('click')

			// Shrink height of lightbox so postoffer looks neater
			$lightboxDemo.css({'height': 'auto','min-height': 0})

			// Show postdemo offer
			$postDemo.slideDown('fast');

			/* re-closing */
			$(this).click(function() {
				$postDemo.hide()
				$lightboxDemo.fadeOut()
				// Restore original height (in case demo plays again)
				$lightboxDemo.css('height', originalHeight['height'])
				$lightboxDemo.css('min-height', originalHeight['min-height'])
				$(this).unbind('click')
			})

			// Resume masthead video
			$('#video-masthead')[0].play()
		})

	} // end if !isDesktop

}


/*
 * Grabs a url param from the url.
 * returns null if not found
 */
function grabUrlParam( name ) {
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp( regexS );
	var results = regex.exec( window.location.href );
	if( results == null )
	return null;
	else
	return results[1];
}


function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}





// Adjust video scale on page load
adjustVideoScale()


// And adjust it when window is resized
window.addEventListener('resize', adjustVideoScale)


/*
 * Function that adjusts video scale
 * in case user resizes browser, or has a viewport
 * less than 1260. To see what this does,
 * comment this out and resize browser width to
 * anything less than 1260
 */
function adjustVideoScale() {
	// 1260 1.1
	// 1129 1.2
	// 1036 1.3
	// 955 1.4
	// 888 1.5
	// 828 1.6
	// 777 1.7
	// 732 1.8
	// 691 1.9
	// < 656 2
	var width = window.innerWidth;
	var scale = 1;

	if (width < 656) {
		scale = 2.6
	} else if (width >= 656 && width <= 691) {
		scale = 2.3
	} else if (width > 691 && width <= 732) {
		scale = 2.1
	} else if (width > 732 && width <= 777) {
		scale = 1.9
	} else if (width > 777 && width <= 828) {
		scale = 1.8
	} else if (width > 828 && width <= 888) {
		scale = 1.7
	} else if (width > 888 && width <= 955) {
		scale = 1.6
	} else if (width > 955 && width <= 1036) {
		scale = 1.5
	} else if (width > 1036 && width <= 1129) {
		scale = 1.4
	} else if (width > 1129 && width <= 1260) {
		scale = 1.3
	}

	$videoMasthead.css({'transform':'scale(' + scale + ')'})
}





/*
 * Custom caroussel
 * ---------------------------------------
 * because we're trying to keep
 * things lightweight around here. Smallest
 * carrousel ever, but it works. (each index
 * in the queue array corresponds with an index
 * in the $li array )
 *
 * When you hit left arrow, queue[] does this:
 * [0, 1, 2]           --> [1, 2, 0]   --> [2, 0, 1]
 *  (initial state)
 *
 * When you hit right arrow, queue[] does this:
 * [0, 1, 2] --> [2, 0, 1] --> [1, 2, 0]
 *
 * After we rearrange the queue array, we proceed with the
 * On initial state, every li is pushed right 100% by
 * being applied left: 100%. Except li at index 0,
 * we give that li a left:0 so its visible. After each
 * animation is done, the li gets a left:100% so that it's
 * hidden. When we are animating right (right arrow),
 * we move the next li left:-100%, so that it can slide in
 * from the right
 */
var $lis = $('#reviews ul li')

// Queue for li work
var queue = [];
easing = ''
duration = 500
/*
 * Every li, with exception of the initial first one at
 * index 0, gets a left position of 100% so they're hidden
 */
$.each($lis, function(i, e) {
	if (i) $(this).css('left', '100%')
	queue.push(i)
})

$('#arrow-right').click(function() {

	var current = queue[0]
	var next = queue[1]

	// Slide current li out to the left. When it's finished, reset position to left: 100%
	$lis.eq(current).animate({
		left: '-=100%'
	}, duration, function() { $(this).css('left', '100%')	})

	// Slide next element in
	$lis.eq(next).animate({
		left: '-=100%'
	}, duration, easing, function() { $(this).css('left', '0')	})

	// current element will become the last in queue
	queue.push(queue.shift())

})


$('#arrow-left').click(function() {

	// When you go right, the next element is the last one
	var current = queue[0]
	var next = queue[queue.length - 1]

	$lis.eq(current).animate({
		left: '+=100%'
	}, duration, easing, function() { $(this).css('left', '100%') })

	// Slide next element in (from the right)
	$lis.eq(next).css('left', '-100%').animate({
		left: '+=100%'
	}, duration, easing, function() { $(this).css('left', '0') })

	// Last element becomes the first one!
	queue.unshift(queue.pop())

})





function hideEmailInput() {
	$('.input-text-wrapper').hide()
	$('.input-submit-wrapper').css({'width':'100%', 'float':'none'})
	$('.input-submit-wrapper input[type=submit]').css({'font-size':'150%'})
}


// if we have a democookie, dont show email form
// var date = new Date()
// date.setMonth(d.getMonth() + 1)
// document.cookie = 'demotaken=1;path=/;domain=rosettastone.com;js cookixpires=' + date.toUTCString()
if (RSUI.util.getCookie('demotaken')) {
	hideEmailInput()
}





if (grabUrlParam('email')) {
	$('#input-wrapper').css({'max-width':'570px'})
	$('.input-text-wrapper').remove()
	$('.input-submit-wrapper').css({
		float : 'none',
		width:'100%'
	})
	sendEmailToE(grabUrlParam('email'), 'hi-en', false)
}




/*
 * Change page's contnets based on parts of the cid
 * example 1: se-br-gg-hispanic-tf3-ym2-50
 * 	 tf3 will be a phone number mapped
 *   ym2/ym1/esp = will define whats inside yellow box
 *   50 = % off displayed in yellow box (applies only if if ym2 is in cid)
 */

var cidphones = {
	tf1: '866-357-9123',
	tf2: '800-236-6707',
	tf3: '01800-925-0335',
	tf4: '800-238-8309',
  tf5: '800-448-7230',
  tf6: '855-255-7791',
  tf7: '877-207-6608',
  tf8: '800-234-2570',
  tf9: '800-542-0954',
  tf10: '800-382-9491'
}

var cidchunks = null
var cid = grabUrlParam('cid')


// by default we show lang dropdown, email input, submit button
// which is all shown if ym = esp
var tfn = 'tf1'
var ym = 'esp'

// overwrite above defaults
if (cid) cidchunks = cid.split('-')
if (cidchunks && typeof cidphones[cidchunks[4]] != 'undefined') {
	tfn = cidchunks[4]
	ym = cidchunks[5]
	var percentage = cidchunks[6]
}




var cidinfo = {
	ym1: {
		topbox: "<p>&iquest;Quieres Probarlo GRATIS?</p><p>Llama al <a href='tel:"+cidphones[tfn].replace(/-/g,"")+"'>"+cidphones[tfn]+"</a></p>"
	},
	ym2: {
		topbox: "<p>Llama ya y recibe " + percentage + "% de descuento.</p><p><a href='tel:"+cidphones[tfn].replace(/-/g,"")+"'>"+cidphones[tfn]+"</a></p>"
	},
	esp: {
		// nothing. load defaults
	}
}




// will change page's contents based on cid
$phone = $('.phone')
$topbox = $('#form-wrapper')
function updatePage() {
	// Update yellow boxto message specified in cidinfo object.
	// If it's a molio campaign though, we show the demo form on desktop,
	// and cidinfo message in mobile.
	if (!window.location.href.match(/-molio/i) ) {
		// NOT mobile
		$topbox.html(cidinfo[ym].topbox)
	} else {
		// this is molio mobile
		if (!isDesktop) {
			$topbox.html(cidinfo[ym].topbox)
		}
	}
	$phone.html('<span class="phone"><a href="tel:'+cidphones[tfn].replace(/-/g,"")+'">'+cidphones[tfn]+'</a></span>')
	$('#mextf').hide()
}


// If it isn't esp, change things according to cid
if (ym != 'esp') {
	updatePage()
}

$('#form-wrapper > *').animate({
	opacity: 1
})


// pixel  - this pixel will replace the tfn
if (cid == 'af-cj-yy-hispanic-tf6-ym1') {
	var script   = document.createElement("script");
	script.type  = "text/javascript";
	script.src   = "//js13.invoca.net/13/integration.js";
	document.body.appendChild(script);
	script.addEventListener('load', function(e){
		Invoca.advertiser_integration = {
	    id : '82685'
  	};
	})
	/*window.setTimeout(function() {
		Invoca.advertiser_integration = {
	    id : '82685'
  	};
  },300)*/


}



// For molio campaign, fire pixels on phone number clicks
if (window.location.href.match(/-molio/i)) {
	var hrefs = Array.prototype.slice.call(document.querySelectorAll('a[href^=tel]'));
	if (hrefs) {
		hrefs.forEach(function(link, i) {
			link.addEventListener('click', function() {
				/* <![CDATA[ */
			  goog_snippet_vars = function() {
			    var w = window;
			    w.google_conversion_id = 896364551;
			    w.google_conversion_label = "bzpFCLWnimEQh-C1qwM";
			    w.google_remarketing_only = false;
			  }

				// DO NOT CHANGE THE CODE BELOW.
				goog_report_conversion = function(url) {
				  goog_snippet_vars();
				  window.google_conversion_format = "3";
				  window.google_is_call = true;
				  var opt = new Object();
				  opt.onload_callback = function() {
				  	if (typeof(url) != 'undefined') {
				    	window.location = url;
				  	}
					}

					var conv_handler = window['google_trackConversion'];
					if (typeof(conv_handler) == 'function') {
					  conv_handler(opt);
					}
				}
				/* ]]> */
				var script   = document.createElement("script");
				script.type  = "text/javascript";
				script.src   = "//www.googleadservices.com/pagead/conversion_async.js";
				document.body.appendChild(script);
			})
		})
	}
}





/*****************************************************************************************
YIELDMO PHONE NUMBER CLICK PIXEL
	owner: Cesar
	purpose: attributing phone number clicks to yieldmo when appropriate
	added: feb 4, 2016
*****************************************************************************************/
(function(){
	if(window.location.href.match(/cid=sm-fb-yy-hispanic-tf8-ym1/i)){

		var hrefs = Array.prototype.slice.call(document.querySelectorAll('a[href^=tel]'));
		if(hrefs){
			hrefs.forEach(function(link, i) {
				
				// on phone number click
				link.addEventListener('click', function(){

					// fire yieldmo pixel

					/* DO NOT MODIFY CODE BELOW */
					(function()
						{ var __yms, __p; __p = document.body || document.head; __yms = document.createElement('script'); __yms.async = true; __yms.src = '//static.yieldmo.com/ym.adv.min.js'; __yms.className = 'ym-adv'; if(__p) __p.appendChild(__yms); }
					)();
					window['_ymq'] = window._ymq || [];
					/* DO NOT MODIFY CODE ABOVE */

					window['_ymq'].push(['lead', '1263044174809512070']);
				});
			});
		}
	}
})();




