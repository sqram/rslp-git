var gulp  = require('gulp')
var $     = require('gulp-load-plugins')()
var gutil = require('gulp-util')
var prod  = gutil.env.type == 'production';



gulp.task('html', () => {
  gulp.src(['./src/*.html'], { base: './src' })
    .pipe(gulp.dest('./dist/'))
    .pipe($.connect.reload())
});


gulp.task('css', () => {
  gulp.src(['src/css/**/*.styl'], { base: './src' })
    .pipe($.stylus({
      'include css': true,
      compress: prod
    }))
    .pipe(gulp.dest('./dist/'))
    .pipe($.connect.reload())
});



gulp.task('js', () => {
  gulp.src(['src/js/**/*.*'], { base: 'src' })
    .pipe(gulp.dest('./dist/'))
    .pipe($.connect.reload())
})


// assets
gulp.task('assets', () => {
  return gulp.src(['src/assets/**/*.*'], { base: 'src' })
  /*.pipe($.cache($.imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    })))
    */
    .pipe(gulp.dest('./dist/'))
});



// connect localserver
gulp.task('connect', () => {
  $.connect.server({
    root: 'dist',
    port: 9000,
    livereload: true
  })
});



// watch
gulp.task('watch', ['connect'], () => {

  // watch html files
  gulp.watch('src/*.html', ['html']);

  // watch .styl files
  gulp.watch('src/css/**/*', ['css']);

  // watch .js files
  gulp.watch('src/js/**/*.*', ['js']);

  // watch image files
  gulp.watch('src/assets/**/*.*', ['assets']);

});



gulp.task('default', ['connect', 'watch'])


gulp.task('build', ['js', 'css', 'html', 'assets'])