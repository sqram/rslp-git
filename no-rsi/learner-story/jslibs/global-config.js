// JavaScript Document
/**
 * Use this in the HTML as lazyload cb to alter carousel elements
 * rs.carouselOnCreate  = function() { }
 */
 
 
rs = {

	mobile_redirect:'http://www.rosettastone.com/lp/mobile-lp/',

	debugjs:false,
	 
	//Quickbuy
	downloadableBox:true,
	fblogin:true,
 	tosubBox:true,
 	showStrike:true,
	
	//Mbox	
	mbox_env:"forQA=true",
	
	//Flowplayer
	flowplayer_lic:"#@4febdbbf4825e91674c",
	
	//Facebook
	facebook_app_id:'421303711221535',
	stg_facebook_app_id:'121466874664786',

	/* Omniture Variables */
	//pageid:null,
	community:"USA",
	omniSuite:"rstonecom",
	omniSuite_staging:"rstonedev",
	currency:"USD",
	omniture_tracksrv:"o.rosettastone.com",
	omniture_tracksrv_secure:"s.rosettastone.com",
	type: "",
	/* Site Wide Offer */
	domainName:".rosettastone.com",
	promo: 'rsfreeship13',
	siteCode:"US_WEBSITE",
	
	/* DEMO */
	demo_ver:'both', //1,2 or both
	demoplaylang:"en-US", //de-DE or en-US
	demo_msglang:'en-US', //es-US or en-US
	demo_skipbuynow:false,
	demo_skiplogin:false,
	demo_optout:true,
	demo_defaultlang:'esp',
	
	cfg: { 
		site_uri_prod: 'http://www.rosettastone.com',
		site_uri_stage: 'http://www.stg.rosettastone.com',
		jslib: "../ui/jslibs/", 
		js: "../ui/js/",
		css: "../ui/css/",
		img: "../ui/img/",
		content: "../ui/content/",
		cart_staging: "http://secure.stg.rosettastone.com/us_en_store_view/checkout/cart/",
		cart_production: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/",
		tosub_prod_id: "1297"
	}
}
/* Google */
google_analytics_id='UA-6029041-1'
google_conversion_id=1072735889
google_conversion_language="en"
google_conversion_format="3"
google_conversion_color="666666"
google_conversion_label="vPuBCN_i8QIQkc3C_wM"
google_conversion_value=0


/* Dynamic Configs */


/*
 * Change some rs variables if we're in production or staging.
 * For local to be treated as staging. have .local in vhost.
 */
rs.windowurl = (window.location.href).split('.');
if( rs.windowurl[1] == 'stg' || rs.windowurl[1] == 'landingpages' ) {
	rs.envName = "staging";
	rs.mbox_env="forQA=true";
	//rs.debugjs=true;
	rs.cart_uri = rs.cfg.cart_staging;
	rs.cart_sku_uri = rs.cfg.cart_sku_staging;
	rs.site_uri = rs.cfg.site_uri_stage;
	rs.omniSuite = rs.omniSuite_staging;

} else {
	rs.envName = "production";
	rs.mbox_env="forProd=true";
	rs.debugjs=false;
	rs.cart_uri = rs.cfg.cart_production;
	rs.cart_sku_uri = rs.cfg.cart_sku_production;
	rs.site_uri = rs.cfg.site_uri_prod;
}

// need to be revisited - should fix omniture now.
var omn_dststdate = "03/10/2013";
var omn_dsteddate = "11/03/2013";




