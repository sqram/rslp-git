/*
 * File created mostly for the need to redirect without
 * waiting for original page to load
*/


// mobile exclusive redirect.


var date = new Date()
var month = "0" + (date.getMonth() + 1)
var day = ("0" + date.getDate()).slice(-2)
var hour = ("0" + date.getHours()).slice(-2)

if (day == 28 && month == '05') {
    mobileexclusive()
}

function mobileexclusive() {
    var destination = 'mobbai'
    var pathname = window.location.pathname
    if (
        (pathname.match(/\/lp\/mobile\//i) || pathname.match(/\/lp\/sbsr\//i))
         && RSI.rsi != destination
    ){
    	if (RSI.rsi.substr(-3) == 'bdi'
            || RSI.rsi.substr(-3) == 'bbi'
            || RSI.rsi == 'sale'
            || RSI.rsi == 'sitewide'
            || RSI.rsi == 'rmsitewide'
       ) {
            // send an ?ed on url to show clock (only if mobile exclusive is for 1 day)
            var ed = (window.location.search ? '&' : '?') + 'ed=0015052800'
    		var r  =  '/lp/mobile/' + destination +'/' + ed
    		window.location.href = r
    	}
    }
}


// while simplified test is on, send all tablet to sbs
if (window.location.pathname.match(/\/lp\/tablet\//i))  {
    var urlrsi = window.location.pathname.split('/')[3]
    if (urlrsi) {
        window.location.href = 'http://rosettastone.com/lp/sbs/' + urlrsi + window.location.search
    }
}




/*;(function() {

    if (window.location.pathname.match(/mobile|why|reviews|methodology/i)
     && RSI.rsi.match(/prodpurple/))
    {
        var tmp = RSI.header.mastheadImage.split('.')
        var name = tmp[0]
        var ext = tmp[1]
        var price = RSI.rsi.slice(-3)
        var masthead = name + price + '.' + ext
        var css = ''
        	+ '<style>'
        	+' #masthead { '
        	+ 'background-image: url("../../globals/img/mastheads/mobile/'+masthead+'")!important;'
        	+ '}'
        	+ '#masthead > * { visibility: hidden}'
        	+ '</style>'


        $('head').append(css)



    }

})()*/
