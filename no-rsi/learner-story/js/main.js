var myaccounturl
    , urlobject = RSUI.util.parseUri(window.location)
    , mode = urlobject.queryKey.mode || null
    , freetrialparam = mode == 'trial' ? '/free_trial' : ''
    , cid = urlobject.queryKey.cid || ''
    ;


// will be used for both form submissions

     // var baseURL = (window.location.href.search(/(\.stg\.)|(\.local)/i)>-1 ?
     //  'http://www.stg' :
     //  'http://www.')
     //  + '.rosettastone.com/?p_p_id=rosettaajaxsubmit_WAR_rosettaajaxsubmitportlet&p_p_lifecycle=2';

    myaccounturl = (window.location.href.search(/(\.stg\.)|(\.local)/i)>-1 ?
            'http://myaccount.stg' : 'https://myaccount')

    myaccounturl += '.rosettastone.com/forms' + freetrialparam + '?callback=?&data='


// show demo or trial based on ?mode url param. demo by default.
// by default demo is shown
if (mode == 'trial') {
    $('#masthead .container button span').html('trial')
    $('#trialarea').show()
    $('#demoarea').hide()
}


// if we have a demotaken cookie, hide email input.
if (RSUI.util.getCookie('rsDemoEmail')) {
    $('#email-demo').hide().val(RSUI.util.getCookie('rsDemoEmail'))
}


//Carousel Setup
var owl =  $(".storCarousel").owlCarousel({
  items : 1,
  navigation : true,
  navigationText : ['Previous Story','Next Story'],
  slideSpeed : 500,
  itemsDesktop : [1199, 1],
  itemsDesktopSmall : [979, 1],
  itemsTablet : [768, 1]
});



$(function(){

  $(".content").owlCarousel({
    items : 1,
    navigation : true,
    pagination : false,
    slideSpeed : 500,
    itemsDesktop : [1199, 1],
    itemsDesktopSmall : [979, 1],
    itemsTablet : [768, 1]
  });


  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

});




//Set video on masthead
/*
 * Detects if we are on a mobile device.
 * User-agent sniffing isn't reliable, but Yolo.
 */
var isDesktop = (function()  {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return false;
    }
    return true;

})();


 // Alert the dev these aren't defined in the HTML file. They must be.
 if ((typeof guid == 'undefined') ||
    (typeof newsletter_type == 'undefined') ||
    (typeof cis_name == 'undefined')) {
        console.log("Missing guid, cis_name or newsletter type.\nPlease try again.")
    }





/*
 * First, if we are on a mobile or tablet device, we
 * don't waste bandwidth with the masthead video.
 * Here we check to see if we're on mobile/tablet,
 * and if so, do not prepend video to body
 */
var video = ''
    + '<video '
    + 'preload="auto"'
    + ' autoplay="true"'
    + ' loop="loop"'
    + ' muted="muted"'
    + ' volume="0"'
    + ' id="video-masthead"'
    + ' poster="img/masthead.jpg">'
    + '<source src="video/grace.webm" type="video/webm">'
    + '<source src="video/stories.mp4" type="video/mp4">'
    + '</video>'
    ;

var $mastSection = $('#masthead');
var $ctaButton = $('button.cta');

if (isDesktop) {
    $mastSection.prepend(video);
}

var $vidMast = $('#video-masthead');

 // Returns date in format yyyy-mm-dd
function getCurrentDate()
{
    var date = new Date();
    var month = date.getMonth() + 1
    var day = date.getDate()
    if (day < 10) day = '0' + day
    if (month < 10) month = '0' + month

    return date.getFullYear() + '-' + month + '-' + day;
}

function getExpirationDate()
{
    var date = new Date()
    var expDate = new Date(date)
    expDate.setDate(expDate.getDate() + 3)
    var nd = new Date(expDate)
    var day = nd.getDate()
    var month = nd.getMonth() + 1
    if (day < 10) day = '0' + day
    if (month < 10) month = '0' + month

    return nd.getFullYear() + '-' + month + '-' + day;
}


/*
 * Function that adjusts video scale
 * in case user resizes browser, or has a viewport
 * less than 1260. To see what this does,
 * comment this out and resize browser width to
 * anything less than 1260
 */
function adjustVideoScale() {
    // 1260 1.1
    // 1129 1.2
    // 1036 1.3
    // 955 1.4
    // 888 1.5
    // 828 1.6
    // 777 1.7
    // 732 1.8
    // 691 1.9
    // < 656 2
    var width = window.innerWidth;
    var scale = 1;

    if (width < 656) {
        scale = 2.6;
    } else if (width >= 656 && width <= 691) {
        scale = 2.3;
    } else if (width > 691 && width <= 732) {
        scale = 2.1;
    } else if (width > 732 && width <= 777) {
        scale = 1.9;
    } else if (width > 777 && width <= 828) {
        scale = 1.8;
    } else if (width > 828 && width <= 888) {
        scale = 1.7;
    } else if (width > 888 && width <= 955) {
        scale = 1.6;
    } else if (width > 955 && width <= 1036) {
        scale = 1.5;
    } else if (width > 1036 && width <= 1129) {
        scale = 1.4;
    } else if (width > 1129 && width <= 1260) {
        scale = 1.3;
    } else if (width > 1260 && width <= 1440) {
        scale = 1.6;
    } else if (width > 1440) {
        scale = 2.4
    }
    $vidMast.css({'transform':'scale(' + scale + ')'});
}


// And adjust it when window is resized
window.addEventListener('resize', adjustVideoScale);

// Adjust video scale on page load
adjustVideoScale();

//function for smoothScroll


// Same one from main site
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}






// Demo form submission
$('#demoform').submit(function(e) {
    e.preventDefault();
    var language = $('#demoform select').children(":selected").attr('value');
    var email = RSUI.util.getCookie('rsDemoEmail') || $('#email-demo').val()

    if (language && validateEmail(email) ) {
        $.when(submitEmailToEC(email, language)).then(openDemo(language))
        // Event18 - when start demo button is clicked

    }
    s.events = "event18";s.t();delete s.events;
});

function submitEmailToEC(email, langCode) {
    var defer = $.Deferred()

    var map = {
        'en-fr' : 'fra',
        'en-de' : 'deu',
        'en-it' : 'ita',
        'en-en' : 'eng',
        'en-es' : 'esp'
    }
    var data = {
        email : email,
        demo_lang : map[langCode],
        cis_name : 'mobile demo',
        website: 'US_WEBSITE',
        form_type : 'demo',
        demo_type : isDesktop?'Demo_Desktop':'Demo_Mobile',
        form_url : window.location.pathname,
        newsletter_type : "Top_Landing_Stories",
        cid : _satellite.getVar("mostrecentcampaign")
    }

    var url = (window.location.href.search(/(\.stg\.)|(\.local)/i)>-1 ?'http://www.stg' :'http://www')
                         + '.rosettastone.com/?p_p_id=rosettaajaxsubmit_WAR_rosettaajaxsubmitportlet&p_p_lifecycle=2&redirect2mobile=no';
      

      var request = $.ajax({              
        url: url,
        type: "POST",
        data: data
      });
      // S-EVENTS
      request.done(function(msg) {
        if (JSON.parse(msg)[0]['cisFlag']  == 'true') { 
            // Doty's demo pixel
            $('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1041440066/?label=WzGCCNzOy1gQwrrM8AM&amp;guid=ON&amp;script=0"/>')
            // Event19 - Email collected in EC
            s.events="event19"; s.t(); delete s.events;


            // record the email in a cookie that expires in 30 days
            RSUI.util.setCookie('rsDemoEmail', email, 1, 30 * 1000 * 60 * 60 * 24);

            $('#email-demo').hide()

            defer.resolve('success')
        }

    })
    return defer.promise()
}


function openDemo(lang) {
    lang = typeof lang !== 'undefined' ? lang : 'en-es'
    if (!isDesktop) {
        setTimeout(function() {
            window.location.href = 'http://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=' + lang
        }, 1500)

    } else {
        var demoFrame = ""
        + "<iframe "
        + " id='demoframe'"
        + " src='http://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=" + lang + "'"
        + " width='100%'"
        + " height='100%'"
        + " frameborder='0'"
        + " scrolling='no'"
        + "></iframe>"
        ;

        $.colorbox({
            iframe: false,
            html: demoFrame,
            innerWidth: '90%',
            innerHeight: '90%'
        });
    }
}


// Trial Form submission
var formError;

$('#signup-form').submit(function(e) {
    e.preventDefault();

    var email = $('input[name=email]').val();
    var first_name = $('input[name=first_name]').val();
    var last_name = $('input[name=last_name]').val();

    var text = {
        content_header : '',
        content_body : '',
        button_text : '',
        button_href : ''
    };

    var $loading = $('form .loading');

    var form_error;
    if (first_name.length < 1) {
        $('input[name=first_name]').addClass('invalid')
        form_error = true
    } else {
        $('input[name=first_name]').removeClass('invalid')
    }

    if (last_name.length < 1) {
        $('input[name=last_name]').addClass('invalid')
        form_error = true
    }  else {
        $('input[name=last_name]').removeClass('invalid')
    }

    if (!validateEmail(email)) {
        $('input[name=email]').addClass('invalid')
        form_error = true
    }  else {
        $('input[name=email]').removeClass('invalid')
    }


    if (form_error === true)
        return false;

    // if(rs.pagename == "freetrial72"){$(".middle").append('<img src="https://www.e-miles.com/autocredit.do?pc=5248KUXSM2FSK7M&icampaignID=6EFR95" width=1 height=1>');}

    // Bring up loading div
    $loading.css({'z-index' : 10})
    var lang = $('#signup-form select').children(":selected").attr('value');
    var data = {
        first_name : first_name,
        last_name : last_name,
        email : email,
        site_code : 'US_WEBSITE',
        language : lang.toUpperCase(),
        trial_lang : lang.toUpperCase(),
        guid : guid,
        cis_name : cis_name,
        newsletter_type : newsletter_type,
        trial_lang : lang.toUpperCase(),
        cid : cid,
        trial_start_date : getCurrentDate(),
        trial_end_date : getExpirationDate(),
        form_type: 'trial_register'
    }


    json = $.toJSON(data);
    var request = $.ajax({
        contentType: "application/json; charset=utf-8",
        url:  myaccounturl + encodeURIComponent(json),
        type: "GET",
        // data: data,
        dataType: "jsonp"
    });

    request.done(function(msg) {
        $loading.css({'z-index' : -1})

        switch (msg.error) {
            // success
            case 0:
                text['content_header'] = "Thanks for signing up!";
                text['content_body']  = "Please check your email for log-in information to start your Rosetta Stone free trial.";
                text['button_text'] = 'LOGIN NOW';
                text['button_href'] = 'https://totale.rosettastone.com/sign_in'
                /* Doty's trial conversion pixel */
                $('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1041440066/?label=7FT1CLzE514QwrrM8AM&amp;guid=ON&amp;script=0"/>')
                s.events = "event91";
                s.t()
                delete s.events
                break;

            // bad email or no language
            case 2:
                text['content_header'] = "Thanks for your interest in Rosetta Stone.";
                text['content_body']  = "There seems to be a problem with your e-mail address. Please check to make sure it's valid.";
                text['button_text'] = '';
                break;

            case 5: // Account exists
                text['content_header'] = "Thanks for your interest in Rosetta Stone.";
                text['content_body']  = "There is already a Rosetta Stone account associated with that email address. You may login with that email address, or choose a different language.";
                text['button_text'] = 'LOGIN NOW';
                text['button_href'] = 'https://totale.rosettastone.com/sign_in'
                break;
            // Max trials has reached
            case 6:
                text['content_header'] = "Thanks for your interest in Rosetta Stone.";
                text['content_body']  = "Our records indicate you have signed up for 5 free trials in the past 30 days and are not eligible for another free trial.";
                text['button_text'] = '';
                break;

            case 3: // Unknown duration
            case 4: //
            case 7: // Trial registration failed
                text['content_header'] = "Thanks for your interest in Rosetta Stone.";
                text['content_body']  = "Sorry! An unexpected error occurred. Please try again.";
                text['button_text'] = '';
                break;
            default:
                text['content_header'] = "Thanks for your interest in Rosetta Stone.";
                text['content_body']  = "Please fill out the form to get started on your free trial!.";
                text['button_text'] = 'LOGIN NOW';
                break;
        }


        $('#trialresult > h2').html( text['content_header'] )
        $('#trialresult > p').html( text['content_body'] )
        $('#trialresult >  a.button span').html( text['button_text'] )
        $('#trialresult > a.button').prop('href', text['button_href'])


        // Hide login button if it doesn't have a text set.
        if (text['button_text'] == "") {
            $('#trialresult > a.button').hide()
        } else {
            $('#trialresult > a.button').show()
        }


        $('.left-column > *').css({visibility:'hidden'})
        $('#trialresult').css({visibility:'visible'}).fadeIn()




    }); // end request done


    request.fail(function(jqXHR, msg) {
        // failed request
    });
});




$('.watch > a, #video > .clip a').click(function(e) {
  e.preventDefault()
  var self = $(this)
  if (isDesktop)  {

   /* $.featherlight(self, {
        iframe : e.currentTarget.href,
        iframeMaxWidth : '100%',
        iframeWidth : 640,
        iframeHeight : 480
    })*/
    $('.youtube').colorbox({
        iframe: true,
        innerWidth: '45%',
        innerHeight: '40%'
    })
  } else {
    window.location.href = e.currentTarget.href;
    return
  }

})

//Placeholder fallback
 function supports_input_placeholder()
  {
    var i = document.createElement('input');
    return 'placeholder' in i;
  }

  if(!supports_input_placeholder()) {
    var fields = document.getElementsByTagName('INPUT');
    for(var i=0; i < fields.length; i++) {
      if(fields[i].hasAttribute('placeholder')) {
        fields[i].defaultValue = fields[i].getAttribute('placeholder');
        fields[i].onfocus = function() { if(this.value == this.defaultValue) this.value = ''; }
        fields[i].onblur = function() { if(this.value == '') this.value = this.defaultValue; }
      }
    }
  }


  // When clicking the masthead button, scroll to form
  $('#masthead a.button').click(function(e) {
    e.preventDefault()
    $('html,body').animate({
        scrollTop: $('.signup').filter(':visible').offset().top
    }, 1000);
  })



// change video, masthead image, and carousel order based on url param
// ?name=mark|chris|delaine|grace

// load grace by default onload.
//owl.trigger("owl.jumpTo", 0)

var map = {
    mark: {
        sliderIndex: 0,
        video: 'mark.webm',
        masthead: 'masthead.jpg'
    },
    grace: {
        sliderIndex: 1,
        video: 'grace.webm',
        masthead: 'grace-masthead-lg.jpg'
    },
    chris: {
        sliderIndex: 2,
        video: 'chris.webm',
        masthead: 'chris-masthead-lg.jpg'
    },
    delaine: {
        sliderIndex: 3,
        video: 'delaine.webm',
        masthead: 'delaine-masthead-lg.jpg'
    }
  }

if (typeof urlobject.queryKey.name != 'undefined' && typeof map[urlobject.queryKey.name] != 'undefined') {

    // update slider
    owl.trigger("owl.jumpTo", map[urlobject.queryKey.name].sliderIndex)

    // update masthead video
    $('video source:eq(0)').attr('src', 'video/' + map[urlobject.queryKey.name].video)

    // update masthead image
    $('#masthead').css({
        background: 'url(img/' + map[urlobject.queryKey.name].masthead + ') 0 -7em no-repeat'
    })
} else {

    // grace slider by default
    owl.trigger("owl.jumpTo", 1)
}


























































































































































































































































































































































