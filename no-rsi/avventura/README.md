## Peter in Italy

_**important**_
please don't commit the dist and node_modules folder.   
Dev files are  in the src folder.  
1. make youre changes in the files inside src/
2. commit
3. run `npm run build`
4. upload  


##### 1. npm i 

##### 2. always edit css via the .styl file(s)

##### 3. Start developing by running   
```
$ gulp
```
and the browser should open a tab to http://localhost:8000

##### 4. Compile for production: (minifies css for now)
```
$ npm run build
```
and a 'dist' directory will be created with production ready files


## Overlays
give each overlay trigger a class of `.overlay-trigger` and a  
`data-overlay=$id`  where $id is the id of the overlay to show.  
Also, put overlay divs as a direct child of <div id='overlays'>