var isDesktop = (function() {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
    return false
  }else{
    return true
  }
})();

// for nav notification
//var chaptersViewed = window.localStorage.getItem('chaptersViewed') || 0


/* Ajax our chapters in */
/*var is_stg = window.location.href.match(/stg=true/i) ? true : false
var chaptersUrl = is_stg ? '/lp/avventura/stage_chapter_content/data-stg.html' : '/lp/avventura/chapter_content/data.html'
if (window.location.href.match(/localhost/i)) chaptersUrl = 'data.html'

$.get(chaptersUrl, function(e) {

  // append html to dom
  $('#chapters').html(e)

  // scroll to a chapter if theres a url param
  if (grabUrlParam('chapter')) {
    scroll($('a[href^="#' + grabUrlParam('chapter') + '"]'))
  }

  // Handle grid overlay image clickss
  var chaptersCount = $(e).find('.chapter-video').length
  var html = ""
  for(i = 1; i <= chaptersCount; i++) {
    var chapterName = $(e).find('.photos').eq(i-1).attr('title').toUpperCase()
    html += '<li><a href="#chapter'+ i +'" onClick="return scrollToChapter(event, ' + i +')"><img src="http://rosettastone.com/lp/avventura/chapter_content/chapter' + i + '/checkpointImage" alt=""></a>' + chapterName + '</li>'
  }
  $('#overlay-checkpoints-grid ul').append(html)



  // Handle notification number
  $('.icon-checkpoints .notification').show().html(chaptersCount )


  $.each($('iframe[id^=video]'), function(i, e) {
    var id =  $(e).attr('id')
    window.setTimeout(function() {
      $(document).trigger('youtube_framewatch', {iframe: document.getElementById(id), title: $(e).attr('id')})
    }, 1000)

  })


})
*/



/*
 * How overlays work:
 * In the html, give all triggers a .overlay-trigger class
 * and overlay=<id> where id is the id of the div to show
 * inside the overlay. Put overlays inside
 * the <div id='overlays'></div> element
 */

var $overlays = $('#overlays')

$('.overlay-trigger').click(function(e) {
  e.preventDefault()
  var overlay_to_show = '#' + $(this).data('overlay')
  $overlays.fadeIn()
  $overlays.find(overlay_to_show).fadeIn()
})

$overlays.find('a.close').click(function(e) {
  e.preventDefault()
  var $visible_overlay = $overlays.find('> div:visible')
  $visible_overlay.fadeOut()
  $overlays.fadeOut()

  // stop any videos if this overlay has one
  var id  = $visible_overlay.find('iframe').attr('id')
  try {
    players[id].stopVideo()
  } catch(e) {}

})


/* These are overlays for the photos section
 * they require a bit of extra work because they
 * contain a slider, and some paragraph with each image.
 * Each img should have the paragraph text in
 * data-text=...
 */
if (isDesktop) {
  $photo_div = $overlays.find('#overlay-photos').find('.photo')

  $('.photos > img').click(function(e) {
    e.preventDefault()

    console.log('clicked')
    // This chapters images. Will be used in overlay slideshow
    var $images = $(this).parent().children()

    // Get index of image clicked so we open slideshow on it
    var index = $images.index($(this))

    // Change the empty img src inside lightbox to the src of img just clicked

    $photo_div.find('img').attr('src', $(this).attr('src').replace('__Thumbs/', ''))



    $photo_div.css({
      //width: $photo_div.find('img').width(),
      //height:  $photo_div.find('img').height()
    })
    // Handle next/prev clicks. We simply update the img src and data-text
    $('span.prev, span.next').click(function(e) {

      if ($(this).attr('class') == 'prev')
        index = typeof $images[index - 1] != 'undefined' ? --index : $images.length

      if ($(this).attr('class') == 'next')
        index = typeof $images[index + 1] != 'undefined' ? ++index : 0

      var image_to_show = $images.get(index)

      // Update the text next to img


      // Image animation effects
      var $img = $photo_div.find('img')

      $img.css({
        //width: $(image_to_show).width() + 'px',
        //height: $(image_to_show).height() + 'px'
      })
    /*  .parent().css({
        width: $(image_to_show).width() + 'px',
        height: $(image_to_show).height() + 'px'
      })*/

      $img.attr('src', $(image_to_show).attr('src').replace('__Thumbs/', ''))
    })
  })
}



/* Scroll to chapter if chapter=N is in url */
/*if (grabUrlParam('chapter')) {
  var target = $('a[href^="#' + grabUrlParam('chapter') + '"]')
  $('html, body').stop().animate({
    scrollTop: (target.offset().top - $('#nav').height()) + 'px'
  }, 1000)
}
*/


/* Scroll to sweepstakes section when a cta is clicked. */
$('a[href^="#woobox"]').on('click', function(e) {
  e.preventDefault()
  var target = $(this.getAttribute('href'))
  $('html, body').stop().animate({
    scrollTop: (target.offset().top - $('#nav').height()) + 'px'
  }, 1000)
})



/* Handle click for 'try demo' buttons, and the form inside those areas */

/*$('.demo-cta a').click(function(e) {
  e.preventDefault()
  $(this).siblings('form').slideDown()
})
  .siblings('form').submit(processForm)

*/

// Handles newsletter form submission
$('#newsletter form').submit(processForm)

$('#chapters').on('click', '.demo-cta a', function(e) {
  e.preventDefault()
  $(this).siblings('form').slideDown().submit(processForm)
})
  //.siblings('form').submit(processForm)



function scroll(target) {
  //var target = $('a[href^="#' + grabUrlParam('chapter') + '"]')
  $('html, body').stop().animate({
    scrollTop: (target.offset().top - $('#nav').height()) + 'px'
  }, 1000)
}





// Scroll to chapter when image in overlay is clicked
$('#overlay-checkpoints-grid li a').click(function(e) {
  e.preventDefault()
  console.log($(this).data('scrollto'))
  $(this).parents('#overlay-checkpoints-grid').find('a.close').click()
  scroll($('a[href^="#' + $(this).data('scrollto') + '"]'))
})

// scroll to a chapter if theres a url param
if (grabUrlParam('chapter')) {
  scroll($('a[href^="#' + grabUrlParam('chapter') + '"]'))
}







/*
 * Grabs a url param from the url.
 * returns null if not found
 */
function grabUrlParam(name) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
  return null;
  else
  return results[1];
}

$(function() {
  $(document).trigger('demo_events_version',{version: 1})

    // Get youtube videos ready
    window.setTimeout(function() {
      $.each($('iframe[id^=video]'), function(i, e) {
        var id =  $(e).attr('id')
        $(document).trigger('youtube_framewatch', {iframe: document.getElementById(id), title: id})
      })
    }, 1000)

})









/*
 * Handles form submissions such as demo
 * or newsletter updates
 */
function processForm(e) {

  e.preventDefault()
  $loader = $(e.target).find('div.loader')
  $notification = $(this).find('div.notification')

  // hide previous error, if any
  $notification.hide()
  function validateEmail(email){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  // not using input[type=email] because its ie10+
  var email = $(this).find('input:first-of-type').val()
  var langcode = $(this).find('select').val()

  if (!validateEmail(email)) {
    $notification.fadeIn()
    return
  } else {
    $notification.fadeOut()
  }

  //briteverify sometimes takes a while, show loader now
  $loader.show()

   // data to send to ec
    var data = {
      email : email,
      cis_name : 'Flash demo',
      newsletter_type : $(this).data('newslettertype'),
      website: 'US_WEBSITE',
      form_url : 'avventura',
      cid : grabUrlParam('cid') || ''
    };


    // Need to send a few extras values to EC if its demo form
    if (data.newsletter_type == 'Top_Landing_Peterdemo') {
      data.demo_lang = langcode
      data.form_type = 'demo'
      data.demo_type = isDesktop ? 'Demo_Desktop' : 'Demo_Mobile'
    }

  // called by briterify if email is valid
  var success = function() {
    $notification.hide()
    $loader.hide()

    var save_url = (window.location.href.match(/www.rosetta/) ? 'http://www' : 'http://www.stg')
      + '.rosettastone.com/?p_p_id=rosettaajaxsubmit_WAR_rosettaajaxsubmitportlet&p_p_lifecycle=2&redirect2mobile=no';

    var request = $.ajax({
      url: save_url,
      type: "POST",
      data: data
    })
    .done(function(msg) {
      if (JSON.parse(msg)[0]['cisFlag']  == 'true') {
        if (data.newsletter_type == 'Top_Landing_Peterupdates') {
          /* This was an email submission for newsletter */

          $('#newsletter').html("<h1 style='font-size:2.5em;margin-bottom: 10px'>Thanks!</h1>You'll receive updates about Peter as he travels down the boot.")
          if (RSUI.util.getCookie('avventura_newsletter') == null) {
            RSUI.util.setCookie('avventura_newsletter', "1", 1, 30 * 1000 * 60 * 60 * 24);
            var days_til_expiration = 30;
            var expiration = new Date(new Date().getTime() + days_til_expiration*24*60*60*1000).toUTCString();
            s.events = "event19, event119"
            s.tl()
            delete s.events;
          }

          //$(document).trigger('demo_email',{email: data.email})
          //s.addEvent ? s.addEvent("event119") : (function(s){ s.events="event119"; })(s); s.tl();

          /*****************************************************************************************
            AMOBEE LEAD CONVERSION PIXEL
            Other names: Adconion, amgdgt
            Owners: Caitlin and Nakesa
            Placements: all demos and peterb signups (/lp/peter/)
            Purpose: tracking leads
            Other notes: the CID should start with: ba-bn-ab
            Date added: July 7, 2016
          *****************************************************************************************/
          try{
            (function amobee_adconion() {
              var cid = 'ba-bn-ab';
              if(window.location.href.match(cid) || document.referrer.match(cid)){
                var cachebuster = Math.floor(Math.random()*999999999999);
                var amobee_pixel = document.createElement('img');
                amobee_pixel.setAttribute('src','https://ad.amgdgt.com/ads/?t=ap&px=69698&rnd='+cachebuster);
                amobee_pixel.setAttribute('width','1');
                amobee_pixel.setAttribute('height','1');
                amobee_pixel.setAttribute('border','0');
                document.body.appendChild(amobee_pixel);
              }
            })();
          }
          catch(e){}


        } else {
          /* This was an email submission for demo */
          if (RSUI.util.getCookie('demotaken') == null) {
            $(document).trigger('demo_email',{email: email})
            RSUI.util.setCookie('demotaken', "1", 1, 30 * 1000 * 60 * 60 * 24);
            var days_til_expiration = 30;
            var expiration = new Date(new Date().getTime() + days_til_expiration*24*60*60*1000).toUTCString();
            document.cookie='rsDemoEmail='+email+'; expires='+expiration+'; path=/; domain=.rosettastone.com';
          }
          open_demo(langcode)
        }
      }
    })



  }.bind(this, data)

  var invalid = function() {
    $notification.fadeIn()
    $loader.hide()
  }

  verify_real_email(email, success, invalid)
}


/*
 * Opens demo by creating iframe.
 * Also stores data in ec
 *
 * @param {langcode} str
 *   3 letter language code (ita, esp, etc)
 */

function open_demo(langcode) {
  var langcode = langcode || 'ita'
  var langmap = {
    'fra': 'en-fr',
    'deu': 'en-de',
    'ita': 'en-it',
    'eng': 'en-en',
    'esp': 'en-es'
  }

  var stg_or_not = window.location.href.match(/\.stg\./i) ? 'stg.':'';
  var demourl = 'http://resources.' + stg_or_not + 'rosettastone.com/CDN/us/rs-i-demo-6/?lang=' + langmap[langcode]
  var demoFrame = ""
    + "<iframe "
    + " id='demo'"
    + " src='"+ demourl + "'"
    + " width='100%'"
    + " height='100%'"
    + " scrolling='no'"
    + "></iframe>"
    ;

  $(document).trigger('demo_start')
  if (isDesktop) {
    $overlays.show().find('#overlay-demo')
      .show()
      .find('.overlay-content > .iframe')
      .append(demoFrame)
    } else {
      window.location.href = demourl
    }


}

/**************************************************
 BRITEVERIFY EMAIL VERIFICATION
***************************************************/
function get_token(){
  window.briteverifyToken = false;

  var form_token = '4047df1c-5078-4e96-b335-f61aa2b92891'; // public key
  var url = 'https://forms-api-v1.briteverify.com/api/submissions/view.json?callback=getBriteverifyToken&form_token='+form_token+'&_='+Date.now();

  // append script to body to get cross-domain jsonp data
  var script = document.createElement('script');
  script.src = url;
  document.body.appendChild(script);

  window.getBriteverifyToken = function(data){
    window.briteverifyToken = data.token;
    delete window.getBriteverifyToken;
    document.body.removeChild(script);
  }
}

// if the demo hasn't already been taken, get an email verification token
if(!RSUI.util.getCookie('demotaken')) {
  get_token();
}

function verify_real_email(email,success,failure){


  if(window.briteverifyToken) {
    var token = window.briteverifyToken;
    var url = 'https://forms-api-v1.briteverify.com/api/submissions/verify.json?callback=briteverify&form_token=4047df1c-5078-4e96-b335-f61aa2b92891&token='+token+'&email='+email+'&_='+Date.now();
    var script = document.createElement('script');
    script.src = url;
    document.body.appendChild(script);

    // if the script url doesn't work (maybe they changed the api)
    script.onerror = function(e) {

    };

    window.briteverify = function(data){
      try {
        // if email is valid
        if(data.status!=='invalid'){
          success()
        } else {
          failure();
        }
        document.body.removeChild(script);
      } catch(e) {
        success();
      }
      delete window.briteverify;
    }
  } else {
    // if no token, ignore verification and move on
    success();
  }
}


