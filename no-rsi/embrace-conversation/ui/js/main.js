/* Default size of video player */
var videoConfig = {
	width: 1180,
    height: 664
}

function getHashParams() {

    var hashParams = {};
    var e,
        a = /\+/g,  // Regex for replacing addition symbol with a space
        r = /([^&;=]+)=?([^&;]*)/g,
        d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
        q = window.location.hash.substring(1);

    while (e = r.exec(q))
       hashParams[d(e[1])] = d(e[2]);
    return hashParams;
}
function windowResized(){
	/* Stuff to make the video fit nice to the screen with a 20% margin around it */
	var h_ratio = videoConfig.height / videoConfig.width;
	var w_ratio = videoConfig.width / videoConfig.height;
	videoConfig.width = Math.floor( $('body').width() - ($('body').width() * .20) );
	videoConfig.height = Math.floor(videoConfig.width * h_ratio);
	if(videoConfig.height > (window.innerHeight - (window.innerHeight * .10)) ) {
		videoConfig.height = Math.floor((window.innerHeight - (window.innerHeight * .20)));
		videoConfig.width = Math.floor(videoConfig.height * w_ratio);
	}
}

var hashParams = getHashParams();
if(!$.isEmptyObject(hashParams) && hashParams.v !== undefined)
	popVideo(hashParams.v);
if(!$.isEmptyObject(hashParams) && hashParams.a !== undefined)
	anchorLink(hashParams.a);

/* Dom Stuff */
var scenes = {};
scenes.scene1Watch = function(){
	if(scenes.scene1_street && scenes.scene1_girls && scenes.scene1_wall){
		scenes.scene1Loaded();
	} else {
		setTimeout(function(){ scenes.scene1Watch() },1000);
	}
}
scenes.scene1Loaded = function(){
	scenes.scene1Ready = true; 
	$('#top .block').fadeOut('fast',function(){
		$(this).remove();
		animateScene();
	})
}

$(function() {
	windowResized();
	setTimeout(function(){$(document).trigger('video_events_version', {version: 1})},500);
	var ie_ver = detectIE(); /* sometimes it is better to just know */
	if(ie_ver)
		$('html').addClass('is-ie ie-'+ie_ver);
	/* IE has no support for background-size transitions so doing all with js */
	$('.hover-tile').hover(function(){ 
		$(this).find('.tile-content')
			.animate({ 
				backgroundSize: '104%' }, 1000);
	}, function(){
		$(this).find('.tile-content')
			.animate({ 
				backgroundSize: '100%' }, 500);
	});
	if (!Modernizr.svg) {
  		$('#s-map-img').attr('src', 'ui/img/us_map.png');
	}
	// Dot Navigation
	$('#dotnav li a').on('click',function(){
		$('#dotnav li.current').removeClass('current');
		$(this).parent().addClass('current');
	});
	//console.log('h: '+$('.tiles .row.third .col.first.fluid').height() +' - t '+ $('#peter-food').height());
	// Image Load w/ Callback
	$('<img>',{src: 'ui/img/bg_main_street.jpg','class':'cache'}).load($(this).attr('src'),function(){
		scenes.scene1_street = true;
	});
	$('<img>',{src: 'ui/img/feature_girls.png','class':'cache'}).load($(this).attr('src'),function(){
		scenes.scene1_girls = true;
	});
	$('<img>',{src: 'ui/img/wall.png','class':'cache'}).load($(this).attr('src'),function(){
		scenes.scene1_wall = true;
	});
	scenes.scene1Watch();
			
	// Get element positions to trigger animation
	scenes.scene2_Y = $('#meet-peter').offset()['top'] - $(window).height() + 200;
	scenes.tiles = $('.tiles').offset()['top'] - $(window).height() + 100;
   	// Get Position for DotNav
   	scenes.follow = $('#bottom').offset()['top'] - $(window).height();
   	scenes.sweepstakes = scenes.follow + 240;
   	animateScene();
   	/* Shim up tile in last row 
   	setTimeout(function(){
		$('#family-travel-tips').css('height',$('.tiles .row.third .col.first.fluid').height() - $('#peter-food').height() );
	},5000);*/
	// Animated scrolling on anchor click for in-page nav
	$('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	    	var target = $(this.hash);
	    	target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      	if (target.length) {
	        	$('html,body').animate({
	          		scrollTop: target.offset().top - 100
	        	}, 1000);
	        	return false;
	      	}
	    }
	});
	/* Captions added to photo using a title attribute on the trigger element */
	$('.photo-tile .tile-content').featherlight(null, { 
		beforeContent: function(){
			$(this.$instance).find('.featherlight-content').append('<div class="lb-caption">'+$(this.$currentTarget).attr('title')+'</div>')
		}
	});
	
	/* To Use this put a data attribute on the link (with class of .link-video ) 
	 or object like: data-video-path="url-to-video"
	 or just slap it in the href of Trigger (anchor) element will also need an ID
	 as the player gets assigned the same ID with 'dynvideo-' prepended to it. 
	 For YouTube put data-youtube='YOUTUBE-ID-OF-VIDEO' on the attrib*/

	$('.link-video').featherlight('<div/>', { 
			variant: 'lightbox-video',
			beforeContent: function(){
				var $c = $(this.$instance).find('.featherlight-content');
				var yt = $(this.$currentTarget).data('youtube');														

				if(typeof yt !== 'undefined') {
					window.currentVideo = 'youtube';
										var iframe = '<iframe \
									class="youtube-embed" \
									style="width:'+videoConfig.width+'px; height:'+videoConfig.height+'px" \
									src="https://www.youtube.com/embed/'+yt+'?rel=0&autoplay=1&showinfo=0" \
									frameborder="0" \
									allowfullscreen> \
									</iframe>';
					$c.append(iframe);

					$(document).trigger('video_start', {
							type: 'PeterB', //REQUIRED
							name: $(this.$currentTarget).attr('id'),
							org: 'Consumer'
					});
				} else {
					var path = (typeof $(this.$currentTarget).data('video-path') !== 'undefined') ? $(this.$currentTarget).data('video-path') : $(this.$currentTarget).attr('href');
					if(typeof path != 'undefined'){
						var vId = (typeof $(this.$currentTarget).attr('id') !== 'undefined') ? 'dynvideo-' + $(this.$currentTarget).attr('id') : 'dynvideo-' + makeid();
						$thisVideo = $('<video id="'+vId+'" width="'+videoConfig.width+'" height="'+videoConfig.height+'" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto"></video>')
										.append('<source src="'+path+'" type="video/mp4"/>');
						$c.append($thisVideo);
						videojs(vId, {}, function(){
							var player = this;
							setTimeout(function(){ player.play() }, 500);
							window.currentVideo = player;
						});

					}
					$(document).trigger('video_start', {
							type: 'PeterB', //REQUIRED
							name: $(this.$currentTarget).attr('id'),
							org: 'Consumer'
					});

				}
			},
			afterClose:function(){
				if(window.currentVideo != undefined && window.currentVideo != 'youtube')
					window.currentVideo.dispose();
				if(window.stop !== undefined) {
				    window.stop();
				} else if(document.execCommand !== undefined) {
				    document.execCommand("Stop", false);
				}

				$(document).trigger('video_end');
			}
	});
	/* Disable links inside of video tiles so we can bind the entire tile */
	//$('.video-tile a').unbind().off().on('click',function(e){ e.preventDefault(); })

	$('.demo-tile').addClass('link-demo').css('cursor','pointer');

	$('.link-demo').featherlight('#demo_lightbox', { 
		variant:'demo-lightbox',
		afterContent:function(){
			var $dfo, $dd, $ii, $iw;
			$c = $(this.$instance).find('.featherlight-content');
			$f = $c.find('form');
			$dfo = $c.find('.default-option').on('click',function(event) {
				$dd = $c.find('.dropdown').slideToggle();
			});
			if (!RSUI.util.getCookie('demotaken') && !RSUI.util.getCookie('rsdemo')) {
				$c.find('.input-wrapper .input-text-wrapper').fadeIn();
			} else {
				$c.find('.input-wrapper .input-submit-wrapper').css({float:'none', margin:'0 auto'});
			}
			// When a language is clicked in the dropdown...
			$ii = $c.find('.dropdown li').on('click', function(event) {
				$dd.slideUp(250);
				$dfo.data('code',$(this).data('code'));
				// Add a class of 'selected'
				$ii.removeClass('selected');
				$(this).addClass('selected');

				// Set defaultOption's value to the selected language
				$dfo.html( $(this).html() );
			});
			$f.on('submit',function(event){
				event.preventDefault();
				$f.submitEmail();
			});			
		},

		beforeClose:function(){
			if($c.hasClass('demo-on')){
				terminateDemo($c);
				return false;
			}
		}
	});

}); /* DOM READY */
$( window ).resize(function() {
  	windowResized();
});
$(window).scroll(function(){
 	animateScene();
});

animateScene = function(){
	var $scene1 = $('#top .animate');
	var $scene2 = $('#meet-peter .animate');
	var $scene3 = $('#donde .animate');
 	var $tiles = $('#tiles');
	var scrollTop = parseInt($(window).scrollTop());
	//console.log(scrollTop);
	//Remove sticky header
	/* if(scrollTop < 5){
		$('.header-container').fadeOut();
	} else if (scrollTop > 4){
		$('.header-container').fadeIn(); 
	} else */
	if(scrollTop < 250 && scenes.scene1Ready){
 		$scene1.addClass('animated');
 		$scene2.removeClass('animated');
 		$('#dotnav li.current').removeClass('current');
 		$('#dotnav li:eq(0)').addClass('current');
 	} else if(scrollTop > scenes.scene2_Y - 150 && scrollTop < 1300){
 		$scene1.removeClass('animated');
 		$scene2.addClass('animated');
 		$('#dotnav li.current').removeClass('current');
 		$('#dotnav li:eq(1)').addClass('current');
 	} 
 	if(scrollTop >= 1300  ){
 		$scene1.removeClass('animated');
 		$scene2.removeClass('animated');
 		$('#dotnav li.current').removeClass('current');
 		$('#dotnav li:eq(2)').addClass('current');
 	}
 	if(scrollTop >= 2300  ){
 		$('#dotnav li.current').removeClass('current');
 		$('#dotnav li:eq(3)').addClass('current');
 	} 
 	if(scrollTop >= scenes.follow  ){
 		$('#dotnav li.current').removeClass('current');
 		$('#dotnav li:eq(4)').addClass('current');
 	}
 	if(scrollTop >= scenes.sweepstakes  ){
 		$('#dotnav li.current').removeClass('current');
 		$('#dotnav li:eq(5)').addClass('current');
 	} 	

 	if(scrollTop > scenes.tiles){
 		$('.tile').each(function(){
 			var $tile = $(this);
 			var inview = $tile.offset()['top'] - $(window).height() + 100;
 			
 			if(scrollTop > inview){
 				//console.log('scroll->'+scrollTop+'| tile->'+ inview + '| tile->'+$tile.attr('id'));
 				if(!$tile.hasClass('open') && !$tile.hasClass('loading')){
	 				$tile.addClass('loading');
	 				setTimeout(function(){ $tile.addClass('open') }, 1000);
	 				setTimeout(function(){ $tile.removeClass('loading') }, 2000);
	 			}
 			} 
 		});


 	}
}

function popVideo(videoLinkID){
	/* 
	 * link-video-meet-peter
	 */
	$(function() { 
		setTimeout(function(){ $('#'+videoLinkID).click(); },500);
	});
}
function anchorLink(anchorLinkID){
	/* Use any of the navigation anchors
	 * #enter-sweepstakes
	 */
	$(function() { 
		setTimeout(function(){ $('#'+anchorLinkID).click(); },500);
	});
}
function makeid(){
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

/* Facebook */
$(function() {
	FB.init({
        appId: '1091422140883673',
        status: true,
        cookie: true,
        xfbml: true,
        oauth: true,
        channelUrl: window.location.protocol + "//" + window.location.hostname + "/facebook/channel"
    });

	$(".fb_button").on('click',function(event) {
        FB.login(function(response) {
            if (response.authResponse) {
                FB.api("/me", function(me) {
                    if (me.email) {
                        if (typeof dtm_fbdemoemail_submitted == "function") {
                            dtm_fbdemoemail_submitted()
                        } else {
                            s.events = "event21";
                            if (s.events == "event21") {
                                s.prop11 = "Email Submit"
                            }
                            s.t();
                            delete s.events
                        }
                        $("#field-email")
                            .val(me.email);
                        $("#email")
                            .val(me.email);
                        RSUI.util.setCookie("curEmailIdsc", me.email);
                        if ((optin_when_checked == "true") || (optin_when_checked == "")) {
                            registerEmail(me.email)
                        } else {
                            loadDemo()
                        }
                    }
                })
            } else {
                if (typeof dtm_fbdemoemail_submitfailed == "function") {
                    dtm_fbdemoemail_submitfailed()
                } else {
                    s.events = "event22";
                    s.t();
                    s.events = ""
                }
            }
        }, {
            scope: "email"
        })
    });
});

/**************************************************
DEMO BUSINESS
***************************************************/
window.isDesktop = !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

// turn the new mobile demo on or off
window.new_mobile_demo_turned_on = false;
/**************************************************
BRITEVERIFY EMAIL VERIFICATION
***************************************************/
function get_token(){
	window.briteverifyToken = false;

	var form_token = '4047df1c-5078-4e96-b335-f61aa2b92891'; // public key
	var url = 'https://forms-api-v1.briteverify.com/api/submissions/view.json?callback=getBriteverifyToken&form_token='+form_token+'&_='+Date.now();

	// append script to body to get cross-domain jsonp data
	var script = document.createElement('script');
	script.src = url;
	document.body.appendChild(script);
	
	window.getBriteverifyToken = function(data){
		// console.log(data);
		// console.log('token: '+data.token);
		
		window.briteverifyToken = data.token;

		delete window.getBriteverifyToken;
		document.body.removeChild(script);
	}
}
// if the demo hasn't already been taken, get an email verification token
if(!RSUI.util.getCookie('demotaken')) {
	get_token();
}

function verify_real_email(email,success,failure){
	if(!success){success = function(){};}
	if(!failure){failure = function(){};}
	if(window.briteverifyToken){

		var token = window.briteverifyToken;
		var url = 'https://forms-api-v1.briteverify.com/api/submissions/verify.json?callback=briteverify&form_token=4047df1c-5078-4e96-b335-f61aa2b92891&token='+token+'&email='+email+'&_='+Date.now();

		var script = document.createElement('script');
		script.src = url;
		document.body.appendChild(script);
		
		// if the script url doesn't work (maybe they changed the api)
		script.onerror = function(e){
			// console.log('onerror');
			// console.log(e);

			// ignore verification and move on
			success();
		};
		// script.onload = function(){this.remove();}

		window.briteverify = function(data){
			try{
				// console.log(data);
				// console.log('validity status: '+data.status);
				
				// if email is valid
				if(data.status!=='invalid'){
					success();
				}
				// if email is invalid
				else{
					// error handling
					failure();
				}

				document.body.removeChild(script);
			}
			catch(e){
				// if error, ignore verification and move on
				// console.log('jsonp callback error');

				success();
			}
			delete window.briteverify;
		}
	}
	else{
		// if no token, ignore verification and move on
		// console.log('no token');

		success();
	}
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
/*
 * Grabs a url param from the url.
 * returns null if not found
 */
function grabUrlParam( name ) {
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp( regexS );
	var results = regex.exec( window.location.href );
	if( results == null )
	return null;
	else
	return results[1];
}
/**************************************************
FORM SUBMIT
***************************************************/
(function($) {
	$.fn.submitEmail = function(options){
		$form = this;
		var $inputEmail = $form.find('input[type=text]')
			, $languageError = $form.find('.language-error')
			, error = false
			, email = $inputEmail.val()
			, $dropdown = $form.find('ul.dropdown')
			, $dropdownLi = $dropdown.find('li')
			, selectedLanguage = $dropdown.find('.selected').data('code') || 'en-es'
			;

		// This should never happen since to see email field, you have to select a language.
		// but add it for good measure anyway.
		if (!$dropdownLi.find('.selected')) {
			error = true
			$languageError.fadeIn()
		} else {
			$languageError.fadeOut()
		}

		// show or hide email error messages
		function show_email_error(spelling){
			error = true;
			$inputEmail.addClass('input-error');
			if(spelling){
				$('.spelling_error_message').show();
			}
			else{
				$('.email_error_message').show();
			}
		}
		function hide_email_error(){
			$inputEmail.removeClass('input-error');
			$('.email_error_message').hide();
			$('.spelling_error_message').hide();
		}


		// Only validate email if user doesn't have the demotaken cookie
		if (!(RSUI.util.getCookie('rsdemo') || RSUI.util.getCookie('demotaken'))) {

			// check if the email format is valid
			if (!validateEmail(email)) {
				show_email_error();
			} else {
				hide_email_error();

				// make sure there's no language dropdown error (should never happen, but just to be safe)
				if(!error){

					// briteverify validation to make sure email is real
					verify_real_email(email,
						function(){
							submitEmailToEC(email, selectedLanguage,$form);
						},
						function(){
							show_email_error('spelling');
						});
				}
			}


			// Event18 - when start demo button is clicked
			s.events = "event18";s.t();delete s.events;
		}

		// if no errors, and demo already taken, submit (the demo will open)
		else {
			!error && submitEmailToEC(email, selectedLanguage, $form);
		}

	};
	
}(jQuery));


function submitEmailToEC(email, lang, $form) {
	if(typeof $form !== 'undefined') /* handle to lightbox content */
		$c = $form.parents('.featherlight.demo-lightbox .featherlight-content');

	if (RSUI.util.getCookie('demotaken') || RSUI.util.getCookie('rsdemo')) {
		$c.addClass('demo-on').find('.demo-lightbox-inner').fadeOut();
		$c.append( bootDemo() ).find('iframe').fadeIn('slow');
		return;
	} else {
		var map = {
			'en-fr' : 'fra',
			'en-de' : 'deu',
			'en-it' : 'ita',
			'en-en' : 'eng',
			'en-es' : 'esp'
		}

		// Send to EC
		var data = {
			email : email,
			demo_lang : map[lang],
			cis_name : 'mobile demo',
			website: 'US_WEBSITE',
			form_type : 'demo',
			demo_type : isDesktop?'Demo_Desktop':'Demo_Mobile',
			form_url : window.location.pathname,
			newsletter_type : "Top_Landing_Embrace",
			cid : _satellite.getVar("mostrecentcampaign")
		};
		
		var url = (window.location.href.search(/(\.stg\.)|(\.local)/i)>-1 ?'http://www.stg' :'http://www')
                         + '.rosettastone.com/?p_p_id=rosettaajaxsubmit_WAR_rosettaajaxsubmitportlet&p_p_lifecycle=2&redirect2mobile=no';
		
		$.ajax({
			url: url,	
			type: "POST",
			data:data				
		}).done(function(msg) {
			if (JSON.parse(msg)[0]['cisFlag']  == 'true') {       
				$(document).trigger('demo_email', {
					email: email
					,is_offer: false
				});
						
				if (!$('#mboxinnner').length) {
					var tgtdiv = document.createElement("div");
					tgtdiv.setAttribute("id", "mboxinner");
					document.querySelector("body").appendChild(tgtdiv);
					mboxDefine('mboxinner','US_RS_DemoLeadConfirm', rs.mbox_env);
					mboxUpdate('US_RS_DemoLeadConfirm',  rs.mbox_env);
				}

				// Cookie that demo is taken so they don't input email again
				RSUI.util.setCookie('demotaken', "1", 1, 30 * 1000 * 60 * 60 * 24);

				// record the email in a cookie that expires in 30 days
				var days_till_expiration = 30;
				var expiration = new Date(new Date().getTime() + days_till_expiration*24*60*60*1000).toUTCString();
				document.cookie='rsDemoEmail='+email+'; expires='+expiration+'; path=/; domain=.rosettastone.com';

				//Load Demo
				$c.find('.demo-lightbox-inner').fadeOut();
				$c.addClass('demo-on').find('.demo-lightbox-inner').fadeOut();
				$c.append( bootDemo() ).find('iframe').fadeIn('slow');

				//$('input[type=text]').removeClass('input-error').attr('placeholder', 'E-mail')
			} else {
				// Very unlikely we'll reach this. Must've been an error in myaccount
				$form.find('input[type=text]').addClass('input-error').attr('placeholder', 'Sorry, try again!')
			}
		}).error(function(){
			$form.find('input[type=text]').addClass('input-error').attr('placeholder', 'Sorry, try again!')
		});

	} // else
}
function hideEmailInput($form) {
	$form.find('.input-text-wrapper').hide()
	$form.find('.input-submit-wrapper').css({'width':'100%', 'float':'none'})
}
function bootDemo(lang) {
	
	lang = typeof lang !== 'undefined' ? lang : 'en-es'

	// open new mobile demo if we see the query parameter new_mobile_demo
	var lang_name = {
		'en-fr' : 'french',
		'en-de' : 'german',
		'en-it' : 'italian',
		'en-en' : 'spanish', // because new mobile demo doesn't have english
		'en-es' : 'spanish'
	}
	
	// show new mobile demo
	if(new_mobile_demo_turned_on && (!isDesktop || window.location.search.match(/new_mobile_demo/i)) ){
		var stg_or_not = window.location.href.match(/\.stg\./i) ? 'stg.':'';
		setTimeout(function(){
			window.location.href = 'http://m.'+stg_or_not+'rosettastone.com/demo/?lang='+lang_name[lang];
		},1000);
	}


	// old mobile demo
	if (!new_mobile_demo_turned_on && !isDesktop) {
		setTimeout(function() {
			window.location.href = 'http://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=' + lang
		}, 2000);
	} else {

		return demoFrame = ""
			+ "<iframe "
			+ " style='display:none;'"
			+ " id='demo'"
			+ " src='http://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=" + lang + "'"
			+ " width='100%'"
			+ " height='100%'"
			+ " scrolling='no'"
			+ "></iframe>"
			;

	} // end if !isDesktop
}
function terminateDemo($c){
	$c.find('iframe').fadeOut('fast').remove();
	$c.css('overflow','hidden');
	// Shrink height of lightbox so postoffer looks neater
	var $offer = $('.post-demo-offer').clone();
	$c.removeClass('demo-on')
		.addClass('post-offer-on')
		.append( $offer );
	// Show postdemo offer
	$offer.fadeIn('slow');
}

/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // IE 12 => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}