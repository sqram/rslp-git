/*
 * RSUI UTIL STUFF
 *********************/
if(!this["RSUI"]||RSUI==null||RSUI==undefined){RSUI={}}RSUI.util=new Object();RSUI.util.getParamFromLandingPageUrl=function(b){b=b.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");var a="[\\?&]"+b+"=([^&#]*)";var d=new RegExp(a);var c=d.exec(window.location.href);if(c==null){return""}else{return c[1]}};RSUI.util.getLocalTimeIncrement=function(){var b;try{RSUI.util.dateFormat.masks.timeIncrement="yyyy-mm-dd HH:";var a=new Date();var e=a.format_rs("timeIncrement");var c=Math.floor(a.getMinutes()/10).toString();b=e+c+"0"}catch(d){b=d}return b};RSUI.util.setCookie=function(c,h,b,d,k,e,a){if(!b){d=30*1000*60*60*24;k="/";var j=RSUI.util.parseUri(document.location).host.split(".");j.reverse();e="."+j[1]+"."+j[0];if(j.length>=3&&j[2]!="www"&&j[2]!="stg"){e="."+j[2]+"."+j[1]+"."+j[0]}}var f=new Date();var g=new Date(f.getTime()+(d));document.cookie=c+"="+escape(h)+((d)?";expires="+g.toUTCString():"")+((k)?";path="+k:"")+((e)?";domain="+e:"")+((a)?";secure":"")};RSUI.util.getCookie=function(a){var f=document.cookie.split(";");var b="";var d="";var e="";var c=false;for(i=0;i<f.length;i++){b=f[i].split("=");d=b[0].replace(/^\s+|\s+$/g,"");if(d==a){c=true;if(b.length>1){e=unescape(b[1].replace(/^\s+|\s+$/g,""))}return e;break}b=null;d=""}if(!c){return null}};RSUI.util.parseUri=function(e){var d=RSUI.util.parseUri.options,a=d.parser[d.strictMode?"strict":"loose"].exec(e),c={},b=14;while(b--){c[d.key[b]]=a[b]||""}c[d.q.name]={};c[d.key[12]].replace(d.q.parser,function(g,f,h){if(f){c[d.q.name][f]=h}});return c};RSUI.util.parseUri.options={strictMode:false,key:["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],q:{name:"queryKey",parser:/(?:^|&)([^&=]*)=?([^&]*)/g},parser:{strict:/^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,loose:/^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/}};RSUI.util.dateFormat=function(){var a=/d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,b=/\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,d=/[^-+\dA-Z]/g,c=function(f,e){f=String(f);e=e||2;while(f.length<e){f="0"+f}return f};return function(j,w,r){var g=RSUI.util.dateFormat;if(arguments.length==1&&Object.prototype.toString.call(j)=="[object String]"&&!/\d/.test(j)){w=j;j=undefined}j=j?new Date(j):new Date;if(isNaN(j)){throw SyntaxError("invalid date")}w=String(g.masks[w]||w||g.masks["default"]);if(w.slice(0,4)=="UTC:"){w=w.slice(4);r=true}var u=r?"getUTC":"get",n=j[u+"Date"](),e=j[u+"Day"](),k=j[u+"Month"](),q=j[u+"FullYear"](),t=j[u+"Hours"](),l=j[u+"Minutes"](),v=j[u+"Seconds"](),p=j[u+"Milliseconds"](),f=r?0:j.getTimezoneOffset(),h={d:n,dd:c(n),ddd:g.i18n.dayNames[e],dddd:g.i18n.dayNames[e+7],m:k+1,mm:c(k+1),mmm:g.i18n.monthNames[k],mmmm:g.i18n.monthNames[k+12],yy:String(q).slice(2),yyyy:q,h:t%12||12,hh:c(t%12||12),H:t,HH:c(t),M:l,MM:c(l),s:v,ss:c(v),l:c(p,3),L:c(p>99?Math.round(p/10):p),t:t<12?"a":"p",tt:t<12?"am":"pm",T:t<12?"A":"P",TT:t<12?"AM":"PM",Z:r?"UTC":(String(j).match(b)||[""]).pop().replace(d,""),o:(f>0?"-":"+")+c(Math.floor(Math.abs(f)/60)*100+Math.abs(f)%60,4),S:["th","st","nd","rd"][n%10>3?0:(n%100-n%10!=10)*n%10]};return w.replace(a,function(m){return m in h?h[m]:m.slice(1,m.length-1)})}}();RSUI.util.dateFormat.masks={"default":"ddd mmm dd yyyy HH:MM:ss",shortDate:"m/d/yy",mediumDate:"mmm d, yyyy",longDate:"mmmm d, yyyy",fullDate:"dddd, mmmm d, yyyy",shortTime:"h:MM TT",mediumTime:"h:MM:ss TT",longTime:"h:MM:ss TT Z",isoDate:"yyyy-mm-dd",isoTime:"HH:MM:ss",isoDateTime:"yyyy-mm-dd'T'HH:MM:ss",isoUtcDateTime:"UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"};RSUI.util.dateFormat.i18n={dayNames:["Sun","Mon","Tue","Wed","Thu","Fri","Sat","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],monthNames:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","January","February","March","April","May","June","July","August","September","October","November","December"]};Date.prototype.format_rs=function(a,b){return RSUI.util.dateFormat(this,a,b)};

// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License
RSUI.util.parseUri = function (str) {

  var o   = RSUI.util.parseUri.options,
    m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
    uri = {},
    i   = 14;

  while (i--) uri[o.key[i]] = m[i] || "";

  uri[o.q.name] = {};
  uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
    if ($1) uri[o.q.name][$1] = $2;
  });

  return uri;
};

RSUI.util.parseUri.options = {
  strictMode: false,
  key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
  q:   {
    name:   "queryKey",
    parser: /(?:^|&)([^&=]*)=?([^&]*)/g
  },
  parser: {
    strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
    loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
  }
};

/***********************
 * END RSUI UTIL STUFF
 *********************/



//append facebook api script
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));


//Assign this to a URL on demo load, redirect after demo is closed.
var redirect;

/*========================== Open, close lightbox =====================*/
$(document).ready(function(){
  /*Open on click*/
  $(".open-demo-btn").click(function(){
    //preselct lang in button class name
    for (var c in this.classList) {
      if(typeof this.classList[c]==="string" && this.classList[c].match(/^\w\w\w$/)) {
        $("a[rel="+this.classList[c]+"]").click();
      }
    }
    // if no language found, go back to lang list
    if ($(".demo-drop li a.selected").length==0) {
      $("#demo-change-lang").click();
      $(".demo-lang-wrap").hide();
    }
    //create new yellow transition
    var tb = new TransitionBox(this,$(".demo-wrap")[0]);
    $(".demo-lightbox-mask").addClass("active");
    $(document).trigger('demo_events_version', {
      version: 1,
      description: '',
      standard: 'http://tabbit.org/%E2%98%8D%E2%98%B9W'
    });
  });
  /*Close on click outside */
  $(".demo-lightbox-mask").click(function(){closeLightbox()});

  /* Close on ESC */
  $(document).keyup(function(e){
    if (e.keyCode == 27) {
      closeLightbox();
    }
  });
  /*Close on 'close' click*/
  $("span#close-lb").click(function(){closeLightbox()})
  $("#demo-box .close-btn").click(function(){closeLightbox()});
  closeLightbox();
});
function closeLightbox() {
  $(".demo-lightbox-transition").css("background","#ecc200");
  $(".demo-wrap").removeClass("play");
  $(".demo-wrap").removeClass("active");
  $(".demo-lightbox-mask").removeClass("active");
  $(".arrow").removeClass("active");
  $("html, body").removeClass("stop-scrolling");
  $("#demo-box iframe").attr('src', ''); $("#demo-box iframe").remove();
  if (typeof redirect != "undefined") {
    //setTimeout(function() {window.location.href = redirect;},0);
  }
}


function TransitionBox (from, to) { // yellow transition effect
  var self = this;
  this.from = from;
  this.to = to;
  this.tb = document.createElement("div");
  this.tb.className = "demo-lightbox-transitions";
  this.tb.style.width = from.offsetWidth+"px";
  this.tb.style.height = from.offsetHeight+"px";
  this.tb.style.top = from.className.match("demo-wrap")?window.scrollY+from.offsetTop-from.offsetHeight/2:from.offsetTop+"px";
  this.tb.style.left = from.className.match("demo-wrap")?window.scrollX+from.offsetLeft-from.offsetWidth/2:from.offsetLeft+"px";
  var par = document.getElementsByClassName("demo-lightbox-transition")[0];
  par.parentNode.insertBefore(this.tb, par.nextSibling);
  $(this.tb).animate({
    "top": $(to).offset().top + "px",
    "left": $(to).offset().left + "px",
    "width": $(to).outerWidth() + "px",
    "height": $(to).outerHeight() + "px"
  }, 300, function() {
    if ($(".demo-lightbox-mask").hasClass("active")) {$(self.to).addClass("active");}
    $(this).fadeOut(300,function(){this.remove;});
  });
}

/*========================= Open, close dropdowns =====================*/
$(document).ready(function(){
  $(".demo-wrap").click(function (e) {
    var dd = $(e.target).find(".arrow").addBack('.arrow');
    if($(".dropdown-box").has($(e.target)).length>0) {
      e.stopPropagation(); return false; /* do nothing on click inside */
    }
    if($(e.target).hasClass("demo-drop") || $(e.target).hasClass("arrow")){
      if (dd.hasClass("active")) { /* toggle on dd click */
        dd.removeClass("active");
      } else {
        $(".arrow").removeClass("active");dd.addClass("active");
      }
      e.stopPropagation(); fade(); return false;
    }
      $(".arrow").removeClass("active"); /* and close on click outside */
      fade();
  });
  function fade() {
    var all = $(".arrow").parents(".lightbox-product-inner");
    var active = $(".arrow.active").parents(".lightbox-product-inner");
      if(active.length>0) {
        all.css("opacity","0.5");
      active.css("opacity","1");
    } else {
      all.css("opacity","1");
    }
  }
});

/*===================== Select Language from dropdown =================*/
$(document).ready(function(){
  $(".demo-drop ul li a").click(function (e) {
    $(document).trigger('demo_language', {language: $(this).text(), lang_code: $(this).attr("rel").toUpperCase()});
    $(".lightbox-product-inner").find(".email-input").attr("required",false);
    $(".demo-drop ul li a").removeClass("selected");
    $(this).addClass("selected");
    $(".selected-lang").html($(this).html());
    $(".arrow").removeClass("active");
    $(".demo-lang-wrap").show();
    $(".demo-lang-wrap").removeClass("lang-select");
    $(".signup-options").slideDown();
    $(".start-demo-btn").slideDown();
    $(".demo-drop").slideUp();
    if (RSUI.util.getCookie("curEmailIdsc")) {
      $(this).parents(".lightbox-product-inner").find(".email-input").val(RSUI.util.getCookie("curEmailIdsc"));
    }
    $(this).parents(".lightbox-product-inner").find(".email-input").attr("required",true).focus();
  });
});
/* ======================== Go back to lang list =========================*/
$(document).ready(function(){
  $("#demo-change-lang").click(function () {
    if ($(".demo-lang-wrap").hasClass("lang-select")) {
      $(".demo-drop li a.selected").click();
    } else {
      $(".demo-lang-wrap").addClass("lang-select");
      $(".signup-options").slideUp();
      $(".start-demo-btn").slideUp();
      $(".demo-drop").slideDown();
    }
  });
});
/* ========================= Facebook FB login ===========================*/
$(document).ready(function doFB(xn){
  if(typeof FB == 'undefined'){
     var xn = xn || 100;
     return xn > 10000000 ? false : setTimeout(doFB, xn, 10 * xn);
  }
  FB.init({
    appId: "1091422140883673",
    status: true,
    cookie: true,
    xfbml: true,
    oauth: true,
    channelUrl: window.location.protocol + '//' + window.location.hostname + '/facebook/channel'
  });
  $("#facebook-login").click(function(){
    FB.login(function(response) {
          if (response.authResponse) {
              FB.api('/me', function(me) {
                  //fb login success
                  var selectedLang = $(".demo-drop .selected");
                  var demoURL = selectedLang.attr("data-url");
                  var demoLangCode = selectedLang.attr("rel");
                  jQuery.when(submitEmailToEC(me.email, demoLangCode,"mobile demo")).then(openDemo(demoURL));
              });
          } else {
              //facebook failed
              console.log("fb fail");
          }
      }, {
          scope: 'email'
    });
  });
});
/* ============================ Validate Form ============================*/
$(document).ready(function(){
  $(".start-demo-btn").click(function (e) {
    function validateEmail(email) {
        var re = /\S+@\S+\.\S+/;
        re.test(email) ? verify_real_email(email,validateSuccess,validateFail) : validateFail();
    }
    function validateSuccess() {
        if (!RSUI.util.getCookie("curEmailIdsc")) {
            jQuery.when(submitEmailToEC(email.val(), lang,"mobile demo")).then(openDemo(demoURL));
        } else {
            openDemo(demoURL);
        }
        return false;
    }
    function validateFail() {
        email.css("border","2px solid red").val("").prop("placeholder","PLEASE ENTER A VALID EMAIL").focus();
    }
    var selLang = $(this).parents(".lightbox-product-inner").find("li a.selected")
    var lang = selLang.attr("rel");
    var demoURL = selLang.attr("data-url")
    if (typeof lang == "undefined") {return false;}
    var email = $(this).parent().find(".email-input");
    if (email.prop("required")) {
        validateEmail(email.val())
    }
  });
});

/* ======================== Brite Verify Validation ========================*/
function get_token(){
    window.briteverifyToken = false;

    var form_token = '4047df1c-5078-4e96-b335-f61aa2b92891'; // public key
    var url = 'https://forms-api-v1.briteverify.com/api/submissions/view.json?callback=getBriteverifyToken&form_token='+form_token+'&_='+Date.now();

    // append script to body to get cross-domain jsonp data
    var script = document.createElement('script');
    script.src = url;
    document.getElementsByTagName('head')[0].appendChild(script);

    window.getBriteverifyToken = function(data){
        // console.log(data);
        // console.log('token: '+data.token);

        window.briteverifyToken = data.token;

        delete window.getBriteverifyToken;
        document.getElementsByTagName('head')[0].removeChild(script);
    }
}
// if the demo hasn't already been taken, get an email verification token
if(!RSUI.util.getCookie('demotaken')) {
    get_token();
}

function verify_real_email(email,success,failure){
    if(!success){success = function(){};}
    if(!failure){failure = function(){};}
    if(window.briteverifyToken){

        var token = window.briteverifyToken;
        var url = 'https://forms-api-v1.briteverify.com/api/submissions/verify.json?callback=briteverify&form_token=4047df1c-5078-4e96-b335-f61aa2b92891&token='+token+'&email='+email+'&_='+Date.now();
        var script = document.createElement('script');
        script.src = url;
        document.getElementsByTagName('head')[0].appendChild(script);

        // if the script url doesn't work (maybe they changed the api)
        script.onerror = function(e){
            // ignore verification and move on
            success();
        };

        window.briteverify = function(data){
            try{
                // if email is valid
                if(data.status!=='invalid'){
                    success();
                }
                // if email is invalid
                else{
                    failure();
                }

                document.getElementsByTagName('head')[0].removeChild(script);
            }
            catch(e){
                // if error, ignore verification and move on
                success();
            }
            delete window.briteverify;
        }
    }
    else{
        // if no token, ignore verification and move on
        success();
    }
}

/* ============================ Open Demo ============================*/
function openDemo(demoURL) {
  $(document).trigger('demo_start', {
    version: 'd4',
    version_description: 'desktop_demo',
    start_time: new Date()
  });
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    setTimeout(function() {
      window.location.href = demoURL;
    }, 0);
  }
  var iframe = document.createElement("iframe");
  iframe.src = demoURL;
  iframe.width = "100%";
  iframe.height = "100%";
  iframe.scrolling = "no";
  iframe.frameborder = "0";
  $(".demo-wrap").addClass("play");
  $("#demo-box").append(iframe);

}

/* ========================== Send Email To EC ========================*/
function submitEmailToEC(email, langCode, cis_name) {
      var defer = jQuery.Deferred()

      $(document).trigger('demo_email', {
        email: email,
        is_offer: false
      });

      var baseURL = (window.location.href.search(/(\.stg\.)|(\.local)/i)>-1 ?'http://www.stg' :'http://www')
                  + '.rosettastone.com/?p_p_id=rosettaajaxsubmit_WAR_rosettaajaxsubmitportlet&p_p_lifecycle=2&redirect2mobile=no';

      var data = {
          email : email,
          demo_lang : langCode,
          cis_name : cis_name,
          website: 'US_WEBSITE',
          form_type : 'demo',
          demo_type : isDesktop ? 'Demo_Desktop' : 'Demo_Mobile',
          form_url : window.location.pathname,
          newsletter_type : "Bottom_Landing_STORYBOX",
          cid : _satellite.getVar("mostrecentcampaign")
      }

      var request = jQuery.ajax({
          //contentType: "application/json; charset=utf-8",
          url: baseURL, //+ encodeURIComponent(JSON.stringify(data)),
          type: "POST",
          data: data
      });

      request.done(function(msg) {
          if  (JSON.parse(msg)[0]['cisFlag']  == 'true') {
              // Doty's demo pixel
              $('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1041440066/?label=WzGCCNzOy1gQwrrM8AM&amp;guid=ON&amp;script=0"/>')
              // record the email in a cookie that expires in 30 days
              RSUI.util.setCookie("curEmailIdsc",email)
              //document.cookie = 'curEmailIdsc='+email+'; max-age='+60*60*24*30+'; path=/;';
              defer.resolve('success')
          }

      })
      return defer.promise()
  }

/* ===================== Remove v3 demo for tablet ===================*/
$(document).ready(function(){
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $(".drop-col:not(:first-child)").remove();
    $(".drop-col ul").css("width","16.5em");
  }
});
/* ===================== extend addBack for old jQuery ===================*/
$(document).ready(function(){
  jQuery.fn.addBack = function (selector) {
      return this.add(selector == null ? this.prevObject : this.prevObject.filter(selector));
  }
});





















  /*
   * Mobile Demo
   * The code above is for desktop demo and is
   * tightly coupled with some htm elements, so
   * here's a separate code for mobile, since the
   * html is different. We'll still use some
   * functions from above. Someone's going to look
   * at this in the future and say 'wtf?'. it is
   * what it is lol.
   **********************************************/

  $('.mobile-demo-start').click(function(e) {

    var $inputemail = $('.mobile-input-email')
    var email = $inputemail.val().trim()

    function validateEmail(email) {
      var re = /\S+@\S+\.\S+/;
      re.test(email)
        ? verify_real_email(email,validateSuccess,validateFail)
        : validateFail();
    }

    var validateFail = function() {
      // make text red
      $inputemail.addClass('invalid').after('<div><b>Invalid e-mail</b></div>')
    }

    var map = {
      esp: 'es',
      eng: 'en',
      fra: 'fr',
      deu: 'de',
      ita: 'it'
    }
    var validateSuccess = function() {
      // redirect
      var lang = $('.demo-language-select option:selected').data('lang') || 'esp'

      $.when(submitEmailToEC(email, lang, "Flash demo"))
      .then(function() {
        window.location.href = 'https://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=en-' + map[lang]
      });
    }

    validateEmail(email, validateSuccess, validateFail)
  })


