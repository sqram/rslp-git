var gulp = require('gulp')
var stylus = require('gulp-stylus');
var webserver = require('gulp-webserver');


gulp.task('stylus', function() {
    gulp.src(['../css/*.styl'], { base: '..' })
    .pipe(stylus({
      compress: false
    }))
    .pipe(gulp.dest('..'));
});



gulp.task('webserver', function() {
  gulp.src('..')
    .pipe(webserver({
      livereload: true,
      directoryListing: false,
      open: true
    }));
});


// Watch
gulp.task('watch', function() {
  gulp.watch('../css/*.styl', ['stylus'])
});


gulp.task('default', ['webserver', 'watch'])