/*
 * Detects if we are on a mobile device.
 * User-agent sniffing isn't reliable, but Yolo.
 */
window.isDesktop = !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);


/* Grabs a url param from the url.
 * returns null if not found
 */
function grabUrlParam( name ) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
  return null;
  else
  return results[1];
}


// will hold lang in format like 'eng', 'ita'
var selectedLang = null


var $loading = $('.sk-fading-circle')
var $inputEmail = $('input[type=email]')
var $submit = $('input[type=submit]')
var $demowrapper = $('.demo-wrapper')
var $buttonSelectLanguage = $('a.regular-button').eq(0)
var $video = $('video')

$('form a.regular-button').click(function(e) {
  e.preventDefault()
  $(this).siblings('div.languages').slideToggle()
})


// A language is selected
$('div.languages a').click(function(e) {
  e.preventDefault()
  $(this).parent().find('.selected').removeClass('selected')
  $(this).parent().children('a').slideUp()
  $buttonSelectLanguage.html( $(this).html() )
  selectedLang = $(this).data('lang')
})





// If we have demotaken cookie, hide email form
if (RSUI.util.getCookie('demotaken'))
{
  $('input[type=email], input[type=email] ~ small').hide()
}

$('form').submit(function(e) {
  e.preventDefault()

  // We already collected this user's email within the past 30 days.
  // No need to show email form, just open demo.
  if (RSUI.util.getCookie('demotaken'))
  {
    toggleDemo('on')
    return
  }

  var email = $inputEmail.val() || ''

  $submit.hide()
  $loading.show()

  if (!selectedLang)
  {
    selectedLang = 'esp'
  }

  // Don't waste briteverify quota if these basics arent matched
  if (!email.match(/@{1}.+\.+/))
  {
    $submit.show()
    $loading.hide()
    toggleErrorMessage('on')
    return
  }

  if (email)
  {
   verify_real_email(email, emailAccepted, emailRejected)
  }

})


/*
 * Call back function from BriteVeryfiy.
 * It's invoked by the success() call inside
 * verify_real_email
 */
function emailAccepted(email)
{
  var email = email || $inputEmail.val()

  $.when( submitEmailToEC(email, selectedLang) ).then(
    // Success
    function() {
      toggleErrorMessage('off')
      $submit.show()
      $loading.hide()
      toggleDemo('on')
    },
    // Rejected
    function() {
      toggleErrorMessage('on')
      $submit.show()
      $loading.hide()
    }
  )

}

/*
 * Briteverify callback for rejected emails
 */

function emailRejected()
{
  toggleErrorMessage('on')
  $submit.show()
  $loading.hide()
}


/*
 * Shows/Hides bad email error message
 *
 * @param {str} flag - on/off to show/hide
 */

function toggleErrorMessage(flag)
{
  if (flag == 'on')
  {
    $inputEmail.addClass('invalid')
    $('.notify-error').fadeIn(150)
  }
  if (flag == 'off')
  {
    $inputEmail.removeClass('invalid')
    $('.notify-error').fadeOut(150)
  }
}



/*
 * Visually opens the demo.
 * ie, creates/destroy iframe
 *
 * @param {string} flag - on/off
 */

function toggleDemo(flag)
{

  if (flag == 'on')
  {
    var democode = $('a[data-lang='+ selectedLang + ']').data('democode')
    var fullname = $('a[data-lang='+ selectedLang + ']').html().toLowerCase()

    // Analytics - Event23
    $(document).trigger('demo_start', {
      version: !isDesktop ? 'm1' : 'd4',
      version_description: !isDesktop ? 'mobile_demo' : 'desktop_demo',
      start_time: new Date()
    });

    // Analytics - Evar7
    $(document).trigger('demo_language', {
      language: fullname,
      lang_code: selectedLang
    });


    if (!isDesktop && democode != 'en')
    {
      setTimeout(function(){
        window.location.href = 'http://m.rosettastone.com/demo/?lang=' + fullname
      },1000);
    }
    else
    {
      var demourl = 'http://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=en-' + democode
      var demoFrame = ""
        + "<iframe "
        + " id='demo'"
        + " src='"+ demourl + "'"
        + " width='100%'"
        + " height='500px'"
        + " scrolling='no'"
        + "></iframe>"
        ;

        $demowrapper.slideDown(400, function()
        {
          $('body').animate({
            scrollTop: $("a.close").offset().top
          }, 1000);
          $demowrapper.append(demoFrame)
          $('#demo').fadeIn()
        })
      }
    }

  else
  {
    // Closing/hiding the demo (flag is 'off')
    $('iframe#demo').fadeOut().remove()
    $demowrapper.slideUp()
  }

}


// X close button for demo
$('.demo-wrapper a.close').click(function(e) {
  e.preventDefault()
  toggleDemo('off')
})



/*
 * Bright Verify.
 */

function get_token()
{
  window.briteverifyToken = false;

  var form_token = '4047df1c-5078-4e96-b335-f61aa2b92891'; // public key
  var url = 'https://forms-api-v1.briteverify.com/api/submissions/view.json?callback=getBriteverifyToken&form_token='+form_token+'&_='+Date.now();

  // append script to body to get cross-domain jsonp data
  var script = document.createElement('script');
  script.src = url;
  document.body.appendChild(script);

  window.getBriteverifyToken = function(data)
  {

    window.briteverifyToken = data.token;
    delete window.getBriteverifyToken;
    document.body.removeChild(script);
  }
}


// if the demo hasn't already been taken, get an email verification token
if (!RSUI.util.getCookie('demotaken'))
{
  get_token();
}

function verify_real_email(email,success,failure)
{

  if (!success)
  {
    success = function(){};
  }

  if (!failure)
  {
    failure = function(){};
  }

  if(window.briteverifyToken)
  {

    var token = window.briteverifyToken;
    var url = 'https://forms-api-v1.briteverify.com/api/submissions/verify.json?callback=briteverify&form_token=4047df1c-5078-4e96-b335-f61aa2b92891&token='+token+'&email='+email+'&_='+Date.now();

    var script = document.createElement('script');
    script.src = url;
    document.body.appendChild(script);


    // if the script url doesn't work (maybe they changed the api)
    script.onerror = function(e)
    {
      // ignore verification and move on
      success(email);
    };


    window.briteverify = function(data)
    {
      try
      {

        if(data.status!=='invalid')
        {
          // email is valid
          success(email);
        }
        else
        {
          // email is invalid
          failure();
        }

        document.body.removeChild(script);
      }
      catch(e)
      {
        // if error, ignore verification and move on
        success(email);
      }
      delete window.briteverify;
    }
  }
  else
  {
    // if no token, ignore verification and move on
    success(email);
  }
}


/*
 * Send email to EC
 *
 * @param {str} email - email to send
 * @param {str} lang - 3-letter lang code, ie, 'ita'
 */

function submitEmailToEC(email, lang)
{

 var defer = jQuery.Deferred()

  var baseURL = (window.location.href.search(/(\.stg\.)|(local)/i)>-1 ?
  'http://www.stg' :
  'http://www')
  + '.rosettastone.com/?redirect2mobile=no&p_p_id=rosettaajaxsubmit_WAR_rosettaajaxsubmitportlet&p_p_lifecycle=2&data=';

  var data = {
    email : email,
    demo_lang : lang || 'esp',
    cis_name : 'mobile demo',
    website: 'US_WEBSITE',
    form_type : 'demo',
    demo_type : 'PE',
    form_url : '/lp/opdemo',
    cid: grabUrlParam('cid') || '',
    newsletter_type : "DTC_mobile-demo"
  }

  var request = jQuery.ajax({
    contentType: "application/json; charset=utf-8",
    url: baseURL + encodeURIComponent(JSON.stringify(data)),
    type: "GET",
    data:data
  });

  request.done(function(msg)
  {
    if (/"cisFlag":"true"/.test(msg))
    {

      // Event 19
      $(document).trigger('demo_email', { email: email, is_offer: false });

      // Doty's demo pixel
      $('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1041440066/?label=WzGCCNzOy1gQwrrM8AM&amp;guid=ON&amp;script=0"/>')
      // record the email in a cookie that expires in 30 days
      RSUI.util.setCookie("curEmailIdsc", email)
      RSUI.util.setCookie('demotaken', "1", 1, 30 * 1000 * 60 * 60 * 24);
      defer.resolve('success')
    }
    else
    {
      defer.reject('Unknown error. Please try again.')
    }
  })

  request.fail(function() {
    defer.reject('Netowrk error. Please try again.')
  })

  return defer.promise()
}




/*
 * If we have a ?lang= param, preselect lang
 */

var urllang = grabUrlParam('lang')
if (urllang)
{
  $buttonSelectLanguage.trigger('click')
  //$buttonSelectLanguage.hide()
  $('body').animate({
    scrollTop: $(".select-language-wrapper h1").offset().top
  }, 2000, function() {
    $('a[data-lang=' + urllang + ']').click()
  });
}





$('.card-container').flip({
  trigger: isDesktop ? 'hover' : 'click'
}).on('flip:done', function() {
  // This won't work because when flpping back to the
  // front of the card, sound will play again.
  //$(this).find('audio')[0].play()
})



// Story videos - Delaine and Peter
$('.video-thumb').click(function() {
  var who = $(this).data('video')
  $video = $('video[data-who='+ who +']')
  $videowrapper = $(this).find('.video-wrapper')
  $videowrapper.fadeIn()
  $video.slideDown()
  $video[0].play()

  // We hide reviews behind the video since video doesn't
  // take up enough height
  if (isDesktop)
  {
    $('.reviews > div').css({visibility: 'hidden'})
  }
  //$video.css({visibility: 'visible'})

})


// Closing a story video
$('.video-thumb  a.close').click(function(e) {
  e.preventDefault()
  e.stopPropagation()
  var $video = $(this).parent().find('video')
  $video[0].pause()
  $video.slideUp()
  $('.video-wrapper').fadeOut()

  // Make reviews visible again
  $('.reviews > div').css({visibility: 'visible'})
  //$video.css({visibility: 'visible'})
})



/*
 * Play sounds when cards are hovered
 * our tapped. Since the animation is css
 * based, for mobile, we will remove the
 * .hover class when done so card flips
 * back to front side
 */
if (isDesktop)
{
  $('div.card-container').hover(function() {
    $(this).find('audio')[0].play()
  }, function() {
    // Audio done. flip card to back
    $('.card-container').flip(false)
  })
}
else
{
  $('div.card-container').click(function() {
    var $self = $(this)
    var $audio = $(this).find('audio')
    $audio.get(0).play()
    $audio.get(0).addEventListener('ended', function() {
      // Audio done. flip card to back
      $('.card-container').flip(false)
    }, false)
  })
}





// Change html text based on device
if (!isDesktop)
{
  $('.card-action').html('Tap on')
}







// Engage Analytics Listeners for Demo:
jQuery(document).ready(function engageanalytics() {
  jQuery(document).trigger('demo_events_version', {
    version: 1
    ,description: ''
    ,standard: 'http://tabbit.org/Z+onh'
  });
});