import 'babel-polyfill'

import Koa from 'koa'
import Router from 'koa-router'
import session from "koa-session2"
import MemStore from './memstore'

const app = new Koa();
const router = new Router()



app.use(session({store:  new MemStore()}))
app.use(router.routes())


router.get('/', async (ctx, next) => {
  ctx.session.username = 'jdoe'
  ctx.body = ctx.session
})


router.get('/user', async (ctx, next) => {
  console.log('username: ', ctx.session.username)
  ctx.body = ctx.session

})










app.listen(3000);

