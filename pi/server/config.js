import path from 'path'

export default  {

  // SSH connection settings
  connection: {
    dev: {
      host: 'ashcclp00.lan.flt',
      user: '',
      path: ':./pi/',
      passwd: '+'
    },
    stg: {
      host: '10.130.250.237',
      user: 'lpstagingadmin',
      path: '/mnt/landingpagestaging/autolandingpages/us/',
      passwd: 'n0y0uc@nt'
    },
    prod: {
      host: '',
      user: '',
      path: '',
      passwd: ''
    }
  },

  stg_base: 'lpstagingadmin:n0y0uc@nt@10.130.250.237:/mnt/landingpagestg/autolandingpages/us',


  // Constant live assets. (usually anything inside sitewide-promo)
  // They typically change with each flip. We list them here
  // so we can allow for a user to replace them in prod
/*  liveAssets: [
    'mdot-rto.jpg',
    'us-buynow.jpg',
    'us-freedemo.jpg',
    'us-freedemo-mobile.jpg',
    'us-hslpbanner.jpg',
    'es-homepage-bg.png',
    'es-catalog-bg',
    'es-buynow.jpg'
  ],*/



  ldapServer: 'ldap://ashdc2.rosettastone.local',

  //gitRepo: __dirname +
  files: {

    // File that stores our landing pages
    pages: 'pages.json',

    // File that stores our landing pages
    promosets: 'promosets.json',

    // File that holds our flips schedule
    // Will be read by cron script
    schedule: '../data/schedule.json',

    // Our landing pages redirects
    redirects: 'redirects.js',
  },

  dirs: {

    // Directory where we'll store flip files
    flipFiles: '../data/files/',

    // Public dir
    public:  './public/',

    // Mastheads folder
    mastheads: '../../deploy/globals/img/mastheads/sbsrc/',

    // Mastheads symlink
    mastheadsSymlink: './public/mastheads',

    // Masthead offers folder
    mastheadTypes: '../../offertemplates/',

    // Crescendos
    crescendos: '../../deploy/globals/crescendos/',

  },

  remotefiles: {

  },

  remotedirs: {
    sitewidepromo: 'autolandingpages/us/sitewide-promo'
  }

}

