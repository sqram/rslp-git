/*
 * Handles transfer of files through FTP.
 * I didn't want to name this file ftp.js out
 * of embarassment and in hopes we move out of it.
 * We should have a proper build & deploy process.
 * Maybe we should be using
 *
   ▄▄▄█████▓  ██░ ██  ▓█████ ███▄ ▄███▓
  ▓  ██▒ ▓▒▓ ██░ ██▒▓ █     ▀▓██▒▀█▀ ██▒
  ▒ ▓██░ ▒░▒ ██▀▀██░▒ ███    ▓██    ▓██░
  ░ ▓██▓ ░ ░ ▓█ ░██ ▒ ▓█  ▄▒ ██    ▒██
    ▒██▒ ░ ░ ▓█▒░██▓░ ▒████▒ ██▒   ░██▒
    ▒ ░░    ▒ ░░▒░▒░░  ▒░ ░   ▒░   ░  ░
      ░     ▒ ░▒░ ░ ░  ░  ░  ░      ░
    ░       ░  ░░ ░    ░  ░      ░
            ░  ░  ░    ░  ░      ░
   ▄████▄  ██▓     ▒█████   █    ██▓█████▄   ██████
  ▒██▀ ▀█ ▓██▒    ▒██▒  ██▒ ██  ▓██▒██▀ ██▌▒██    ▒
  ▒▓█    ▄▒██░    ▒██░  ██▒▓██  ▒██░██   █▌░ ▓██▄
  ▒▓▓▄ ▄██▒██░    ▒██   ██░▓▓█  ░██░▓█▄   ▌  ▒   ██▒
  ▒ ▓███▀ ░██████▒░ ████▓▒░▒▒█████▓░▒████▓ ▒██████▒▒
  ░ ░▒ ▒  ░ ▒░▓  ░░ ▒░▒░▒░ ░▒▓▒ ▒ ▒ ▒▒▓  ▒ ▒ ▒▓▒ ▒ ░
    ░  ▒  ░ ░ ▒  ░  ░ ▒ ▒░ ░░▒░ ░ ░ ░ ▒  ▒ ░ ░▒  ░ ░
  ░         ░ ░   ░ ░ ░ ▒   ░░░ ░ ░ ░ ░  ░ ░  ░  ░
  ░ ░         ░  ░    ░ ░     ░       ░          ░
  ░                                 ░

 The only things (categories) we push to the server are:
 1. landing pages (landingpages).
 2. sitewide-assets images (sitewide-assets)
 3. criteo (criteo)
 4. catalog pages (catalog)
 5. crescendos (crescendos)
 6. masthead images (masthead-images)

 Because each category above requires special treatment, forms must tell
 this script what we are uploading.

 How to handle each category above:

 1. Landing Pages
    * Accept an array of page names as argument (sitewide, sale, etc).
    * Get all templates by scanning templates folder.
    * Combine all templates with page names. Should end up with something like
      sbsr/sitewide
      sbsr/sale
      solo/sitewide
      solo/sale
    * Open connection
    * Check to see if template dir exist - create if not
    * Check to see if page dir exist - create if not


 */

/*** are we uploading a sitewide-promo assets?
if so, we know the destination. just accept img and drop in dst server

** are we uploading a landing page?
if so, accept an array of names [sitewide, sale, etc]
get all the templates [sbsr, solo]
combine into
sbsr/sitewide
sbsr/sale
solo/sitewide
solo/sale
open connection. check that the path exists
 1. check that sbbsr exists
 2. check that sitewide exists
 create them if not.
upload
*/

export default class upload {

  constructor(category, args) {
    this[category](args)
  }

  assets(filesArray) {

  }


  landingPage(pageNames) {

  }

}

// landing pages
// 1. get landing page in question by name (sitewide, sale, etc)
// 2. get all templates (rslp/templates)
// 3.put it all in an arrya like
// ['sbsr', 'sitewide', 'index.html'], ['solo', 'sitewide', 'index.html']
// 4. check if each template exists in prod/stg. if not, create folder
// 5. check if each page (sitewide, etc) exist. if not create folder.
// 6. push the index
//

// sitewide-assets
// put the images in sitewide-assets