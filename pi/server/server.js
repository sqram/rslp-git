import 'babel-polyfill'
import os from 'os'
import path from 'path'
import fs from 'fs-extra'
import exec from 'child_process'
import Koa from 'koa'
import cors from 'kcors'
import Router from 'koa-router'
import koaBody from 'koa-body'
import session from "koa-session2"
import WebSocket from 'ws'
import * as route from './routes'
import cfg from './config'
import Store from './memstore'
import SocketStore from './stores/socket-store'

const serve = require('koa-static')
const app = new Koa()
const router = new Router()


// Start websockets server
const wss = new WebSocket.Server({
  perMessageDeflate: false,
  port: 3003
})


// Koabody handles form submission data, like formdata
const koabody = new koaBody({
  multipart: true,
  formidable: {uploadDir: os.tmpdir()}
})


// So bad. TODO find clean way to pass
// sockets down to middleware
global.sockets = new SocketStore(wss)





app
  .use(session({key:'RSSESSION', store: new Store()}))

  .use(cors())
  .use(serve(path.join(__dirname, '/public/'), {}))
  .use(router.allowedMethods())
  .use(router.routes())
  ;

router
  .get('/pages'                 , route.pages.getPages)
  .post('/page'                 , koabody, route.pages.upsertPage)
  .put('/page/:id'              , koabody, route.pages.upsertPage)
  .delete('/page/:id'           , koabody, route.pages.deletePage)
  .get('/promosets'             , route.promosets.getPromosets)
  .post('/promoset'             , koabody, route.promosets.upsertPromoset)
  .put('/promoset/:id'          , koabody, route.promosets.upsertPromoset)
  .get('/crescendos'            , route.crescendos.getCrescendos)
  .get('/mastheads'             , route.mastheads.getMastheads)
  .post('/upload-masthteads'    , koabody, route.mastheads.uploadMastheads)
  .get('/masthead-types'        , route.mastheads.getMastheadTypes)
  .get('/flips'                 , route.flips.getFlips)
  .post('/flip'                 , koabody, route.flips.upsertFlip)
  .put('/flip/:id'              , koabody, route.flips.upsertFlip)
  .post('/login'                  , koabody, async (ctx, next) => {

    await route.auth.login(ctx, next)
    next()
    ctx.body = {result: 1, payload: 'success'}

  })
  .post('/upload/:cat'            ,koabody, async (ctx, next) => {
    await route.upload[ctx.params.cat](ctx, next)
  })






/****************************************************
 * Initial setup "script". Just create needed files
 * if they don't yet exist
 ****************************************************/

// Create pages.json (it should certainly already exist though)
fs.readJson(cfg.files.pages, (e, data) => {
  if (!data) {
    fs.outputFileSync(cfg.files.pages, '[]')
  }
})

// Create promosets.json (it should certainly already exist though)
fs.readJson(cfg.files.promosets, (e, data) => {
  if (!data) {
    fs.outputFileSync(cfg.files.promosets, '{}')
  }
})

// Create our schedule.json file with [] in it
fs.readJson(cfg.files.schedule, (e, data) => {
  if (!data) {
    fs.outputFileSync(cfg.files.schedule, '[]')
  }
})

// Create our files dir
fs.ensureDirSync(cfg.dirs.flipFiles)

// Create our public dir so we can serve masthead imgs, etc
fs.ensureDirSync(cfg.dirs.public)

// Create symbolic mastheads folder
fs.ensureSymlinkSync(`${cfg['dirs']['mastheads']}`, `${cfg['dirs']['mastheadsSymlink']}`)



app.listen(3000);




/*
Because it's promise-based
It's not like Express which is callback-based
next() simply returns a promise that is resolved when the next chain of middleware is done,
which is what allows a single Koa middleware to act as a before and after
If you are using await next() then you dont have to return is,
because the middleware calling await isn't done until it reaches it's end

I should probably do a small tutorial on how Koa middleware works some day


https://eev.ee/blog/2013/03/03/the-controller-pattern-is-awful-and-other-oo-heresy/


is it simply doing app.clients = [] ? not sure if antipattern, polluting the app namespace like that


catch(NetworkFailure failure) {
    // Retry the request now or later
    ...
}

855-894-1076

xoom
squarecash
 */

