/*
 * Handle authentication things like logging in,
 * out, etc. Our login system is silly simple -
 * when user logs in, send key to to client to
 * store in localstorage. Fingers crossed no
 * one internally will session hijack.
 */

import fs from 'fs-extra'
import ldap from 'ldapjs'
import cfg from '../config'


export default class Auth
{

  static async login(ctx, next)
  {
   /* const data = JSON.parse(ctx.request.body)

    if (!data)
    {
      return
    }

    var promise = new Promise( (resolve, reject) => {
      var client = ldap.createClient({ url: cfg.ldapServer })

      client.on('error', err => reject('LDAP server not found'))

      client.on('connect', () => {
        client.bind( `ROSETTASTONE\\${data.username}`, `${data.passwd}`, err => {
          if (err)
          {
            reject('Invalid Credentials')
          }
          else
          {
            // Set time out just for some loading icon swag
            //setTimeout(() => {
              resolve()
            //}, 2000)
          }
        })
      })
    })

    var res

    await promise.then(() => {
      ctx.session.username = data.username
      res = {result: 1, payload: 'success'}
    })
    .catch(e => {
      res =  {result: 0, payload: e}
    })

    ctx.body = res*/
    ctx.session.username = 'fooo'
    ctx.body = {result: 1, payload: 'success'}
    await next()

  }

}








/*
 * In the future, if you need more ldap info from a certain
 * user, use the code below. It should go after the first
 * bind (where user auths with user/pw)
  opts = {
    filter: '(samAccountName=bfranchi)',
    scope: 'sub'
  }


  client.search('dc=rosettastone,dc=local', opts, (err, res) =>
  {
    res.on('searchEntry', entry =>
    {
      console.log(entry.Object)
    })

    res.on('error', err =>
    {
      // TODO. handle client/tcp error here
    })
  })


  command line:
  ldapsearch -h 10.130.250.253 -D bfranchi@rosettastone.local -bdc=rosettastone,dc=local  -W "(samAccountName=bfranchi)
  */