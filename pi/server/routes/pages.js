/*
 * Handles all routes regarding pages.
 * I changed up my brackets/braces style here
 * to be very Java-like, for shits and giggles.
 * Let us know what you think by emailing  Vinoth
 * at vjohn@rosettastone.com
 * ( ͡° ͜ʖ ͡°)
 */


import fs from 'fs-extra'
import prettyjson from 'json-format'
import logger from '../logger'
import cfg from '../config'
import Validate from '../validate'

export default class Pages
{



  static getPages(ctx, next)
  {
    console.log('...getPages')
    ctx.body = fs.readFileSync(cfg.files.pages, 'utf8')
    return next()
  }


  /*
   * Creates or update a page in pages.json.
   * We know which one to do because when updating,
   * ctx.params.id will have a value (page name)
   */

  static upsertPage(ctx, next)
  {

    var pages = fs.readJsonSync(cfg.files.pages, 'utf8')

    const
      data = ctx.request.body.fields
      , id = ctx.params.id || null
      , env = data.inprod == 'true' ? '' : 'www.stg.'
      , link = `http://${env}rosettastone.com/lp/sbsr/${data.name}`
      , validate = new Validate()
      , action = id ? 'update' : 'create'
      , msg = {}
      ;

    msg.create = {
      success: `Page <a href='${link}'>created</a>.`,
      badData: `user tried to create page ${data.name} but passed bad params.`
    }
    msg.update = {
      success: `Page <a href='${link}'>updated</a>.`,
      badData: `user tried to update page ${data.name} but passed bad params.`
    }


    if (!id && pages.some(p => p.name == data.name))
    {
      // We are creating. Make sure it doesn't exist
      ctx.body = { result: 0, payload: "Page exists. Try a different name." }
    }
    else
    {
      // We are updating. Delete the existing page
      pages = pages.filter(p => p.name != data.name)
    }


    if (validate.page(data))
    {
      if (Pages._writePages(pages, data)) {
        logger.info(`--user-- ${action}d page ${data.name}.`, { user: 'username' })
        ctx.body = { result: 1, payload: msg[action].success }
      }
      else
      {
        ctx.body = { result: 0, payload: 'Could not write to pages.json.' }
      }
    }
    else
    {
      logger.warn(msg[action].badData)
      ctx.body = {result: 0, payload: 'Some invalid data was passed'}
    }

    return next()

  }



  /*
   * Deletes a page.
   * It does so by removing the obejct from
   * pages.json. It then removes from prod.
   * There really is no need to remove from stg.
   *
   * @param {str} name
   *  The name of the page to remove
   */

  static deletePage(name) {

  }



  /*
   * Will insert a pages object into pages.json,
   * Doesn't check if the same object exists or not,
   * that is up to the caller to decide
   *
   * @param {array} pages
   *   An array containing all the existing pages obj.
   *   It will be written to pages.json with the new
   *   page (`data` param) appended to it.
   *
   * @param {obj} data
   *   An object containing page data (name, promo, etc)
   *   These are the information visible on a web page.
   */

  static _writePages(pages, data)
  {
    pages.push({
      "name": data.name,
      "promoset": data.promoset,
      "mastheadtype": data.mastheadtype,
      "mastheadimage": data.mastheadimage,
      "strikethrough": [],
      "tophtml": data.tophtml,
      "bottomhtml": data.bottomhtml,
      "phone": data.phone,
      "expiration": data.expiration,
      "crescendo": data.crescendo,
      "js": data.js,
      "css": data.css,
      "metadata": {
        "constant": data.constant === 'true',
        "inprod": data.inprod === 'true',
        "market": data.market
      }
    })

    try
    {
      fs.writeFileSync(cfg.files.pages, prettyjson(pages))
      return true
    }
    catch(e)
    {
      return false
    }
  }


}



