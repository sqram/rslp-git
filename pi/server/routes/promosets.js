/*
 * Handles all routes regarding promosets.
 * I changed up my brackets/braces style here
 * to be very Java-like, for shits and giggles.
 * Let us know what you think by emailing  Vinoth
 * at vjohn@rosettastone.com
 * :D
 */

import fs from 'fs-extra'
import prettyjson from 'json-format'
import cfg from '../config'
import logger from '../logger'

export default class Promosets
{

  static async getPromosets(ctx, next)
  {
    ctx.body = fs.readFileSync(cfg.files.promosets, 'utf8')
    await next()
  }



  /*
   * Creates or update a promoset in promosets.json.
   * We know which one to do because when updating,
   * ctx.params.id will have a value (promoset name)
   */

  static async upsertPromoset(ctx, next)
  {
    const
      data = JSON.parse(ctx.request.body)
      , name = Object.keys(data)[0]
      , file = fs.readJsonSync(cfg.files.promosets)
      , id = ctx.params.id || null
      , action = id ? 'update' : 'create'
      ;

    // We are creating. Make sure it doesn't exist
    if (!id && Object.keys(file).includes(name))
    {
      return ctx.body = { result: 0, payload: "Promoset exists. Try a different name." }
    }

    // Add object (overwrites existing one, if any)
    file[name] = Object.values(data)[0]

    try
    {
      fs.writeFileSync(cfg.files.promosets, prettyjson(file))
      logger.info(`--user-- ${action}d promoset ${name}.`, { user: 'username' })
      ctx.body = { result: 1, payload: `Promoset ${action}d.` }
    }
    catch(e)
    {
      logger.error(`Could not write to promosets.json. ${e}`, { user: 'username' })
      ctx.body = { result: 0, payload: "Could not write to promosets.json" }
    }

    await next()
  }



}


