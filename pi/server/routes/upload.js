/*
 * Handles transfer of files through SFTP.
 * I didn't want to name this file ftp.js out
 * of embarassment and in hopes we stop  using it
 * and host our files on
 *
   ▄▄▄█████▓  ██░ ██  ▓█████ ███▄ ▄███▓
  ▓  ██▒ ▓▒▓ ██░ ██▒▓ █     ▀▓██▒▀█▀ ██▒
  ▒ ▓██░ ▒░▒ ██▀▀██░▒ ███    ▓██    ▓██░
  ░ ▓██▓ ░ ░ ▓█ ░██ ▒ ▓█  ▄▒ ██    ▒██
    ▒██▒ ░ ░ ▓█▒░██▓░ ▒████▒ ██▒   ░██▒
    ▒ ░░    ▒ ░░▒░▒░░  ▒░ ░   ▒░   ░  ░
      ░     ▒ ░▒░ ░ ░  ░  ░  ░      ░
    ░       ░  ░░ ░    ░  ░      ░
            ░  ░  ░    ░  ░      ░
   ▄████▄  ██▓     ▒█████   █    ██▓█████▄   ██████
  ▒██▀ ▀█ ▓██▒    ▒██▒  ██▒ ██  ▓██▒██▀ ██▌▒██    ▒
  ▒▓█    ▄▒██░    ▒██░  ██▒▓██  ▒██░██   █▌░ ▓██▄
  ▒▓▓▄ ▄██▒██░    ▒██   ██░▓▓█  ░██░▓█▄   ▌  ▒   ██▒
  ▒ ▓███▀ ░██████▒░ ████▓▒░▒▒█████▓░▒████▓ ▒██████▒▒
  ░ ░▒ ▒  ░ ▒░▓  ░░ ▒░▒░▒░ ░▒▓▒ ▒ ▒ ▒▒▓  ▒ ▒ ▒▓▒ ▒ ░
    ░  ▒  ░ ░ ▒  ░  ░ ▒ ▒░ ░░▒░ ░ ░ ░ ▒  ▒ ░ ░▒  ░ ░
  ░         ░ ░   ░ ░ ░ ▒   ░░░ ░ ░ ░ ░  ░ ░  ░  ░
  ░ ░         ░  ░    ░ ░     ░       ░          ░
  ░                                 ░

 The only things (categories) we push to the server are:
 1. landing pages (landingpages).
 2. sitewide-assets images (sitewide-assets)
 3. criteo (criteo)
 4. catalog pages (catalog)
 5. crescendos (crescendos)
 6. masthead images (masthead-images)

 Because each category above requires special treatment, forms must tell
 this script what we are uploading.

 How to handle each category above:

 1. Landing Pages
    * Accept an array of page names as argument (sitewide, sale, etc).
    * Get all templates by scanning templates folder.
    * Combine all templates with page names. Should end up with something like
      sbsr/sitewide
      sbsr/sale
      solo/sitewide
      solo/sale
    * Open connection
    * Check to see if template dir exist - create if not
    * Check to see if page dir exist - create if not


 */

/*** are we uploading a sitewide-promo assets?
if so, we know the destination. just accept img and drop in dst server

** are we uploading a landing page?
if so, accept an array of names [sitewide, sale, etc]
get all the templates [sbsr, solo]
combine into
sbsr/sitewide
sbsr/sale
solo/sitewide
solo/sale
open connection. check that the path exists
 1. check that sbbsr exists
 2. check that sitewide exists
 create them if not.
upload
*/


import os from 'os'
import fs from 'fs'
import path from 'path'
import client from 'scp2'
import cfg from '../config'
import logger from '../logger'

export default class Upload {


  /*
   * Push assets (sitewide-promo) files to the server
   * Very basic. Just take from tmp dir and transfer
   */

  static async asset(ctx, next)
  {
    console.log('sess: ', ctx.session.username)
    const file = ctx.request.body.files['file']
    if (!file)
    {
      return ctx.body = {result: 0, payload: 'no file'}
    }

    var res
    const localUri = file.path
    const remotelUri = `${cfg.stg_base}/sitewide-promo/${file.name}`
    const result = Upload._transfer(localUri, remotelUri)

    await result.then(() => {
      global.sockets.broadcast(`${ctx.session.username} uploaded ${file.name}`)
        res = {result: 1, payload: 'success'}
      })
      .catch(e => {
        res = {result: 1, payload: 'could not upload'}
      })
      ctx.body = res
    }




  static _transfer(localUri, remoteUri)
  {
    const promise = new Promise((resolve, reject) => {
      client.scp(localUri, remoteUri , err => {
        if (err)
        {
          reject(err)
        }
        else
        {
          resolve()
        }
      })
    })
    return promise
  }

}

