/*
 * This file simply exists so we can do
 * `import * as routes from './routes'` from
 * server.js, and use it like so:
 * routes.page.createPage.
 * Alternatively, from server.js, we could
 * import each class individually, ie,
 * import page from './routes/pages'
 * but we ain't about that.
 */


import Pages from './pages'
import Promosets from './promosets'
import Crescendos from './crescendos'
import Flips from './flips'
import Mastheads from './mastheads'
import Auth from './auth'
import Upload from './upload'

export  {
  Pages as pages
  , Promosets as promosets
  , Crescendos as crescendos
  , Flips as flips
  , Mastheads as mastheads
  , Auth as auth
  , Upload as upload
}