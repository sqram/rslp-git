/*
 * Handles all routes regarding mastheads.
 * I changed up my brackets/braces style here
 * to be very Java-like, for shits and giggles.
 * Let us know what you think by emailing  Vinoth
 * at vjohn@rosettastone.com
 * :D
 */

import fs from 'fs-extra'
import cfg from '../config'


export default class Mastheads
{

  static async getMastheads(ctx, next)
  {
    ctx.body = fs.readdirSync(cfg.dirs.mastheads).map(f =>  f)
    await next()
  }

   static async  getMastheadTypes(ctx, next)
  {
    ctx.body = fs.readdirSync(cfg.dirs.mastheadTypes)
    return next()
  }

  /*
   * Upload mastheads.
   * Two arrays are send here from the client:
   * formdata("files[]"), formdata("overwrites[]").
   * These are parallel arrays, to see if we should
   * overwrite a file if it exists
   */

  static  uploadMastheads(ctx, next)
  {
    ctx.body = []
    var files = ctx.request.body.files['files[]']
    const overwrites = JSON.parse(ctx.request.body.fields['overwrites[]'])

    if (typeof files === 'undefined')
    {
      ctx.body = {result: 0, code: 'empty', payload: 'No files received'}
    }

    // koa-body doesnt put file into an array if it's a single file
    // fucking broken.
    files = Array.isArray(files) ? files : [files]
    const promise = new Promise( (resolve, reject) => {
      files.forEach( (file, i) => {
        let dst = `${cfg.dirs.mastheadsSymlink}/${file.name}`
        var opt = {clobber: overwrites[i]}
        fs.move(file.path, dst, opt, err => {
          if (err)
          {
            reject()
          }
          else
          {
            resolve()
          }
        })
      })
    })

    return promise.then((ctx, next) => {
      ctx.body = {result: 1, code: 'upload', payload: 'Files uploaded'}
      return next()
    })
    .catch(e => {
      ctx.body = {result: 0, code: 'upload', payload: 'Could not upload files'}
      return next()
    })

  }


}


