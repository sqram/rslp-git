/*
 * Handles all routes regarding flips.
 * I changed up my brackets/braces style here
 * to be very Java-like, for shits and giggles.
 * Let us know what you think by emailing  Vinoth
 * at vjohn@rosettastone.com
 * :D
 */

import fs from 'fs-extra'
import prettyjson from 'json-format'
import moment from 'moment'
import cfg from '../config'
import logger from '../logger'

export default class Flips
{

  static async getFlips(ctx, next)
  {
    ctx.body = fs.readFileSync(cfg.files.schedule, 'utf8')
    await next()
  }



  /*
   * Create flip.
   * Mainly does 3 things:
   * 1. Write to schedule.json
   * 2. Create folder flips/<flipdate>/sitewide-promo
   * 3. Create config.json (rto info, etc)
   * 4. Create file flips/<flipdate>/flip.json.
   * The cron job script will periodically scan
   * schedule.json trying to match a datetime. If it
   * matches, it's time to flip. It then looks for a
   * folder with the datetime as its name. Inside this
   * folder is flips.json, which basically contains user
   * input from client's create-flip form. It also contains
   * the folder sitewide-promo which holds assets
   * to upload. When creating a flip and there's a flip
   * already schedule for the same datetime, we delete the
   * flip/<datime>/ folder so we can overwrite.
   * Structure:
   * pi/
   * |
   * |-- data/
   * |-- |-- schedule.json
   * |   |-- flips/
   * |   |   |-- dec 25 2016, 11:00 PM/
   * |   |   |   |-- flip.json (constants, rtos, promoset, etc)
   * |   |   |   |-- sitewide-promo/ (assets for this flip)
   * |   |   |   |
   * |   |   |   |
   * |-- server/
   * |-- client/
   *
   */

  static async upsertFlip(ctx, next)
  {
    const data = ctx.request.body.fields
    const overwrite = ctx.query.overwrite
    const id = ctx.params.id || ''
    const action = id ? 'update' : 'create'
    const idDate = id ? moment(new Date(id)).format('MMM DD YYYY, hh:mm a') : null
    var assets = ctx.request.body.files['assets[]']

    if (!data.name || !data.date || !data.promoset)
    {
      logger.error(`
        Error. missing required data.
        Name: ${data.name},
        Date: ${data.date},
        Promoset: ${data.promoset}`,
        { user: 'username' }
      )
      ctx.body =  { result: 0, payload: `Error. Missing required data.` }
    }

    const date = moment(new Date(data.date)).format('MMM DD YYYY, hh:mm a')


    /*
     * Open and read our flip schedule. Attempt to filter out any flip
     * that already exists, in case we're overwriting
     */

    try
    {
      const filter = id ? id : data.date
      var schedule = fs.readJsonSync(cfg.files.schedule, 'utf8')
      schedule = schedule.filter(s => s.rawdate !== filter)
    }
    catch(e)
    {
      logger.error(`Couldn't open schedule.json: ${e}`, { user: 'username' })
      ctx.body = { result: 0, payload: `Error. Couldn't open schedule.json.` }
    }


   /*
    * We always overwrite. We already filtered out existing flips
    * info when we opened schedule.json, so now delete any
    * files directory, if any, that was here before. Basically
    * delete a file directory whose name is this flip's date.
    */

    const dirs = fs.readdirSync(cfg.dirs.flipFiles)
    const dirname = id ? idDate : date
    if (dirs.includes(dirname))
    {
      try
      {
        console.log('removing ', dirname)
        fs.removeSync(`${cfg.dirs.flipFiles}/${dirname}`)
      }
      catch(e)
      {
        logger.error(`Could not remove files/${dirname}: ${e}.`, { user: 'username' })
        ctx.body = { result: 0, payload: 'Could not remove old files direcory.' }
      }
    }

    /*
     * At this point we have already filetered out our schedule
     * to filter out existing flips on this date, and also deleted
     * any existing dir relating to this date. It's like any
     * existing flip on this date never existed. Starting new.
     * Create our new files dir. The name is the date of this flip.
     */

    try
    {
      fs.ensureDirSync(`${cfg.dirs.flipFiles}/${date}/sitewide-promo`)
    }
    catch(e)
    {
      logger.error(`Could not create flips/${date}/ and its subdirs: ${e}`, { user: 'username' })
      ctx.body = { result: 0, payload: 'Error. Could not create folder(s).' }
    }



    /* If we have assets to upload, do so here. */

    if (undefined !== assets)
    {
      // koa-body doesnt put obj into an array if it's a single file.
      // Fucking broken. Put single obj inside an array here.
      assets = Array.isArray(assets) ? assets : [assets]

      var ret = {}
      var failedUploads = []

      for (let file in assets)
      {
        let dst = `${cfg.dirs.flipFiles}/${date}/sitewide-promo/${assets[file].name}`
        fs.move(assets[file].path, dst, err =>
        {
          if (err)
          {
            failedUploads.push(`${assets[file].name}`)
          }
        })
      }

      if (failedUploads.length)
      {
        fs.removeSync(`${cfg.localUris.flips}/${date}`)
        ctx.body =  {result: 0, payload: `Failed up upload: ${failedUploads}`}
      }
    }


    /*
     * Write our flip info to schedule.json
     * jason-format stringifies, so JSON.parse objects
     * inside first
     */

    data.constants = JSON.parse(data.constants)
    data.config = JSON.parse(data.config)
    schedule.push({
      dirname: date,
      rawdate: data.date,
      ...data
    })

    try
    {
      fs.writeFileSync(cfg.files.schedule, prettyjson(schedule))
    }
    catch(e)
    {
      logger.error(`Could write to schedule.json: ${e}`, { user: 'username' })
      ctx.body =  {result: 0, payload: `Couldn't write to schedule.json: ${e}.`}
    }

    logger.info(`--user-- ${action}d flip ${data.name}`, { user: 'username' })
    ctx.body =  {result: 1, payload: `Success. Flip ${action}d.`}

    await next()
  }



}


