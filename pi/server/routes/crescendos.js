/*
 * Handles all routes regarding crescendos.
 * I changed up my brackets/braces style here
 * to be very Java-like, for shits and giggles.
 * Let us know what you think by emailing  Vinoth
 * at vjohn@rosettastone.com
 * :D
 */

import fs from 'fs-extra'
import cfg from '../config'


export default class Crescendos
{

  static  getCrescendos(ctx, next)
  {
    ctx.body = fs.readdirSync(cfg.dirs.crescendos).map(c => c.slice(0, -5))
    return next()
  }


  static async createCrescendo(ctx, next)
  {

  }

}


