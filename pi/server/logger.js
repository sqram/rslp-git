import winston from 'winston'

winston.configure({
  transports: [
    new (winston.transports.File)({ filename: 'app.log' })
  ]
})

export default winston