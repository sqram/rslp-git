import {Store} from 'koa-session2'


export default class MemStore extends Store {

  constructor()
  {
    super()
    this.store = {}
  }

  get(sid)
  {
    const data = this.store[`RSSESSION:${sid}`]
    if (data)
    {
      return JSON.parse(data)
    }
    return false

  }


  set(session, opts)
  {
    if (!opts.sid)
    {
      opts.sid = this.getID(24)
    }

    this.store[`RSSESSION:${opts.sid}`] = JSON.stringify(session)
    return opts.sid
  }


  destroy(sid)
  {
    delete this.store[`RSSESSION:${sid}`]
  }

}