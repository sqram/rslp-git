/*
 * Class strictly made for validating
 * user input.
 * PS: I know these are very basic and should
 * be more strict. Maybe I'll comeback to it later.
 */

//import v from 'validator'

export default class Validate {

  /*
   * Evaluate entire page object
   * Required fields are:
   * name, promoset, phone, expiration, market
   * mastheadType, mastheadImage
   */
  page(pageObj) { 
    const 
      name = pageObj.name.trim()
      , promoset = pageObj.promoset.trim()
      , phone = pageObj.phone.trim()
      , expiration = pageObj.expiration.trim()
      , mastheadType = pageObj.mastheadtype.trim()
      , mastheadImage = pageObj.mastheadimage.trim()
      , market = pageObj.market.trim()
      ;

    var passed = true

    if (!name) passed = false
    if (!promoset) passed = false
    if (phone.length < 10) passed = false
    if (!expiration) passed = false
    if (!mastheadType) passed = false
    if (mastheadImage.split('-').length < 3) passed = false
    if (!market) passed = false

    return passed

    
  }


}