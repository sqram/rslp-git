/*
 * This is the store for Websocket clients.
 * All connected clients are here. This is used
 * so we can send a message to everyone's client
 * when something happens. In the UI, these messages
 * are displayed inside a `Nag` component
 */


export default class Sockets
{

  constructor(wss)
  {
    this.clients = []
    wss.on('connection', ws => this.add(ws))
  }

  /*
   * Adds a client to the list.
   * @param {object} client - the client to add
   */

  add(client)
  {
    client.on("close", () => this.remove(client))
    this.clients.push(client)
  }

  /*
   * Remove a websocket client from the list
   * @param {object} - the client to remove
   */

  remove(client)
  {
    const clientIndex = this.clients.indexOf(client)
    if (clientIndex !== -1)
    {
      this.clients.splice(clientIndex, 1);
    }
  }

  /*
   * Broadcast a message to all clients.
   * @param {string} msg - the message to display in UI nags
   */

  broadcast(msg)
  {
    const message = msg
    this.clients.forEach(client =>  client.send(message))
  }
}