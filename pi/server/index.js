'use strict';

require('babel-register')({
  plugins: [
    'transform-async-to-generator'
  ],
  presets: [
    'babel-preset-es2015',
    'babel-preset-stage-0'
  ]
});

require('./server.js')