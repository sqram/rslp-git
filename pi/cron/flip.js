/*
 * This file performs the actual flipping.
 * It is the file executed by the cron job
 * 
 * 1. copy constant landing pages
 * 2. rsifast
 * 3. catalog gulp
 * 4. upload results
 * 5. upload images (sitewide-promo)
 * 6. upload criteo
 * 7. compile & homeschool
 * *
 */






/*
 * This function uploads assets such as images and
 * criteo feed to the server. Note that this is for images
 * only. Actual uploading of landing and catalog pages
 * is done by another function.
 *
 * @param {obj} pathObj
 *   An object with {src:dst} paths, where:
 *   src: path inside data/flips/{data}/{dir}/file
 *   dst: server path.
 *   example:
 *   {
 *     '~/app/data/flips/12-05-2016/sitewide-promo/us-buynow.jpg':
 *     'ashcclp00.lt:/landingpagesprod/sitewide-promo/us-buynow.jpg'
 *   }
 */

export function uploadAssets(ctx, next, pathObj) {
  ctx.body = 'Hello Koa';
}