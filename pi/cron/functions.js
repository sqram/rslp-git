import fs from 'fs'
import config from './config'


// TODO fix and finish this fn
/*
 * Mirror constant pages. Pages like sitewide, rmsitewide
 * sale, social, etc, are constant. With each flip, they
 * usually mirror a page that already exist. For instance,
 * rmsitewide may mirror 'valentinebdi'. So we copy
 * all values (exp date, masthead, etc) into rmsitewide.
 * Same two pages, just different names.
 * 
 * @param {obj} mirrorMap
 *   An object with the mapping. ie,
 *   {sitewide: 'valentinebdi', ...}
 *   sitewide will be a copy of valentinebdi
 *
 * @returns true
 */

export function mirrorConstantPages(mirrorMap = {sitewide: 'prodpurpleaii', nope: 'sale'}) {  
  
  // Pages in mirrorMap that are not in the pages.json.
  var notFound = []

  var fd = fs.openSync(config.localPaths.pages, 'r+')    
  var pages = JSON.parse(fs.readFileSync(fd, 'utf8'))  

  Object.keys(mirrorMap).forEach(page => {
    if (page in pages) {
      // JSON.stringify for deep copy (header section)
      pages[page] = pages[mirrorMap[page]]
    } else {
      notFound.push(page)      
    }
  })

  // Tried to mirror a page that doesn't exist.
  if (notFound.length) {    
    return notFound
  }

  
  fs.truncateSync(fd)
  fs.writeFileSync(fd, JSON.stringify(pages))
  return true 
    
}
