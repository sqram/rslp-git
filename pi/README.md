# Pi ○

Pi allows for scheduled automation of sitewide flips.
As well as landing page creation

It comes in two parts: a server, and a client.
I tried to keep them as separated as possible. If someone is bored,
they could write an entire new UI and just make API calls to the server.
This means, however, that we'll need two node instances running,
one for the client, one for the api server.

Let's try to keep  consistencyof ports:
 - serve the api server on port 3000
 - serve the web client on port 3001
 
### What happens in a flip?
  0. ~ fucking magic ~
  1. Campaign specific landing pages get compiled & uploaded, if not done so already.
  2. Constant landing pages get mirrored to look like another page.  
    For example, during a valentine's campaign, we may want sitewide
    to look just like 'valentineaii'. I call this constant mirroring.
  3. Catalog pages get compiled & updated with new prices.
    This is done by opening the gulpconfig file, specifying which json file 
    you want to compile with (ie, RSI_199.json). To get the values for this
    json file, you can visit any landing page with that price, and run
    copy(RSI()) in the console. Then you'll also need to append homeschool
    data to that json file.
  4. Hispanic catalog pages get updated with new prices (currently liferay)
  5. Assets need to be replaced. All assets are in a folder called sitewide-promo.
    So we just need to upload images to that folder in prod
  6. Criteofeed.csv is uploaded
  7. Homeschool catalog pages need to be updated (english-curriculum, etc)
  8. Homeschool hslp (rosettastone.com/hslp) needs to be updated (liferay)
  9. rosettastone.com/pricing needs to be updated.
    svn file: /us/ctalogpages/js/rsi_pricing.js
    prod file: /lp/ctalogpages/js/rsi_pricing.js
  10. router needs to be updated
    svn file: /us/router/index.html
    prod file: /lp/router/index.html
  11. Check everything is right. This whole process takes 2hrs - 2.5hrs



### How to work on Pi
  1. Open terminal to ./pi/server and run $ node server.js
  2. Open terminal to ./pi/client and run $ node server.js
     - now you can visit http://localhost:3001
  3. Open terminal to ./pi/client and run $ gulp watch
     - Allows you to edit web client source code
     - visit http://localhost:9000 for live changes