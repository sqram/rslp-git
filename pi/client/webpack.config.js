import webpack from 'webpack'
var path = require('path')

module.exports = {
  entry: './src/js/index.js',
  output: {
    path: __dirname,
    filename: 'bundle.js'
  },


  resolve: {
    alias: {
      'semantic': path.resolve(__dirname, '../client/semantic/dist/semantic.js')
      // If desired, you can do a per-module import. Just alias them here individually:
      //'semantic-dropdown': path.resolve(__dirname, '../client/semantic/dist/components/dropdown.js')
    }
  },

  module: {

    loaders: [
      {
        // Babelify js files
        test: /\.js?$/,
        exclude: /vendor|dist|node_modules/,
        loader: 'babel'
      },
      {
        // Parse .vue files with vue, not js
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        //Make $ global with expose-loader plugin//
        test: require.resolve('jquery'),
        loader: 'expose?jQuery!expose?$'
      }
    ]
  }
}