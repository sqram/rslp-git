/*
 * These are our mutation types.
 * Any action function that changes the state
 * will call a mutation function, where the
 * muation function name is one of these const
 * values. By looking at this file you can see
 * all the mutations possible in the entire app.
 * (in the vuex store, not local components)
 */

export const LOG_IN = 'LOG_IN'
export const LOG_OUT = 'LOG_OUT'
export const SET_USERNAME = 'SET_USERNAME'
export const ADD_NAG = 'ADD_NAG'
export const REMOVE_NAG = 'REMOVE_NAG'
export const TOGGLE_MASTHEAD_MODAL = 'TOGGLE_MASTHEAD_MODAL'

