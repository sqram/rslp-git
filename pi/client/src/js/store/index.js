import Vue from 'vue'
import * as types from './mutation-types'


// Components will dispatch these actions. Actions can be async
const actions = {
  toggleMastheadModal({commit}) {
    commit(types.TOGGLE_MASTHEAD_MODAL)
  },
  logIn({commit}) {
    commit(types.LOG_IN)
  },
  logOut({commit}) {
    commit(types.LOG_OUT)
  },
  setUsername({commit}, username) {
    commit(types.SET_USERNAME, username)
  },
  addNag({commit}, obj) {
    commit(types.ADD_NAG, [obj, ...state.nags])
  },
  removeNag({commit}, id) {
    commit(types.REMOVE_NAG, [...state.nags.filter(o => o.id != id)])
  }
}


// Functions that mutate the vuex state. committed by actions above.
const mutations = {
  [types.UPDATE_PAGES] (state, pages) {
    state.pages = pages
  },
  [types.UPDATE_PROMOS] (state, promos) {
    state.promos = promos
  },
  [types.UPDATE_MASTHEADS] (state, images) {
    state.mastheads = images
  },
  [types.TOGGLE_MASTHEAD_MODAL] (state) {
    state.modal_masthead = {show: !state.modal_masthead.show, message: null}
  },
  [types.LOG_IN] (state) {
    state.isLoggedIn = true
  },
  [types.LOG_OUT] (state) {
    state.isLoggedIn = false
  },
  [types.SET_USERNAME] (state, username) {
    state.username = username
  },
  [types.ADD_NAG] (state, nagsClone) {
    state.nags = nagsClone
  },
  [types.REMOVE_NAG] (state, nagsClone) {
    state.nags = nagsClone
  },

}


// Vuex state. Changed by mutations above
const state = {
  isLoggedIn: window.sessionStorage.getItem('loggedIn') || false,
  username: window.sessionStorage.getItem('username') || null,
  mastheadImages: [],
  pages: [],
  promos: [],
  nags: [],
  modal_masthead: {
    show: false,
    message: null
  }
}



export default {
  state,
  actions,
  mutations
}

