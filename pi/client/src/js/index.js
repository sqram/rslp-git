import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import mainStore from './store'
import Home from './containers/Home.vue'
import App from './containers/App.vue'
import Login from './containers/Login.vue'
import CreateFlip from './containers/CreateFlip.vue'
import CreatePage from './containers/CreatePage.vue'
import CreatePromoset from './containers/CreatePromoset.vue'
import CreateCrescendo from './containers/CreateCrescendo.vue'
import ListFlips from './containers/ListFlips.vue'
import ListPages from './containers/ListPages.vue'
import ListPromosets from './containers/ListPromosets.vue'
import LiveAssets from './containers/LiveAssets.vue'
import Me from './containers/Me.vue'
import semantic from 'semantic'
import _ from 'lodash'


window.server = 'http://localhost:3000'

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VueResource)

const store = new Vuex.Store(mainStore)
const router = new VueRouter({
  routes: [
    { path: '/', component: Home },
    { path: '/login', component: Login },
    { path: '/create/flip', component: CreateFlip },
    { path: '/create/flip/edit/:id', component: CreateFlip },
    { path: '/flips', component: ListFlips },
    { path: '/pages', component: ListPages },
    { path: '/promosets', component: ListPromosets },
    { path: '/create/page', component: CreatePage },
    { path: '/create/page/edit/:id', component: CreatePage },
    { path: '/create/promoset', component: CreatePromoset },
    { path: '/create/promoset/edit/:id', component: CreatePromoset },
    { path: '/create/crescendo', component: CreateCrescendo },
    { path: '/live-assets', component: LiveAssets },
    { path: '/me', component: Me }
  ]
})




new Vue({
  el: 'app',
  router,
  store,
  render: h => h(App)
})



// Vue.filter('nl2br', v =>  v.replace(/\n/g, '<br>') )