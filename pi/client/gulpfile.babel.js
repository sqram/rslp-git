var os = require('os')
var gulp  = require('gulp')
var $     = require('gulp-load-plugins')()
var webpackstream = require('webpack-stream')

// will be true || false. Allow us to do "if (prod) {..}" when building
var prod = $.util.env.type == 'production';

var babelconfig = require('./webpack.config')



String.prototype.toWindows = function() {
  return os.platform().match(/win/i) ? this.replace(/\//g, '\\') : this
}



// Simply copy our html file to dist,
// where the localhost servers it from
gulp.task('html', () => {
    gulp.src(['./src/*.html'.toWindows()], { base: './src'.toWindows() })
      .pipe(gulp.dest('./dist/'.toWindows()))
      .pipe($.connect.reload())
});


// Process our stylus into css
gulp.task('css', () => {
    gulp.src(['src/css/*.styl'.toWindows()], { base: './src'.toWindows() })
      .pipe($.stylus({
        'include css': true,
        compress: true
      }))
      .pipe(gulp.dest('./dist/'.toWindows()))
      .pipe($.connect.reload())
});


// Copy .css files over to dist/css. They don't
// need to parsed since they're not a preprocessor file.
gulp.task('copy-css', () => {
  gulp.src(['./src/css/**/*.css'.toWindows()], { base: './src'.toWindows() })
    .pipe(gulp.dest('./dist/'))
})


// Transpile and bundle our js
gulp.task('js', () => {
  gulp.src(['src/js/**/*.js'.toWindows()], { base: 'src'.toWindows() })
    .pipe($.plumber())
    .pipe(webpackstream(babelconfig))
    .pipe(prod ? $.uglify() : $.util.noop())
    .pipe(gulp.dest('./dist/js'.toWindows()))
    .pipe($.connect.reload())
})


// Copy 3rd party js to dist/js/vendor folder.
// Since they don't go through the webpack plugin,
// and require no compiling/transpiling, just copy.
gulp.task('copy-js', () => {
  gulp.src(['./src/js/vendor/*.js'.toWindows()], { base: './src'.toWindows() })
    .pipe(gulp.dest('./dist/'.toWindows()))
})

// assets
gulp.task('assets', () => {
  return gulp.src(['src/assets/**/*'.toWindows()], { base: 'src'.toWindows() })
    //.pipe($.cache())
    .pipe(gulp.dest('dist'.toWindows()))
    .pipe($.connect.reload())
});

// connect localserver
gulp.task('connect', () => {
  $.connect.server({
    root: 'dist',
    port: 9000,
    livereload: true
  })
});


// watch
gulp.task('watch', ['connect'], () => {

  // watch html files
  gulp.watch('src/*.html'.toWindows(), ['html']);

  // watch .styl files
  gulp.watch('src/css/*'.toWindows(), ['css']);

  // watch .js files
  gulp.watch('src/js/**/*.js'.toWindows(), ['js']);

  // watch .vue files
  gulp.watch('src/js/**/*.vue'.toWindows(), ['js']);

  // watch image files
  gulp.watch('src/assets/**/*'.toWindows(), ['assets']);

});


// for dev - just run 'gulp'
gulp.task('default', ['connect', 'watch'])

// for distribution. call with gulp build --type production
// but really, you should be doing $ npm run build
gulp.task('build', ['copy-js', 'js', 'copy-css', 'css', 'html', 'assets'])