/*
 * You're probably thinking why there's a server.js
 * file inside the client folder.
 * The whole purpose of this file is to serve the
 * web client. This web client makes API calls
 * to a separate server. That's it!
 */



const Koa = require('koa')
const app = new Koa()
const serve = require('koa-static')

app.use(serve('./dist'))

app.listen(3001)

