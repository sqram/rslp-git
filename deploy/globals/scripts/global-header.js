/**
 * File created mostly for the need to redirect without
 * waiting for most of the page to load
 */


// Detects if we are on a mobile device.
// User-agent sniffing isn't reliable, but Yolo.

window.isMobile = (function() {
	var check = false;
	var param = navigator.userAgent || navigator.vendor || window.opera;
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(param)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(param.substr(0,4)))
		return true;
	return false;
})()



// Create RSI for pages that have the RSI model injected after this file
// Array list is tiny so this should be fine
/*
if (typeof RSI == 'undefined') {
	var rsi = window.location.pathname.split('/').filter(function(e) {
		return e === 0 ? '0' : e
	}).reverse()[0]

	RSI = { rsi: rsi }
}
*/

// mobile exclusive redirect.

var date 	= new Date()
var month = ("0" + (date.getMonth() + 1)).slice(-2)
//var day 	= ("0" + date.getDate()).slice(-2)
var day = date.getDate()
var hour 	= ("0" + date.getHours()).slice(-2)


// Keys are months, values are array of days.
// Months: double digits. (ie, [02, 08])
// Days: double digits. (ie, [07, 15])

var schedule = {
	// month    day(s)
	"04":       [10,13,16,18]
}

if (schedule[month] && schedule[month].indexOf(day) != -1) {
	mobileexclusive()
}

function mobileexclusive() {
	if (!isMobile == true) return

	if (typeof RSI == 'undefined') return

	// the mobile exclusive page to redirect to
	// var destination = 'mobaii'
	var destination = 'mobafi'


	// Don't infinite loop
	if ( RSI.rsi == destination ) return


	var dst_template = 'sbsr'

	// if it's a responsive template, we don't send them to mobile

	var match = window.location.pathname.match(/sbsr|offer|sbsre|solo|sbshybrid/i) || []
	if (match.length) dst_template = match[0]

	var price = RSI({cat: 'esp', lvl: 'S5'})[0].price

	// Affected pages/prices. Pages where S5 prices
	// are in the array, will redirect to `destination`
	var affected = [229, 189, 179]


	if (affected.indexOf(parseInt(price)) != -1) {

		// set ?ed's value as the current date. these offers are usually 24hrs only
		var d = '0017' + month + ("0" + day).slice(-2) + '99'

		// send an ?ed on url to show clock (only if mobile exclusive is for 1 day)
		var ed = (window.location.search ? '&' : '?') + 'ed=' + d
		var r  =  '/lp/' + dst_template + '/' + destination +'/' + window.location.search + ed
		window.location.href = r
	}

}



// For dutyfree, hide v3 langs
if (RSI.rsi == 'dutyfree') {
  document.styleSheets[0].insertRule('.v3{ display: none }', 0)
}



if (window.location.href.match(/sitewide\/\?onemonth=true/i)) {
	window.setTimeout(function() {
		$('span#expiration').html("01/08/2017 11:59pm")
	}, 4000)
}