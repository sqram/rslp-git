// English Dictionary

product = {
  name: "English",
  copy: "Get a FREE Barron's English Dictionary",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/eng-dictionary.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40229/category_id/eng/?pc=ESSTN"
}


var langcode = rs.pixp_lang.slice(0, 3).toLowerCase()

//langcode = (langcode == 'esc') ? 'esp' : langcode



var rtohtml= "";
var rtocd = ''
  +'<div id="rtocd">'
  +'  <div id="rto">'
  +'    <div id="rto_showhide">'
  +'      <img src="https://rosettastone.com/lp/rto-test/close_black.png" id="rto_show_button">'
  +'      <img src="https://rosettastone.com/lp/rto-test/close_black.png" id="rto_hide_button">'
  +'    </div>'
  +'      <img id="rtosideimage" src="'+ product.image + '">'
  +'    <div id="rtoheading">'
  +'      <div class="saveadditional">'
  +'        ' + product.copy
  +'      </div>'
  +'      <div class="nextten">'
  +'        If you order in the next ten minutes'
  +'      </div>'
  +'    </div>'
  +'    <div id="clock">'
  +'      <div class="clockdiv timeleft">TIME LEFT:</div>'
  +'      <div class="clockdiv"><span class="timenumber" id="clockminutes">09</span> min</div>'
  +'      <div class="clockdiv clockdiv-last"><span class="timenumber" id="clockseconds">07</span> sec</div>'
  +'      <div class="clockshadow"></div>'
  +'    </div>'
  +'    <div class="bannerbutton">'
  +'      <a href="#" id="rtobutton">'
  +'        CLAIM YOUR MYSTERY  GIFT'
  +'      </a>'
  +'    </div>'
  +'  </div>'
  +'</div>';





// Free headset w/ S5 CDs: Headset15 || $10 RTO w/ S5 Downloads: 10PROMO || 1 free month of fitbrains w/ 24 month TOSUB: free1m
var yb = { id : function(str){return document.getElementById(str)} };

// initial settings
var countdown_minutes = 10; // set how many minutes we want the clock to run for
var endtime = new Date( new Date().getTime() + countdown_minutes*60*1000 ); // calculate the javascript end time





// what is the first rto product match in cart (S5DL || 24M || S5CD)?
// function rto_type(){return rs.pixp_name.match(/totale|download pto|consumer pto/i);}
//function rto_type(){return rs.pixp_name.match(/totale.+24|s5\s*consumer|s5\s*v4.+download/i);}
// has the the rto already been seen?
function rto_not_seen_yet(){return !document.cookie.match(/rto_cart_cookie/i);}
// has the rto clock expired yet?
function rto_expired(){return (new Date()) > endtime;}



// rto promo code already in cart?
function check_promo_name(){
  if(!!jQuery('.couponlabel')[0]){ // if desktop, check the desktop layout for promo codes
    for(var i=0; i < jQuery('.couponlabel').length; i++){
      if(!!jQuery('.couponlabel')[i].innerHTML.match(/Headset15/i)){return true}
    }
  }
  else if(!!jQuery('#promoapplied')[0]){ // if mobile, check the mobile layout for promo codes
    if(!!jQuery('#promoapplied')[0].innerHTML.match(/Headset15/i)){return true}
  }
  return false;
}


// rto product name already in cart?
function check_prodname(){
  for(var i=0; i < jQuery('.prodname').length; i++){
    if(!!jQuery('.prodname')[i].innerHTML.match(/headset/i)){return true}
  }
  return false;
}





function showRTO(){
  // create a container for the rto and put the right html in there
  var rtocontainer = document.createElement('div');
  rtocontainer.innerHTML = rtohtml;

  // show the rto at the beginning of the body
  document.body.insertBefore(rtocontainer, document.body.firstChild);


  // this function updates the clock
  function setClock(){
    var timeleft = new Date( endtime - (new Date()) ); // remaining time is end time minus current time
    // update the values in the clock
    yb.id('clockseconds').innerHTML = timeleft.getSeconds() < 10 ? "0" + timeleft.getSeconds() : timeleft.getSeconds();
    yb.id('clockminutes').innerHTML = timeleft.getMinutes() < 10 ? "0" + timeleft.getMinutes() : timeleft.getMinutes();
    // when time runs out, stop the rto
    if(Math.floor(timeleft/1000)<=0){stopRTO();}
  }

  setClock(); // start the clock
  var clockinterval = setInterval(setClock,50); // keep the clock running


  // this function stops the clock and hides the rto
  function stopRTO(){
    document.cookie = 'rto_accepted=1'
    clearInterval(clockinterval); // stop the clock
    yb.id('rto').style.display = 'none'; // hide the rto
  }

  // when user clicks rto button, hide rto and apply promo
  yb.id('rtobutton').onclick = function(){
    stopRTO(); // stop the rto
    var params = window.location.search ? window.location.search.replace(/\?/, '&') : ''
    //var dst = 'https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/'+sku+'/category_id/'+langcode+'/?pc='+promo + params
    window.location.href = product.carturl + params

  }


    // this section toggles the rto by sliding it to the side
    var rto_showing = true;

    yb.id('rto_showhide').onclick = function(){
      if(rto_showing){
        rto_showing = false;
        yb.id('rto_show_button').style.bottom = '90px';
        yb.id('rto_show_button').style.display = 'block';
        yb.id('rto_hide_button').style.display = 'none';
        yb.id('rto').style.display = 'none';
      }
      else{
        rto_showing = true;
        yb.id('rto_hide_button').style.display = 'block';
        yb.id('rto_show_button').style.display = 'none';
        yb.id('rto').style.display = 'none';
      }
    };
}




// is rto available && not already accepted?
jQuery('document').ready(function(){
  if( !(check_promo_name() || check_prodname())){

    //var rtot = rto_type()[0].toLowerCase();
    // if rto has not seen yet, configure endtime
    rto_not_seen_yet() ? document.cookie = 'rto_cart_cookie=' + endtime : endtime = new Date(document.cookie.split('rto_cart_cookie=')[1].split(';')[0]);
    // choose rto html for first matched product in cart (S5DL || 24M || S5CD)
    rtohtml = rtocd;
    // show the rto

    if((document.cookie.match(/rto_accepted/i) == null) && !rto_expired()){showRTO();}
  }
})