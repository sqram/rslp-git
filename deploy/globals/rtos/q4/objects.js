/*********************************************
 *  DICTIONARY BOOKS
 *********************************************/

// EN Dictionary
product = {
  sku: 40229,
  promo: "ESSTN",
  name: "English",
  copy: "Get a FREE Barron's English Dictionary",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/eng-dictionary.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40229/category_id/eng/?pc=ESSTN"
}

// FRA Dictionary
product = {
  sku: 40232,
  promo: "FRASTN",
  name: "French",
  copy: "Get a FREE Barron's French Dictionary",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/fra-dictionary.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40232/category_id/eng/?pc=FRASTN"
}

// ITA Dictionary
product = {
  sku: 40238,
  promo: "ITASTN",
  name: "Italian",
  copy: "Get a FREE Barron's Italian Dictionary",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/ita-dictionary.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40238/category_id/eng/?pc=ITASTN"
}

// DEU Dictionary
product = {
  sku: 40244,
  promo: "DEUSTN",
  name: "German",
  copy: "Get a FREE Barron's German Dictionary",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/deu-dictionary.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40244/category_id/eng/?pc=DEUSTN"
}

// ESP Dictionary
product = {
  sku: 40229,
  promo: "ESSTN",
  name: "Spanish",
  copy: "Get a FREE Barron's Spanish Dictionary",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/esp-dictionary.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40229/category_id/eng/?pc=ESSTN"
}

/*********************************************
 *  GRAMMAR BOOKS
 *********************************************/

// EN Grammar
product = {
  sku: 40269,
  promo: "ENGGMR",
  name: "English",
  copy: "Get a FREE Barron's English Grammar Book",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/eng-grammar.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40269/category_id/eng/?pc=ENGGMR"
}

// FRA Grammar
product = {
  sku: 40263,
  promo: "FRAGMR",
  name: "French",
  copy: "Get a FREE Barron's FrenchGrammar Book",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/fra-grammar.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40263/category_id/eng/?pc=FRAGMR"
}

// ITA Grammar
product = {
  sku: 40265,
  promo: "ITAGMR",
  name: "Italian",
  copy: "Get a FREE Barron's Italian Grammar Book",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/ita-grammar.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40265/category_id/eng/?pc=ITAGMR"
}

// DEU Grammar
product = {
  sku: 40267,
  promo: "DEUGMR",
  name: "German",
  copy: "Get a FREE Barron's German Grammar Book",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/deu-grammar.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40267/category_id/eng/?pc=DEUGMR"
}

// ESP Grammar
product = {
  sku: 40271,
  promo: "ESPGMR",
  name: "Spanish",
  copy: "Get a FREE Barron's Spanish Grammar Book",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/esp-grammar.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40271/category_id/eng/?pc=ESPGMR"
}


/*********************************************
 *  TRAVEL BOOKS
 *********************************************/

// EN Grammar
product = {
  sku: 40270,
  promo: "ENGBIZ",
  name: "English",
  copy: "Get a FREE Barron's English Travel Book",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/eng-travel.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40270/category_id/eng/?pc=ENGBIZ"
}

// FRA Grammar
product = {
  sku: 40264,
  promo: "FRATVL",
  name: "French",
  copy: "Get a FREE Barron's FrenchTravel Book",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/fra-travel.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40264/category_id/eng/?pc=FRATVL"
}

// ITA Grammar
product = {
  sku: 40266,
  promo: "ITATVL",
  name: "Italian",
  copy: "Get a FREE Barron's Italian Travel Book",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/ita-travel.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40266/category_id/eng/?pc=ITATVL"
}

// DEU Grammar
product = {
  sku: 40268,
  promo: "DEUTVL",
  name: "German",
  copy: "Get a FREE Barron's GermanTravel Book",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/deu-travel.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40268/category_id/eng/?pc=DEUTVL"
}

// ESP Grammar
product = {
  sku: 40272,
  promo: "ESPGMR",
  name: "Spanish",
  copy: "Get a FREE Barron's Spanish Travel Book",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/esp-travel.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/40272/category_id/eng/?pc=ESPGMR"
}



/*********************************************
 *  Rick Steves
 *********************************************/

product = {
  sku: 99063,
  promo: "rsebook15",
  name: "",
  copy: "Get a FREE Rick Steves eBook",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/" + langcode + "-travel.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/99063/?pc=rsebook15"
}



/*********************************************
 *  Headset TRRS Boom Yel
 *********************************************/
product = {
  sku: 54084,
  promo: "ylheadset",
  name: "",
  copy: "Get a FREE Headset",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/headset-1.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/54084/?pc=ylheadset"
}


/*********************************************
 *  Headset USB Bulk 7AU-IS
 *********************************************/
product = {
  sku: 50984,
  promo: "ylheadset",
  name: "",
  copy: "Get a FREE Headset",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/headset-2.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/50984/?pc=headsetbk"
}


/*********************************************
 *  HeadSet Headset USB Bulk Earbud 7AU-IS
 *********************************************/
product = {
  sku: 52154,
  promo: "rsebook15",
  name: "",
  copy: "Get a FREE Headset",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/headset-1.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/52154/?pc=rsebook15"
}


/*********************************************
 *  Headset USB Retail 7AU-IS
 *********************************************/
product = {
  sku: 20003,
  promo: "headsetre",
  name: "",
  copy: "Get a FREE Headset",
  image: "https://rosettastone.com/lp/globals/rtos/q4/img/headset-2.png",
  carturl: "https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/sku/20003/?pc=headsetre"
}

