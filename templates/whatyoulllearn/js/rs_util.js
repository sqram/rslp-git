if(!this["RSUI"]||RSUI==null||RSUI==undefined){RSUI={}}RSUI.util=new Object();RSUI.util.getParamFromLandingPageUrl=function(b){b=b.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");var a="[\\?&]"+b+"=([^&#]*)";var d=new RegExp(a);var c=d.exec(window.location.href);if(c==null){return""}else{return c[1]}};RSUI.util.getLocalTimeIncrement=function(){var b;try{RSUI.util.dateFormat.masks.timeIncrement="yyyy-mm-dd HH:";var a=new Date();var e=a.format_rs("timeIncrement");var c=Math.floor(a.getMinutes()/10).toString();b=e+c+"0"}catch(d){b=d}return b};RSUI.util.setCookie=function(c,h,b,d,k,e,a){if(!b){d=30*1000*60*60*24;k="/";var j=RSUI.util.parseUri(document.location).host.split(".");j.reverse();e="."+j[1]+"."+j[0];if(j.length>=3&&j[2]!="www"&&j[2]!="stg"){e="."+j[2]+"."+j[1]+"."+j[0]}}var f=new Date();var g=new Date(f.getTime()+(d));document.cookie=c+"="+escape(h)+((d)?";expires="+g.toUTCString():"")+((k)?";path="+k:"")+((e)?";domain="+e:"")+((a)?";secure":"")};RSUI.util.getCookie=function(a){var f=document.cookie.split(";");var b="";var d="";var e="";var c=false;for(i=0;i<f.length;i++){b=f[i].split("=");d=b[0].replace(/^\s+|\s+$/g,"");if(d==a){c=true;if(b.length>1){e=unescape(b[1].replace(/^\s+|\s+$/g,""))}return e;break}b=null;d=""}if(!c){return null}};RSUI.util.parseUri=function(e){var d=RSUI.util.parseUri.options,a=d.parser[d.strictMode?"strict":"loose"].exec(e),c={},b=14;while(b--){c[d.key[b]]=a[b]||""}c[d.q.name]={};c[d.key[12]].replace(d.q.parser,function(g,f,h){if(f){c[d.q.name][f]=h}});return c};RSUI.util.parseUri.options={strictMode:false,key:["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],q:{name:"queryKey",parser:/(?:^|&)([^&=]*)=?([^&]*)/g},parser:{strict:/^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,loose:/^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/}};RSUI.util.dateFormat=function(){var a=/d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,b=/\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,d=/[^-+\dA-Z]/g,c=function(f,e){f=String(f);e=e||2;while(f.length<e){f="0"+f}return f};return function(j,w,r){var g=RSUI.util.dateFormat;if(arguments.length==1&&Object.prototype.toString.call(j)=="[object String]"&&!/\d/.test(j)){w=j;j=undefined}j=j?new Date(j):new Date;if(isNaN(j)){throw SyntaxError("invalid date")}w=String(g.masks[w]||w||g.masks["default"]);if(w.slice(0,4)=="UTC:"){w=w.slice(4);r=true}var u=r?"getUTC":"get",n=j[u+"Date"](),e=j[u+"Day"](),k=j[u+"Month"](),q=j[u+"FullYear"](),t=j[u+"Hours"](),l=j[u+"Minutes"](),v=j[u+"Seconds"](),p=j[u+"Milliseconds"](),f=r?0:j.getTimezoneOffset(),h={d:n,dd:c(n),ddd:g.i18n.dayNames[e],dddd:g.i18n.dayNames[e+7],m:k+1,mm:c(k+1),mmm:g.i18n.monthNames[k],mmmm:g.i18n.monthNames[k+12],yy:String(q).slice(2),yyyy:q,h:t%12||12,hh:c(t%12||12),H:t,HH:c(t),M:l,MM:c(l),s:v,ss:c(v),l:c(p,3),L:c(p>99?Math.round(p/10):p),t:t<12?"a":"p",tt:t<12?"am":"pm",T:t<12?"A":"P",TT:t<12?"AM":"PM",Z:r?"UTC":(String(j).match(b)||[""]).pop().replace(d,""),o:(f>0?"-":"+")+c(Math.floor(Math.abs(f)/60)*100+Math.abs(f)%60,4),S:["th","st","nd","rd"][n%10>3?0:(n%100-n%10!=10)*n%10]};return w.replace(a,function(m){return m in h?h[m]:m.slice(1,m.length-1)})}}();RSUI.util.dateFormat.masks={"default":"ddd mmm dd yyyy HH:MM:ss",shortDate:"m/d/yy",mediumDate:"mmm d, yyyy",longDate:"mmmm d, yyyy",fullDate:"dddd, mmmm d, yyyy",shortTime:"h:MM TT",mediumTime:"h:MM:ss TT",longTime:"h:MM:ss TT Z",isoDate:"yyyy-mm-dd",isoTime:"HH:MM:ss",isoDateTime:"yyyy-mm-dd'T'HH:MM:ss",isoUtcDateTime:"UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"};RSUI.util.dateFormat.i18n={dayNames:["Sun","Mon","Tue","Wed","Thu","Fri","Sat","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],monthNames:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","January","February","March","April","May","June","July","August","September","October","November","December"]};Date.prototype.format_rs=function(a,b){return RSUI.util.dateFormat(this,a,b)};

// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License
RSUI.util.parseUri = function (str) {
	
	var	o   = RSUI.util.parseUri.options,
		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
		uri = {},
		i   = 14;

	while (i--) uri[o.key[i]] = m[i] || "";

	uri[o.q.name] = {};
	uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
		if ($1) uri[o.q.name][$1] = $2;
	});

	return uri;
};

RSUI.util.parseUri.options = {
	strictMode: false,
	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
	q:   {
		name:   "queryKey",
		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
	},
	parser: {
		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
	}
};
