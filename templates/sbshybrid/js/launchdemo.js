//append facebook api script
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));


//Assign this to a URL on demo load, redirect after demo is closed.
var redirect;

/*========================== Open, close lightbox =====================*/
$(document).ready(function(){
  /*Open on click*/
  $(".open-demo-btn").click(function(){
    //preselct lang in button class name
    for (var c in this.classList) {
      if(typeof this.classList[c]==="string" && this.classList[c].match(/^\w\w\w$/)) {
        $("a[rel="+this.classList[c]+"]").click(); 
      }
    }
    // if no language found, go back to lang list
    if ($(".demo-drop li a.selected").length==0) {
      $("#demo-change-lang").click();
      $(".demo-lang-wrap").hide();
    }
    //create new yellow transition
    var tb = new TransitionBox(this,$(".demo-wrap")[0]);
    $(".demo-lightbox-mask").addClass("active");
    $(document).trigger('demo_events_version', { 
      version: 1,
      description: '',
      standard: 'http://tabbit.org/%E2%98%8D%E2%98%B9W'
    });
  });
  /*Close on click outside */
  $(".demo-lightbox-mask").click(function(){closeLightbox()});

  /* Close on ESC */
  $(document).keyup(function(e){
    if (e.keyCode == 27) {
      closeLightbox();
    }
  });
  /*Close on 'close' click*/
  $("span#close-lb").click(function(){closeLightbox()})
  $("#demo-box .close-btn").click(function(){closeLightbox()});
  closeLightbox();
});
function closeLightbox() {
  $(".demo-lightbox-transition").css("background","#ecc200");
  $(".demo-wrap").removeClass("play");
  $(".demo-wrap").removeClass("active");
  $(".demo-lightbox-mask").removeClass("active");
  $(".arrow").removeClass("active");
  $("html, body").removeClass("stop-scrolling");
  $("#demo-box iframe").attr("src","");
  if (typeof redirect != "undefined") {
    setTimeout(function() {window.location.href = redirect;},0);
  }
}

function TransitionBox (from, to) { // yellow transition effect
  var self = this;
  this.from = from;
  this.to = to;
  this.tb = document.createElement("div");
  this.tb.className = "demo-lightbox-transitions";
  this.tb.style.width = from.offsetWidth+"px";
  this.tb.style.height = from.offsetHeight+"px";
  this.tb.style.top = from.className.match("demo-wrap")?window.scrollY+from.offsetTop-from.offsetHeight/2:from.offsetTop+"px";
  this.tb.style.left = from.className.match("demo-wrap")?window.scrollX+from.offsetLeft-from.offsetWidth/2:from.offsetLeft+"px";
  var par = document.getElementsByClassName("demo-lightbox-transition")[0];
  par.parentNode.insertBefore(this.tb, par.nextSibling);
  $(this.tb).animate({
    "top": $(to).offset().top + "px",
    "left": $(to).offset().left + "px",
    "width": $(to).outerWidth() + "px",
    "height": $(to).outerHeight() + "px"
  }, 300, function() {
    if ($(".demo-lightbox-mask").hasClass("active")) {$(self.to).addClass("active");}
    $(this).fadeOut(300,function(){this.remove;});
  });
}

/*========================= Open, close dropdowns =====================*/
$(document).ready(function(){
  $(".demo-wrap").click(function (e) {
    var dd = $(e.target).find(".arrow").addBack('.arrow');
    if($(".dropdown-box").has($(e.target)).length>0) {
      e.stopPropagation(); return false; /* do nothing on click inside */
    }
    if($(e.target).hasClass("demo-drop") || $(e.target).hasClass("arrow")){
      if (dd.hasClass("active")) { /* toggle on dd click */
        dd.removeClass("active");
      } else {
        $(".arrow").removeClass("active");dd.addClass("active");
      }
      e.stopPropagation(); fade(); return false;
    }
      $(".arrow").removeClass("active"); /* and close on click outside */
      fade();
  });
  function fade() {
    var all = $(".arrow").parents(".lightbox-product-inner");
    var active = $(".arrow.active").parents(".lightbox-product-inner");
      if(active.length>0) {
        all.css("opacity","0.5");
      active.css("opacity","1");
    } else {
      all.css("opacity","1");
    }
  }
});

/*===================== Select Language from dropdown =================*/
$(document).ready(function(){
  $(".demo-drop ul li a").click(function (e) {
    $(document).trigger('demo_language', {language: $(this).text(), lang_code: $(this).attr("rel").toUpperCase()});
    $(".lightbox-product-inner").find(".email-input").attr("required",false);
    $(".demo-drop ul li a").removeClass("selected");
    $(this).addClass("selected");
    $(".selected-lang").html($(this).html());
    $(".arrow").removeClass("active");
    $(".demo-lang-wrap").show();
    $(".demo-lang-wrap").removeClass("lang-select");
    $(".signup-options").slideDown();
    $(".start-demo-btn").slideDown();
    $(".demo-drop").slideUp();
    if (RSUI.util.getCookie("curEmailIdsc")) {
      $(this).parents(".lightbox-product-inner").find(".email-input").val(RSUI.util.getCookie("curEmailIdsc"));
    }
    $(this).parents(".lightbox-product-inner").find(".email-input").attr("required",true).focus();
  });
});
/* ======================== Go back to lang list =========================*/
$(document).ready(function(){
  $("#demo-change-lang").click(function () {
    if ($(".demo-lang-wrap").hasClass("lang-select")) {
      $(".demo-drop li a.selected").click();
    } else {
      $(".demo-lang-wrap").addClass("lang-select");
      $(".signup-options").slideUp();
      $(".start-demo-btn").slideUp();
      $(".demo-drop").slideDown();
    }
  });
});
/* ========================= Facebook FB login ===========================*/
$(document).ready(function doFB(xn){
  if(typeof FB == 'undefined'){
     var xn = xn || 100;
     return xn > 10000000 ? false : setTimeout(doFB, xn, 10 * xn);
  }
  FB.init({
    appId: "1091422140883673",
    status: true,
    cookie: true,
    xfbml: true,
    oauth: true,
    channelUrl: window.location.protocol + '//' + window.location.hostname + '/facebook/channel'
  });
  $("#facebook-login").click(function(){
    FB.login(function(response) {
          if (response.authResponse) {
              FB.api('/me', function(me) {
                  //fb login success
                  var selectedLang = $(".demo-drop .selected");
                  var demoURL = selectedLang.attr("data-url");
                  var demoLangCode = selectedLang.attr("rel");
                  jQuery.when(submitEmailToEC(me.email, demoLangCode,"mobile demo")).then(openDemo(demoURL));
              });
          } else {
              //facebook failed
              console.log("fb fail");
          }
      }, {
          scope: 'email'
    });
  });
});
/* ============================ Validate Form ============================*/
$(document).ready(function(){
  $(".start-demo-btn").click(function (e) {
    function validateEmail(email) {
        var re = /\S+@\S+\.\S+/; 
        re.test(email) ? verify_real_email(email,validateSuccess,validateFail) : validateFail();
    }
    function validateSuccess() {
        if (!RSUI.util.getCookie("curEmailIdsc")) { 
            jQuery.when(submitEmailToEC(email.val(), lang,"mobile demo")).then(openDemo(demoURL)); 
        } else {
            openDemo(demoURL);
        } 
        return false;
    }
    function validateFail() {
        email.css("border","2px solid red").val("").prop("placeholder","PLEASE ENTER A VALID EMAIL").focus();
    }
    var selLang = $(this).parents(".lightbox-product-inner").find("li a.selected")
    var lang = selLang.attr("rel");
    var demoURL = selLang.attr("data-url")
    if (typeof lang == "undefined") {return false;}
    var email = $(this).parent().find(".email-input");
    if (email.attr("required")) {
        validateEmail(email.val())
    }
  });
});

/* ======================== Brite Verify Validation ========================*/
function get_token(){
    window.briteverifyToken = false;

    var form_token = '4047df1c-5078-4e96-b335-f61aa2b92891'; // public key
    var url = 'https://forms-api-v1.briteverify.com/api/submissions/view.json?callback=getBriteverifyToken&form_token='+form_token+'&_='+Date.now();

    // append script to body to get cross-domain jsonp data
    var script = document.createElement('script');
    script.src = url;
    document.getElementsByTagName('head')[0].appendChild(script);
    
    window.getBriteverifyToken = function(data){
        // console.log(data);
        // console.log('token: '+data.token);
        
        window.briteverifyToken = data.token;

        delete window.getBriteverifyToken;
        document.getElementsByTagName('head')[0].removeChild(script);
    }
}
// if the demo hasn't already been taken, get an email verification token
if(!RSUI.util.getCookie('demotaken')) {
    get_token();
}

function verify_real_email(email,success,failure){
    if(!success){success = function(){};}
    if(!failure){failure = function(){};}
    if(window.briteverifyToken){
        
        var token = window.briteverifyToken;
        var url = 'https://forms-api-v1.briteverify.com/api/submissions/verify.json?callback=briteverify&form_token=4047df1c-5078-4e96-b335-f61aa2b92891&token='+token+'&email='+email+'&_='+Date.now();
        var script = document.createElement('script');
        script.src = url;
        document.getElementsByTagName('head')[0].appendChild(script);
        
        // if the script url doesn't work (maybe they changed the api)
        script.onerror = function(e){
            // ignore verification and move on
            success();
        };

        window.briteverify = function(data){
            try{
                // if email is valid
                if(data.status!=='invalid'){
                    success();
                }
                // if email is invalid
                else{
                    failure();
                }

                document.getElementsByTagName('head')[0].removeChild(script);
            }
            catch(e){
                // if error, ignore verification and move on
                success();
            }
            delete window.briteverify;
        }
    }
    else{
        // if no token, ignore verification and move on
        success();
    }
}

/* ============================ Open Demo ============================*/
function openDemo(demoURL) {
  $(document).trigger('demo_start', {
    version: 'd4',
    version_description: 'desktop_demo',
    start_time: new Date()
  });
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    setTimeout(function() {
      window.location.href = demoURL;
    }, 0);
  }
  $("#demo-box iframe").remove();
  var iframe = document.createElement("iframe");
  iframe.src = demoURL;
  iframe.width = "100%";
  iframe.height = "100%";
  iframe.scrolling = "no";
  iframe.frameborder = "0";
  $(".demo-wrap").addClass("play");
  $("#demo-box").append(iframe);
  if (~demoURL.indexOf("type=k12")) redirect = "http://www.rosettastone.com/k12";
  if (~demoURL.indexOf("type=busn")) redirect = "http://www.rosettastone.com/business";
}

/* ========================== Send Email To EC ========================*/
function submitEmailToEC(email, langCode, cis_name) {
      var defer = jQuery.Deferred()

      $(document).trigger('demo_email', {
        email: email,
        is_offer: false
      });

      var baseURL = (window.location.href.search(/(\.stg\.)|(local)/i)>-1 ?
            'http://myaccount.stg' : 
            'https://myaccount') 
            + '.rosettastone.com/forms?callback=?&data=';
 
      var data = {
          email : email,
          demo_lang : langCode,
          cis_name : cis_name,
          website: 'US_WEBSITE',
          form_type : 'demo',
          demo_type : 'PE',
          form_url : 'homepage',
          newsletter_type : "DTC_mobile-demo"
      }

      var request = jQuery.ajax({
          contentType: "application/json; charset=utf-8",
          url: baseURL + encodeURIComponent(JSON.stringify(data)),
          type: "POST",
          dataType: "jsonp"
      });

      request.done(function(msg) {
          if (msg['return_code'] == true) {
              // Doty's demo pixel
              $('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1041440066/?label=WzGCCNzOy1gQwrrM8AM&amp;guid=ON&amp;script=0"/>')
              // record the email in a cookie that expires in 30 days
              RSUI.util.setCookie("curEmailIdsc",email)
              //document.cookie = 'curEmailIdsc='+email+'; max-age='+60*60*24*30+'; path=/;';
              defer.resolve('success')
          }

      })
      return defer.promise()
  }
/* ======================= Send Email To Eloqua ======================*/
function submitEmailToEloqua(email, elqFormName) {
  function GetElqCustomerGUID(){
    var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8); return v.toString(16);});
    return guid;
  }
  var elqURL = "http://s1294.t.eloqua.com/e/f2?C_EmailAddress="+email+"&redirect_url=&elqFormName="+elqFormName+"&elqSiteID=1294&elqCustomerGUID="+GetElqCustomerGUID()+"&elqCookieWrite=0";
  ifrm = document.createElement("IFRAME");
  ifrm.setAttribute("src", elqURL);
  ifrm.style.width = "0px";
  ifrm.style.height = "0px";
  ifrm.style.display = "none";
  $("body").append(ifrm);
    // Doty's demo pixel
  $('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1041440066/?label=WzGCCNzOy1gQwrrM8AM&amp;guid=ON&amp;script=0"/>')
  $(document).trigger('demo_email', {
    email: email,
    is_offer: false
  });
  // record the email in a cookie that expires in 30 days
  document.cookie = 'curEmailIdsc='+email+'; max-age='+60*60*24*30+'; path=/;';

}
/* ===================== Remove v3 demo for tablet ===================*/
$(document).ready(function(){
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $(".drop-col:not(:first-child)").remove();
    $(".drop-col ul").css("width","16.5em");
  }
});
/* ===================== extend addBack for old jQuery ===================*/
$(document).ready(function(){
  jQuery.fn.addBack = function (selector) {
      return this.add(selector == null ? this.prevObject : this.prevObject.filter(selector));
  }
});

