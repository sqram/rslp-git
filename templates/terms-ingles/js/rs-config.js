/*
 * RS object variables. Can be overwritten
 * in the HTML file after this file is included. Additionally,
 * since this file is included in every landing page, and lazyload
 * is pretty much needed everywhere, we define it here in this file.
 * Desktop pages should use this when codebase is re-done.
 *
 * 1. This file must be included after utils.js
 */

