/*************************************************************************
 *
 * Util functions to be used in RS Javascript.
 * Since it provides a handful of functions and embeded
 * jquery plugins, it should be included at the top of the
 * page since other scripts will likely use functions from here
 *
 ************************************************************************/


// Holds custom defined methods (ie, not embbeded plugins)
RSUtils = new Object();



// Same one from main site
RSUtils.validateEmail = function(email)
{
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}


 // Returns date in format yyyy-mm-dd
RSUtils.getCurrentDate = function()
{
	var date = new Date();
	var month = date.getMonth() + 1
	var day = date.getDate()
	if (day < 10) day = '0' + day
	if (month < 10) month = '0' + month
	return date.getFullYear() + '-' + month + '-' + day;
}

/*
 * Returns a trial expiration date. pass the number
 * of days. So if days is 3, it's a 3 day trial
 * (expires 3 days from the current date). Default
 * value is 3 since most trials are 3 days
 */
RSUtils.getExpirationDate = function(days)
{
	var days = days || 3
	var date = new Date()
	var expDate = new Date(date)
	expDate.setDate(expDate.getDate() + days)
	var nd = new Date(expDate)
	var day = nd.getDate()
	var month = nd.getMonth() + 1
	if (day < 10) day = '0' + day
	if (month < 10) month = '0' + month

	return nd.getFullYear() + '-' + month + '-' + day;
}



/*
 * Function built to search products-info.json.
 * Will take a a whole json object and value (ie, sku, langcode),
 * and return that value's json object. For instance, value is
 * "heb", it will return the following: (json should always be producs-info.json)
 *	{
 *		highestLevel: 3
 *		langCode: "heb"
 *		language: "Hebrew"
 *		levelsDescription: "Level 1, 2 & 3"
 *		ratings: 4.8
 *		reviewCount: 60
 *		sku_cd: 27809
 *		sku_download: 95605
 *		sku_tosub: 34166
 *	}
*/
RSUtils.searchJsonByValue = function(json, value) {
	var ret;
	$.each(json, function(k,v) {
		if (v.langCode == value) {
			ret = v
			// break out of the $.each loop, not function
			return false
		}
	})
	return ret
}



RSUtils.getLocalTimeIncrement = function(){
    var t;
    try {
        RSUtils.dateFormat.masks.timeIncrement="yyyy-mm-dd HH:";
        var now = new Date();
        var t1 = now.format_rs("timeIncrement");
        var t2 = Math.floor(now.getMinutes()/10).toString();
        t = t1 + t2 + "0";
    }
    catch(err){t = err;}
    return t;
}



/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

RSUtils.dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = RSUtils.dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
RSUtils.dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
RSUtils.dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};
/*
RSUI.util.mboxCreate = function(id,params) {
	var funct = "mboxCreate('"+id+"'";
	var argLength = params.length;

	for(var i = 0; i < argLength; i++){
		funct += ", "+params[i];
	}

	funct += ")";

	eval(funct);
}
*/

// For convenience...
Date.prototype.format_rs = function (mask, utc) {
	return RSUtils.dateFormat(this, mask, utc);
};








// Submits to EC. dataObj is what to send to EC. ({email:'foo@bar.com, etc})
RSUtils.submitEmailToEC = function(data)
{


	try {
		s.eVar7 = data.demo_lang
	} catch(e) {

	}

	var url = 'https://myaccount.rosettastone.com/forms?callback=?&data='

	/*
	$.post(url, dataObj, function(data)
	{
		if (data != "") {
			data = eval("("+data+")");

			if (data[0].cisFlag == "true") {
				s.events="event19";
				s.t();
				delete s.events;
				window.location = 'http://www.rosettastone.com/lp/html-demo/?e=0'
			}
		} else {
				// unknown error. show
			s.t()
		}

	});
	*/


	json = $.toJSON(data);
	var request = $.ajax({
		contentType: "application/json; charset=utf-8",
		url:  url + encodeURIComponent(json),
		type: "POST",
		dataType: "jsonp"
	});

	request.done(function(msg) {
		if (msg['return_code'] == true) {
			s.events="event19";
			s.t();
			delete s.events;

			//window.location = 'http://www.rosettastone.com/lp/html-demo/?e=0'


		}
	})


	// Commenting this out, thursday 29th - console says s is not defined
	s.eVar7 = data.demo_lang
	return request



}







// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License
parseUri = function (str) {
	var	o   = parseUri.options,
		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
		uri = {},
		i   = 14;

	while (i--) uri[o.key[i]] = m[i] || "";

	uri[o.q.name] = {};
	uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
		if ($1) uri[o.q.name][$1] = $2;
	});

	return uri;
};

parseUri.options = {
	strictMode: false,
	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
	q:   {
		name:   "queryKey",
		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
	},
	parser: {
		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
	}
};



/* https://github.com/rgrove/lazyload/ */
LazyLoad=function(k){function p(b,a){var g=k.createElement(b),c;for(c in a)a.hasOwnProperty(c)&&g.setAttribute(c,a[c]);return g}function l(b){var a=m[b],c,f;if(a)c=a.callback,f=a.urls,f.shift(),h=0,f.length||(c&&c.call(a.context,a.obj),m[b]=null,n[b].length&&j(b))}function w(){var b=navigator.userAgent;c={async:k.createElement("script").async===!0};(c.webkit=/AppleWebKit\//.test(b))||(c.ie=/MSIE/.test(b))||(c.opera=/Opera/.test(b))||(c.gecko=/Gecko\//.test(b))||(c.unknown=!0)}function j(b,a,g,f,h){var j=
function(){l(b)},o=b==="css",q=[],d,i,e,r;c||w();if(a)if(a=typeof a==="string"?[a]:a.concat(),o||c.async||c.gecko||c.opera)n[b].push({urls:a,callback:g,obj:f,context:h});else{d=0;for(i=a.length;d<i;++d)n[b].push({urls:[a[d]],callback:d===i-1?g:null,obj:f,context:h})}if(!m[b]&&(r=m[b]=n[b].shift())){s||(s=k.head||k.getElementsByTagName("head")[0]);a=r.urls;d=0;for(i=a.length;d<i;++d)g=a[d],o?e=c.gecko?p("style"):p("link",{href:g,rel:"stylesheet"}):(e=p("script",{src:g}),e.async=!1),e.className="lazyload",
e.setAttribute("charset","utf-8"),c.ie&&!o?e.onreadystatechange=function(){if(/loaded|complete/.test(e.readyState))e.onreadystatechange=null,j()}:o&&(c.gecko||c.webkit)?c.webkit?(r.urls[d]=e.href,t()):(e.innerHTML='@import "'+g+'";',u(e)):e.onload=e.onerror=j,q.push(e);d=0;for(i=q.length;d<i;++d)s.appendChild(q[d])}}function u(b){var a;try{a=!!b.sheet.cssRules}catch(c){h+=1;h<200?setTimeout(function(){u(b)},50):a&&l("css");return}l("css")}function t(){var b=m.css,a;if(b){for(a=v.length;--a>=0;)if(v[a].href===
b.urls[0]){l("css");break}h+=1;b&&(h<200?setTimeout(t,50):l("css"))}}var c,s,m={},h=0,n={css:[],js:[]},v=k.styleSheets;return{css:function(b,a,c,f){j("css",b,a,c,f)},js:function(b,a,c,f){j("js",b,a,c,f)}}}(this.document);









RSUI = {
	utils: RSUtils,
	util : RSUtils
}


RSUI.util.setCookie = function(name, value, b_custom, expires_ms, path, domain, secure){
    //name        = cookie name to set.
    //value         = cookie value to set.
    //b_custom      = optional boolean value which will allow custom settings
    //expires_ms    = optional milliseconds till cookie expires (#days * 1000 * 60 * 60 * 24).
    //path          = optional path.
    //domain        = optional domain.
    //secure        = optional secure setting.
    if(!b_custom){
        expires_ms = 30 * 1000 * 60 * 60 * 24; //30 days
        path = "/";
        domain = this.cookieDomain();
    }
	var today = new Date();
    var exdate = new Date(today.getTime() + (expires_ms));
	exdate = exdate.toUTCString();
	var c_verb = 'setting'; //for debugging string below

	if(expires_ms == -1) {
		expires_ms = 1;
		exdate = new Date(today.getTime() - (30 * 1000 * 60 * 60 * 24));
		exdate = exdate.toUTCString();
		c_verb = 'removing'; //for debugging string below
	}

    document.cookie = name + "=" + escape(value)+
        ((expires_ms) ? ";expires=" + exdate : "") +
        ((path) ? ";path="+path : "") +
        ((domain) ? ";domain="+domain : "") +
        ((secure) ? ";secure" : "");
};
RSUI.util.cookieDomain = function(){
	var hp = parseUri(document.location).host.split('.');
	hp.reverse();
	domain = "." + hp[1] + "." + hp[0];
	if (hp.length >= 3 && hp[2] != 'www' && hp[2] == 'stg')
		domain = "." + hp[2] + "." + hp[1] + "." + hp[0];

	return domain;
}

RSUI.util.getCookie = function(check_name){
    var a_all_cookies = document.cookie.split(";");
    var a_temp_cookie = "";
    var cookie_name = "";
    var cookie_value = "";
    var b_cookie_found = false;

    for(i = 0; i < a_all_cookies.length; i++){
        a_temp_cookie = a_all_cookies[i].split("=");
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g,"");
        if(cookie_name == check_name){
            b_cookie_found = true;
            if (a_temp_cookie.length > 1) {
				cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g,""));
			}
            return cookie_value;
            break;
        }
        a_temp_cookie = null;
        cookie_name = "";
    }
    if(!b_cookie_found) {
		return null;
	}
};

RSUI.util.eraseCookie = function(name) {
    this.setCookie(name,"",true,-1,'/',this.cookieDomain());
}




/**
 * jQuery.browser.mobile (http://detectmobilebrowser.com/)
 *
 * jQuery.browser.mobile will be true if the browser is a mobile device
 *
 **/
//(function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);









