/*
 * Because Dynamic Tag Manager uses and RS and RSUI object, we unfortunally have to create one. gg dependency hell
 * So everything below this comment is so that tag manager works. yes, the code in tag manages does expect
 * parseUri to be on the global scope.
 */

var rs = {}
var RSUI = {
    util: {}
}






// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License
parseUri = function (str) {
    var o   = parseUri.options,
        m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i   = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
};

parseUri.options = {
    strictMode: false,
    key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
    q:   {
        name:   "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};




RSUI.util.setCookie = function(name, value, b_custom, expires_ms, path, domain, secure){
    //name        = cookie name to set.
    //value         = cookie value to set.
    //b_custom      = optional boolean value which will allow custom settings
    //expires_ms    = optional milliseconds till cookie expires (#days * 1000 * 60 * 60 * 24).
    //path          = optional path.
    //domain        = optional domain.
    //secure        = optional secure setting.
    if(!b_custom){
        expires_ms = 30 * 1000 * 60 * 60 * 24; //30 days
        path = "/";
        domain = this.cookieDomain();
    }
    var today = new Date();
    var exdate = new Date(today.getTime() + (expires_ms));
    exdate = exdate.toUTCString();
    var c_verb = 'setting'; //for debugging string below

    if(expires_ms == -1) {
        expires_ms = 1;
        exdate = new Date(today.getTime() - (30 * 1000 * 60 * 60 * 24));
        exdate = exdate.toUTCString();
        c_verb = 'removing'; //for debugging string below
    }

    document.cookie = name + "=" + escape(value)+
        ((expires_ms) ? ";expires=" + exdate : "") +
        ((path) ? ";path="+path : "") +
        ((domain) ? ";domain="+domain : "") +
        ((secure) ? ";secure" : "");
};
RSUI.util.cookieDomain = function(){
    var hp = parseUri(document.location).host.split('.');
    hp.reverse();
    domain = "." + hp[1] + "." + hp[0];
    if (hp.length >= 3 && hp[2] != 'www' && hp[2] == 'stg')
        domain = "." + hp[2] + "." + hp[1] + "." + hp[0];

    return domain;
}

RSUI.util.getCookie = function(check_name){
    var a_all_cookies = document.cookie.split(";");
    var a_temp_cookie = "";
    var cookie_name = "";
    var cookie_value = "";
    var b_cookie_found = false;

    for(i = 0; i < a_all_cookies.length; i++){
        a_temp_cookie = a_all_cookies[i].split("=");
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g,"");
        if(cookie_name == check_name){
            b_cookie_found = true;
            if (a_temp_cookie.length > 1) {
                cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g,""));
            }
            return cookie_value;
            break;
        }
        a_temp_cookie = null;
        cookie_name = "";
    }
    if(!b_cookie_found) {
        return null;
    }
};

RSUI.util.eraseCookie = function(name) {
    this.setCookie(name,"",true,-1,'/',this.cookieDomain());
}



/* Former jb_utils.js
 * helper functions for analytics client-side code
 * jbeach, created: 2010-04-06, updated: 2010-09-02
 */

/* get local time increment */
//This will output a time string in client browser's local time, only in 10 minute increments.
RSUI.util.getLocalTimeIncrement = function(){
    var t;
    try {
        RSUI.util.dateFormat.masks.timeIncrement="yyyy-mm-dd HH:";
        var now = new Date();
        var t1 = now.format_rs("timeIncrement");
        var t2 = Math.floor(now.getMinutes()/10).toString();
        t = t1 + t2 + "0";
    }
    catch(err){t = err;}
    return t;
}


RSUI.util.parseUri = parseUri





/*
 * Needed. Something in DTM uses this function. Don't
 * know why it's not there itself.
*/
String.prototype.getHostname = function(){
    var a = new RegExp("^((?:f|ht)tp(?:s)?://[^/]+)","im");
    return this.match(a) && this.match(a)[1].toString();
};
function ltrim(b,a){
    a=a||"\\s";if(typeof b!="undefined"){return b.replace(new RegExp("^["+a+"]+","g"),"")}else{return""}
}
function rtrim(b,a){
    a=a||"\\s";if(typeof b!="undefined"){return b.replace(new RegExp("["+a+"]+$","g"),"")}else{return""}
}



