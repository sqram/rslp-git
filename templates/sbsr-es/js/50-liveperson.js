var lpMTagConfig = lpMTagConfig || {};
lpMTagConfig.vars = lpMTagConfig.vars || [];
lpMTagConfig.dynButton = lpMTagConfig.dynButton || [];
lpMTagConfig.lpProtocol = document.location.toString().indexOf("https:") == 0 ? "https" : "http";
lpMTagConfig.pageStartTime = (new Date).getTime();
if (!lpMTagConfig.pluginsLoaded) lpMTagConfig.pluginsLoaded = !1;

lpMTagConfig.loadTag = function() {
    for (var a = document.cookie.split(";"), b = {}, c = 0; c < a.length; c++) {
        var d = a[c].substring(0, a[c].indexOf("="));
        b[d.replace(/^\s+|\s+$/g, "")] = a[c].substring(a[c].indexOf("=") + 1)
    }
    for (var a = b.HumanClickRedirectOrgSite, b = b.HumanClickRedirectDestSite, c = ["lpTagSrv", "lpServer", "lpNumber", "deploymentID"], d = !0, e = 0; e < c.length; e++) lpMTagConfig[c[e]] || (d = !1, typeof console != "undefined" && console.log && console.log("LivePerson : lpMTagConfig." + c[e] + " is required and has not been defined before lpMTagConfig.loadTag()."));
    if (!lpMTagConfig.pluginsLoaded && d) lpMTagConfig.pageLoadTime = (new Date).getTime() - lpMTagConfig.pageStartTime, a = "?site=" + (a == lpMTagConfig.lpNumber ? b : lpMTagConfig.lpNumber) + "&d_id=" + lpMTagConfig.deploymentID + "&default=simpleDeploy", lpAddMonitorTag(lpMTagConfig.deploymentConfigPath != null ? lpMTagConfig.lpProtocol + "://" + lpMTagConfig.deploymentConfigPath + a : lpMTagConfig.lpProtocol + "://" + lpMTagConfig.lpTagSrv + "/visitor/addons/deploy2.asp" + a), lpMTagConfig.pluginsLoaded = !0
};

function lpAddMonitorTag(a) {
    if (!lpMTagConfig.lpTagLoaded) {
        if (typeof a == "undefined" || typeof a == "object") a = lpMTagConfig.lpMTagSrc ? lpMTagConfig.lpMTagSrc : lpMTagConfig.lpTagSrv ? lpMTagConfig.lpProtocol + "://" + lpMTagConfig.lpTagSrv + "/hcp/html/mTag.js" : "/hcp/html/mTag.js";
        a.indexOf("http") != 0 ? a = lpMTagConfig.lpProtocol + "://" + lpMTagConfig.lpServer + a + "?site=" + lpMTagConfig.lpNumber : a.indexOf("site=") < 0 && (a += a.indexOf("?") < 0 ? "?" : "&", a = a + "site=" + lpMTagConfig.lpNumber);
        var b = document.createElement("script");
        b.setAttribute("type",
            "text/javascript");
        b.setAttribute("charset", "iso-8859-1");
        b.setAttribute("src", a);
        document.getElementsByTagName("head").item(0).appendChild(b)
    }
}
window.attachEvent ? window.attachEvent("onload", function() {
    lpMTagConfig.disableOnLoad || lpMTagConfig.loadTag()
}) : window.addEventListener("load", function() {
    lpMTagConfig.disableOnLoad || lpMTagConfig.loadTag()
}, !1);

function lpSendData(a, b, c) {
    if (arguments.length > 0) lpMTagConfig.vars = lpMTagConfig.vars || [], lpMTagConfig.vars.push([a, b, c]);
    if (typeof lpMTag != "undefined" && typeof lpMTagConfig.pluginCode != "undefined" && typeof lpMTagConfig.pluginCode.simpleDeploy != "undefined") {
        var d = lpMTagConfig.pluginCode.simpleDeploy.processVars();
        lpMTag.lpSendData(d, !0)
    }
}

function lpAddVars(a, b, c) {
    lpMTagConfig.vars = lpMTagConfig.vars || [];
    lpMTagConfig.vars.push([a, b, c])
};
<!-- Do not ALTER the contents - ENDS -->

var demoEmailSignedlp = 0;

function lpDemoEmailSignupSuccess() {
    var longlang = $('#selectedLang').val();
    if (checkLangCode(longlang)) {
        lpMTagConfig.vars.push(["page", "DemoSignUpSuccess", "Y"]);
        lpMTagConfig.vars.push(["page", "DemoEmailInputExit", "N"]);
        demoEmailSignedlp = 1;
        lpSendData();
    } else {
        lpMTagConfig.vars.push(["session", "DemoSignUpSuccess", "Y"]);
        lpMTagConfig.vars.push(["session", "DemoEmailInputExit", "N"]);
    }
}

function lpDemoEmailSignupQuit() {
    if (demoEmailSignedlp == 0) {
        lpMTagConfig.vars.push(["page", "DemoEmailInputExit", "Y"]);
        lpSendData();
    } else {
        demoEmailSignedlp = 0;
    }
}

function lpDemoEmailSignupFailed() {
    /*lpMTagConfig.vars.push(["page","DemoEmailInputExit","Y"]);
	lpSendData();*/
}

var lp_lob = "Personal";
switch (true) {
    case (document.location.href.indexOf("homeschool") != -1):
        lp_lob = "Home School";
        break;
    case (document.location.href.indexOf("business") != -1):
        lp_lob = "Business";
        break;
    case (document.location.href.indexOf("publicsector") != -1):
        lp_lob = "Public Sector";
        break;
    case (document.location.href.indexOf("education") != -1):
        lp_lob = "Education";
        break;
    case (document.location.href.indexOf("homeschool") != -1):
        lp_lob = "Homeschool";
        break;
}

lpMTagConfig.lpServer = "sales.liveperson.net";
lpMTagConfig.lpTagSrv = lpMTagConfig.lpServer;

windowurl = (window.location.href).split('.');
if (windowurl[1] == 'stg' || windowurl[1] == 'landingpages') {
    lpMTagConfig.lpNumber = "6448506";
} else {
    lpMTagConfig.lpNumber = "51771877";
}

lpMTagConfig.deploymentID = "personal";

//Unit and Language variables, important, include on ALL tagged pages
lpMTagConfig.vars.push(["page", "unit", "personal"]);
lpMTagConfig.vars.push(["session", "language", "english"]);

//Variables to pass on page load, if applicable
lpMTagConfig.vars.push(['page', 'Section', 'Product Selection']);
lpMTagConfig.vars.push(['page', 'LineOfBusiness', lp_lob]);

if (rs.pagename.indexOf('learn') != -1) {
    lpMTagConfig.vars.push(['page', 'PageType', 'Catalog Page']);
} else if (rs.pagename.indexOf('qbc') != -1 || rs.pagename.indexOf('qbh') != -1 || rs.pagename.indexOf('demoa') != -1) {
    lpMTagConfig.vars.push(['page', 'PageType', 'Landing Page']);
} else {
    lpMTagConfig.vars.push(['page', 'PageType', '']);
}




$(document).ready(function() {

    lpMTagConfig.vars.push(['page', 'PageName', rs.pagename]);


    try {
        if (typeof testGroupIndicator != "undefined") {
            lpMTagConfig.vars.push(["session", "testGroup", "Y"]);
        }
    } catch (err) {}

    lpSendData();

    if ($("#level-prod-details").length > 0 && $("#level-prod-details").overlay().length == undefined) {
        $("#level-prod-details").overlay().onBeforeLoad(function() {
            lpMTagConfig.vars.push(["page", "SWYL", "Y"]);
            lpSendData();
        });
        $("#level-prod-details").overlay().onClose(function() {
            lpMTagConfig.vars.push(["page", "SWYLExit", "Y"]);
            lpSendData();
        });
    }
    if ($("#online-catalog-details").length > 0 && $("#online-catalog-details").overlay().length == undefined) {
        $("#online-catalog-details").overlay().onBeforeLoad(function() {
            lpMTagConfig.vars.push(["page", "SWYL", "Y"]);
            lpSendData();
        });
        $("#online-catalog-details").overlay().onClose(function() {
            lpMTagConfig.vars.push(["page", "SWYLExit", "Y"]);
            lpSendData();
        });
    }
    if ($("#compare-online-levels").length > 0 && $("#compare-online-levels").overlay().length == undefined) {
        $("#compare-online-levels").overlay().onBeforeLoad(function() {
            lpMTagConfig.vars.push(["page", "CompareProducts", "Y"]);
            lpSendData();
        });
        $("#compare-online-levels").overlay().onClose(function() {
            lpMTagConfig.vars.push(["page", "CompareProductsExit", "Y"]);
            lpSendData();
        });
    }
    if ($("#compare-online-products").length > 0 && $("#compare-online-products").overlay().length == undefined) {
        $("#compare-online-products").overlay().onBeforeLoad(function() {
            lpMTagConfig.vars.push(["page", "CompareProducts", "Y"]);
            lpSendData();
        });
        $("#compare-online-products").overlay().onClose(function() {
            lpMTagConfig.vars.push(["page", "CompareProductsExit", "Y"]);
            lpSendData();
        });
    }

    if (/[homeschool|learn]-(.*)[\/]*$/.exec(document.location.pathname) != null) {
        var lp_productlang = /[homeschool|learn]-(.*)[\/]*$/.exec(document.location.pathname)[1];
        lpMTagConfig.vars.push(["page", "ProductLanguage", lp_productlang]);
    }

    try {
        if ($("#demoform").length > 0 && $("#demoform").overlay().length == undefined) {
            $("#demoform").overlay().onClose(function() {
                lpDemoEmailSignupQuit();
            });
        }
    } catch (err) {}

}); 