var getId;
var prodData = '';
var promoData = '';
var promomap = '';
var wylLink = '';
var wylSale = '';
var wyllevel ='';
var wylPrice = '';

var learn = 'Learn ';
var downs = RSI({cat:'eng',media:'download'});
var boxes = RSI({cat:'eng',media:'box'});
var tsubs = RSI({cat:'eng',media:'subscription'});

$.each(boxes, function(k,v) {
	var input = $('#box_'+v.lvl.toLowerCase())
	var price = $(input).parents('.lineitem').find('.strikeprice');
	var sale = $(input).parents('.lineitem').find('.finalprice');
	$(input).val(v.sku).attr('data-msrp',v.msrp).attr('data-price',v.price).attr('data-code',v.code);
	$(price).html('$'+v.msrp)
	$(sale).html('$'+v.price)
	if (parseFloat(v.price) >= parseFloat(v.msrp)) {
		$(price).hide();
	} else {
		$(price).show();
	}
});
$.each(downs, function(k,v) {
	var input = $('#download_'+v.lvl.toLowerCase())
	var price = $(input).parents('.lineitem').find('.strikeprice');
	var sale = $(input).parents('.lineitem').find('.finalprice');
	$(input).val(v.sku).attr('data-msrp',v.msrp).attr('data-price',v.price).attr('data-code',v.code);
	$(price).html('$'+v.msrp)
	$(sale).html('$'+v.price)
	if (parseFloat(v.price) >= parseFloat(v.msrp)) {
		$(price).hide();
	} else {
		$(price).show();
	}
});
$.each(tsubs, function(k,v) {
	var input = $('#tosub_'+v.lvl.toLowerCase())
	var price = $(input).parents('.lineitem').find('.strikeprice');
	var sale = $(input).parents('.lineitem').find('.finalprice');
	$(input).val(v.sku).attr('data-msrp',v.msrp).attr('data-price',v.price).attr('data-code',v.code);
	$(price).html('$'+v.msrp)
	$(sale).html('$'+v.price)
});

$('body').on('click','input:radio',function(){
	var newurl = rs.cart_uri+'add/sku/'+$(this).val()+'/category_id/eng/';
	if (typeof($(this).data('code')) != 'undefined' && $(this).data('code') != '')
		newurl = newurl + '?pc='+$(this).data('code');
	$(this).parents('.product-column').find('.shopbutton a').attr('href',newurl);
	var wylobj = $(this).parents('.product-column').find('.learn');
	$(wylobj).attr('wylLink',newurl).attr('wylSale',$(this).data('price')).attr('wylPrice',$(this).data('msrp')).attr('wylLevel',$(this).attr('id').split('_')[1]);
});

$('.pricing p:first-child label input').click();

	$('.coverall').click(function(){
		$('.message').show();
		setTimeout(function() {
			$('.message').fadeOut(2000);
		},3000);
	});

	$('.changebutton a').click(function(e) {
		e.preventDefault();
		$('.languages-wrapper').slideDown('slow');
		$('#yourlang').slideUp('slow');
	});

	$('.learn').click(function(e)  {
		wylSale = $(e.target).attr('wylSale');
		wylPrice = $(e.target).attr('wylPrice');
		wylLink = $(e.target).attr('wylLink');
		wyllevel = $(e.target).attr('wyllevel');
	});

	//what you'll learn overlay

				$('.blue').find('.learn').colorbox({
						opacity : 0.7,

						href: './assets/what-youll-learn-course-download-nuke-esp.html',
						closeButton: true,
						close:'<a class="close"></a>',
						//fixed: false,
						//scrolling:false,
						//overlayClose:true,
						//fadeOut : 300,
						onOpen: function() {
						},
						onLoad: function() {

						},
						onComplete: function() {
							$('.learn-btn').attr('href',wylLink);
							$('.regprice').text('$'+wylPrice);
							$('.price').text('$'+wylSale);
						}
					});
				$('.yellow').find('.learn').colorbox({
						opacity : 0.7,

						href: './assets/what-youll-learn-tosub-nuke-esp.html',
						closeButton: true,
						close:'<a class="close"></a>',
						//fixed: false,
						//scrolling:false,
						//overlayClose:true,
						//fadeOut : 300,
						onOpen: function() {
						},
						onLoad: function() {

						},
						onComplete: function() {
							$('.learn-btn').attr('href',wylLink);
							$('.regprice').text('$'+wylPrice);
							$('.price').text('$'+wylSale);
						}
					})

	//switch tabs
	$('.navigation li a').click(function(e) {
		e.preventDefault();

		targetDiv=$(e.target).attr('rel');
		$(targetDiv).show();
		$(targetDiv).siblings('.overview-inner').hide();
	});

	$('.readmore').click(function(e) {
		e.preventDefault();
		$('.overview-inner').hide();
		$('#reviews').show();
	});

	$('.navi').click(function(e) {
		e.preventDefault();
		tartab=$(e.target).attr('rel')
		$('.overview-inner').hide();
		$(tartab).show();
	});

	$('.readreviews').click(function(e) {
		$('.readmore').trigger('click');
		document.location="#contenttop";
	});







	//compare products overlay
	/*
	$("a.compare[rel]").overlay({
		fixed: false,
		mask : {
			color : '#000',
			loadSpeed : 200,
			opacity : 0.7
		},
		onBeforeLoad: function() {
		// grab wrapper element inside content
			var wrap = this.getOverlay().find(".contentWrap");
			// load the page specified in the trigger
			wrap.load(this.getTrigger().attr("href"));

		}
	});

	*/















	// reviews lightbox & carousel
	$('.nuke-sthumb a[rel]').add('.nuke-lthumb a[rel]').click(function(e) {
		factor=$(e.target).attr('pos')-1;
		position=0-(874*factor);
		$('.items').css('left',position+'px');
		if (position == 0) {
			$('.link-page-nav.left').hide();
			$('.link-page-nav.right').show();
		}
		if (position == -3496) {
			$('.link-page-nav.right').hide();
			$('.link-page-nav.left').show();
		}
		if ((position < 0) && (position > -3496)) {
			$('.link-page-nav.right').show();
			$('.link-page-nav.left').show();
		}
	});

	$('.link-page-nav.right').click(function() {
		position = parseInt($('.items').css('left'));
		if (position > -3496) {
			newpos=position-874;
			newpos=newpos.toString()+'px';
			$('.items').animate({
				left: newpos
			},500);
			if (parseInt(newpos) <= -3496) {
				$('.link-page-nav.right').hide();
			} else {
				$('.link-page.nav.right').show();
			}
			if (parseInt(newpos) < 0) {
				$('.link-page-nav.left').show();
			}
		}
	});

	$('.link-page-nav.left').click(function() {
		position = parseInt($('.items').css('left'));
		if (position < 0) {
			newpos=position+874;
			newpos=newpos.toString()+'px';
			$('.items').animate({
				left: newpos
			},500);
			if (parseInt(newpos) >= 0) {
				$('.link-page-nav.left').hide();
			} else {
				$('.link-page-nav.right').show();
			}
			if (parseInt(newpos) > -3496) {
				$('.link-page.nav.right').show();
			}
		}
	});


	winwidth = parseInt($(window).width());
	if (winwidth > 960) {
		leftpos = (winwidth - 960) / 2;
	} else {
		leftpos = 0;
	}

	leftpos = leftpos + 'px';
	$('.nuke-sthumb a[rel]').add('.nuke-lthumb a[rel]').overlay({
		fixed:false,
		mask: {
			color: '#000',
			loadSpeed: 200,
			opacity: 0.7
			},
		left: leftpos
	});

	$('#reviews-scroller').scrollable();


