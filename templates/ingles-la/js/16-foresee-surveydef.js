FSR.surveydefs = [{
    site: 'rosettastone.de',
    section: 'campaign',
    name: 'browse',
    invite: {
        when: 'onentry'
    },
    pin: 1,
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 75,
        lf: 1
    },
    include: {
        urls: ['/bs/brand11', '/bs/brand-qb', '/gs/generic11', '/landing-pages/lp2da10p', '/landing-pages/lp2gs10p', '/landing-pages/sebg1qb', '/landing-pages/segg1qb', '/lp/da-bts-qb0', '/lp/da-bts-qb1', '/lp/da-china-qb2', '/lp/demo', '/lp/freeshipping', '/lp/ny12', '/lp2gs10p', '/lp2rm10p', '/offer/googelde', '/condor12', '/lh11', '/ww11_eur', '/pm_11', '/pmh_11', '/geok911', '/geoe611', '/lh811', '/ng12', '/zw12', '/me911', '/airb12', '/ngw911', '/tui11', '/bt11', '/ww11', '/pm11', '/pmp11', '/pmb11', '/geow511', '/pmh11', '/cta811', '/rf811', '/ng11', '/airb111', '/globe04', '/st11', '/lh911', '/ng911_eur', '/aj10', '/zw10', '/ml711', '/gw711', '/airb10', '/tuif09']
    }
}, {
    site: 'rosettastone.de',
    name: 'browse',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 75,
        lf: 2
    },
    include: {
        urls: ['.']
    }
}, {
    site: 'rosettastone.co.uk',
    section: 'campaign',
    name: 'browse',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    pin: 1,
    criteria: {
        sp: 75,
        lf: 1
    },
    include: {
        urls: ['/s80101t', '/s80201t', '/promoe8001t', '/1atosub', '/2atosub', '/rs-demo', '/brand13demo', '/learn13demo']
    }
}, {
    site: 'rosettastone.co.uk',
    name: 'browse',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 75,
        lf: 2
    },
    include: {
        urls: ['.']
    }
}, {
    site: 'rosettastone.com',
    section: 'campaign',
    name: 'browse',
    invite: {
        when: 'onentry'
    },
    pin: 1,
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 75,
        lf: 1
    },
    include: {
        urls: ['/hscm12', '/tmcp212', '/hstm1011', '/sehslp1', '/totaledemo', '/selp1', '/selp2', '/selprm', '/selprm2', '/selpmb', '/selpmb2', '/selpdemo1', '/sehslp1', '/emlp1', '/emlp2', '/emlp3', '/empl4', '/empl6', '/balp2', '/balp1', '/balpmb', '/balpdemo1', '/balpdemo2', '/print']
    }
}, {
    site: 'rosettastone.com',
    name: 'browse',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 75,
        lf: 2
    },
    include: {
        urls: ['.']
    }
}];
FSR.properties = {
    repeatdays: 90,
    
    repeatoverride: false,
    
    altcookie: {},
    
    language: {
        locale: 'en',
        src: 'location',
        locales: [{
            match: 'rosettastone.de',
            locale: 'de'
        }, {
            match: 'rosettastone.co.uk',
            locale: 'uk'
        }]
    },
    
    exclude: {},
    
    zIndexPopup: 10000,
    
    ignoreWindowTopCheck: false,
    
    ipexclude: 'fsr$ip',
    
    invite: {
        // Is this an MDOT Site?
        isMDOT: false,
        
        // For no site logo, comment this line:
        siteLogo: "sitelogo.gif",
        
        /* Desktop */
        dialogs: [{
            reverseButtons: false,
            headline: "We'd welcome your feedback!",
            blurb: "Thank you for visiting RosettaStone.com. You have been selected to participate<br>in a brief customer satisfaction survey to let us know how we can improve<br>your experience.",
            noticeAboutSurvey: "The survey is designed to measure your entire experience, please look for it at the <u>conclusion</u> of your visit.",
            attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
            closeInviteButtonText: "Click to close.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll give feedback",
            
            locales: {
                "de": {
                    headline: "Wir freuen uns auf Ihr Feedback.",
                    blurb: "Vielen Dank für Ihren Besuch bei RosettaStone.de. Sie wurden ausgewählt, um an einer kurzen Umfrage zur Kundenzufriedenheit teilzunehmen und uns mitzuteilen, wie wir noch besser sein könnten.",
                    noticeAboutSurvey: "Mit dieser Umfrage wird Ihr Gesamteindruck gemessen. Bitte schauen Sie nach ihr am Ende Ihres Besuchs.",
                    attribution: "Diese Umfrage wird von einem unabhängigen Unternehmen, ForeSee, im Auftrag der von Ihnen besuchten Website durchgeführt.",
                    closeInviteButtonText: "Klicken Sie hier, um das Fenster zu schließen",
                    declineButton: "Nein, danke",
                    acceptButton: "Ja, ich möchte Feedback geben"
                },
                "uk": {
                    headline: "We would welcome your feedback.",
                    blurb: "Thank you for visiting RosettaStone.co.uk. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
                    noticeAboutSurvey: "The survey is designed to measure your entire experience. Please look for it at the <u>conclusion</u> of your visit.",
                    attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
                    closeInviteButtonText: "Click to close.",
                    declineButton: "No, thanks",
                    acceptButton: "Yes, I'll give feedback"
                }
            }
        }],
        
        exclude: {
            local: ['/store/cart', '/store/ship_and_bill', '/store/ship_and_bill_detail', '/store/review', '/store/summary', '/store/cart', '/store/billing', '/store/confirmation'],
            referrer: []
        },
        include: {
            local: ['.']
        },

        delay: 0,
        timeout: 0,
        hideOnClick: false,
		
        css: 'foresee-dhtml.css',
		
        hide: [],
        type: 'dhtml',
        /* desktop */
        //url: 'invite.html'
        /* mobile */
        url: 'invite-mobile.html'
    },
    
    tracker: {
        width: '690',
        height: '415',
        timeout: 3,
        adjust: true,
        alert: {
            enabled: true,
            message: 'The survey is now available.',
            locales: [{
                locale: 'de',
                message: 'Ihre Umfrage steht jetzt zur Verfügung.'
            }]
        },
        url: 'tracker.html',
        locales: [{
            locale: 'de',
            url: 'tracker_de.html',
            height: '435'
        }, {
            locale: 'uk',
            url: 'tracker_uk.html'
        }]
    },
    
    survey: {
        width: 690,
        height: 600
    },
    
    qualifier: {
        footer: '<div div id=\"fsrcontainer\"><div style=\"float:left;width:80%;font-size:8pt;text-align:left;line-height:12px;\">This survey is conducted by an independent company ForeSee,<br>on behalf of the site you are visiting.</div><div style=\"float:right;font-size:8pt;\"><a target="_blank" title="Validate TRUSTe privacy certification" href="//privacy-policy.truste.com/click-with-confidence/ctv/en/www.foreseeresults.com/seal_m"><img border=\"0\" src=\"{%baseHref%}truste.png\" alt=\"Validate TRUSTe Privacy Certification\"></a></div></div>',
        width: '690',
        height: '500',
        bgcolor: '#333',
        opacity: 0.7,
        x: 'center',
        y: 'center',
        delay: 0,
        buttons: {
            accept: 'Continue'
        },
        hideOnClick: false,
        css: 'foresee-dhtml.css',
        url: 'qualifying.html'
    },
    
    cancel: {
        url: 'cancel.html',
        width: '690',
        height: '400'
    },
    
    pop: {
        what: 'survey',
        after: 'leaving-site',
        pu: false,
        tracker: true
    },
    
    meta: {
        referrer: true,
        terms: true,
        ref_url: true,
        url: true,
        url_params: false,
        user_agent: false,
        entry: false,
        entry_params: false
    },
    
    events: {
        enabled: true,
        id: true,
        codes: {
            purchase: 800,
            items: 801,
            dollars: 802,
            followup: 803,
            information: 804,
            content: 805
        },
        pd: 30,
        custom: {
            purchase: {
                enabled: true,
                repeat: false,
                source: 'url',
                patterns: ['/store/summary', '/store/confirmation']
            },
            items: {
                enabled: true,
                repeat: false,
                source: 'variable',
                name: 'qty'
            },
            dollars: {
                enabled: true,
                repeat: false,
                source: 'variable',
                name: 'cartValue'
            }
        }
    },
    
    pool: 20,
    
    previous: false,
    
    analytics: {
        google: false
    },
    
    cpps: {
        cid: {
            source: 'parameter',
            name: 'cid'
        }
    },
    
    mode: 'hybrid'
};
