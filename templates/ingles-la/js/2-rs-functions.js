rs.functions = {

	parseUri : function(url) {
		var url = url || window.location.search.substr(1).split('&')
		//console.log(url)
		//if (url == "") return {};
			var b = {};
			for (var i = 0; i < url.length; ++i) {
				var p=url[i].split('=');
				if (p.length != 2) continue;
				b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
			}
			return b;
		},




	setCookie: function(name, value, b_custom, expires_ms, path, domain, secure) {
		//name        	= cookie name to set.
		//value         = cookie value to set.
		//b_custom      = optional boolean value which will allow custom settings
		//expires_ms    = optional milliseconds till cookie expires (#days * 1000 * 60 * 60 * 24).
		//path          = optional path.
		//domain        = optional domain.
		//secure        = optional secure setting.
		if(!b_custom) {
			expires_ms = 30 * 1000 * 60 * 60 * 24; //30 days
			path = "/";
			domain = this.cookieDomain();
		}
		var today = new Date();
		var exdate = new Date(today.getTime() + (expires_ms));
		exdate = exdate.toUTCString();
		var c_verb = 'setting'; //for debugging string below

		if(expires_ms == -1) {
			expires_ms = 1;	
			exdate = new Date(today.getTime() - (30 * 1000 * 60 * 60 * 24));
			exdate = exdate.toUTCString();
			c_verb = 'removing'; //for debugging string below
		}
		
		document.cookie = name + "=" + escape(value)+
		((expires_ms) ? ";expires=" + exdate : "") +
		((path) ? ";path="+path : "") +
		((domain) ? ";domain="+domain : "") +
		((secure) ? ";secure" : "");
	},



	cookieDomain: function(){
		var hp = this.parseUri(document.location).host.split('.');
		hp.reverse();
		var domain = "." + hp[1] + "." + hp[0];
		if (hp.length >= 3 && hp[2] != 'www' && hp[2] == 'stg')
		domain = "." + hp[2] + "." + hp[1] + "." + hp[0];
		return domain;
	},


	getCookie: function(check_name) {
		var a_all_cookies = document.cookie.split(";");
		var a_temp_cookie = "";
		var cookie_name = "";
		var cookie_value = "";
		var b_cookie_found = false;

		for(i = 0; i < a_all_cookies.length; i++) {
			a_temp_cookie = a_all_cookies[i].split("=");
			cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g,"");
			if(cookie_name == check_name) {
				b_cookie_found = true;
				if (a_temp_cookie.length > 1) {
					cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g,""));
				}
				return cookie_value;
				break;
			}
			a_temp_cookie = null;
			cookie_name = "";
		}
		if(!b_cookie_found) {
			return null;
		}
	},

	deleteCookie: function(name) {
		this.setCookie(name,"",true,-1,'/',this.cookieDomain());
	}







}




/* Former jb_utils.js
 * helper functions for analytics client-side code
 * jbeach, created: 2010-04-06, updated: 2010-09-02
 */

/* get local time increment */
//This will output a time string in client browser's local time, only in 10 minute increments.
/*
RSUI.util.getLocalTimeIncrement = function(){
    var t;
    try {
        RSUI.util.dateFormat.masks.timeIncrement="yyyy-mm-dd HH:";
        var now = new Date();
        var t1 = now.format_rs("timeIncrement");
        var t2 = Math.floor(now.getMinutes()/10).toString();
        t = t1 + t2 + "0";
    }
    catch(err){t = err;}
    return t;
}
*/


