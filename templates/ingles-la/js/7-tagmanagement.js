/* cookie setting and getting functions 
 * adapted from http://techpatterns.com/downloads/javascript_cookies.php */
function setCookie (name, value, b_custom, expires_ms, path, domain, secure){
    //name        = cookie name to set.
    //value         = cookie value to set.
    //b_custom      = optional boolean value which will allow custom settings
    //expires_ms    = optional milliseconds till cookie expires (#days * 1000 * 60 * 60 * 24).
    //path          = optional path.
    //domain        = optional domain.
    //secure        = optional secure setting.
    if(!b_custom){
        expires_ms = 30 * 1000 * 60 * 60 * 24; //30 days
        path = "/";
        domain = this.cookieDomain();
    }
	var today = new Date();
    var exdate = new Date(today.getTime() + (expires_ms));
	exdate = exdate.toUTCString();
	var c_verb = 'setting'; //for debugging string below
	
	if(expires_ms == -1) {
		expires_ms = 1;	
		exdate = new Date(today.getTime() - (30 * 1000 * 60 * 60 * 24));
		exdate = exdate.toUTCString();
		c_verb = 'removing'; //for debugging string below
	}
	
    document.cookie = name + "=" + escape(value)+
        ((expires_ms) ? ";expires=" + exdate : "") +
        ((path) ? ";path="+path : "") +
        ((domain) ? ";domain="+domain : "") +
        ((secure) ? ";secure" : "");
};


function cookieDomain() {
	var hp = this.parseUri(document.location).host.split('.');
	hp.reverse();
	domain = "." + hp[1] + "." + hp[0];
	if (hp.length >= 3 && hp[2] != 'www' && hp[2] == 'stg')
		domain = "." + hp[2] + "." + hp[1] + "." + hp[0];
	
	return domain;
}
			
function getCookie (check_name){
    var a_all_cookies = document.cookie.split(";");
    var a_temp_cookie = "";
    var cookie_name = "";
    var cookie_value = "";
    var b_cookie_found = false;
    
    for(i = 0; i < a_all_cookies.length; i++){
        a_temp_cookie = a_all_cookies[i].split("=");
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g,"");
        if(cookie_name == check_name){
            b_cookie_found = true;
            if (a_temp_cookie.length > 1) {
				cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g,""));
			}
            return cookie_value;
            break;
        }
        a_temp_cookie = null;
        cookie_name = "";
    }
    if(!b_cookie_found) {
		return null;
	}
};

function eraseCookie (name) {
    this.setCookie(name,"",true,-1,'/',this.cookieDomain());
}








// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License
parseUri = function (str) {
	var	o   = parseUri.options,
		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
		uri = {},
		i   = 14;

	while (i--) uri[o.key[i]] = m[i] || "";

	uri[o.q.name] = {};
	uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
		if ($1) uri[o.q.name][$1] = $2;
	});

	return uri;
};

parseUri.options = {
	strictMode: false,
	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
	q:   {
		name:   "queryKey",
		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
	},
	parser: {
		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
	}
};






function set_cookie ( name, value, exp_y, exp_m, exp_d, path, domain, secure )
{
	var cookie_string = name + "=" + escape ( value );
	if ( exp_y ) {
		var expires = new Date ( exp_y, exp_m, exp_d );
		cookie_string += "; expires=" + expires.toGMTString();
	}
	if ( path ) cookie_string += "; path=" + escape ( path );
	if ( domain ) cookie_string += "; domain=" + escape ( domain );
	if ( secure ) cookie_string += "; secure";
	document.cookie = cookie_string;
}




var tm_instance = "live";

if (document.location.host.indexOf("stg.") != -1 ) {
	tm_instance = "stage";
}

set_cookie('s_tagEnv', tm_instance, 2020, 1, 1, '/', '.rosettastone.com');






/* 
 * Adobe Marketing Cloud Tag Loader Code
 * Copyright 1996-2013 Adobe, Inc. All Rights Reserved
 * More info available at http://www.adobe.com/solutions/digital-marketing.html
 */

var amc=amc||{};if(!amc.on){amc.on=amc.call=function(){}};
document.write("<scr"+"ipt type=\"text/javascript\" src=\"//www.adobetag.com/d1/v2/ZDEtcm9zZXR0YXN0b25lLTQzNTktMzMxMS0=/amc.js\"></sc"+"ript>");

// End Adobe Marketing Cloud Tag Loader Code -->



