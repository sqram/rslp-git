;(function(urlparams) {

	// same format as key in crescendo.json. ie,  03-07-2015 10:00:00
	var date = new Date()
	var now = "0" + (date.getMonth() + 1) + '/'
		+ ("0" + date.getDate()).slice(-2)  + '/'
		+ date.getFullYear() + ' '
		+ ("0" + date.getHours()).slice(-2) + ':'
		+ ("0" + date.getMinutes()).slice(-2) + ':'
		+ ("0" + date.getSeconds()).slice(-2)
		;

	delete date;

	var expirationdate = RSI.expirationDate || null
	var domviolator = document.getElementById('violator') || null

	// grab url params
	if (window.location.search) {
		var parts = window.location.search.substring(1).split('&');
		for (var i = 0; i < parts.length; i++) {
			var nv = parts[i].split('=');
			if (!nv[0]) continue;
			urlparams[nv[0]] = nv[1] || true;
		}
	}

	/*
	 * If we have ?ed=**yymmdd** in the url, we don't run crescendo
	 * schedule. ?ed= will come in the format 99yyMMdd00
	 * (99 and 00 are intentional noise)
	 */
	if (urlparams.ed) {
		var urlyear ='20' + urlparams.ed.substr(2,2)
		, urlmonth = urlparams.ed.substr(4,2)
		, urlday =  urlparams.ed.substr(6,2)
		, urldate = [urlmonth, urlday, urlyear].join('/')
		;

		// if ?ed date is the same as now's date (exclude the timestamp)
		if (urldate && urldate ==  now.split(' ')[0]) {
			startClock(urldate + ' ' + ' 23:59:59')
			document.getElementById('expirationdate').innerHTML = [urlmonth, urlday, urlyear].join('/')
			return
		}
	}


	/*  ?ed is NOT in the url. follow crescendo schedule defined in crescendo.json
	-----------------------------------------------------------------------------------------------------------------------*/

	$.getJSON('crescendo.json', startCrescendo)

	 /*
	 * Crescendo.json keys are dates. We sort them them reverse order. We then
	 * loop through them, and stop the loop where 'now' < loop key. So if we have dates
	 * [01-01-2015, 01-03-2015] and 'now' is 01-02-2015, the loop will stop when key
	 * is 01-01-2015, and we use the crescendo settings for that key. If 'now' is greater
	 * than the last date key in the array, it will run forever, until it passes the 'stop' date.
	 * if a 'stop' date isn't specified in the json, as a precaution we make it the last date,
	 * with time set to 23:59:59. (otherwise it'd run forever)
	 */
	function startCrescendo(json) {

		// If empty json, leave
		if (!json || !Object.keys(json).length) return

		// Arrayf of dates from json. Sort it too
		var dates = Object.keys(json).sort(compare)


		// Stop date defined in the json
		var stop = json.stop || null

		/*
		 * Remove 'stop' from dates array. Also, if 'stop' is not defined
		 * in json, its value will be last date key, with time set to 23:59:59
		 */
		if (stop) {
			dates.pop()
		} else {
			stop = dates[0].split(' ')[0] + ' 23:59:59'
		}

		if (new Date(now) > new Date(stop)) return

		dates.every(function(key, i) {

			if (! (new Date(now) <= new Date(key))) {

				var settings = json[key]

				// redirect
				if (settings.redirect) window.location.href = settings.redirect + window.location.search

				// clock
				if (settings.clock) startClock(settings.clock)

				// violator
				if (settings.violator && domviolator) {
					domviolator.setAttribute('src', '../../globals/img/violators/' + settings.violator)
					domviolator.style.display = 'block'
				}

				// expiration date
				if (settings.expiration) {
					expirationdate = settings.expiration
				}

				return false
			}
			return true
		});

		/*
		 * Sort timestamps. We want from highest to lowest.
		 * ie, latest dates to closer dates. ie, 2017, 2016, etc
		 */
		function compare(a, b) {
			// move 'stop' to the end
			//if (isNaN(a[0])) return -1
			if (new Date(a) < new Date(b)) return 1
			if (new Date(a) > new Date(b)) return -1
			if (new Date(a) == new Date(b)) return 0
		}
	}

	document.getElementById('expirationdate').innerHTML  = expirationdate
})({})
