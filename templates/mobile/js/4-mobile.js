urlObject		= parseUri(window.location.href)

/**********************************************************************
 *	PROMO
 * If we have a promo in a cookie, they probably got here through
 * a redirect and the referrer might've set a cookie. Let's use it.
 *************************************************************************
 */

if(typeof rs.promo != 'undefined' && rs.promo !='') {
	$.cookie('promo', rs.promo)
} else {
	if($.cookie("promo") != null) {
		rs.promo = $.cookie("promo").toLowerCase();
	}
}


/**********************************************************************
 *	CID
 **********************************************************************/
if (typeof urlObject.queryKey.cid != 'undefined') {

	rs.cid = urlObject.queryKey.cid
}

if (typeof urlObject.queryKey.anchor != 'undefined' && urlObject.queryKey.anchor == 'method') {
	window.location.href = '../../methodology/' + RSI.rsi + window.document.location.search
}




// This should be in qb_promos.json..but i don't know how warren parses that file, so adding it
// there might break. and there's not really time to go figuring it out, so, hardcode here
var regularPrices = {
	"S5" : 499,
	"S3" : 399,
	"S2" : 299,
	"totale_12" : 299
}





$(document).on("click", 'a.link-toggler', function(e)
{
	e.preventDefault()
	// Remove is-active from other link, and add it to this one
	$('a.link-toggler').removeClass('is-active')
	$(this).addClass('is-active')

	// Make the div visible. div's data-toggable-name is same as clicked link's ID
	var id = $(this).data('toggler-name')

	$toShow = $('div[data-toggable-name=' + id + ']')
	$('.toggable-section').fadeOut('fast')
	$toShow.fadeIn('fast')


});



// If there's a ?tab param, activate that tab (CD, totale). If not load CD
/*
if (typeof rs.urlObject.queryKey.tab != 'undefined') {
	$('a[data-toggler-name=' + rs.urlObject.queryKey.tab + ']').trigger('click')
} else {
	$('a[data-toggler-name=cd]').trigger('click')
}
*/










$(function() {
	$menuIcon = $('#meta-nav--toggle')
	// Fire event4 when menu link is clicked
	$menuIcon.click(function() {
		s.events="event4";
		s.t();
		delete s.events;
	});


	/*
	 * The have to change logo link since new architecture
	 * has each link as a template.
	*/
	$('#meta-nav--logo').attr('href', '/lp/mobile/'+RSI.rsi)


	// Hide menu if anywhere outside it is clicked
	/*
	$(document).on('click', 'body', function(event) {
		if( ($(event.target).parents().index($('#global-nav')) == -1) && (!$(event.target).hasClass('icon-menu'))) {
			if ( $menuIcon.hasClass('is-active')) {
				$menuIcon.removeClass('is-active')
				$('#global-nav').removeClass('is-expanded').addClass('is-closed')
			}
		}
	})
	*/


	/*
	* If we have items in cart, show the number in little blue circle.
	* And if they click the cart icon that has a number (aka, items in cart)
	* take them to magento page. If no items are in cart, show div
	* saying cart is empty
	*/
	$cartIcon = $('#meta-nav--cart');
	$cartIcon.removeClass('is-active')
	$.ajax({
		url: 'https://secure.rosettastone.com/us_en_store_view/checkout/cart/flyout',
		dataType: 'jsonp'
	}).done(function(d) {


		if (d.count == 0) {

			$('.cart-dropdown').html("<h2 style='color:#eee;text-align:center'>Your cart is empty</h2>")
			$('body').on('click', '#meta-nav--cart', function(e) {
				$('.cart-dropdown').slideToggle()
			});
		} else {
			$('.cart-wrap').append('<span class="cart-counter">'+d.count+'</span>')
			$('body').on('click', '#meta-nav--cart', function(e) {
				window.location = 'https://secure.rosettastone.com/us_en_store_view/checkout/cart/'
			});
		}

		$('#checoutnow').hide()

		/*
		* WORKS ON DESKTOP BUT NOT MOBILE. MAGENTO SENDS DIFFERENT DATA BASED ON PLATFORM
		 * Check to see if we have a product in the cart.
		 * We check by checking to see if the  first char
		 * inside .flyheader h1's text is a number. For instancem
		 * "2 items in your cart". If the first char in that string
		 * is not numeric, then assume cart is empty

		var firstChar = $('.fly-header h1').text()[0]
		if (!isNaN(firstChar)) {
			$('.cart-wrap').append('<span class="cart-counter">'+firstChar+'</span>')
		} else {
			//$('.button-standard').hide()
			$('.cart-dropdown').html("<h2 style='color:#eee;text-align:center'>Your cart is empty</h2>")
		}
		*/



	});




	function stageCookie(b,d){expires_ms=30*1000*60*60*24;path="/";domain="";var c= parseUri(document.location).host.split(".");c.reverse();domain="."+c[1]+"."+c[0];if(c.length>=3&&c[2]!="www"){domain="."+c[2]+"."+c[1]+"."+c[0]}var a=new Date();var f=new Date(a.getTime()+(expires_ms));var e="";document.cookie=b+"="+escape(d)+((expires_ms)?";expires="+f.toUTCString():"")+((path)?";path="+path:"")+((domain)?";domain="+domain:"")+((e)?";secure":"")}




	//Pixels logic for Mobile Marketing activities
	if(typeof urlObject.queryKey.cid != 'undefined' && urlObject.queryKey.cid != '' ) {
		if(urlObject.queryKey.cid == 'ba-mb-voltari') {
			$.getScript('http://click.moadlink.com/tinyurl/js/RS_2013-11-14_690');
			$.cookie('rs_marketing_src','voltari');
		}

		if(urlObject.queryKey.cid == 'ba-mb-mm-tb') {
			$.cookie('rs_marketing_src','millenialmedia');
			if(typeof urlObject.queryKey.urid != 'undefined' && urlObject.queryKey.urid != '' ) {
				$.cookie('millenialmedia_urid', urlObject.queryKey.urid);
			}
		}

		/*if(urlObject.queryKey.cid == 'ba-mb-ym-ph') {
			if (typeof urlObject.queryKey.click_id != 'undefined' || urlObject.queryKey.click_id != '' ) {
				$.cookie('rs_marketing_src','yieldmo', {path: '/', expires: 60, domain: '.rosettastone.com' });
				$.cookie('rs_marketing_click_id', urlObject.queryKey.click_id, {path: '/', expires: 60, domain: '.rosettastone.com' });
			}
		}*/
		if(typeof urlObject.queryKey.cid != 'undefined' && urlObject.queryKey.cid != '' && urlObject.queryKey.click_id != '' &&  typeof urlObject.queryKey.click_id != 'undefined') {
		    if(urlObject.queryKey.cid.match('ba-bn-ym') || urlObject.queryKey.cid.match('ba-rm-ym') ) {
		        stageCookie('rs_marketing_src','yieldmoad',true, 30*1000*60*60*24, '/', ".rosettastone.com");
		        stageCookie('rs_marketing_click_id', urlObject.queryKey.click_id, true, 30*1000*60*60*24, '/', ".rosettastone.com");

		    }
		}

	}



/*if(urlObject.queryKey.cid == 'ba-mb-mm-ld') {
    $('body').append('<img height="1" width="1" src="https://sa.jumptap.com/a/conversion?event=Lead&eventId=4939" />')
}

if(urlObject.queryKey.cid == 'ba-mb-ym-ph') {
    if (typeof urlObject.queryKey.click_id != 'undefined' || urlObject.queryKey.click_id != '' ) {
        RSUI.util.setCookie('rs_marketing_src','yieldmo', true, (60 * 1000 * 60 * 60 * 24), '/', '*.rosettastone.com');
        RSUI.util.setCookie('rs_marketing_click_id', urlObject.queryKey.click_id, true, (60 * 1000 * 60 * 60 * 24), '/', '.stg.rosettastone.com');
    }
}

*/






}) // end docready






