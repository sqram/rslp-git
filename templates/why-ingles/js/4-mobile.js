urlObject		= parseUri(window.location.href)

/**********************************************************************
 *	PROMO
 * If we have a promo in a cookie, they probably got here through
 * a redirect and the referrer might've set a cookie. Let's use it.
 *************************************************************************
 */

if(typeof rs.promo != 'undefined' && rs.promo !='') {
	$.cookie('promo', rs.promo)
} else {
	if($.cookie("promo") != null) {
		rs.promo = $.cookie("promo").toLowerCase();
	}
}


/**********************************************************************
 *	CID
 **********************************************************************/
if (typeof urlObject.queryKey.cid != 'undefined') {

	rs.cid = urlObject.queryKey.cid
}


// This should be in qb_promos.json..but i don't know how warren parses that file, so adding it
// there might break. and there's not really time to go figuring it out, so, hardcode here
var regularPrices = {
	"S5" : 499,
	"S3" : 399,
	"S2" : 299,
	"totale_12" : 299
}





$(document).on("click", 'a.link-toggler', function(e)
{
	e.preventDefault()
	// Remove is-active from other link, and add it to this one
	$('a.link-toggler').removeClass('is-active')
	$(this).addClass('is-active')

	// Make the div visible. div's data-toggable-name is same as clicked link's ID
	var id = $(this).data('toggler-name')

	$toShow = $('div[data-toggable-name=' + id + ']')
	$('.toggable-section').fadeOut('fast')
	$toShow.fadeIn('fast')


});




// If there's a ?tab param, activate that tab (CD, totale). If not load CD
/*
if (typeof rs.urlObject.queryKey.tab != 'undefined') {
	$('a[data-toggler-name=' + rs.urlObject.queryKey.tab + ']').trigger('click')
} else {
	$('a[data-toggler-name=cd]').trigger('click')
}
*/










$(function() {
	$menuIcon = $('#meta-nav--toggle')
	// Fire event4 when menu link is clicked
	$menuIcon.click(function() {
		s.events="event4";
		s.t();
		delete s.events;
	});

	$('#meta-nav--logo').attr('href', '/lp/mobile-ingles/'+RSI.rsi)

	// Hide menu if anywhere outside it is clicked
	/*
	$(document).on('click', 'body', function(event) {
		if( ($(event.target).parents().index($('#global-nav')) == -1) && (!$(event.target).hasClass('icon-menu'))) {
			if ( $menuIcon.hasClass('is-active')) {
				$menuIcon.removeClass('is-active')
				$('#global-nav').removeClass('is-expanded').addClass('is-closed')
			}
		}
	})
	*/


	/*
	* If we have items in cart, show the number in little blue circle.
	* And if they click the cart icon that has a number (aka, items in cart)
	* take them to magento page. If no items are in cart, show div
	* saying cart is empty
	*/
	$cartIcon = $('#meta-nav--cart');
	$cartIcon.removeClass('is-active')
	$.ajax({
		url: 'https://secure.rosettastone.com/us_en_store_view/checkout/cart/flyout',
		dataType: 'jsonp'
	}).done(function(d) {


		if (d.count == 0) {

			$('.cart-dropdown').html("<h2 style='color:#eee;text-align:center'>Your cart is empty</h2>")
			$('body').on('click', '#meta-nav--cart', function(e) {
				$('.cart-dropdown').slideToggle()
			});
		} else {
			$('.cart-wrap').append('<span class="cart-counter">'+d.count+'</span>')
			$('body').on('click', '#meta-nav--cart', function(e) {
				window.location = 'https://secure.rosettastone.com/us_en_store_view/checkout/cart/'
			});
		}

		$('#checoutnow').hide()

		/*
		* WORKS ON DESKTOP BUT NOT MOBILE. MAGENTO SENDS DIFFERENT DATA BASED ON PLATFORM
		 * Check to see if we have a product in the cart.
		 * We check by checking to see if the  first char
		 * inside .flyheader h1's text is a number. For instancem
		 * "2 items in your cart". If the first char in that string
		 * is not numeric, then assume cart is empty

		var firstChar = $('.fly-header h1').text()[0]
		if (!isNaN(firstChar)) {
			$('.cart-wrap').append('<span class="cart-counter">'+firstChar+'</span>')
		} else {
			//$('.button-standard').hide()
			$('.cart-dropdown').html("<h2 style='color:#eee;text-align:center'>Your cart is empty</h2>")
		}
		*/

	});









	//Pixels logic for Mobile Marketing activities
	if(typeof urlObject.queryKey.cid != 'undefined' && urlObject.queryKey.cid != '' ) {
		if(urlObject.queryKey.cid == 'ba-mb-voltari') {
			$.getScript('http://click.moadlink.com/tinyurl/js/RS_2013-11-14_690');
			$.cookie('rs_marketing_src','voltari');
		}

		if(urlObject.queryKey.cid == 'ba-mb-mm-tb') {
			$.cookie('rs_marketing_src','millenialmedia');
			if(typeof urlObject.queryKey.urid != 'undefined' && urlObject.queryKey.urid != '' ) {
				$.cookie('millenialmedia_urid', urlObject.queryKey.urid);
			}
		}

		if(urlObject.queryKey.cid == 'ba-mb-ym-ph') {
			if (typeof urlObject.queryKey.click_id != 'undefined' || urlObject.queryKey.click_id != '' ) {
				$.cookie('rs_marketing_src','yieldmo', {path: '/', expires: 60, domain: '.rosettastone.com' });
				$.cookie('rs_marketing_click_id', urlObject.queryKey.click_id, {path: '/', expires: 60, domain: '.rosettastone.com' });
			}
		}

	}



/*if(urlObject.queryKey.cid == 'ba-mb-mm-ld') {
    $('body').append('<img height="1" width="1" src="https://sa.jumptap.com/a/conversion?event=Lead&eventId=4939" />')
}

if(urlObject.queryKey.cid == 'ba-mb-ym-ph') {
    if (typeof urlObject.queryKey.click_id != 'undefined' || urlObject.queryKey.click_id != '' ) {
        RSUI.util.setCookie('rs_marketing_src','yieldmo', true, (60 * 1000 * 60 * 60 * 24), '/', '*.rosettastone.com');
        RSUI.util.setCookie('rs_marketing_click_id', urlObject.queryKey.click_id, true, (60 * 1000 * 60 * 60 * 24), '/', '.stg.rosettastone.com');
    }
}

*/



$('body').on('click', '#submit-demo', function()
            {

                var $email = $('input#input-email')
                var $dropdown = $('#demo-select')
                var lang = $dropdown.find(':selected').attr('value') || 'en-es'
                var errorMsg = '';

                // Do we have an email?
                if (! validateEmail( $email.val() )) {
                    errorMsg = errorMsg + "<li>Email inválido.</li>";
                } else {
                    var email = $email.val()
                }

                // Do we have a language selected
                if ( $dropdown.find(':selected').attr('value') == '' ) {
                    errorMsg = errorMsg +  "<li>Por favor seleccione un idioma.</li>"
                } else {
                    var demoLang = $dropdown.find(':selected').attr('value')
                }

                // Show errors if any, and exit function
                if (errorMsg.length > 0) {
                    $('.notification').fadeIn().addClass('error').html(errorMsg)
                    return false
                }

                // No errors. If there were any errors before, hide.
                $('.notification').fadeOut()

                if(typeof rs.cid == 'undefined') {
                    rs.cid = ''
                }
                var map = {
                  'hi-fr' : 'fra',
                  'hi-de' : 'deu',
                  'hi-it' : 'ita',
                  'hi-en' : 'eng',
                  'hi-es' : 'esp'
                }

                // Send to EC
                var data = {
                    email : email,
                    demo_lang : map[lang] || 'eng',
                    cis_name : 'US Hispanic Demo Leads',
                    website: 'EH_WEBSITE',
                    demo_type:'Hispanic_Demo_Mobile',
                    form_type : 'demo',
                    form_url : window.location.pathname,                    
                    newsletter_type : "Top_Landing_Why",
                    cid : _satellite.getVar("mostrecentcampaign")

                }
			
                  var url = (window.location.href.search(/(\.stg\.)|(\.local)/i)>-1 ?'http://www.stg' :'http://www')
                         + '.rosettastone.com/?p_p_id=rosettaajaxsubmit_WAR_rosettaajaxsubmitportlet&p_p_lifecycle=2&redirect2mobile=no';
               

                var request = $.ajax({
                  url:  url,
                  type: "POST",
                  data: data
                });

                request.done(function(msg) {                	
                 	if (JSON.parse(msg)[0]['cisFlag']  == 'true') {			 	
                        s.events="event19";
                        s.t()
                        delete s.events;
                        if(urlObject.queryKey.cid == 'ba-mb-yiel') {
                            $('body').append('<img src="https://tkr.yieldmo.com/t_adt/adt?click_id='+urlObject.queryKey.click_id+'&conversion_type=leads" />')
                        }
                        setTimeout(function() {
                            window.location.href = 'http://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=' + lang
                        }, 500)

                    }
                })
            })

     // phone number based on channel.
    var cid = rs.urlObject.queryKey.cid || ""

    var phonesmap = {
        'sm': '800-338-1639',
        'em': '855-255-7790',
        'se': '855-267-8796',
        'af': '855-255-7791',
        'ba': '800-941-7670',
        'mx': '018000-831-280'
    }

    RSI.phone = phonesmap[cid.substr(0,2)] || '' // if none of the cid is matched, use this phone no.

    if (RSI.rsi == 'curso') RSI.phone = '855-267-8795'

    // if 'mx' is in anywhere in the cid, phone number must be this, regardless of phonesmap
    if (cid.match(/mx-/)) RSI.phone = '018000-831-280'


}) // end docready






