/*
 * File: crescendo.js
 * Regulates violars and clocks, based on two things:
 * 1. Their settings in pages.json
 * 2. ?rd url parameter.
 *
 * It also changes the expiration date displayed on the footer.
 *
 *
 * Dependencies:
 * ----------------------------------------------------------
 * 1. the RSI model
 * 2. rs.js (needs the rs.obj to grab url query params)
 * 3. clock.js
 *
 */

var expirationDiv = document.getElementById('expiry')
expirationDiv.innerText = RSI.expirationDate

/*
 * A url might contain an expiration(ed) date parameter.
 * Exp date is in the format 99yyMMdd00 (where 99 and 00 are noise)
 * If we have the ?ed param, we overwrite the footer's expiration date.
 * furthermore, if the ?ed expiration date is the same as the current
 * localtime date, show clock (clocks always expire at midnight).
 */
;(function() {

	var expdate = rs.urlObject.queryKey.ed || null

	// Set footer's expiration date



	if (expdate) {
		// will come in the format 99yyMMdd00 (99 and 00 are intentional noise)
		var urlyear = '20' + expdate.substr(2,2)
		var urlmonth = expdate.substr(4,2)
		var urlday = parseInt(expdate.substr(6,2))


		// Put url date into formate mm/dd/yyyy to match pages.json
		urlDate = [urlmonth, urlday, urlyear].join('/')

		// Check to see if above day,month,yr is the same as localtime
		localdate = new Date()
		localMonth = (localdate.getMonth()+1) + ""
		localMonth = (localMonth.length == 1) ? "0" + localMonth : localMonth
		localDateStr = [
			localMonth,
			localdate.getDate(),
			localdate.getFullYear()
		].join('/')

		if (localDateStr == urlDate) {
			if (typeof startClock != 'undefined') {
				startClock(urlDate + ' ' + '23:59:59')
			}
		}

		// change footer's expiration date
		expirationDiv.innerText = [urlmonth, urlday, urlyear].join('/')
	}
})();



/*
 * Show clock and violator, if they're supposed to be
 * shown on this date. These are set in pages.json
 * ps: even though in pages.json we give a clock's
 * start time in hh:mm:ss, we pretty much only check
 * the hour, since we never start a countdown clock
 * mid-hour. Remember: if clock start day has to be
 * a single digit instead of two(ie, 9 and not 09)
 * in pages.json
 *
 */
;(function() {

	var date = new Date();
	var day = (date.getDate() < 10) ? day = "0" + date.getDate() : day = date.getDate()
	var todayDateStr = [
		date.getMonth()+1,
		day,
		date.getFullYear()
	].join('/');




	RSI.header.clock.forEach(function(k, i) {
		if (k.date == todayDateStr) {
			var dateClock = new Date(k.date)
			if (date.getHours() >= k.starts) {
				startClock(k.ends)
			}
		}

	});


	/*
	 * Have to compare today's datetime with pages.json's datetime.
	 * Since we only set the end time in pages.json (ie, "11:59:59"),
	 * we must prepend today's date to that endtime and create a new
	 * date object - new Date(todayDateStr + ' ' + k.ends)
	 * We could just specify a whole date time in pages.json as
	 * "11/25/2014 11:59:59", but there's no point in putting a date there
	 * as we can only script for the current day since we are client side only.
	 * For instance, we cant have a start date as 11/25/2014 13:00:00 and and
	 * end date as 11/26/2014 13:00:00. If we want to do such thing, we would have
	 * the start date be 11/25/2014 13:00:00 and end date as 11/25/2014 11:59:50,
	 * and then for the next day, start 11/26/2014 0:00:00 and end 11/25/2014 13:00:00,
	 */
	RSI.header.violator.forEach(function(k, i) {
		if (k.date == todayDateStr) {
			var endTime = new Date(todayDateStr + ' ' + k.ends)

			if ((date.getHours() >= k.starts) && (endTime - date > 0)) {

				var violator = document.querySelector('#violator')
				if (violator) {
					violator.setAttribute('src', '../../globals/img/violators/' + k.name)
					violator.style.display = 'block'
				}

			}

			// lawd have mercy..
			if (typeof k['violator2'] != 'undefined' && k['violator2'].starts < k.starts && !(date.getHours() >= k.starts)) {
				if (violator) {
					var violator = document.querySelector('#violator')
					violator.setAttribute('src', '../../globals/img/violators/' + k['violator2'].name)
					violator.style.display = 'block'
				}
			}
		}
	})
})();







