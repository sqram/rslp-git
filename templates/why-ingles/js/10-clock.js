var clockHtml="<style>"
+"	#clock-container {"
+"		height: 66px;"
+"		background-color: #363533;"
+"		position:relative;"
+"	}"
+"	#clock-wrapper {"
+"		display: none;"
+"		font-family: effralightregular, arial;"
+"		display: none;"
+"		text-align:center;"
+"		margin: 0 auto;"
+"		padding: 10px 0 10px;"
+"		background: #363533;"
+"	}"
+"	#clock-wrapper ul.numbers, #clock-wrapper ul.numbers li  {"
+"		margin: 0;"
+"		padding: 0;"
+"		list-style: none;"
+"		position:  relative;"
+"		top:  -3px;"
+"		display:  inline-block;"
+"	}"
+"	#clock-wrapper ul.numbers li {"
+"		display:  inline-block;"
+"		background: #eee;"
+"		width: 30px;"
+"		height: 35px;"
+"		line-height: 35px;"
+"		text-align: center;"
+"		font-size: 20px;"
+"		position: relative;"
+"		border: 1px solid #e6e6e6;"
+"		background: url(data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgTWFjaW50b3NoIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkM2Qjg4MTQ1MUZBRTExRTM5QkE0Q0I4NzkyQTM2MzhDIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkM2Qjg4MTQ2MUZBRTExRTM5QkE0Q0I4NzkyQTM2MzhDIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QzZCODgxNDMxRkFFMTFFMzlCQTRDQjg3OTJBMzYzOEMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QzZCODgxNDQxRkFFMTFFMzlCQTRDQjg3OTJBMzYzOEMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wAARCAA2AAEDAREAAhEBAxEB/8QAZAABAQAAAAAAAAAAAAAAAAAABgoBAQEAAAAAAAAAAAAAAAAAAAABEAABAgQFBQAAAAAAAAAAAAAAkRTwAWFiEVFx0eECkqIDFREBAAIBBQAAAAAAAAAAAAAAAAERIVFhkRIi/9oADAMBAAIRAxEAPwC9xzWO0JcCz3VeQchj+6ayLhPQN9C+FFHYHf3QoS40DXtZAzuO4+/Lr8dwj//Z) #eee;"
+"	}"
+"	#clock-wrapper ul.numbers li.delimiter {"
+"		background: none;"
+"		color: #fff;"
+"		width: 10px;"
+"		border: none;"
+"	}"
+"	#clock-wrapper ul.numbers li div.hours,"
+"	#clock-wrapper ul.numbers li div.minutes,"
+"	#clock-wrapper ul.numbers li div.seconds {"
+"		font-size: 10px;"
+"		position:  absolute;"
+"		bottom: -27px;"
+"		color: #fff;"
+"		right: -16px;"
+"	}"
+"	#clock-wrapper ul.numbers li div.minutes {"
+"		right: -26px;"
+"	}"
+"	#clock-wrapper ul.numbers li div.seconds {"
+"		right: -26px;"
+"	}"
+"	#clock-wrapper .sale-ends {"
+"		color: #fff;"
+"		display: inline-block;"
+"		width: 53px;"
+"		padding: 10px 0;"
+"		font-size: 14px;"
+"	}"
+"</style>"

/*****************************************
 * CLock HTML
 *****************************************/
+ "<div id='clock-wrapper'>"
+ "	<div class='sale-ends'>Oferta Termina:</div>"
+ "	<ul class='numbers'>"
+ "	<li>"
+ "		<span>1</span>"
+ "		<div class='hours'>Horas</div>"
+ "	</li>"
+ " <li>"
+ "		<span>2</span>"
+ "	</li>"
+ " <li class='delimiter'>:</li>"
+ "	<li>"
+ "		<span>5</span>"
+ "		<div class='minutes'>Minutos</div>"
+ " </li>"
+ " <li>"
+ "		<span>3</span>"
+ " </li>"
+ "	<li class='delimiter'>:</li>"
+ "	<li>"
+ "		<span>4</span>"
+ "		<div class='seconds'>Segundos</div>"
+ " </li>"
+ " <li>"
+ "		<span>6</span>"
+ " </li>"
+ "</ul>"
+ "</div> <!-- /clock wrapper -->";


var difference,
	timeToDisplay,
	clientDate,
	clientTime,
	clientOffset,
	UTC,
	estTime,
	estDate,
	endDate,
	estOffset = -5,
	offFlag = true,
	expires;

/*
 * Converts milliseconds to time.
 * Returns a multidimensional array like so:
 * [0, 5, 1, 2, 0, 1]
 * (h, h, m, m, s, s)
 */
function millisecondsToTime(s) {

	/* Prepends 0 to single digits */
	function zero(n) {
  		return (n < 10 ? '0' : '') + n;
	}

	var ms = s % 1000;
	s = (s - ms) / 1000;
	var secs = s % 60;
	var s = (s - secs) / 60;
	var mins = s % 60;
	var hrs = (s - mins) / 60;

	var tmp = [zero(hrs).split(''), zero(mins).split(''), zero(secs).split('') ];
	return tmp[0].concat(tmp[1]).concat(tmp[2])
}




/* Show & Update the clock every 1s */
// but do some prep dom prep first
// remember that clock starts and ends on same date.
// so don't be confused if the function call is something
// like startClock(startDate)
// it also always ends at midnight

var expires;

function startClock(expirationDate) {

	expires = new Date(expirationDate)

	$('#masthead').before(clockHtml);

	$clock = $("#clock-wrapper");

	$spans = $('#clock-wrapper ul.numbers li span')


	$('#global-header').after($clock);
	$('.phone-number').css({'margin-top':'2px'});




	setInterval(updateClock, 1000);
}


function schedulerWrongClock() {
	setInterval(updateWrongClockUsers, 1000);
}

function updateWrongClockUsers() {
	timeToDisplay = millisecondsToTime(difference);
	$clock.css("display", "block");
	//$clock.text(timeToDisplay);
	difference -= 1000;
}


/*
 * There's a span for each clock number. So the number of spans in
 * the $spans array is the same as the time array. (should be 6).
 * In a loop, update each span's text with the value of the parallel
 * time[] element
 */
function updateHTMLElements(time) {
	for (var i = 0; i < $spans.length; i++) {
		$spans[i].innerHTML = time[i]
	}
}


/* Update clock (html elements) */
function updateClock() {
	if (offFlag) {



		clientDate  = new Date();
		clientTime = clientDate.getTime();
		clientOffset = clientDate.getTimezoneOffset() * 60000;
		UTC = clientTime + clientOffset;
		estTime = UTC + (3600000 * estOffset);
		estDate = new Date();
		endDate = expires;
		difference = endDate - estDate;

		// 86400000 is 1 day
		/*if(difference > 86400000) {
			$clock.hide();
			return false;
		}*/

		if (difference < 0) {
			$clock.hide();
			return false;
		}

		var time = millisecondsToTime(difference);

		// Update the html Elements with new numbers
		updateHTMLElements(time)

		$clock.show()


	} else {
		return false;
	}
} //end updateClock()









/*
 * startClock() is called from the template's html file,
 * or some js script included within.
*/
