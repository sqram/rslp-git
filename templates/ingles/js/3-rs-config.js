
rs = {

	
	
	
	pagename: '',

	mbox_env:"forQA=true",
	

	flowplayer_lic:"#@4febdbbf4825e91674c",
	
	//Facebook // changes per env
	facebook_app_id:'421303711221535',
	stg_facebook_app_id:'121466874664786',

	
	
	community:"USA",
	omniSuite:"rstonecom",
	// chnages per env
	omniSuite_staging:"rstonedev",
	currency:"USD",
	omniture_tracksrv:"o.rosettastone.com",
	omniture_tracksrv_secure:"s.rosettastone.com",

	

	domainName:".rosettastone.com",
	siteCode:"US_WEBSITE",
	
	cfg: {
		cart_staging: "http://secure.stg.rosettastone.com/hispanic_store_view/checkout/cart/",
		cart_production: "https://secure.rosettastone.com/hispanic_store_view/checkout/cart/"
	}
}


rs.windowurl = (window.location.href).split('.');
if( rs.windowurl[1] == 'stg' || rs.windowurl[1] == 'landingpages' ) {
	rs.envName = "staging";
	rs.mbox_env="forQA=true";
	//rs.debugjs=true;
	rs.cart_uri = rs.cfg.cart_staging;
	rs.cart_sku_uri = rs.cfg.cart_sku_staging;
	rs.site_uri = rs.cfg.site_uri_stage;
	rs.omniSuite = rs.omniSuite_staging;

} else {
	rs.envName = "production";
	rs.mbox_env="forProd=true";
	rs.debugjs=false;
	rs.cart_uri = rs.cfg.cart_production;
	rs.cart_sku_uri = rs.cfg.cart_sku_production;
	rs.site_uri = rs.cfg.site_uri_prod;
}
