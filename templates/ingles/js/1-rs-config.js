/*
 * RS object variables. Can be overwritten
 * in the HTML file after this file is included. Additionally,
 * since this file is included in every landing page, and lazyload
 * is pretty much needed everywhere, we define it here in this file.
 * Desktop pages should use this when codebase is re-done.
 *
 * 1. This file must be included after utils.js
 */
 
 
rs = {
	
	// Promo should change to whatever sitewide is
	promo : 'sitewide',
	cid: '',  
	

	/*
	 * If we're in mobile ('m' is in the url), then
	 * our page name is the 3rd element in the array
	 * since it doesn't have /lp/ in the url.
	 * ie, m.rosettastone.com/qb1
	 */
	pageName:  (function() 
	{
		if (window.location.href.split('.').indexOf('m') != -1) {
			var page = '/lp/' + window.location.href.split('/')[4]			
		} else {
			var page = "m:/" + window.location.href.split('/')[3]
		}
		return page
	})(),
	

	// Handy object holding url params, etc.
	urlObject : (function()
	{
		return parseUri(window.location)
	})(),

	// The template we're this page uses. This could be done better
	template: window.document.URL.split('/')[window.document.URL.split('/').length -2],
	
	/*
	proxy : (function() {
		if (window.location.href.split('.').indexOf('stg') != -1) {
			return 'stg'
		} else {
			return ''
		}
	})(),
	*/
	/* Facebook */
	fbappid: "327787780623486",
	
	/* Flowplayer */
	flowplayerLicence: "#@152cab2e1c57936da7a",
	
	/* Site Wide Offer */
	domainName:".rosettastone.com",
	
	
	
	
	
	
cfg: {
		cart_staging: "http://secure.stg.rosettastone.com/hispanic_store_view/checkout/cart/",
		cart_production: "https://secure.rosettastone.com/hispanic_store_view/checkout/cart/"
	}




}



rs.windowurl = (window.location.href).split('.');
if( rs.windowurl[1] == 'stg' || rs.windowurl[1] == 'landingpages' ) {
	rs.envName = "staging";
	rs.mbox_env="forQA=true";
	//rs.omniSuite = rs.omniSuite_staging;
	rs.cart_uri = rs.cfg.cart_staging;
} else {
	rs.envName = "production";
	rs.mbox_env="forProd=true";
	rs.cart_uri = rs.cfg.cart_production;
}
 
 
LazyLoad=function(k){function p(b,a){var g=k.createElement(b),c;for(c in a)a.hasOwnProperty(c)&&g.setAttribute(c,a[c]);return g}function l(b){var a=m[b],c,f;if(a)c=a.callback,f=a.urls,f.shift(),h=0,f.length||(c&&c.call(a.context,a.obj),m[b]=null,n[b].length&&j(b))}function w(){var b=navigator.userAgent;c={async:k.createElement("script").async===!0};(c.webkit=/AppleWebKit\//.test(b))||(c.ie=/MSIE/.test(b))||(c.opera=/Opera/.test(b))||(c.gecko=/Gecko\//.test(b))||(c.unknown=!0)}function j(b,a,g,f,h){var j=
function(){l(b)},o=b==="css",q=[],d,i,e,r;c||w();if(a)if(a=typeof a==="string"?[a]:a.concat(),o||c.async||c.gecko||c.opera)n[b].push({urls:a,callback:g,obj:f,context:h});else{d=0;for(i=a.length;d<i;++d)n[b].push({urls:[a[d]],callback:d===i-1?g:null,obj:f,context:h})}if(!m[b]&&(r=m[b]=n[b].shift())){s||(s=k.head||k.getElementsByTagName("head")[0]);a=r.urls;d=0;for(i=a.length;d<i;++d)g=a[d],o?e=c.gecko?p("style"):p("link",{href:g,rel:"stylesheet"}):(e=p("script",{src:g}),e.async=!1),e.className="lazyload",
e.setAttribute("charset","utf-8"),c.ie&&!o?e.onreadystatechange=function(){if(/loaded|complete/.test(e.readyState))e.onreadystatechange=null,j()}:o&&(c.gecko||c.webkit)?c.webkit?(r.urls[d]=e.href,t()):(e.innerHTML='@import "'+g+'";',u(e)):e.onload=e.onerror=j,q.push(e);d=0;for(i=q.length;d<i;++d)s.appendChild(q[d])}}function u(b){var a;try{a=!!b.sheet.cssRules}catch(c){h+=1;h<200?setTimeout(function(){u(b)},50):a&&l("css");return}l("css")}function t(){var b=m.css,a;if(b){for(a=v.length;--a>=0;)if(v[a].href===
b.urls[0]){l("css");break}h+=1;b&&(h<200?setTimeout(t,50):l("css"))}}var c,s,m={},h=0,n={css:[],js:[]},v=k.styleSheets;return{css:function(b,a,c,f){j("css",b,a,c,f)},js:function(b,a,c,f){j("js",b,a,c,f)}}}(this.document);
