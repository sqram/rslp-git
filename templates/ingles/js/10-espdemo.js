/*
 * Detects if we are on a mobile device.
 * User-agent sniffing isn't reliable, but Yolo.
 */
isDesktop = (function()  {
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
 		return false
	return true

})()



rs.demo_ver = 0;
				rs.demo_msglang = 'es-US';
				rs.demo_defaultlang = 'eng';
				rs.qb_default = 'eng';
				rs.qblang = "es-419";
//RSUI.util.LazyLoad.css([rs.cfg.css+"try3.css"]);
var rsdemo={};
rsdemo.test = false;
rsdemo.langname = '';
rsdemo.lblangselect = false;
rsdemo.ajaxSubmitURL = '/?p_p_id=rosettaajaxsubmit_WAR_rosettaajaxsubmitportlet&p_p_lifecycle=2';
//rs.fblogin = false;
if(typeof rs.fblogin == "undefined") {
	rs.fblogin = true;
}

/* facebook connect resources*/
var fAppId = ((rs.envName == "staging") ? rs.stg_facebook_app_id:rs.facebook_app_id);
RSUI.util.debug.log('FB ID: '+fAppId+' env='+rs.envName);
var fb_button_code = $(' <div id="fb-button-blk" style="display:none;float: left; padding-left: 53px; padding-top: 11px;font-size: 12px;"> <a class="fb_button fb_button_medium" id="aui_3_2_0_1758"><span class="fb_button_text" id="aui_3_2_0_1752">Login with Facebook</span></a> <b>Login using your facebook account</b></div> <div style="clear: both;"> &nbsp;</div>');
var fb_connect = false;


function fbEnsureInit(callback) {
    if(!window.fbApiInit) {
        setTimeout(function() {fbEnsureInit(callback);}, 50);
    } else {
        if(callback) {
            callback();
        }
    }
}

function addFB() {
	$("body").prepend('<div id="fb-root"></div>');

	window.fbAsyncInit = function() {

		(function(){
		FB.init({
			 appId      : fAppId,
			 status     : true, // check login status
			 cookie     : true, // enable cookies to allow the server to access the session
			 xfbml      : true,  // parse XFBML
			 oauth      : true,
			 channelUrl : window.location.protocol + '//' + window.location.hostname +'/facebook/channel'
		});
		})();
		fbEnsureInit(function() {
		    //console.log("this will be run once FB is initialized");
		});
		//"http://www.rosettastone.com/facebook/channel"//

		(function(d){//alert("Asyn2");
			var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = "//connect.facebook.net/en_US/all.js";
			ref.parentNode.insertBefore(js, ref);
		}(document));

		(function(){//alert("Asyn3");
			FB.getLoginStatus(function(response) {
				if (response.status === 'connected') {
					// connected
					//alert("connected");
				} else if (response.status === 'not_authorized') {
					// not_authorized
					//alert("not_authorized");
				} else {
					// not_logged_in
					//alert("not_logged_in");
				}
		 	});
		})();
	};

}

if(rs.fblogin){
	 RSUI.util.LazyLoad.js('//connect.facebook.net/en_US/all.js', function () {
		fb_connect = true;
		addFB();
	});
}

RSUI.util.LazyLoad.js('scripts/swfobject.js');
/*end fb connect*/
if (typeof rs.demo_msglang != 'undefined')
	rsdemo.lang_msging = rs.demo_msglang;
else
	rsdemo.lang_msging = 'en-US';
rsdemo.lang = 'es-ES'; //default demo lang -- will be set by user.
if (typeof rs.demoplaylang != 'undefined')
	rsdemo.playlang = rs.demoplaylang;
else
	rsdemo.playlang = 'es-US';

if (typeof rs.demo_ver != 'undefined')
	rsdemo.ver = rs.demo_ver; //this could be 1 2 or both
else
	rsdemo.ver = 1;

if(typeof rs.demo_skiplogin != 'undefined')
	rsdemo.skiplogin = rs.demo_skiplogin;
else
	rsdemo.skiplogin = false;

if(typeof rs.demo_skipbuynow != 'undefined')
	rsdemo.skipbuynow = rs.demo_skipbuynow;
else
	rsdemo.skipbuynow = false;
if(typeof rs.demo_optout != 'undefined')
	rsdemo.optout = rs.demo_optout;
else
	rsdemo.optout = false;
if(typeof rs.demo_defaultlang != 'undefined') {
	rsdemo.defaultlang = rs.demo_defaultlang; //spanish
	rsdemo.lang = langSetter(rsdemo.defaultlang,rsdemo.ver);
} else {
	rsdemo.defaultlang = 'esp'; //spanish
}

rsdemo.baseURL_v0 = 'http://www.rosettastone.com/us_assets/redesign/flash/personal_demo/';
rsdemo.swfFile_v0 = 'ahaMultiA.swf';
rsdemo.baseURL_v1 = 'http://resources.rosettastone.com/CDN/us/metternich/demo/';
rsdemo.swfFile_v1 = 'OnlineDemoContainer.swf';
rsdemo.baseURL_v2 = 'http://resources.rosettastone.com/CDN/us/rsdemo02/';
rsdemo.swfFile_v2 = 'rs_Demo_02.swf';
rsdemo.v0langs = ['tur','por','rus','grk'];
rsdemo.v1langs = ['eng','fra','deu','ita','esp','esc','sve'];
rsdemo.v2langs = ['eng','fra','deu','ita','esc','esp','sve'];




if(rsdemo.ver == 1) {
	rsdemo.baseURL = rsdemo.baseURL_v1;
	rsdemo.swfFile = rsdemo.swfFile_v1;
} else if(rsdemo.ver == 2) {
	rsdemo.baseURL = rsdemo.baseURL_v2;
	rsdemo.baseURL = rsdemo.swfFile_v2;
} else if(rsdemo.ver == 0) {
	rsdemo.baseURL = rsdemo.baseURL_v0;
	rsdemo.baseURL = rsdemo.swfFile_v0;
}


var fbappid = rs.facebook_app_id;
var demo_start_time_obj, demo_start_time = 0;

var translate = {
    "es-US": {
        "email_label": "por favor ingresa tu dirección de email:",
        "email_error": "Por favor, ascribe una dirección de email válida",
        "first_headline": "Estás a un paso del demo",
        "button_launch": "Iniciar Demo",
        "privacy_msg": "Para continuar, por favor ingresa tu dirección de email.",
        "privacy_link": "<a target=\"_blank\" href=\"http://www.rosettastone.com/esp-privacy-policy\">Política de privacidad</a>",
        "no_thanks": "No, Gracias",
        "lbheading": "Rosetta Stone Demo Para ",
        "facebook_loginlink": "Ingresar con Facebook",
        "facebook_msg": "Inicia la sesión con tu cuenta de Facebook",
        "questions": "¿Tienes alguna pregunta? Visita nuestra <a href=\"http://www.rosettastone.com/faq\">página de preguntas frecuentes</a>.",
        "shop_now": "Compra ya",
        "thank_you": "FELICIDADES<br> <span> por haber probado el demo!</span>",
        "ready_start": "¿Listo para comenzar tu viaje de aprendizaje de idiomas?",
				"target_url": "../ingles"
    },
    "en-US": {
        "email_label": "Please enter your email address",
        "email_error": "Please enter a valid email address",
        "first_headline": "Experience the award-winning Rosetta Stone method for yourself. Simply submit your e-mail address below to jump into our interactive software demo.",
        "button_launch": "Launch Demo",
        "privacy_msg": "Your privacy is important to us. We will not share your personal information with third parties without your consent. View our ",
        "privacy_link": "<a href=\"http://www.rosettastone.com/privacy\" target=\"_blank\">privacy policy</a>.",
        "no_thanks": "No Thanks",
        "lbheading": "Rosetta Stone Demo For ",
        "facebook_loginlink": "Login with Facebook",
        "facebook_msg": "Login using your facebook account",
        "questions": "Any questions? Visit our <a href=\""+rs.site_uri+"/faq\">FAQs</a>.",
        "shop_now": "Shop Now",
        "thank_you": "THANK YOU<br> <span> For Trying Our Demo!",
        "ready_start": "Ready to start your language journey?",
				"target_url": "../lp/sbs"
    },
	"de-DE": {
		"email_label": "Please enter your email address",
        "email_error": "Please enter a valid email address",
        "first_headline": "Lernen Sie in nur wenigen Minuten lhre ersten SÃ¤tze in Englisch (Amerikanisch). Sie werden Ã¼berrascht sein, wie schnell Sie mit Rosetta Stone Fortschritte machen werden. Bitte beachten Sie der Download enthÃ¤lt nur Rosetta Stone Course.",
        "button_launch": "Launch Demo",
        "privacy_msg": "Your privacy is important to us. We will not share your personal information with third parties without your consent. View our ",
        "privacy_link": "<a href=\"http://www.rosettastone.com/privacy\" target=\"_blank\">privacy policy</a>.",
        "no_thanks": "No Thanks",
        "lbheading": "Rosetta Stone Demo For ",
        "facebook_loginlink": "Login with Facebook",
        "facebook_msg": "Login using your facebook account",
        "questions": "Any questions? Visit our <a href=\""+rs.site_uri+"/faq\">FAQs</a>.",
        "shop_now": "Shop Now",
        "thank_you": "THANK YOU<br> <span> For Trying Our Demo!",
        "ready_start": "Ready to start your language journey?",
				"target_url": "../qbc"
	}
};



$(document).ready(function() {

	if($('#form-demo').length == false) {
		$('body').append('<form style="display:none;" id="form-demo" class="form" onsubmit="return false"> <input type="hidden" style="width: 212px; margin-bottom:10px; float:none;" value="'+translate[rsdemo.lang_msging].email_label+'" id="email-default-text" name="email"> <input type="hidden" style="width: 212px; margin-bottom:10px; float:none;" value="'+translate[rsdemo.lang_msging].email_error+'" id="field-email-error"> <input name="targetform" id="targetform" type="hidden" value="'+rsdemo.ajaxSubmitURL+'"> <input name="formurl" id="formurl" type="hidden" value="/web/usa/demo"> <input name="cis_name" id="cis_name" type="hidden" value="Flash demo"> <input name="selectedLang" id="selectedLang" type="hidden" value="eng"> <input name="sitecode" id="sitecode" type="hidden" value="'+rs.siteCode+'"> <input name="source" id="source" type="hidden" value="demo"> </form>');
	}
	if($('#demoform').length == false) {
		$('body').append('<div style="display: none;" id="demoform" class="overlay"> <div style="background:#f5f5f5;border-radius:  25px 25px 28px 28px;"><div class="olfooter" style="display: block;"> <p class="olheading"> '+translate[rsdemo.lang_msging].lbheading+' <span id="first-lang-selected"></span></p> </div> <div class="olcontent"> <img src="../../globals/img/demo-lightbox-image.jpg" style="border-width: 0px; border-style: solid; width: 293px; height: 305px;"> <div class="cont-right"> <div id="form_div"> <div id="language_full"> <p>'+translate[rsdemo.lang_msging].first_headline+'</p> </div> <div id="flash-demo-lang" style="display: block;"><div id="lightbox-langselect" style="position: relative; display: none;"><div id="lang-select-button" class="link-demo" style="cursor: pointer; float: left; display: inline; margin-left: 0px;"> <a href="#"><em class="select-down">English (American)</em></a></div><div id=\"trythedemo_list\" style=\"display: none; \">\r\n\t<ul id=\"lbdemolang\" class=\"drpdown lang-list\" style=\"list-style-type: none;\"><li id=\"eng\" class=\"first-child\"><a href=\"javascript:void(0);\">Inglés (Estadounidense)<\/a><\/li><li id=\"fra\"><a href=\"javascript:void(0);\">French<\/a><\/li><li id=\"deu\"><a href=\"javascript:void(0);\">German<\/a><\/li><li id=\"grk\"><a href=\"javascript:void(0);\">Greek<\/a><\/li><li id=\"ita\"><a href=\"javascript:void(0);\">Italian<\/a><\/li><li id=\"por\"><a href=\"javascript:void(0);\">Portuguese (Brazilian)<\/a><\/li><li id=\"rus\"><a href=\"javascript:void(0);\">Russian<\/a><\/li><li id=\"esp\"><a href=\"javascript:void(0);\">Spanish (Latin America)<\/a><\/li><li id=\"esc\"><a href=\"javascript:void(0);\">Spanish (Spain)<\/a><\/li><li id=\"sve\"><a href=\"javascript:void(0);\">Swedish<\/a><\/li><li id=\"tur\" class=\"last-child\"><a href=\"javascript:void(0);\">Turkish<\/a><\/li><\/ul><\/div><\/div><input id="field-email" type="text" value="" data_label="'+translate[rsdemo.lang_msging].email_label+'" class=""> <span class="dropshadow" id="online-demo-button"><a class="button blue gradient" id="light-demo"><em class="icon-right">'+translate[rsdemo.lang_msging].button_launch+'</em></a></span> <!--<div id="no-thanks" style="display:none;"> <a onclick="javascript:void(0)">'+translate[rsdemo.lang_msging].no_thanks+'</a></div>--> </div> </div><div id="fb-button-blk" style="float: left; padding-left: 53px; padding-top: 11px;font-size: 12px;"> <a class="fb_button fb_button_medium"><span class="fb_button_text">'+translate[rsdemo.lang_msging].facebook_loginlink+'</span></a> <b>'+translate[rsdemo.lang_msging].facebook_msg+'</b></div> <div style="clear: both;"> &nbsp;</div><div class="footer">'+translate[rsdemo.lang_msging].privacy_msg+' '+translate[rsdemo.lang_msging].privacy_link+'</div> </div> </div> <div style="clear:both" /></div></div></div>');
	}
	if($('#email-entered').length == false) { // comment this block out to hide the demo close out page.
		$('body').append('<div style="display: none;" id="email-entered" class="overlay"> <div class="thankscontent"> <p class="titlefont"> '+translate[rsdemo.lang_msging].thank_you+'</span></p> <p class="tcontfont1">'+translate[rsdemo.lang_msging].ready_start+'</p> <p class="faqlink"> '+translate[rsdemo.lang_msging].questions+'</p> <div style="text-align:center;"><a class="blue-button shop-now-button"><span style="display:inline-block;">'+translate[rsdemo.lang_msging].shop_now+'</span></a></div></div></div>');
	}
	if($('#demo').length == false) {
		$('body').append('<div style="height: 630px; width: 990px; display: none;" id="demo" class="overlay">'
				+'</div>');

	}

	if(rsdemo.optout) {
		$('#no-thanks').show();
		$('#no-thanks a').click(function() { rsdemo.skiplogin = true; $('#online-demo-button a').trigger('click');  });
	}

	//SET DEFAULT LANG UI
	$('ul.lang-list li').removeClass('selected');
	$('ul.lang-list li#'+rsdemo.defaultlang).addClass('selected');
	rsdemo.langname = $('ul.lang-list li#'+rsdemo.defaultlang+' a').html();
	$('.lang-select em').html(rsdemo.langname);

	//RSUI.util.debug.log('defaulting to:'+rsdemo.langname);

	//LANG SELECTOR IN LIGHTBOX
	if($('#demolang').length == false) {
		rsdemo.lblangselect = true;
		$('#lightbox-langselect').show();
		changeText();
		$('#first-lang-selected,#lightbox-langselect #lang-select-button a em').html(rsdemo.langname);
		RSUI.util.debug.log('no lang select');
	}


	/*
	 * When a language in this demo list is clicked, set rsdemo.lang so the demo loads the right language
	*/
	$('ul.lang-list li a').add('ul.lang-list2 li a').click(function() {
    $('ul.lang-list li').add('ul.lang-list2 li').removeClass('selected');
    $(this).parent('li').addClass('selected');
		$('#lang-select-button span.label').html($(this).html());
		$('#trythedemo_list').hide();
		var clicked_lang = $(this).parent('li').attr("id");
		rsdemo.lang = langSetter(clicked_lang,rsdemo.ver);
		if(rsdemo.lblangselect) {
		 	changeTextOp();
			$('#first-lang-selected').html(rsdemo.lang);
			RSUI.util.debug.log('changeText() function');
		}
		RSUI.util.debug.log('demo lang set to: '+rsdemo.lang);

  });

	var defaultEmailText = $('#email-default-text').val();
	var errorEmailText = $('#field-email-error').val();

	$('#lang-select-button').click(function() {
		toggleElement($('#language-selection-demo'));
	});

	function demoload() {

		var lang = $('#demolang li.selected a').attr('id') == undefined ? 'hi-en' : 'hi-'+$('#demolang li.selected a').attr('id')

	if (!isDesktop) {
		setTimeout(function() {
			window.location.href = 'http://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=' + lang
		}, 4000)

	} else {


		var demoFrame = ""
			+ "<iframe "
			+ " id='demo'"
			+ " src='http://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=" + lang + "'"
			+ " width='100%'"
			+ " height='100%'"
			+ " scrolling='no'"
			+ "></iframe>"
			;

		// Append iframe inside lightbox
	//	$("#demo").append(demoFrame)


		$.colorbox({
			opacity : 0.7,
			html: demoFrame,
			height:"100%",
			width:"100%",
			fixed: false,
			className:'demolightbox',
			close:'<a class="close"></a>',
			scrolling:false,
			overlayClose:false,
			fadeOut : 300,
			onClosed: function() {


					//var email = $('#field-email').val();
					if (rsdemo.skipbuynow == false) {

						if ((rsdemo.email.toLowerCase() != defaultEmailText.toLowerCase()) && (rsdemo.email.toLowerCase() != errorEmailText.toLowerCase())) {

							$.colorbox({
								opacity : 0.7,
								html: $("#email-entered").html(),
								className:'demolightbox',
								fixed: false,
								close:'<a class="close"></a>',
								overlayClose:false,
								fadeOut : 300,
								onComplete: function(){
									$('.blue-button.shop-now-button').click(function(e){
										e.preventDefault();
										$.colorbox.close();
									});
								}});
						} else {

							$.colorbox({
								opacity : 0.7,
								html: $("#no-email").html(),
								className:'demolightbox',
								fixed: false,
								close:'<a class="close"></a>',
								overlayClose:false,
								fadeOut : 300});
						}
					}
			},
			onComplete : function() {
				//$('#exposeMask').show();
				s.eVar8=RSUI.util.getCookie("rsdemocount");
				s.events="event23";
				s.tl();
			}
		});
	}
}

	function submitAndOpenDemo()
	{


				var curEmailId = $('#field-email').val();
//alert(curEmailId + $('#selersdemorsdemo.email = curEmailId;
				var validemail = validateEmail(curEmailId);

			if (validemail) {
				rsdemo.affiliate_cid = '';
				RSUI.util.debug.log('email set to:'+curEmailId+' which validated true:: Posting to '+$("#targetform").val()+' demo lang set to:'+$('#selectedLang').val()+' Path:'+location.pathname+'## cookie:'+rsdemo.affiliate_cid);
				if(RSUI.util.getCookie('affiliate') != 'undefined' && RSUI.util.getCookie('affiliate') !=null ) {
					rsdemo.affiliate_cid = RSUI.util.getCookie('affiliate').trim();
				}
				
				var demoType = isDesktop ? 'Hispanic_Demo_Desktop' : 'Hispanic_Demo_Mobile' 

				$.post(rsdemo.ajaxSubmitURL, {
					email : curEmailId,
					demo_lang : $('#selectedLang').val(),
					cis_name : 'US Hispanic Demo Leads',
					website : 'EH_WEBSITE',
					form_type : 'demo',
					demo_type : demoType,
					newsletter_type : "Bottom_Landing_Ingles",
					form_url : window.location.pathname,
					cid : _satellite.getVar('mostrecentcampaign')
				},function(data) {
					if (data.length) {
					if (data[0].cisFlag == "true") {
						RSUI.util.setCookie("rsdemo", true);
							RSUI.util.setCookie("rsdemocount", '1');
							s.events="event19";
							s.t();
							delete s.events;
							convertroLead(curEmailId);
							doublePositiveConversionCall();
							msnPixel();
							//rocketFuel();
							liveIntent();
							rocketFuelPurchaseConversion();
							vsntrk();
							emiles();
							nanigans();
							myPointsPixel();
							directAgentsPixel(curEmailId);
							mediaWhizPixel(curEmailId);
							telicPixel();
							thirtythreeAcross();
							AMGDGT();
							commissionJunction();
							owneriq();
							resonate();
							burstDemoPixel();
							trueViewPixel();
							adwordsPixel();
							googleDemoConversion();
							adwordsRemarketingPixel();
							mniPixel();
							marinPixel(curEmailId);
						}
					}
				}, "json"); //post

				//$('#demoform').colorbox.close();
				 demoload();
			} else
			{
				$("#field-email").val(errorEmailText);
				$("#field-email").attr("data_label", errorEmailText);
				$('#field-email').css('border', '1px solid red');
			}
			return false;
	}

	function demoform()
	{
		$.colorbox({
			opacity : 0.7,
			html: $("#demoform").html(),
			className:'demolightbox-form',
			close:'<a class="close"></a>',
			overlayClose:false,
			fadeOut : 300,
			scrolling: false,
			onLoad : function() {
				//$("#demoform").show();
				//$("#cboxContent").css("background", "#f6f6f6");
				if(fb_connect && rs.fblogin) {

					$('#demoform .footer').before(fb_button_code);

				}

			},
			onComplete : function() {
				//var overlay = this.getOverlay();
				//var form = overlay.find(form);
				$('#online-demo-button a').bind("click", submitAndOpenDemo);
				$(document.body).keyup(function(e) {
				//$('#field-email').bind('keyup', function(e) {
					var code = (e.keyCode ? e.keyCode : e.which);

					if(code == 13 && $('#field-email').is(':focus')) { //Enter keycode
					//e.preventDefault();
					//alert('click');
					submitAndOpenDemo();
					   //$('#online-demo-button a').trigger('click');
					   //$('#field-email').parent(form).submit();
					 }
					  return false;
				});

		        $('.fb_button').click(function() {
		        	/***/
			    	FB.login(function(response) {
						  if (response.authResponse) {
							  // user has auth'd your app and is logged into Facebook
							  FB.api('/me', function(me) {
								  if (me.email) {
									  $('#field-email').val(me.email);
									  registerEmail(me.email);
								  }
							  });
						   }
						   else {
							  s.events="event22";
							  s.t();
							  s.events="";
						  }
			    	}, {scope: 'email'});
		    	/***/

			   });

			}
		});
	}

	$('#email-entered').colorbox({
		fadeOut : 200,
		opacity : 0.7,
		className:'demolightbox',
		onComplete: function() {
			//$('#exposeMask').show();
		},
		onCleanup: function() {
			//$('#exposeMask').fadeOut('fast').remove();
		}
	});

	$('#no-email').colorbox({
		fadeOut : 200,
		opacity : 0.7,
		className:'demolightbox',
		onLoad: function() {
			//$('#exposeMask').show();
		},
		onClose: function() {
			//$('#exposeMask').fadeOut('fast').remove();
		}
	});

	$('#online-demo-lightbox-button a, #online-demo-no-lang-select, #tryitfree-demo-lightbox-button a').click(function(e) {

			e.preventDefault();
			RSUI.util.debug.log('demo init button clicked');
			var demoCookie = RSUI.util.getCookie("rsdemo");
			var demoCookieCount = RSUI.util.getCookie("rsdemocount");
			$('#online-demo-lightbox-button').click(function (e) {

				e.preventDefault();
				demoCookieCount++;
				RSUI.util.setCookie("rsdemocount",demoCookieCount);
			});
			//if (typeof demoCookie != 'undefined' && demoCookie != false && demoCookie !='')
			RSUI.util.debug.log('demo cookie-->'+demoCookie);
			//s.eVar7=$('#selectedLang').val();
			if(demoCookie=='true')
				rsdemo.skiplogin = true;
			if(rsdemo.skiplogin) {
				changeText();
				RSUI.util.debug.log('No email required');
				 demoload();
			} else {
				demoform();
				$("#field-email").val(defaultEmailText);
				$("#field-email").focus(function(){
					if($(this).val() == $(this).attr("data_label"))
						$("#field-email").val("");
				});

				$("#field-email").blur(function(){
					if($(this).val() == "")
						$("#field-email").val($(this).attr("data_label"));
				});
				changeText();
				RSUI.util.debug.log('changeText() fired');
			//	s.eVar7 = $("#lang-select-button a em").html();

			}
			/*s.events = "event18";
			s.events = "event1"
			s.t();
			s.events="";
			delete s.eVar7;
			delete s.events;*/
	});


	$("#no-thanks").click(function() {
		$('#demo').overlay().load();
		_gaq.push(['_trackPageview', '/GA-demo-no-email-entered']);
	});

/*
	$("#online-demo-button a em").click(function(){
		changeText()
		});
	$("#demolang li a").live("click", function(event)
	{
		event.preventDefault();
		var longlang = $(this).parent().attr("id");

		if(longlang == 'eng')
			rsdemo.lang = 'en-US';
		else if (longlang == 'esc')
			rsdemo.lang = 'es-419';
		else if (longlang == 'esp')
			rsdemo.lang = 'es-ES';
		else if (longlang == 'deu')
			rsdemo.lang = 'de-DE';
		else if (longlang == 'fra')
			rsdemo.lang = 'fr-FR';
		else if (longlang == 'ita')
			rsdemo.lang = 'it-IT';
		else if (longlang == 'sve')
			rsdemo.lang = 'sv-SE';

		$("#selectedLang").val($(this).parent().attr("id"));
		$('#language-selection-demo').fadeOut('fast');
		$('#lang-select-button a em').html($(this).html());

	});
*/
	$('input').focus(function()
	{

		$(this).attr('data_label', $(this).val());
		if ($(this).val().toLowerCase() == defaultEmailText.toLowerCase() || $(this).val().toLowerCase() == errorEmailText.toLowerCase()) {
			$(this).val('');
		}
	});
	$('input').blur(function()
	{
		if ($(this).val().length == 0) {
			$(this).val($(this).attr('data_label'));
		}
	});


});



//}); //lazyload

function changeText() {
	var langId = $('ul.lang-list li.selected').attr('id');
	$('#form-demo #selectedLang').val(langId);
	var langName = $('.select-down').html();
	var langLink = "/buynow";
	if(langId == "eng"){
		langLink = "/learn-english";
		if(rsdemo.lang_msging == 'es-US')
			langLink = "/ingles";

	} else if(langId == "fra"){
			langLink = "/learn-french";
	} else if(langId == "deu"){
			langLink = "/learn-german";
	} else if(langId == "ita"){
			langLink = "/learn-italian";
	} else if(langId == "esc"){
			langLink = "/learn-spanish-spain";
	} else if(langId == "esp"){
			langLink = "/learn-spanish";
	} else if(langId == "sve"){
			langLink = "/learn-swedish";
	} else if(langId == "tur"){
			langLink = "/learn-turkish";
	} else if(langId == "grk"){
			langLink = "/learn-greek";
	} else if(langId == "por"){
		langLink = "/learn-portuguese";
	} else if(langId == "rus"){
			langLink = "/learn-russian";
	}
	if(typeof rs.cid != 'undefined')
			langLink = langLink+'?cid='+rs.cid;

	$("#first-lang-selected").html(langName);
	$("#second-lang-selected").html(langName);
	$("#third-lang-selected").html(langName);
	$("#third-lang-selected").parent().attr('href',langLink);
	$('.shopnowlink a').attr('href',langLink);
}


function validateEmail(email)
{
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function registerEmail(emailval) {
	$.post($("#targetform").val(), {
	email : emailval,
	lang : $('#selectedLang').val(),
	source : $('#source').val(),
	cis_name : $("#cis_name").val(),
	sitecode : $("#sitecode").val(),
	formurl : $("#formurl").val()
	}, function(data) {
		if (data != "")
		{
			data = eval("("+data+")");
			if (data[0].cisFlag == "true")
			{
				s.events="event21";
				s.t();
				delete s.events;
				_gaq.push(['_trackPageview', '/GA-demo-lead']);
				$('#demo').overlay().load();
				$('body').append('<iframe allowtransparency="true" scrolling="no" frameborder="0" border="0" width="1" height="1" marginwidth="0" marginheight="0" background-color="transparent" src="https://ad3.adfarm1.adition.com/track?tid=4855&sid=11950&type=html&orderid=&itemno=&descr=&quantity=&price=0.00&total=252.00"></iframe>');
				$('body').append('<img src="https://farm.plista.com/activity2;domainid:13334;objectid:;event:31" alt="" style="width:0px;height:0px;" />');
			}
			else
			{
				$("#field-email").val(errorEmailText);
				$("#field-email").css('border', '1px solid red');
			}
		}
		else
		{
			$("#field-email").val(errorEmailText);
			$('#field-email').css('border', '1px solid red');
		}
	}, "");
}
/**
 *
 */

/**
 *
 * @param shortcode
 * @param ver
 * @returns {String}
 */
function langSetter(shortcode,ver) {
	var lang = "";
	if(shortcode == 'eng')
		if(ver == 0)
			lang = 'english';
		else
			lang = 'en-US';
	else if (shortcode == 'esc')
		lang = 'es-419';
	else if (shortcode == 'esp')
		lang = 'es-ES';
	else if (shortcode == 'deu')
		lang = 'de-DE';
	else if (shortcode == 'fra')
		lang = 'fr-FR';
	else if (shortcode == 'ita')
		lang = 'it-IT';
	else if (shortcode == 'sve')
		lang = 'sv-SE';
	else if (shortcode == 'grk')
		lang = 'greek';
	else if (shortcode == 'rus')
		lang = 'russian';
	else if (shortcode == 'tur')
		lang = 'turkish';
	else if (shortcode == 'por')
		lang = 'portuguese';
	return lang;
}

function convertroLead(curEmailId){
		$CVO = window.$CVO || [];
		$CVO.push([ 'trackEvent', {
		   type: 'lead-language-demo',
		   id: null,
		   amount: '1'
		}]);
		function djb2_hash (s) {
			var hash = 5381;
			var max = Math.pow(2, 32);
			for (i = 0; i < s.length; i++) {
				var c =  s.charCodeAt(i);
				hash = (33 * hash + c) % max;
			}
			return hash;
		}
		var hashed_userid = djb2_hash(curEmailId);
		$CVO = window.$CVO || [];

		$CVO.push([ 'trackUser', {
			id: hashed_userid
		}]);
		//console.log('new convertro pixel fired');


}

function nanigans() {
	//console.log("User ID: "+nani_user_id);
	var imgsrc= '\/\/api.nanigans.com\/event.php?app_id=18060&type=install&name=reg';
	jQuery("body").append("<img src=\"" + imgsrc + "\" height=\"1\" width=\"1\"/>");
	//$('body').append('<img style=\"display:none\" src=\"\/\/api.nanigans.com\/event.php?app_id=18060&type=install&name=reg&user_id=111\" \/>');
}

function emiles() {
	$('body').append('<img src=\"https://www.e-miles.com/autocredit.do?pc=5248KUXSM2FSK7M&icampaignID=67BB8B\" width=1 height=1>');
}

function liveIntent() {
	$('body').append('<img src=\"https:\/\/p.liadm.com\/p?c=2058\"\/>');
}

function vsntrk() {
	var cache_buster = parseInt(Math.random()*99999999);
	$('body').append('<img src=\"http://vsntrk.com/p.ashx?o=175&f=img&t=TRANSACTION_ID\" width=\"1\" height=\"1\" border=\"0\" />');
}

function rocketFuelPurchaseConversion() {
	var cache_buster = parseInt(Math.random()*99999999);
	$('body').append('<img src=\"http://20497995p.rfihub.com/ca.gif?rb=3087&ca=20497995&ra=' + cache_buster + '\" height="0\" width=\"0\" style=\"display:none\" alt="Rocket Fuel"/>');
}

function googleConversionCall() {
	//Google conversion
	var google_conversion_id = "1020961049";
	var google_conversion_label = "XWafCOfQ9AIQmcLq5gM";
	image = new Image(1,1);
	image.src = "http://www.googleadservices.com/pagead/conversion/"+google_conversion_id+"/?label="+google_conversion_label +"&amp;guid=ON&script=0";
}

function msnPixel() {
	//msn
	$('body').append('<img height="1" width="1" src="http://view.atdmt.com/action/rosettastoneconversion"/>');
}

function myPointsPixel() {
	//My Points
	var visit_id = RSUI.util.getCookie("MY_TRACK");
	if(visit_id !="undefined" && visit_id !=null)
	{
		$('body').append('<img src="http://www.mypoints.com/emp/u/'+visit_id+'/A/ctr.gif" height="1" width="1">');
	}
}

function mediaWhizPixel() {
	//Media Whiz
	$('body').append("<iframe src=\"https://www.mitwodotoh.com/Beacon?o=6216&ref=na&lvl=1&amt=\" width=\"0\" height=\"0\" frameborder=\"0\" marginheight=\"0\" marginwidth=\"0\" scrolling=\"no\"></iframe><noframes><img src=\"https://www.mitwodotoh.com/Pixel?o=6216&ref=na&lvl=1&amt=\" height=\"1\" width=\"1\" border=\"0\" /></noframes>");
}

function telicPixel() {
	//Telic
	$('body').append('<img src="http://jump.tvitrack.com/aff_l?offer_id=210" width="1" height="1" />');
}

function thirtythreeAcross() {
	$('body').append('<img src="https://secure.33across.com/ps/?pid=842&amp;cgn=15724&amp;seg=25890"style="visibility:hidden;width:1px;height:1px;">');
}

function AMGDGT() {
	$('body').append('<!-- Begin: AMGDGT Tag -->\r\n<script language=\"Javascript\">amgdgt_ctr=\"11984\";amgdgt_t=\"x\";<\/script><script type=\"text\/javascript\" src=\"https:\/\/cdns.amgdgt.com\/base\/js\/v1\/amgdgt.js\"><\/script>\r\n<noscript><iframe src=\"https:\/\/ad.amgdgt.com\/ads\/?f=i&t=x&ctr=11984&rnd='+Math.floor(Math.random()*99999999999)+'\" width=\"1\" height=\"1\" frameborder=\"0\"><\/iframe><\/noscript>\r\n<!-- End: AMGDGT Tag -->');
}
function owneriq() {
	$('body').append('<img src="<http://px.owneriq.net/ep?sid%5B%5D=367688033&sid%5B%5D=367708758&rid%5B%5D=1683373&rid%5B%5D=1683377>" width="1" height="1" style="display: none;"/>');
}

function resonate() {
	$('body').append('<IMG SRC="https://ds.reson8.com/insights.gif?rand=%n&t=0&pixt=dfpdir&advkey=0013000001977PxAAI&opptykey=RSIM0812A&evkey=105607&evtype=custom&fl=F1" WIDTH=1 HEIGHT=1 BORDER=0>');
}
function directAgentsPixel(curEmailId) {
	$('body').append('<iframe src="http://smarttrk.com/p.ashx?o=30101&t='+curEmailId+'" height="1" width="1" frameborder="0"></iframe>');
}
function commissionJunction() {
	var guid = GUID();
	$('body').append('<iframe height="1" width="1" frameborder="0" scrolling="no" src="https://www.emjcd.com/tags/c?containerTagId=1584&AMOUNT=0&CID=1500591&OID='+guid+'&TYPE=355357&CURRENCY=0" name="cj_conversion" height="1" width="20" ></iframe>');
}
function doublePositiveConversionCall() {
	//Double Positive
		$('body').append('<iframe src="http://bid-tag.com/Ads/8-21-885" frameborder="0" scrolling ="n" marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" allowtransparency="true" width="1" height="1"></iframe>');
}

function burstDemoPixel() {
	var script   = document.createElement("script");
	script.type  = "text/javascript";
	script.src   = "http://reporting.burstdirect.com/pixel.js?cid=24605&trid=";
	document.body.appendChild(script);
}
//Caitlin's Pixel //48298
function trueViewPixel() {
	$('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1013815393/?value=0&amp;label=6lOwCPePnAMQ4bC24wM&amp;guid=ON&amp;script=0" />');
}
//Katherine's Pixel //53989
function googleDemoConversion() {
	$('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1072735889/?value=0&amp;label=GW2JCL_60AMQkc3C_wM&amp;guid=ON&amp;script=0" />');
}
//Katherine's Pixel // 48252
function adwordsPixel() {
	$('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1072735889/?value=0&amp;label=GW2JCL_60AMQkc3C_wM&amp;guid=ON&amp;script=0" />');
}
function adwordsRemarketingPixel() {
	$('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1072735889/?value=0&amp;label=Pdx-CO_g8QIQkc3C_wM&amp;guid=ON&amp;script=0"/>');
}
function mniPixel() {
	$('body').append('<img src="http://mni.secure-adserver.com/Segment.aspx?sid=b0843ce6-c5a8-498d-8d2b-139687d0788e"/><img src="http://mni.secure-adserver.com/univPixel.aspx?g=856ab04a-2154-4772-ab01-c37bc314c53b"/>');

}
//48057
function marinPixel(emailid){
	$('body').append('<img width="1" height="1" src="http://tracker.marinsm.com/tp?cid=lch5m3a8o0&trans=UTM:I|' + emailid + '|emaillead||||">');
}

function GUID () {
    var S4 = function ()
    {
        return Math.floor(
                Math.random() * 0x10000 /* 65536 */
            ).toString(16);
    };

    return (
            S4() + S4() + "-" +
            S4() + "-" +
            S4() + "-" +
            S4() + "-" +
            S4() + S4() + S4()
        );
}

// This function is used for the flash demo to send messages to omniture.
		var score = {};
		var prevscore = {};
		prevscore.correct = 0; prevscore.wrong = 0; prevscore.skipped = 0;
		score.correct = 0; score.wrong = 0; score.skipped = 0;
		var demo_opened = 0;

		function trackFlash(msg) {

			var track1 = msg.split('|');
			var answer_text = "";
			var curStage;
			for (var i=0; i<track1.length; i++) {
				var track2 = track1[i].split(':');


				if (track1.length > 0) {
					for (var ii=0; ii<track2.length; ii++) {
						if(ii > 0) {

							if(ii == 3 || ii == 1) {
								if(track2[ii].match(/Pathstep\sIndex\s\d+/)) {
									var pathstepA = /Pathstep\sIndex\s\d+/.exec(track2[ii]);

									var pathstepB = /\d+/.exec(pathstepA[0]);

									answer_text = pathstepB[0];
								}
							}
						}
						else {
							if (i == 0 && track1[i] != "") {
								if (track1[i].indexOf('to Introduction') != -1) {
									demo_opened = 1;
									curStage = 'Introduction';
									s.prop6 = curStage;
									s.eVar3 = curStage;
									s.events = "event15";
									s.t();
								}
								else if (track1[i].indexOf('to RosettaCOURSE') != -1) {
									if (demo_opened == 1) {
										curStage = 'Interactive Demo';
										s.prop6 = curStage;
										s.eVar3 = curStage;
										s.events = "event15";
										s.t();
										s.prop6 = "Pathstep 1";
										s.eVar3 = "Pathstep 1";
										s.events = "event15";
										s.t();
										demo_opened = 0;
									}
								}
								else if (track1[i].indexOf('to RosettaSTUDIO') != -1) {
									demo_opened = 1;
									curStage = 'Live Training';
									s.prop6 = curStage;
									s.eVar3 = curStage;
									s.events = "event15";
									s.t();
								}
								else if (track1[i].indexOf('to RosettaWORLD') != -1) {
									demo_opened = 1;
									curStage = 'Games & Community';
									s.prop6 = curStage;
									s.eVar3 = curStage;
									s.events = "event15";
									s.t();
								}
								else if (track1[i].indexOf('Pathstep Index') != -1) {
									demo_opened = 1;
									curpathstep = parseInt(track1[i+1].replace(/.*Pathstep Index /,'').trim()) + 1;
									if (!isNaN(curpathstep)) {
										curStage = "Pathstep " + curpathstep;
										s.prop6 = curStage;
										s.eVar3 = curStage;
										s.events = "event15";
										s.t();
									}
								}

								delete s.events;
								delete s.eVar3;
								delete s.prop6;
							}
						}

					}
				}

				var scoreout = /Score.\(*\d.+\)/.exec(track1[i]);
				var scoreout = /[^\(]*\d[^\)]*/.exec(scoreout);
				if(i == 2) {
					if (track1[i].match(/Score.\(*\d.+\)/)) {
						var uls = $('ul',{id:'scores'});
						var scoreout = scoreout[0].split('/');
						score.correct = [scoreout[0]];
						score.wrong = [scoreout[1]];
						score.skipped = [scoreout[2]];
					}
				}//i=1
				var correct = Math.abs(score.correct - prevscore.correct);
				var wrong = Math.abs(score.wrong - prevscore.wrong);
				var skipped = Math.abs(score.skipped - prevscore.skipped);
				if ( correct > 0) {
					s.events = "event13";
					s.t();
					delete s.events;
				}
				if (wrong > 0) {
					s.events = "event12";
					s.t();
					delete s.events;
				}
			}
			prevscore.correct = score.correct;
			prevscore.wrong = score.wrong;
			prevscore.skipped = score.skipped;

		}


// To toggle about us content in demo pages
$(function(){
			$('.about-rs span').toggle(function(){
			$('.about-rs-content').fadeIn();
			$('.about-rs img').attr("src","../../globals/img/aboutus-up-arrow.png" );
			},function(){
			$('.about-rs-content').fadeOut();
			$('.about-rs img').attr("src","../../globals/img/aboutus-down-arrow.png" );
			});
});
