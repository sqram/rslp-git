$(function(){


	// for experiences 2 or 4 (3-month or 1-month, respectively)
	var current_experience = '2';


	// if 1-month is turned on (ws-1590)
	if(window.location.search.match(/onemonth=true/i)){

		// change experience to one-month (on desktop)
		current_experience = '4';

		// change data lvls from 3-month to 1-month (on mobile)
		$('*[data-lvl=03]').attr('data-lvl','01');

		// change product name from 3-month to 1-month (on mobile)
		$('.js_product_name').html(function(i,txt){return txt.replace('3-Month','1-Month')});

		// update prices and msrps to account for change from 3-month to 1-month (on mobile)
		window.update_prices();
		window.update_msrps();

		// update masthead (desktop and mobile)
		$('head').append('<style type="text/css"> body #masthead{ background-image: url(\'http://www.rosettastone.com/lp/spo/desktop_sbsr_nov_2016/assets/holiday_2016_1m_mobile.jpg\'); } @media(min-width:600px){ body #masthead{ background-image: url(\'http://www.rosettastone.com/lp/spo/desktop_sbsr_nov_2016/assets/holiday_2016_1m_desktop.jpg\'); } } </style>');
	}



	// *******************************************
	// INITIALIZE LANGUAGE AND PAGE
	// *******************************************

	window.current_lang = 'esp';

	// can turn figs-only state on or off (using true or false)
	function isFIGS(lang){
		var figs_only = false;
		return figs_only ? /esp|fra|ita|deu/i.test(lang) : true;
	}


	function getLangUrlParam(){
		return ( (window.location.search.match(/lang(uage)*=([^&]+)/i)||[])[2] || '').toLowerCase();
	}


	function respondToPreselectedLang(){

		// if lang in url parameter
		if( getLangUrlParam() && isFIGS(getLangUrlParam()) ){
			$(document).trigger('lang_change', {
				'lang': getLangUrlParam()
			});
		}

		// if lang in catalog
		if(isCatalog() && isFIGS(getCatalogLang())){
			$(document).trigger('lang_change', {
				'lang': getCatalogLang()
			});
		}
	}

	function isCatalog(){
		return window.location.pathname.match(/learn-/i);
	}

	function getCatalogLang(){
		return window.lang;
	}















	// *******************************************
	// INIT - RENDER EXPERIENCES AND FILL IN DATA
	// *******************************************

	function initPage(current_experience){

		render_experience(current_experience);

		set_up_pre_language();

		respondToPreselectedLang();

		$(document).trigger('price_update');
		$(document).trigger('cart_update');
	}












	// *******************************************
	// EMIT LANGUAGE CHANGE EVENTS
	// *******************************************


	// emit event when language changes in test mode
	$('#temporary_lang_dropdown').on('change',function(){
		var lang = this.value;

		$(document).trigger('lang_change', {
			'lang': lang
		});
	});


	// emit event when language changes on sbsr or ppc
	$('.language-buy-list a').click(function(){
		var lang = this.id;

		$(document).trigger('lang_change', {
			'lang': lang
		});
	});










	// *******************************************
	// RESPOND TO LANGUAGE CHANGE EVENTS
	// *******************************************


	// update everything when language changes
	$(document).on('lang_change', function(e,data){

		// show correct button copy
		var button_copy = 'Add to Cart';
		if(window.location.search.match(/gifting=1/i)){
			button_copy = 'Give as a Gift';
		}

		// restore cart button functionality now that we have a language
		$('.js_oct_2016_addtocart').off('click').html(button_copy);

		var lang = data.lang;

		current_lang = lang; // just so this is available

		check_if_s3_lang(lang);

		// if top langs, run test code
		if(lang.length===3 && isFIGS(lang)){

			// show test products and hide regular products
			// $('#multivariate_container').show();
			// $('#products').hide();

			var lang_map = {
				esp: 'Spanish (Latin America)',
				eng: 'English (American)',
				fra: 'French',
				deu: 'German',
				ita: 'Italian',
				ara: 'Arabic',
				chi: 'Chinese (Mandarin)',
				ned: 'Dutch',
				ebr: 'English (British)',
				tgl: 'Filipino (Tagalog)',
				grk: 'Greek',
				heb: 'Hebrew',
				hin: 'Hindi',
				gle: 'Irish',
				jpn: 'Japanese',
				kor: 'Korean',
				far: 'Persian (Farsi)',
				pol: 'Polish',
				por: 'Portuguese (Brazil)',
				rus: 'Russian',
				esc: 'Spanish (Spain)',
				sve: 'Swedish',
				tur: 'Turkish',
				vie: 'Vietnamese'
			};

			var lang_name_parts = {
				first : lang_map[lang].split(' (')[0],
				last : (lang_map[lang].match(/\(.+/)||[])[0],
				full : lang_map[lang]
			};

			// update language display names
			$('.js_langname_1').html(lang_name_parts.first);
			$('.js_langname_2').html(lang_name_parts.last);
			$('.js_langname').html(lang_name_parts.full);

			// update cart buttons
			$('.js_oct_2016_addtocart').attr('data-lang', lang);

			$(document).trigger('cart_update');
		}


		// if not top langs, hide test and show regular products
		else{
			// $('#multivariate_container').hide();
			// $('#products').show();
		}
	});


	function hasS5(lang){
		return !!RSI({cat:lang, lvl:'S5'})[0];
	}

	function check_if_s3_lang(lang){

		if(hasS5(lang)){
			// enable s5 cart button
			$('.js_oct_2016_no_s5').removeClass('js_oct_2016_no_s5').addClass('js_oct_2016_addtocart');

			// show s5 product
			$('.js_oct_2016_addtocart[data-lvl=S5]').closest('.js_oct_2016_product').show();
		}

		if(!hasS5(lang)){
			// hide s5 product
			$('.js_oct_2016_addtocart[data-lvl=S5]').closest('.js_oct_2016_product').hide();

			// disable s5 cart button
			$('.js_oct_2016_addtocart[data-lvl=S5]').removeClass('js_oct_2016_addtocart').addClass('js_oct_2016_no_s5');
		}
	}













	// *******************************************
	// SET CORRECT PRICES EVERYWHERE
	// *******************************************

	function update_prices(selector){

		var prices = document.querySelectorAll(selector);
		var rsidata, lvl, media, isMsrp, multipay_divisor, msrp, price;

		for(var i=0; i<prices.length; i++){
			lvl = prices[i].getAttribute('data-lvl');
			media = prices[i].getAttribute('data-media');
			isMsrp = prices[i].getAttribute('data-is-msrp');

			multipay_divisor = prices[i].getAttribute('data-multipay-divisor');

			rsidata = RSI({'lvl':lvl, 'media':media})[0];

			// check if rsidata exists (to avoid errors such as looking for S5 data in an S3 product)
			if(rsidata){

				msrp = multipay_divisor ? Math.ceil(rsidata.msrp/multipay_divisor) : rsidata.msrp;
				price = multipay_divisor ? Math.ceil(rsidata.price/multipay_divisor) : rsidata.price;

				if(isMsrp){
					prices[i].innerHTML = msrp;

					if(msrp===price){
						prices[i].parentElement.parentElement.style.visibility = 'hidden';
					}
				}
				else{
					prices[i].innerHTML = price;
				}
			}
		}
	}










	// *******************************************
	// TOGGLE GIFTING
	// *******************************************

	$('.js_oct_2016_gifting').on('change', function(){

		// update data-gifting attribute on nearest cart button
		$(this).closest('.js_oct_2016_product').find('.js_oct_2016_addtocart').attr('data-gifting', this.checked || '');

		// tell cart buttons about the change
		$(document).trigger('cart_update');
	});








	// *******************************************
	// TOGGLE CD AND DOWNLOAD
	// *******************************************

	$('.oct_2016_cd_download').on('change', function(){
		$(document).trigger('cd_download_toggle', {media: this.value});
	});








	// *******************************************
	// RESPOND TO CD/DOWNLOAD TOGGLE EVENT
	// *******************************************

	$(document).on('cd_download_toggle', function(e,data){

		// box or download
		var media = data.media;

		// full names of cd and download
		var item_name_map = {
			'box' : 'CD-ROM',
			'download' : 'Instant Download'
		};
		var item_name = item_name_map[media];

		// show item names
		$('.js_cddl_title').html(item_name);

		// update data-media for all cd/dl elements
		$('.js_cddl_media').attr('data-media', media);

		// tell prices about changes
		$(document).trigger('price_update');

		// tell cart buttons about changes
		$(document).trigger('cart_update');
	});








	// *******************************************
	// PUT CORRECT URLS IN EACH CART BUTTON
	// *******************************************

	function update_cart_urls(cart_selector){

		$(cart_selector).attr('href', function(){
			var lang = this.getAttribute('data-lang');
			var media = this.getAttribute('data-media');
			var lvl = this.getAttribute('data-lvl');
			var gifting = this.getAttribute('data-gifting');

			// var carturl = lang+'/'+media+'/'+lvl;
			var carturl = RSI({'cat':lang, 'media':media, 'lvl':lvl})[0].cart;

			// add or remove gifting as needed
			if(gifting){
				var separator = carturl.indexOf('?')>-1 ? '&' : '?';
				carturl += separator + 'gifting=1';
			}
			else{
				carturl = carturl.replace(/[?&]gifting=1/gi, '');
			}

			return carturl;
		});
	}






	// *******************************************
	// RESPOND TO CART UPDATE EVENT
	// *******************************************

	$(document).on('cart_update', function(){
		update_cart_urls('.js_oct_2016_addtocart');
		$(document).trigger('cart_finished_updating');
	});






	// *******************************************
	// RESPOND TO PRICE UPDATE EVENT
	// *******************************************

	$(document).on('price_update', function(){
		update_prices('.js_oct_2016_price');
		$(document).trigger('prices_finished_updating');
	});






	// *******************************************
	// RENDER CORRECT EXPERIENCE
	// *******************************************

	function render_experience(exp){
		if(exp===2 || exp==='2' || exp==='f'){
			render_experience_f();
		}
	}

	// experience f changes 1-month sub to 3-month sub
	function render_experience_f(){

		// change product title to 3-Month
		document.querySelector('.oct_2016_product_square_header > span').innerHTML = '3-Month';

		// change multipay divisor to 3
		// $('.oct_2016_big_price_square[data-lvl="01"]').attr('data-multipay-divisor','3');

		// change 1m multipay duration to 3m
		$('.js_sub_duration_1m').html('3 months');

		// change data-lvl to "03"
		$('.js_oct_2016_product *[data-lvl="01"]').attr('data-lvl','03');
	}






	// *******************************************
	// SET CART BEHAVIOR BEFORE LANGUAGE IS SELECTED
	// *******************************************

	function set_up_pre_language(){
		$('.js_oct_2016_addtocart').html('Select Language').on('click',function(){
			smoothScrollTo('lang-menu');
			return false;
		});
	}





	// **********************************************************************
	// SMOOTH SCROLL
	// **********************************************************************
	function smoothScrollTo(scrolltargetid){
		var target = document.getElementById(scrolltargetid);
		if(target){
			$('html,body').animate({scrollTop : target.getBoundingClientRect().top + window.scrollY}, 'fast');
		}
	}





	// *****************************************************
	// BEST VALUE
	// *****************************************************
	function add_best_value(lvl){
		var $product_box = $('.js_oct_2016_price[data-lvl='+lvl+']').closest('.js_oct_2016_product');
		var $product_content = $('.js_oct_2016_price[data-lvl='+lvl+']').closest('.oct_2016_product_square_content');

		// $product_box.css({'border':'10px solid gold'});
		$product_content.prepend('<div class="oct_2016_best_value">Best Value</div>');
	}
	add_best_value('24');




	// *****************************************************
	// CHANGE COPY FROM "ALL 5 LEVELS" TO "ALL LEVELS" (because of S3)
	// *****************************************************
	/*(function(){
		var levels_copy = 'Unlimited access to all levels for selected duration';
		var mobile_bullet = document.querySelector('.oct_2016_ul li');
		var desktop_bullet = document.querySelector('.js_nov_2016_online li');
		if(mobile_bullet){
			mobile_bullet.innerHTML = levels_copy;
		}
		if(desktop_bullet){
			desktop_bullet.innerHTML = levels_copy;
		}
	})();*/





	// *****************************************************
	// INITIALIZE PAGE
	// *****************************************************
	initPage(current_experience);





	// *****************************************************
	// UPDATE PAGE IF GIFTING IS TURNED ON (?gifting=1 in url)
	// *****************************************************

	// using a function instead of a variable to avoid worrying where it was declared
	function isGifting(){
		// check cached value first
		if(isGifting.val){
			return isGifting.val;
		}
		// if no cache, then look in url
		else if(window.location.search.match(/gifting=1/i)){

			// if found, store in cache to avoid rechecking url
			isGifting.val = true;
		}
		return !!isGifting.val;
	}


	if(isGifting()){
		// select and hide gifting
		$('.js_oct_2016_gifting, .js_nov_2016_gifting').click();
		$('.oct_2016_gifting_square, .nov_2016_gifting_square').hide();


		// change cta (note: this has been added on lang change only)
		// $('.js_oct_2016_addtocart, .js_nov_2016_addtocart').html('Give as a Gift');

		// change to gifting masthead
		(function(){
			var masthead_html = '<style type="text/css"> .gifting_2016_content{font-family: gothambook, helvetica, sans-serif;} .gifting_2016_masthead{ background-image: url(\'http://www.rosettastone.com/lp/spo/gifting_2016/img/gifting_2016_masthead_mobile.jpg\'); padding-top: 64.9%; background-size: cover; background-position: center; } @media(min-width: 450px){ .gifting_2016_masthead{ background-image: url(\'http://www.rosettastone.com/lp/spo/gifting_2016/img/gifting_2016_masthead_desktop.jpg\'); padding-top: 46%; } } @media(min-width: 780px){ .gifting_2016_masthead{ padding-top: 36%; } } @media(min-width: 1000px){ .gifting_2016_masthead{ padding-top: 30%; } } @media(min-width: 1200px){ .gifting_2016_masthead{ padding-top: 26.75%; } } .gifting_2016_intro{ margin: 20px auto; padding: 0px 10px; text-align: center; color: #333; } @media(min-width: 600px){ .gifting_2016_intro{ margin: 35px auto 30px; } } .gifting_2016_intro_col{ display: inline-block; vertical-align: middle; max-width: 550px; text-align: left; } .gifting_2016_title{ font-size: 20px; color: #0098db; } .gifting_2016_ul{ color: #0098db; list-style-type: disc; margin: 0px 20px; padding: 0px; font-size: 16px; line-height: 1.3; } .gifting_2016_ul li{ margin: 10px 0px; padding: 0px; } .gifting_2016_ul span{ color: #333; } .gifting_2016_mistletoe{ background-image: url(\'http://www.rosettastone.com/lp/spo/gifting_2016/img/gifting_2016_mistletoe.png\'); background-repeat: no-repeat; padding: 50px 20px; background-size: contain; background-position: center; max-width: 380px; text-align: center; vertical-align: middle; line-height: 1.3; font-size: 20px; display: none; } @media(min-width: 1000px){ .gifting_2016_mistletoe{ display: inline-block; } } </style> <div class="gifting_2016_content"> <div class="gifting_2016_masthead"></div> <div class="gifting_2016_intro"> <div class="gifting_2016_intro_col"> <div class="gifting_2016_title"> Here\'s why Rosetta&nbsp;Stone makes a fantastic&nbsp;gift: </div> <ul class="gifting_2016_ul"> <li><span><span class="toggle_bold">Proven to work.</span> Designed by linguists with 20+ years of experience, our award-winning course has been used successfully by millions of people worldwide. </span></li> <li><span><span class="toggle_bold">Never boring.</span> No dry translation or memorization. Our immersive and engaging program is easy to use and fun. </span></li> <li><span><span class="toggle_bold">Convenient.</span> They can learn anytime, anywhere and on any device (desktop, tablet, and mobile).15 minutes a day is all they need! </span></li> <li><span><span class="toggle_bold">Features no one else has.</span> With a proven curriculum, accent training technology, live native speaking tutors, and more, our complete program gives them everything they need to see success on their language journey. </span></li> </ul> </div> <div class="gifting_2016_mistletoe"> This holiday season, get them the language program that sparked a revolution. Gift them the world with Rosetta&nbsp;Stone. </div> </div> </div>';
			$('#masthead').hide().after(masthead_html);
		})();

		// change product description bullets for gifting
		(function(){
			var subscription_html = '<li>Everything they need to start speaking&mdash;from Day 1.</li> <li>Fun, immersive and engaging&mdash;they’ll never get bored.</li> <li>Perfect for their schedule&mdash;bite-sized lessons + mobile capability for ultimate convenience.</li> <li>A life changing experience&mdash;every day is an adventure with another language!</li>';
			var cddl_html = '<li>Course that never expires&mdash;Includes total course and bonus content (language games and online community)</li> <li>Access for up to 5 family members</li> <li>Live online tutoring sessions with a Native Speaker - 3-month trial included</li> <li>Access to award winning mobile app for 3-months - available on Kindle Fire HD, iOS, and Android</li> <li>Instant digital download or CD-ROM by mail</li>';
			$('.oct_2016_ul').eq(0).html(subscription_html);
			$('.nov_2016_ul').eq(0).html(subscription_html);
			$('.oct_2016_ul').eq(1).html(cddl_html);
			$('.nov_2016_ul').eq(1).html(cddl_html);
		})();

		// in case they ask to toggle bold parts in gifting intro
		(function(){
			var they_want_bold = true;
			if(they_want_bold){
				$('.toggle_bold').css({'font-family':'gothambold, gothambook, sans-serif'});
			}
		})();


		// gifting design changes requested by albert in ws-1610
		$('.nov_2016_product_intro, .oct_2016_product_intro').hide();
		$('.oct_2016_ul').css({'padding-left':'32px'});
	}



	/**********************************************************
	LANGUAGE SELECT TRACK
	***********************************************************/

  $('.lang_select_links [data-lang]').on('click.analytics keyup.analytics', function(ev){
    if(ev.type == 'click' || ev.which == 13){
        $(document).trigger('lang_select', {lang: $(this).data('lang')});
    }
  });


	// *****************************************************
	// SHOW USD FOR CANADA
	// *****************************************************
	(function(){
		var userFromCanada = /rosettastone\.ca/i.test(window.location.href) || /rosettastone\.ca/i.test(document.referrer) || /showUSD=true/i.test(document.cookie);

		function showUSD(){
			$('.oct_2016_big_price_square, .nov_2016_big_price_square').after('<span style="font-size: 20px;font-family: gothambold;bottom: 3px;position: relative;padding-left: 5px;">(USD)</span>');
		}

		if( userFromCanada ){
			showUSD();
		}
	})();





});