// SPO3 updates muck up the readability of this file.
// SETS VARIABLES

var $lightboxDemo = $('#lightbox-demo');
var $buttonList = $('#cdButton, #downloadButton, #subscriptionButton');
var $langMenu = $('#lang-menu');
var $langHeader = $('#selected-lang');
var $mobileContainer = $('#selected-lang, #lang-menu');
var container = $("#lightBoxInner");
var cButton = $('#compareButton');
var lButton = $('#learnButton');
var dButton = $('.container .demo div');
var hiwButton = $('#howitworks');
var originalHeight = {'min-height' : $lightboxDemo.css('min-height'),'height' : $lightboxDemo.css('height')};
var cdArr = [['#cdListMobile','cdM'],['.cdListIpad'],['#cdList','cd']];
var dlArr = [['#downloadListMobile','dlM'],['.downloadListIpad'], ['#downloadList','dl']];
var subArr = [['#subscriptionListMobile','subM'],['#subscriptionList','sub']];
var secure = /https:\/\/secure.rosettastone.com\/us_en_store_view\/checkout\/cart\/add\/sku/;
var selectedLang = '';
var demoLang = 'en-es';
var email = '';
var S3 = /S3|24/;
var S5 = /S5|24/;
var desiredProducts = function(){ return RSI({cat:selectedLang,lvl:'S5'}).length ? S5 : S3; };
var tViewcd = "";
var tViewdl = "";
var mViewdl = "";
var mViewcd = "";
var mViews = "";

// SETS LANGUAGE + POPULATES LISTS WITH NORMALIZED PRODUCTS

$('.language-buy-list li a').click(function changeLanguage(){
  // sets language + products
  selectedLang = $(this).attr('id');
  setLanguageProducts(selectedLang);
  // configures headers + buttons content
  $langMenu.slideUp('slow');
  $langHeader.slideDown('slow');
  $('.lang-mob').trigger('click')
  $("#dropdown-language").val(selectedLang).change();
  $('#your-lang span').text("Learn " + RSI({cat:selectedLang,media:'download'})[0].language);
  $buttonList.addClass('buynow').text("ADD TO CART");
  $('#mobileButton').text("ADD TO CART");
  // sets demo language
  var demoLangSet = {"deu":"en-de", "fra":"en-fr", "esp" :"en-es", "ita" :"en-it" , "eng" :"en-en"};
  if(demoLangSet[selectedLang] != undefined){demoLang = demoLangSet[selectedLang];}
  if(priceIsMsrp){$('.strike').remove()}
});


// FETCHES + ORGANIZES + OUTPUTS RSI PRODUCTS

function setLanguageProducts(someLang){
  var blvls = [];
  var dloadlvls = [];
  var slvls = [];
  // clears current product lists
  $('#cdList, #subscriptionList, #downloadList, select.cdListIpad, select.downloadListIpad, #cdListMobile, #subscriptionListMobile, #downloadListMobile').html("");
  // creates ordered arrays of products by media type
  for(var i = 0; i < RSI({cat:someLang}).length; i++){
    setProductArrays(someLang, blvls, 'box',i);
    setProductArrays(someLang, dloadlvls, 'download',i);
    setProductArrays(someLang, slvls, 'subscription',i);
  }
  // normalizes and prepends products to lists
  populateLanguageProducts(blvls, cdArr);
  populateLanguageProducts(slvls, subArr);
  populateLanguageProducts(dloadlvls, dlArr);
  tmView();
}


// CREATES ORDERED ARRAYS OF PRODUCTS BY MEDIA TYPE

function setProductArrays(langClicked,xlvls,mediaType,i){
  var langSet = RSI({cat:langClicked});
  if(langSet[i].media == mediaType){
    if(desiredProducts().test(langSet[i].lvl)){
      xlvls.push(langSet[i])
      xlvls.sort(function(a,b) {
        if (a.lvl < b.lvl){
          return 1;
        }else if(a.lvl > b.lvl){
          return -1;
        }else{
          return 0;
        }
      });
    }
  }
}


// NORMALIZES + PREPENDS PRODUCTS TO VARIOUS LISTS

function populateLanguageProducts(ylvls, mediaList){
  // normalize product name
  // concatenate each product to string, prepend to master string
  // sets default products for lightboxes
  // auto selects first product in lists
  var mOptions = "";
  var tOptions = "";
  var dOptions = "";
  for(var y = 0; y < ylvls.length; y++){
    var productName = ylvls[y].name;
    var prodNameSet = {
      "Levels 1, 2, 3, 4 & 5":"Level 1-5",
      "Levels 1-5 Instant Download":"Level 1-5",
      "Levels 1, 2 & 3":"Level 1-3",
      "Levels 1-3 Instant Download":"Level 1-3",
      "24-Month Online Subscription":"24 Month"
    };
    // normalizes name
    if(prodNameSet[productName] != undefined){productName = prodNameSet[productName];}
    // sorts products into lists for each device a user is using.
    for(var z = 0; z < mediaList.length; z++){
      // mobile
      if(/Mobile/.test(mediaList[z][0])){
        mOptions +=
          '<div class="prod-options"><label><input id="' +
          ylvls[y].media + '_' + ylvls[y].lvl.toLowerCase() +
          '" type="radio" name="' +
          mediaList[z][1] +
          '" value="' +
          ylvls[y].cart + '"><span> ' +
          productName +
          '</span><div class="strikethru"><div class="strike"><del>$' +
          ylvls[y].msrp +
          '</del> |</div><div class="sale">$' +
          ylvls[y].price +
          '</div></div></label></div>'
        ;
        var gOption = "<div style='text-align: center;padding-top: 15px;'><input id='" + ylvls[y].media + "Mobilegift' type='checkbox' autocomplete='off'><span style='vertical-align:text-bottom; padding-left: 25px;background: url(\"http://www.rosettastone.com/lp/catalogpages/img/gifting.png\") no-repeat left;background-size: contain;background-position-x: 5px;font-family: gothamlight;color: #656565;font-size: 13px;'>Give as a gift!</span></div>";

        if(ylvls[y].media == 'subscription'){
          mViews = gOption +
            '<div class="strikethru">' +
            '<div class="strike"><del>$' + ylvls[y].msrp + '</del>'+
            '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAEElEQVQIW2NcycDwnxFEAAAPpwNTfkl0/wAAAABJRU5ErkJggg==" style="height: 2px;width: 100%;margin: -65% 0 0;">' +
            '</div>' +
            '<div class="sale">$' + ylvls[y].price + '</div>' +
            '</div>' +
            '<input style="display:none;" id="' + ylvls[y].media + '_' + ylvls[y].lvl.toLowerCase() +
            '" type="radio" name="' + mediaList[z][1] + '" value="' + ylvls[y].cart + '" checked> '
          ;
        }
        if(ylvls[y].media == 'box'){
          mViewcd = gOption +
            '<div class="strikethru">' +
            '<div class="strike"><del>$' + ylvls[y].msrp + '</del>'+
            '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAEElEQVQIW2NcycDwnxFEAAAPpwNTfkl0/wAAAABJRU5ErkJggg==" style="height: 2px;width: 100%;margin: -65% 0 0;">' +
            '</div>' +
            '<div class="sale">$' + ylvls[y].price + '</div>' +
            '</div>' +
            '<input style="display:none;" id="' + ylvls[y].media + '_' + ylvls[y].lvl.toLowerCase() +
            '" type="radio" name="' + mediaList[z][1] + '" value="' + ylvls[y].cart + '" checked> '
          ;
        }
        if(ylvls[y].media == 'download'){
          mViewdl = gOption +
            '<div class="strikethru">' +
            '<div class="strike"><del>$' + ylvls[y].msrp + '</del>' +
            '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAEElEQVQIW2NcycDwnxFEAAAPpwNTfkl0/wAAAABJRU5ErkJggg==" style="height: 2px;width: 100%;margin: -65% 0 0;">' +
            '</div>' +
            '<div class="sale">$' + ylvls[y].price + '</div>' +
            '</div>' +
            '<input style="display:none;" id="' + ylvls[y].media + '_' + ylvls[y].lvl.toLowerCase() +
            '" type="radio" name="' + mediaList[z][1] + '" value="' + ylvls[y].cart + '" checked> '
          ;
        }
      // tablet
      }else if(/Ipad/.test(mediaList[z][0])){
        tOptions +=
          '<option value="' +
          ylvls[y].cart +
          '" data-sku="' +
          ylvls[y].sku +
          '" data-level="' +
          ylvls[y].lvl +
          '" data-sprice="' +
          ylvls[y].price +
          '" data-rprice="' +
          ylvls[y].msrp +
          '" data-code="' +
          ylvls[y].code +
          '" class="prod-cd">' +
          productName +
          ' $' +
          ylvls[y].price +
          '</option>'
        ;
        var gOption = "<div style='text-align: center;padding-top: 15px;'><input id='" + (ylvls[y].media == 'box' ? "cd" : ylvls[y].media ) + "Tabletgift' type='checkbox' autocomplete='off'><span style='padding-left: 25px;background: url(\"http://www.rosettastone.com/lp/catalogpages/img/gifting.png\") no-repeat left;background-size: contain;background-position-x: 5px;font-family: gothamlight;color: #656565;font-size: 13px; vertical-align:text-bottom;'>Give as a gift!</span></div>";
        if(ylvls[y].media == 'box'){
          tViewcd = gOption +
            '<div class="strikethru">' +
            '<div class="strike"><del>$' + ylvls[y].msrp + '</del>' +
            '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAEElEQVQIW2NcycDwnxFEAAAPpwNTfkl0/wAAAABJRU5ErkJggg==" style="height: 2px;width: 100%;margin: -30% 0 0;">' +
            '</div>' +
            '<div class="sale">$' + ylvls[y].price + '</div>' +
            '</div>'
          ;
        }
        if(ylvls[y].media == 'download'){
          tViewdl = gOption +
            '<div class="strikethru">' +
            '<div class="strike"><del>$' + ylvls[y].msrp + '</del>' +
            '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAEElEQVQIW2NcycDwnxFEAAAPpwNTfkl0/wAAAABJRU5ErkJggg==" style="height: 2px;width: 100%;margin: -30% 0 0;">' +
            '</div>' +
            '<div class="sale">$' + ylvls[y].price + '</div>' +
            '</div>'
          ;
        }
      // desktop
      }else{

        dOptions +=
          "<div style='text-align: center;padding-top: 15px;'><input id='" + (ylvls[y].media == 'box' ? "cd" : ylvls[y].media ) +
          "gift' type='checkbox' autocomplete='off'><span style='padding-left: 25px;background: url(\"http://www.rosettastone.com/lp/catalogpages/img/gifting.png\") no-repeat left;background-size: contain;background-position-x: 5px;font-family: gothamlight;color: #656565;font-size: 13px;'>Give as a gift!</span></div>" +
          '<div class="options"><label><input id="' +
          ylvls[y].media + '_' + ylvls[y].lvl.toLowerCase() + '" type="radio" name="' +
          mediaList[z][1] + '" value="' +
          ylvls[y].cart + '"> ' +
          productName + '<span></span></label>' +
          '<div class="strikethru">' +
          '<div class="strike"><del>$' + ylvls[y].msrp + '</del>' +
          '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAEElEQVQIW2NcycDwnxFEAAAPpwNTfkl0/wAAAABJRU5ErkJggg==" style="height: 2px;width: 100%;margin: -0.75em 0 0;">' +
          '</div>'+
          '<div class="sale">$' + ylvls[y].price + '</div>' +
          '</div></div></div>'
        ;
      }
    }
    // sets default product for lightboxes
    if( typeof mediaList[2] != 'undefined' && mediaList[2][1] == 'dl' && y == ylvls.length - 1){
      if(ylvls[y].cat == 'eng' || ylvls[y].cat == 'fra' || ylvls[y].cat == 'esp' || ylvls[y].cat == 'deu' || ylvls[y].cat == 'ita'){
        $('#dropdown span').html(ylvls[y].language);
      }
    }
  }

  // prepends products to proper lists
  for(var z = 0; z < mediaList.length; z++){
    var type = mediaList[z][0];
    var option = /Mobile/.test(type) ? mOptions : /Ipad/.test(type) ? tOptions : dOptions;
    $(type).prepend(option);
  }


  // auto selects first list item
  $('.downloadListIpad').val($('.downloadListIpad option:first').val()).trigger('change');
  $('.cdListIpad').val($('.cdListIpad option:first').val()).trigger('change');
  $('#cdListMobile, #subscriptionListMobile, #downloadListMobile, #cdList, #downloadList, #subscriptionList').find('input[type=radio]:first').trigger('click');
}


// new view for ipad/iphone products (spo3 update)
function tmView(){
  $('#cdViewIpad').html(tViewcd);
  $('#downloadViewIpad').html(tViewdl);
  $('#downloadListMobile').html(mViewdl);
  $("#subscriptionListMobile").html(mViews);
  $('#cdListMobile').html(mViewcd);
}

// LANGUAGE SELECTION: DESKTOP + TABLET

$('#your-lang button').click(function langSelection(){
  $langHeader.slideUp('slow');
  $langMenu.slideDown('slow');
  $buttonList.removeClass('buynow').text("SELECT A LANGUAGE");
})


// LANGUAGE SELECTION: MOBILE

$('.lang-mob').click(function langSelectionM(e){
  e.preventDefault();
  $('.select-holder').css('display','block');
})

$('select#dropdown-language').change(function(){
  selectedLang = $(this).val();
  setLanguageProducts(selectedLang);
  $('.lang-mob').text($('select#dropdown-language>option:selected').text());
  $('#your-lang span').text("Learn " + RSI({cat:selectedLang,media:'download'})[0].language);
  $('#mobileButton').text("ADD TO CART");
});


// MEDIA SELECTION: TABLET

function tMediaSelect(selected, deselected, display, nodisplay,show){
  $('.menu a.' + selected).click(function mediaSelectionT(e){
    e.preventDefault();
    $('.prod-container.' + display).css('display','inline-block');
    // $('select.' + show + 'ListIpad').css('display','block');
    $('.prod-container.' + nodisplay + ', select.' + deselected + 'ListIpad').css('display','none');
  })
}

tMediaSelect('dl','cd','download','cdrom','download');
tMediaSelect('cd','dl','cdrom','download','cd');


// MEDIA SELECTION: MOBILE

  // download selection: via download button and desktop button
$('.menu a.desktop, .cd-download a.download-option').click(function mediaSelectionD(e){
  e.preventDefault();
  // adds styling to download button and sets background image for download products
  $('.desk-on-container .menu a.desktop, .cd-download a.download-option').addClass('selected');
  $('.desk-on-container .menu a.online, .cd-download a.cd-option').removeClass('selected');
  $('#products-mobile .select-prod .desk-on-container .content.desktop').css('background-image','url("../assets/download.png")');
  // shows download list and download verbage
  $('#downloadListMobile').show();
  $('#cdListMobile, #subscriptionListMobile').hide();
  $('#pOne').html("Instant download for your desktop");
  $('#pTwo').html("Try our award-winning mobile app for 3 months");
  $('#pThree').html("Learn at your own pace with our course that never expires");
  $('#pFour').html("Access for up to 5 family members");
  // make desktop media options visible
  $('.cd-download').show();
});

  // cd selection: via cd button
$('.cd-download a.cd-option').click(function mediaSelectionCD(e){
  e.preventDefault();
  // adds styling to cd button and sets background image for cd products
  $('.desk-on-container .menu a.desktop, .cd-download a.cd-option').addClass('selected');
  $('.desk-on-container .menu a.online, .cd-download a.download-option').removeClass('selected');
  $('#products-mobile .select-prod .desk-on-container .content.desktop').css('background-image','url("../assets/cd-box.png")');
  // shows cd list and cd verbage
  $('#cdListMobile').show();
  $('#downloadListMobile, #subscriptionListMobile').hide();
  $('#pOne').html("Shipped directly to you");
  $('#pTwo').html("Try our award-winning mobile app for 3 months");
  $('#pThree').html("Learn at your own pace with our course that never expires");
  $('#pFour').html("Access for up to 5 family members");
});

  // online selection: via online button
$('.menu a.online').click(function mediaSelectionS(e){
  e.preventDefault();
  // adds styling to online button and sets background image for online products
  $('.desk-on-container .menu a.online').addClass('selected');
  $('.desk-on-container .menu a.desktop').removeClass('selected');
  $('#products-mobile .select-prod .desk-on-container .content.desktop').css('background-image','url("../assets/online.png")');
  // shows online list and online verbage
  $('#subscriptionListMobile').show();
  $('#downloadListMobile, #cdListMobile').hide();
  $('#pOne').html("Instant access to learn online");
  $('#pTwo').html("Learn online with our award-winning mobile apps for 24 months");
  $('#pThree').html("Unlimited access for 24 months");
  $('#pFour').html("Individual access");
  // make desktop media options hidden
  $('.cd-download').hide();
});


// ADD TO CART: DESKTOP + TABLET + MOBILE


// testing affiliate code addition
var affiliateCodes = {
    "employeemall":"af-pg-ey-zz-us",
    "jmu":"af-pg-jm-zz-us",
    "beneplace":"af-pg-be-zz-us",
    "usatfmembers":"af-pg-ut-zz-us",
    "usatfathlete":"af-pg-uf-zz-us",
    "perkspot":"af-pg-pk-zz-us",
    "hbfuller":"af-pg-hb-zz-us",
    "boehringer-ingelheim":"af-pg-bi-zz-us",
    "cbp":"af-pg-cb-zz-us",
    "cypress":"af-pg-cs-zz-us"
};

function addCart(prodType){
  $('#' + prodType + 'Button').click(function addToCart(){
    if($langMenu.css('display') == "block"){
      $(this).attr('href',"#lang-menu");
      $('html, body').animate({scrollTop: $( $.attr(this, 'href') ).offset().top}, 500);
    }else{
      var url = $('#' + prodType + 'List').css('display') == "block" ? $('#' + prodType + 'List input[type=radio]:checked').val() : $('select.' + prodType + 'ListIpad option:selected').val();
      var gifting = $('#' + prodType + 'List').css('display') == "block" || prodType == 'subscription' ? $('#' + prodType + 'gift').is(':checked') : $('#' + prodType + 'Tabletgift').is(':checked');

      if(secure.test(url)){url = gifting ? url + "&gifting=1" : url;}

      // if affiliate page, pass affiliate page cid to cart
      var pageName = window.location.pathname.slice(9,-1);
      url = typeof affiliateCodes[pageName] == "undefined" ? url : url + "&cid=" + affiliateCodes[pageName];
      window.location.href = url;
    }
    return false;
  })
}

addCart('cd');
addCart('download');
addCart('subscription');


$('#mobileButton').click(function addToCartM(){
  if($('#mobileButton').text() == "ADD TO CART"){
    var media = $('#cdListMobile').css('display') == "block" ? "#cd" : $('#downloadListMobile').css('display') == "block" ? "#download" : "#subscription";
    var url = $(media + 'ListMobile input[type=radio]:checked').val();
    var gifting = media == '#cd' ? $('#boxMobilegift').is(':checked') : $(media + 'Mobilegift').is(':checked');

    if(secure.test(url)){url = gifting ? url + "&gifting=1" : url;}
    // if affiliate page, pass affiliate page cid to cart
    var pageName = window.location.pathname.slice(9,-1);
    url = typeof affiliateCodes[pageName] == "undefined" ? url : url + "&cid=" + affiliateCodes[pageName];
    window.location.href = url;

  }else{
    $('html, body').animate({
      scrollTop: $('#products-mobile').offset().top
    }, 500);
  }
  return false;
})

// OPENING LIGHTBOXES: DESKTOP + TABLET
  // compare products + what you'll learn
function showLightBox(contentType,lightBoxContent){
  $(contentType).click(function openLightBox(){
    $('html,body').scrollTop(0);
    $('#lightboxes, #lightBoxInner, ' + lightBoxContent).fadeIn('slow');
  })
}

showLightBox('#compareButton','#compare');
showLightBox('#learnButton','.wyl-wrapper');
showLightBox('#works-hov img, .works', '#howitworks')

  // appends video to how it works lightbox
$('.works').click(function howitworksappend(){
  $('#howitworks').append('<video autoplay controls="" style="width: 80%; margin-left:10%;"><source src="http://www.rosettastone.com/lp/globals/videos/features-hd.mp4" type="video/mp4">Your browser does not support the <code>video</code> element.</video>');
})

  // demo: determines if email form should or should not be shown based on demotaken/fb email cookie
$('.container .demo').click(function initiateDemo(){
  if(RSUI.util.getCookie('demotaken') || RSUI.util.getCookie('curEmailIdsc')){
    $('html,body').scrollTop(0);
    $('#lightboxes, #lightBoxInner').fadeIn('slow');
    openDemo(demoLang);
  }else{
    $('html,body').scrollTop(0);
    $('#lightboxes, #lightBoxInner, #demo').fadeIn('slow');
  }
})


// CLOSING LIGHTBOXES: DESKTOP + TABLET
  // clicking close button
$('.close').click(function closeLightBox(){
  $('#lightboxes, .wyl-wrapper, #compare, #demo, #howitworks').fadeOut('slow');
  $('#howitworks video').remove();
  $(container).fadeOut('slow');
});
  // clicking outside the lightbox content while also not clicking one of the buttons to open a lightbox
$(document).mouseup(function(e){
  if (!lButton.is(e.target) && !cButton.is(e.target) && !dButton.is(e.target) && !hiwButton.is(e.target) && !container.is(e.target) && container.has(e.target).length === 0){
    $('#lightboxes, .wyl-wrapper, #compare, #demo, #howitworks').fadeOut('slow');
    $('#howitworks video').remove();
    $(container).fadeOut('slow');
    $lightboxDemo.fadeOut().css('height', originalHeight['height']).css('min-height', originalHeight['min-height']).find('iframe').fadeOut('fast').remove();
  }
});


// COMPARE PRODUCTS LIGHTBOX FUNCTIONALITY: DESKTOP + TABLET

function compareLB(shown,hidden){
  $('#' + shown + 'Button').click(function compareProds(){
    $('#' + hidden + '-table').fadeOut('fast');
    $('#' + shown + '-table').fadeIn('fast');
    $('#' + hidden + 'Button').removeClass('active');
    $(this).addClass('active');
  });
}

compareLB('features','devices');
compareLB('devices','features');


// DEMO LIGHTBOX FUNCTIONALITY: DESKTOP + TABLET
  // choose language
$('#dropdown').click(function(e){
  $('#lang-drop').slideToggle();
});

$('#demolang li').click(function chooseDemoLang(){
  demoLang = $(this).children('a').data('id');
  $('#dropdown span').html($(this).children('a').html());
});


// DEMO: DEVICE ANALYSIS

  isDesktop = (function() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
      return false
    }else{
      return true
    }
  })();


// DEMO: OPENING DEMO

  function openDemo(lang) {
    lang = typeof lang !== 'undefined' ? lang : 'en-es'
    if(!isDesktop && lang != 'en-en'){
      var map = {
        'en-fr' : 'french',
        'en-de' : 'german',
        'en-it' : 'italian',
        'en-en' : 'english',
        'en-es' : 'spanish'
      }
      // liveperson lead generation
      var lpParam = typeof lpl == 'undefined' ? "" : lpl == true ? "&lpl=true" : "";

      window.location.href = 'http://m.rosettastone.com/demo/?lang=' + map[lang] + lpParam;

    }else{
      var demoFrame = ""
        + "<iframe "
        + " id='demo'"
        + " src='http://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=" + lang + "'"
        + " width='100%'"
        + " height='100%'"
        + " scrolling='no'"
        + "></iframe>"
        ;

      if (!isDesktop && lang == 'en-en')
      {
        window.location.href = 'http://resources.rosettastone.com/CDN/us/rs-i-demo-6/?lang=' + lang
        return
      }

      $lightboxDemo.append(demoFrame)
      var marginLeft = ((window.innerWidth - $lightboxDemo.innerWidth()) / 2) + 'px'
      $lightboxDemo.css({
        'margin-left' :  marginLeft
      })
      $lightboxDemo.fadeIn()
      originalHeight = {
        'min-height' : $lightboxDemo.css('min-height'),
        'height' : $lightboxDemo.css('height')
      };
      $lightboxDemo.find('img').click(function() {
        $lightboxDemo.find('iframe').fadeOut('fast').remove();
        $('#demo').hide();
        $lightboxDemo.fadeOut();
        $lightboxDemo.css('height', originalHeight['height']);
        $lightboxDemo.css('min-height', originalHeight['min-height']);
      })
    }
  }


// DEMO: EMAIL VALIDATION AND SUBMISSION

  // Grabs a url param from the url. returns null if not found used in data creation
  function grabUrlParam( name ) {
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null ){
      return null;
    }else{
      return results[1];
    }
  }

  function validateEmail(email){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  function hideEmailInput() {
    $('#field-email').hide()
    $('.formFoot').css('display','none');
  }

  function submitEmailToEC(email, lang) {

    if (RSUI.util.getCookie('demotaken') || RSUI.util.getCookie('rsdemo')) {

      openDemo(lang)
      return
    } else {
      var map = {
        'en-fr' : 'fra',
        'en-de' : 'deu',
        'en-it' : 'ita',
        'en-en' : 'eng',
        'en-es' : 'esp'
      }
      var data = {
        email : email,
        demo_lang : map[lang],
        cis_name : 'Flash demo',
        website: 'US_WEBSITE',
        form_type : 'demo',
        demo_type : isDesktop ? 'Demo_Desktop' : 'Demo_Mobile',
        form_url : window.location.pathname,
        newsletter_type : "Bottom_Landing_SBSR",
        cid : _satellite.getVar("mostrecentcampaign")
      }

      var url = (window.location.href.search(/(\.stg\.)|(\.local)/i)>-1 ?'http://www.stg' :'http://www')
                         + '.rosettastone.com/?p_p_id=rosettaajaxsubmit_WAR_rosettaajaxsubmitportlet&p_p_lifecycle=2&redirect2mobile=no';


      var request = $.ajax({
        url: url,
        type: "POST",
        data: data
      });
      // S-EVENTS
      request.done(function(msg) {
        if (JSON.parse(msg)[0]['cisFlag']  == 'true') {
          s.addEvent ? s.addEvent("event19") : (function(s){ s.events="event19"; })(s); s.tl();
          delete s.events;
          RSUI.util.setCookie('demotaken', "1", 1, 30 * 1000 * 60 * 60 * 24);
          hideEmailInput();
          var days_till_expiration = 30;
          var expiration = new Date(new Date().getTime() + days_till_expiration*24*60*60*1000).toUTCString();
          document.cookie='rsDemoEmail='+email+'; expires='+expiration+'; path=/; domain=.rosettastone.com';

          // liveperson lead generation
          if(!isDesktop && typeof lpTag == "object" && typeof lpl != 'undefined'){lpl = true;};

          openDemo(lang);
        } else {

          $('#emailForm input[type=text]').addClass('input-error').attr('placeholder', 'Sorry, try again!')
        }
      })
    }
  }

// S-EVENTS
// DEMO: FORM SUBMISSION

  var $form = $('#emailForm');

  $form.submit(function(e) {
    e.preventDefault()
    email = $('#field-email').val();
    var $inputEmail = $('#emailForm input[type=text]')
    var error = false;
    if (typeof demoLang == "undefined") {
      error = true
      $('#field-email').css('border', 'solid thick red');
    } else {
      error = false
      $('#field-email').css('border', 'solid 1px #d7d7d7');
    }
    // Only validate email if user doesn't have the demotaken cookie
    if (!RSUI.util.getCookie('demotaken')) {
      if (!validateEmail(email)) {
        error = true
        $('#field-email').css('border', 'solid thick red');
      } else {
        $('#field-email').css('border', 'solid 1px #d7d7d7');
      }
    s.addEvent ? s.addEvent("event18") : (function(s){ s.events="event18"; })(s);
    s.tl();
    }
    if (!error) {
      $('#demo').hide();
      submitEmailToEC(email, demoLang)
    }
  })

// S-EVENTS



// LANGUAGE SET VIA PARAM

function getUrlParameter(sParam){
  // selects the url search params and subtracts the leading ?
  var sPageURL = window.location.search.substring(1);
  // splits the variable string into an array at every & symbol
  var sURLVariables = sPageURL.split('&');
  // loops through the the entire array
  for (var i = 0; i < sURLVariables.length; i++){
    // splits the string into an array at every = symbol
    var sParameterName = sURLVariables[i].split('=');
    // checks it see if this particular param name == the sParam name you entered
    if (sParameterName[0] == sParam) {
        // returns the associated param you were looking for
        return sParameterName[1];
    }
  }
};

selectedLang = getUrlParameter('lang') || getUrlParameter('language');

if(typeof selectedLang != "undefined" && selectedLang != ""){
  $('#' + selectedLang).trigger('click');
}else{}


// SET INITIAL WYL LIGHTBOX PRODUCT
$(document).ready(function(){
  var rsop1 = RSI({cat:'esp',media:'download',lvl:'L1'})[0]
  $('#access #course-heading').html( rsop1.language + '<br> Level 1');
  $('.wyl-buy-wrapper .wyl-price .wyl-regular').html("$" + rsop1.msrp);
  $('.wyl-buy-wrapper .wyl-price .wyl-discount').html("$" + rsop1.price);
  $('.wyl-buy-wrapper .wyl-buybutton').attr('href',rsop1.cart);
})



// FIXES DOWNLOAD/CD DISPLAY ISSUES WHEN SCALING: TABLET + DESKTOP

$(window).resize(function() {
  var $downContainer = $('.prod-container.download');
  var $diskContainer = $('.prod-container.cdrom')
    // if desktop, show both download and cd products
  if($(window).width() > 1039){
    $diskContainer.css('display','inline-block');
    $downContainer.css('display','inline-block');
    // hide tablet dropdowns
    $('.prices-pad').css('display','none');
    $('.prices').css('display','block');
    // if not desktop
  }else{
    // if resizing down from desktop, show download
    if( $diskContainer.css('display') == 'inline-block' && $downContainer.css('display') =='inline-block'){
      $diskContainer.css('display','none');
      $downContainer.css('display','inline-block');
    }
    // if download is showing, continue to show
    if($diskContainer.css('display') == 'none' && $downContainer.css('display') =='inline-block'){
      $diskContainer.css('display','none');
      $downContainer.css('display','inline-block');
    }
    // if cd is showing, continue to show
    if($diskContainer.css('inline-block') == 'none' && $downContainer.css('display') == 'none'){
      $diskContainer.css('display','inline-block');
      $downContainer.css('display','none');
    }
    // show tablet dropdown
    $('.prod-container.download .prices, .prod-container.cdrom .prices').css('display','none');
    $('.prices-pad').css('display','block');
  }
});


// HIDES/SHOWS LANG MENU/SELECTION BUTTON: DESKTOP + TABLET + MOBILE

$(window).resize(function() {
    // if not mobile, consistent resizing
  if($(window).width() > 743){
    // if menu is showing, continue to show
    if($langMenu.css('display') == 'block'){
      $langHeader.hide();
      $langMenu.show();
    }
    // if header is showing, continue to show
    if($langHeader.css('display') == 'block'){
      $langHeader.show();
      $langMenu.hide();
    }
    // if resizing up from mobile, show header
    if($langMenu.css('display') == 'none' && $langHeader.css('display') == 'none'){
      $langHeader.show();
      $langMenu.hide();
    }
    // if mobile hide menu and header
  }else{
      $langHeader.hide();
      $langMenu.hide();
  }
});
  // needed?
$(document).ready(function(){
  if($(window).width() <= 743){
    $mobileContainer.hide();
  }
})


// HOVER STATES FOR DEMO + HIW + WPS: LINO

$("#demo-hov, #works-hov, #reviews-hov").hover(function(){
    $(this).find("img").animate({opacity: 0}, 500);
}, function() {
    $(this).find("img").animate({opacity: 1}, 500);
});

// IF NO LANGUAGE: SET INITIAL PAGE LOAD TO SPANISH PRICES
$('document').ready(function(){
  if(!/\?lang|&lang/.test(window.location.href)){
    selectedLang = "esp"
    setLanguageProducts(selectedLang);
  }
})





/* Alters Demo Area with TnT winning code */

// preselect spanish in mobile
if (!isDesktop && grabUrlParam('language') == null) {
  // preselect spanish
  $('#dropdown-language option:nth-of-type(2)').prop('selected', true).trigger('change')
}
if (!isDesktop){
  $('a.online').click()
}

if (isDesktop) {
  // alters demo look
   var demohov = $('#demo-hov').detach();
  $('a.demo').html('<b>Experience</b> the NEW Rosetta Stone Demo').append(demohov)
  $('#tryit a.demo').append("<span class='democta'><span> Enjoy a special, interactive tour<br> of our award-winning method.</span><button>START YOUR FREE DEMO</button>")
}

// alters demo look too
$('#reviews-hov > img').attr('src', '../assets/grace.jpg')
$('#works-hov > img').attr('src', '../assets/watch.jpg')
$('#demo-hov > img').attr('src', '../assets/tryit.jpg')


mboxCreate('US_RS_Landing_SBSR_1')


var priceIsMsrp = false;

$(document).ready(function (){

  priceIsMsrp = RSI()[0].price == RSI()[0].msrp;

  if(priceIsMsrp){

      // mastheads
      $('#regular-price').hide()

      // sbsr / ppc
      $('.strike').remove()

  }
})