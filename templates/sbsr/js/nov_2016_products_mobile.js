
/************************************************************
URL PARAMETER HELPER FUNCTIONS
*************************************************************/

function parameterMatches(param,val){
	var reg = new RegExp('[?&]'+param+'='+val+'(?=[&]|$)','i');
	return !!window.location.search.match(reg);
}

function parameterExists(param){
	var reg = new RegExp('[?&]'+param+'=','i');
	return !!window.location.search.match(reg);
}

function getParameter(param){
	var reg = new RegExp('[?&]'+param+'=([^&]+)','i');
	return parameterExists(param) ? window.location.search.match(reg)[1] : '';
}








/************************************************************
URL PARAMETER TO SHOW TOP LEVEL ONLY
*************************************************************/

// if monolvl url parameter true, hide all but top lvl products
(function(urlparam){
	if(urlparam==='on' || urlparam==='true'){
		// hide subscription level radios
		$('.js_nov_2016_online_levels').hide().removeClass('js_nov_2016_online');

		// hide cddl level radios
		$('.js_nov_2016_cddl_levels').hide().removeClass('js_nov_2016_cddl');

		// make select product title only appear when there are multiple products to select
		$('.nov_2016_select_title').hide().addClass('js_nov_2016_cddl');
	}
})(getParameter('monolvl'));







/********************************************
SET CART BEHAVIOR BEFORE LANGUAGE IS SELECTED
*********************************************/

function set_up_pre_language(){
	$('.js_nov_2016_addtocart').html('Select Language').on('click',function(){
		smoothScrollTo('products-mobile');
		return false;
	});
}
set_up_pre_language();


/***********************************************************************
SMOOTH SCROLL
************************************************************************/
function smoothScrollTo(scrolltargetid){
	var target = document.getElementById(scrolltargetid); 
	if(target){
		$('html,body').animate({scrollTop : target.getBoundingClientRect().top + window.scrollY}, 'fast');
	}
}



// show or hide test depending on whether language is preselected

(function(){
	// get preselected lang (if there is one)
	var lang = getLangUrlParam();

	// if no lang selected or is figs, show test
	if(!lang || isFIGS(lang)){
		show_test();
	}
	// for other languages, hide test
	else{
		hide_test();
	}
})();




// css fix
$('#products-mobile .container').css({'width':'auto'});



// on language change
$('#dropdown-language, #temporary_lang_dropdown').on('change', function(){
	var lang = this.value;

	// disable placeholder so user can't select "SELECT A LANGUAGE" as their language
	this.querySelector('option').setAttribute('disabled','true');

	$(document).trigger('product_change', {
		'lang': lang
	});
});

// if figs lang url parameter
if( isFIGS(getLangUrlParam()) ){
	$(document).trigger('product_change', {
		'lang': getLangUrlParam()
	});
}

function getLangUrlParam(){
	return ( (window.location.search.match(/lang(uage)*=([^&]+)/i)||[])[2] || '').toLowerCase();
}



// on change product lvl
$('input[name=nov_2016_sub_length], input[name=nov_2016_cddl_lvl]').on('change', function(){
	var lvl = this.getAttribute('data-lvl');

	$(document).trigger('product_change', {
		'lvl': lvl
	});
});

// on toggle cd/dl
$('input[name=nov_2016_cddl_toggle]').on('change', function(){
	var media = $(this).attr('data-media');

	$(document).trigger('product_change', {
		'media': media
	});
});



// on toggle online/cddl
$('.nov_2016_online_cddl').on('change', function(){
	var tab = this.value;

	var media;

	if(tab==='online'){
		media = 'subscription';

		$('.js_nov_2016_online').show();
		$('.js_nov_2016_cddl').hide();
	}
	else{
		media = $('input[name=nov_2016_cddl_toggle]:checked').attr('data-media');

		$('.js_nov_2016_online').hide();
		$('.js_nov_2016_cddl').show();
	}
	

	$(document).trigger('product_change', {
		'media': media
	});
});




function show_test(){
	$('.nov_2016_mvt_mobile').show();
	$('#products-mobile form, #products-mobile > .container > img').hide();
}

function hide_test(){
	$('.nov_2016_mvt_mobile').hide();
	$('#products-mobile form, #products-mobile > .container > img').show();
}


function check_if_s5_available(lang){

	// if S5 selected but S5 not available
	if(!hasS5(lang)){

		// hide S5
		$('input[data-lvl=S5]').closest('label').hide();

		// if current product is S5, switch to S3
		if($('.js_nov_2016_addtocart').attr('data-lvl')==='S5'){
			$('input[data-lvl=S3]').click();
		}
	}

	// if S5 is available, show it
	if(hasS5(lang)){
		// show S5
		$('input[data-lvl=S5]').closest('label').show();
	}
}

$(document).on('product_change', function(e,data){

	if(data.lang && data.lang.length===3){

		check_if_s5_available(data.lang);

		// if figs, show test
		if(isFIGS(data.lang)){
			show_test();

			// show correct button copy
			var button_copy = 'Add to Cart';
			if(window.location.search.match(/gifting=1/i)){
				button_copy = 'Give as a Gift';
			}

			// restore cart button functionality now that we have a language
			$('.js_nov_2016_addtocart').off('click').html(button_copy);

			// update language
			update_lang(data.lang);
			update_view();
		}

		// if figs, hide test
		else{
			hide_test();
		}

	}

	if(data.lvl){
		update_lvl(data.lvl);
		update_view();
	}
	if(data.media){
		update_media(data.media);

		check_if_s5_available( $('.js_nov_2016_addtocart').attr('data-lang') );

		update_view();
	}
});

// can turn figs-only state on or off (using true or false)
function isFIGS(lang){
	var figs_only = false;
	return figs_only ? /esp|fra|ita|deu/i.test(lang) : true;
}

function hasS5(lang){
	return !!RSI({cat:lang,lvl:'S5'})[0];
}

function update_lang(lang){
	var langmap = {
		esp: 'Spanish',
		eng: 'English',
		fra: 'French',
		deu: 'German',
		ita: 'Italian',
		ara: 'Arabic',
		chi: 'Chinese',
		ned: 'Dutch',
		ebr: 'English',
		tgl: 'Filipino',
		grk: 'Greek',
		heb: 'Hebrew',
		hin: 'Hindi',
		gle: 'Irish',
		jpn: 'Japanese',
		kor: 'Korean',
		far: 'Persian',
		pol: 'Polish',
		por: 'Portuguese',
		rus: 'Russian',
		esc: 'Spanish',
		sve: 'Swedish',
		tur: 'Turkish',
		vie: 'Vietnamese'
	}

	// update lang names
	$('.js_langname_1').html(langmap[lang]);

	// update cart btn data-lang
	$('.js_nov_2016_addtocart').attr('data-lang',lang);
}


function update_lvl(lvl){

	var product_name = $('input[type=radio][data-lvl='+lvl+'] ~ .js_product_name').html();
	$('.js_show_product_name').html(product_name);

	// update data attribs for price, msrp, cartbtn
	$('.js_update_lvl').attr('data-lvl',lvl);
}

function update_media(media){

	// update data-media
	$('.js_update_media').attr('data-media',media);

	// if cddl only, update where needed
	if(media!=='subscription'){
		$('.js_update_cddl').attr('data-media',media);
	}

	// then make sure lvl is possible for current media

	var selector;

	// if subscription get lvl from sub radios
	if(media==='subscription'){
		selector = 'input[name=nov_2016_sub_length]:checked';
	}

	// else get lvl from cddl radios
	else{
		selector = 'input[name=nov_2016_cddl_lvl]:checked';
	}

	var lvl = $(selector).attr('data-lvl');

	update_lvl(lvl);
}




function update_prices(){
	$('.js_nov_2016_price').html(function(){
		var media = this.getAttribute('data-media');
		var lvl = this.getAttribute('data-lvl');




		var price = RSI({media:media, lvl:lvl})[0].price;
		return price;
	});
}

function update_msrps(){
	$('.js_nov_2016_msrp').html(function(){
		var media = this.getAttribute('data-media');
		var lvl = this.getAttribute('data-lvl');
		var price = RSI({media:media, lvl:lvl})[0].price;
		var msrp = RSI({media:media, lvl:lvl})[0].msrp;

		// hide msrp if same as price
		if(price===msrp){
			$(this).closest('.js_msrp_hider').hide();
		}
		else if(price!==msrp){
			$(this).closest('.js_msrp_hider').show();
		}

		return msrp;
	});
}

function update_gifting(){
	var gifting_is_checked = document.querySelector('.js_nov_2016_gifting').checked;
	if(gifting_is_checked){
		$('.js_nov_2016_addtocart').attr('data-gifting','true');
	}
	else{
		$('.js_nov_2016_addtocart').attr('data-gifting','');
	}
}

// handle gifting checkbox input
$('.js_nov_2016_gifting').on('change',function(){
	update_gifting();
	update_cart();
});

function update_cart(){
	$('.js_nov_2016_addtocart').attr('href', function(){
		var lang = this.getAttribute('data-lang');
		var media = this.getAttribute('data-media');
		var lvl = this.getAttribute('data-lvl');
		var gifting = this.getAttribute('data-gifting');

		var carturl = RSI({cat:lang, media:media, lvl:lvl})[0].cart;

		var separator = carturl.match(/\?/) ? '&' : '?';

		if(gifting){
			carturl += separator + 'gifting=1';
		}

		return carturl;
	});
}

function update_view(){
	update_prices();
	update_msrps();
	update_gifting();
	update_cart();
}

update_view();




// fix because sbsr mobile uses esp by default
(function(){
	var lang = $('#dropdown-language').val();

	// if lang code selected
	if(lang && lang.length===3){

		// set that as the lang
		$(document).trigger('product_change', {
			'lang': lang
		});

		// set lang for desktop too just in case
		setTimeout(function(){
			if($('#multivariate_container').css('display')==='block'){
				$('a#'+lang).click();
			}
		},40);
	}
})();