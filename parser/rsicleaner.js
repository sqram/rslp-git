//Reads: pages.js; Consumes: RSI folder, Alters: Deployment folders
var fs = require('fs');
var lo_ = require('lodash');
var path = require('path')
var clog = console.log;

	/*
	 * Try to have this in a path relative to deploy folder,
	 * so it will work out of the box if someone tries to
	 * to go through the deployment process on their machine
	 * instead of the ashclp00 server
	 */
	var envslash = "/";
	var svnroot = path.dirname(__dirname) + envslash
	var pagesjsonpath = svnroot + "data/pages.json";
	var deploymentpath = svnroot + "deploy" + envslash;
	var rsispath = svnroot + "rsi" + envslash;


//language configs for other than default: rsiparser familypagegen -his
switch(process.argv.slice(-1)[0])
{
	case '-his':
			pagesjsonpath = svnroot + "data/pages-his.json";
			deploymentpath = svnroot + "deploy-his" + envslash;
			rsispath = svnroot + "rsi-his" + envslash;
		break;
	default:
		console.log("Config: Default (US/Eng)");
}

function deleteFolderRecursive(path) {
    var files = [];
    if( fs.existsSync(path) ) {
        files = fs.readdirSync(path);
        files.forEach(function(file,index){
            var curPath = path + envslash + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

var pages = JSON.parse( fs.readFileSync(pagesjsonpath) );

var currentrsis = pages.map(p => p.name);


//**Get List of RSIs currently deployed:
var deployedrsilist = fs.readdirSync(rsispath);

deployedrsilist = lo_.filter(deployedrsilist, function(v){
	//only public directories
	return v.slice(0,1) !== "." && fs.statSync(rsispath + envslash + v).isDirectory();
});


//console.log(deployedrsilist);
//=> Array of currently deployed RSIs: ['aaa','peacock', ...etc]

//**Get list of RSIs to delete in each deployment branch:
var rsistodelete = lo_.difference(deployedrsilist, currentrsis);


//console.log(rsistodelete);
//=> Array of RSI values that should be deleted in deployment branches and RSI root folder: ['skierabc','presbgd'...etc.]

//exception for sitewide
rsistodelete = lo_.reject(rsistodelete, function(v){ return v == "sitewide"; });


//**Delete RSI in root folder (do this first to stop any concurrent deployments from re-writing the next step)
lo_.each(rsistodelete, function(v){
	var dirtodelete = rsispath + v;
	if(fs.existsSync(dirtodelete))
	{
		console.log(dirtodelete);
		try
		{
			deleteFolderRecursive(dirtodelete);
			console.log("Deleted: " + dirtodelete);
		}
		catch(e)
		{
			console.log("Error deleting dir:" + e)
		}
	}
})

//**Delete dead RSIs in each branch
var deploymentcontents = fs.readdirSync(deploymentpath);

var branchdirstodelete = lo_(deploymentcontents)
	.map(function(v){
		//only public directories
		return v.slice(0,1) !== "." && fs.statSync(deploymentpath + v).isDirectory() && (deploymentpath + v);
	})
	.compact()
	.map(function(v){
		return lo_(fs.readdirSync(v))
			.filter(function(vn){ return !!~rsistodelete.indexOf(vn); })
			.map(function(vn){ return v + envslash + vn; })
			.value();
	})
	.flatten()
	.value();

//console.log(branchdirstodelete);
//=> ['SVN/deploy/newlang/avergagebii','SVN/deploy/sbs/averagebii',...etc.]

lo_.each(branchdirstodelete, function(v){
	try
	{
		deleteFolderRecursive(v);
		console.log("Deleted: " + v);
	}
	catch(e)
	{
		console.log("Error deleting dir: " + v + '\n' + e)
	}
})


/*
var deploymentcontents = fs.readdirSync(deploymentpath);

for(var i = 0,l = deploymentcontents.length; i < l; i++)
{
	var content = deploymentcontents[i];
	console.log(content)
	console.log(fs.statSync(deploymentpath + envslash + content).isDirectory());
}



var arg = process.argv.slice(2,3);
console.log(arg);
//=> "peacock"
*/
