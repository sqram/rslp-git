function RSI(opts) {
  var ret = ####PRODUCTS####;
  
  function findBy(o) {
    if(!(typeof o === "object"))
      {return [];}
    var _ret = [], tmp = ret.slice(), q = [], x;
    for(var i in o)
    {
       if(o.hasOwnProperty(i))
       {
          q.push([ i, o[i] ]); 
       }
    }
    var topush;
    if(!q.length)
    {
       return []; 
    }
    while(x = tmp.pop())
    {
      topush = true;
      for(var j = 0,l = q.length; j < l; j++)
      {
         var k = q[j][0], v = q[j][1];
         if( !(x[k] && (x[k] === v)) )
         {
            topush = false;
            j = l;
         }
      }
      if(topush)
      {
         _ret.push(x); 
      }
    }
    return _ret;
  };
  return opts ? findBy(opts) : ret;
}
####RSIPROPERTIES####