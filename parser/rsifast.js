const exec = require('child_process').exec

const page = process.argv[2]

process.chdir(__dirname)

const commands = `node rsicleaner.js && node rsiparser.js ${page} && node rsiconcat.js && node familypagegen.js && node rsioffercompiler.js`

const rsifast = exec(commands, (err, stdout, stderr) => {
  if (err)
  {
    console.log('Could not run rsi chain commands: ', err)
    process.exit(1)
  }
  process.exit(0)
})