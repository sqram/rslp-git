"use strict";

let parseModel = require('./parseModel.js');

let getProductIndex = require('./getProductInfo.js');
let product_business_info = getProductIndex('C:\\Users\\kimmel_admin\\Documents\\rslp\\data\\pony.json');

let normalizeJSON = require('./normalizeJSON.js');
let content_model = normalizeJSON({
  "sys": {
    "space": {
      "sys": {
        "type": "Link",
        "linkType": "Space",
        "id": "celb27rot2s1"
      }
    },
    "id": "1qH9CHaK2QcAcgCW2uaOWS",
    "type": "Entry",
    "createdAt": "2016-07-27T20:13:04.277Z",
    "updatedAt": "2016-07-28T19:09:10.688Z",
    "revision": 4,
    "contentType": {
      "sys": {
        "type": "Link",
        "linkType": "ContentType",
        "id": "page"
      }
    },
    "locale": "en-US"
  },
  "fields": {
    "name": "umbrellabdi",
    "mastheadDesktop": {
      "sys": {
        "type": "Link",
        "linkType": "Asset",
        "id": "3VEZXrZym48qcWQiek80Wc"
      }
    },
    "mastheadMobile": {
      "sys": {
        "type": "Link",
        "linkType": "Asset",
        "id": "3Spl9iIhcQW6EEomm64e6Q"
      }
    },
    "promo": [
      {
        "sys": {
          "type": "Link",
          "linkType": "Entry",
          "id": "5Pec3iBWYEiKkEwIk62KyG"
        }
      }
    ],
    "crescendo": [
      {
        "sys": {
          "type": "Link",
          "linkType": "Entry",
          "id": "59tLm66zYQKKoc08i8s4OY"
        }
      }
    ]
  }
});
	
let sitewide_model = {
    "name": "2q_249",
    "tosub24Promo": "amsterdam_24M",
    "tosub24Price": 249,
    "tosub12Promo": "amsterdam_12M",
    "tosub12Price": 179,
    "tosub06Promo": "shanghai_6M",
    "tosub06Price": 119,
    "tosub03Promo": "athens_3M",
    "tosub03Price": 99,
    "tosub01Promo": "athens_1M",
    "tosub01Price": 20,
    "downloadS5Promo": "amsterdam_S5",
    "downloadS5Price": 249,
    "downloadS3Promo": "amsterdam_S3",
    "downloadS3Price": 199,
    "downloadS2Promo": "amsterdam_S2",
    "downloadS2Price": 159,
    "downloadL1Promo": "",
    "downloadL1Price": 124,
    "boxS5Promo": "amsterdam_S5",
    "boxS5Price": 249,
    "boxS3Promo": "amsterdam_S3",
    "boxS3Price": 199,
    "boxS2Promo": "amsterdam_S2",
    "boxS2Price": 159,
    "boxL1Promo": "",
    "boxL1Price": 124
};


let model = Promise.all([sitewide_model, content_model, product_business_info])
  .then(parseModel)
  .then(v=>console.log(v));