"use strict";
var fs = require('fs');
var path = require('path');
module.exports = function(artefacts){
	console.log('Deploying HTML');
	var _dir = this.localpath;
	var stream = fs.createWriteStream(_dir + path.sep + 'index.html');
	return new Promise(function(resolve, reject){
		stream.on('open', function(v){
			stream.end(artefacts[0], 'utf-8', function(){ resolve(); });
		});
	});
	
}