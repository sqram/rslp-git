module.exports = function(endpoint){
	if(!endpoint) {
		return Promise.reject('Module getAPIJSON must be called with a valid URL');
	}
	var url = require('url');
	var options = url.parse(endpoint);
	if(!options) {
		return Promise.reject('Module getAPIJSON must be called with a valid URL');
	}
	
	var https = require('https');
	
	var p = new Promise(function(resolve, reject){
		var req = https.request(options, function(res,err){
			!!err && reject('API Endpoint Request Failed: ' + endpoint);
			var body = '';
			res.on('data', function(chunk){ body += chunk; });
			res.on('end', function(){
				try {
					var resultJSON = JSON.parse(body);
					resolve(resultJSON);
				}
				catch(e) {
					reject('Response did not parse to JSON as expected from provided endpoint: ' + endpoint);
				}
			});
		});
		req.end();
	});
	
	return p;

}



/*
p
 .then(x=>console.log(x))
 .catch(reason=>console.log(reason))

var x = Promise.resolve('x');

x.then(x=>console.log(x));
*/