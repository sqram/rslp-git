'use strict';

/*********** Temporary Fixed Values for Dev: ************/
//Call: -sitewide:url -model:url -parentartefactsfolder:path -pathtopony:path
const path = require('path');
const sitewide_url = 'https://cdn.contentful.com/spaces/celb27rot2s1/entries/5Pec3iBWYEiKkEwIk62KyG?access_token=701ff15332e0d10fb95c4032ea5a81afd8563b98e487c1188f53f390f5d6495d';
const model_url = 'https://cdn.contentful.com/spaces/celb27rot2s1/entries/1qH9CHaK2QcAcgCW2uaOWS?access_token=701ff15332e0d10fb95c4032ea5a81afd8563b98e487c1188f53f390f5d6495d';
const pony_path = __dirname + path.sep + '..' + path.sep + '..' + path.sep + 'data' + path.sep + 'pony.json';//'C:\\Users\\kimmel_admin\\Documents\\rslp\\data\\pony.json';
const templatefolder_path = __dirname + path.sep + 'sampletemplatefolder';

/*********** Module Loading: ************/
//To compile artefacts
let getModelJSON = require('./getAPIJSON.js');
let normalizeJSON = require('./normalizeJSON.js');
let createLocalFolderFromModel = require('./createFolder.js');
let getProductIndex = require('./getProductInfo.js');
let parseModel = require('./parseModel.js');
let getPageArtefacts = require('./getPageArtefacts.js');
let swapHTMLRSI = require('./swapRSI.js');
let swapHTMLImages = require('./swapImages.js');
let swapHTMLCrescendoURL = require('./swapCrescendo.js');

//After artefacts are compiled:
let moveImagesToLocalFolder = require('./deployImages.js');
let moveStylesAndScriptsToLocalFolder = require('./deployStylesAndScripts.js');
let moveAssetsToLocalFolder = require('./deployAssets.js');
let moveCompiledHTMLToLocalFolder = require('./deployHTML.js');

//After all artefacts are written:
let pushLocalFolderToFTP = require('./stub.js');

/*********** Module Orchestration: ************/

//Grab the sitewide business model
let sitewide_model = getModelJSON(sitewide_url)
	.then(v=>v.fields);
  
//Grab the promo model being compiled  
let content_model = getModelJSON(model_url)
	.then(normalizeJSON)
	.catch((reason)=>{ throw new Error(reason) });

//Grab the product information
let product_business_info = getProductIndex(pony_path)
	.catch((reason)=>{ console.log('Business Info Error'); throw new Error(reason) });

//parseModel: takes content from the content layer and compiles it with business info to produce the business model
/*
model => [
	[array of: all products with cart links, cat, lvl etc.],
	[array of: <model_source object>,<crescendo object>,<promo name string>,<desktop masthead image object>,<mobile masthead image object>]
]
*/
let model = Promise.all([sitewide_model, content_model, product_business_info])
	.then(parseModel)
	.catch((reason)=>{ throw new Error('Error compiling promo model: ' + reason.toString()); });
	
let rawArtefacts = getPageArtefacts(templatefolder_path);
//rawArtefacts.then((v)=>{console.log('####\n'),console.log(v[3][0][v[4]])});



//settled_artefacts == [<utf-8 encoded HTML with all prod values injected>,<css readstreams>,<js readstreams>,<asset readstreams>,<path Symbol() for streams>]
//Note: path Symbol()s were added so that the file readstreams can be piped into write streams with the same filename
let settled_artefacts = Promise.all([model, rawArtefacts])
	.then(function(v){
		let model = v[0], artefacts = v[1];
		let settledHTML = Promise.all([model, artefacts[0]])
			.then(swapHTMLRSI)
			.then(swapHTMLImages)
			.then(swapHTMLCrescendoURL)
			.then(v=>v[1]);
		return Promise.all([settledHTML].concat(artefacts.slice(1)));
	})
	.catch((reason)=>{ throw new Error('Error settling artefacts: ' + reason.toString()); });
	
//Deploy Things to Local Folder:
let local_folder_path = model
	.then(createLocalFolderFromModel);
local_folder_path
	.catch((reason)=>{ throw new Error('Error creating folder: ' + reason.toString()); });
	
//local_folder_path.then(v=>console.log('Path Settled...'));
//model.then(v=>console.log('Model Settled...'));
//settled_artefacts.then(v=>console.log('Artefacts Settled...'));
	
let ready_to_deploy = Promise.all([local_folder_path, model, settled_artefacts])
	.then((v)=>{
		//console.log('Deploying');
		let model = v[1], localpath = v[0], settled_artefacts = v[2];
		let images_moved = moveImagesToLocalFolder.call({localpath: localpath}, model);
		let html_moved = moveCompiledHTMLToLocalFolder.call({localpath: localpath}, settled_artefacts);
		let scriptsandstyles_moved = moveStylesAndScriptsToLocalFolder.call({localpath: localpath}, settled_artefacts);
		let assets_moved = moveAssetsToLocalFolder.call({localpath: localpath}, settled_artefacts);
		
		return Promise.all([images_moved, html_moved, scriptsandstyles_moved, assets_moved])
	})
	.catch((reason)=>{ throw new Error('Error deploying: ' + reason.toString()); });

//Push To FTP When Finished:
Promise.all([local_folder_path, ready_to_deploy])
	.then(pushLocalFolderToFTP)
	.catch((reason)=>{ throw new Error('Error pushing to FTP: ' + reason.toString()); });

