"use strict";
var fs = require('fs');
var lo_ = require('lodash');
module.exports = function(pony_path){
	
	function parseProducts(products) {
	
		function descendparse(obj, patharr) {
		  patharr = patharr || [];
		  if (typeof obj !== "object")
		  {
			patharr.push("#" + obj.toString());
			return [patharr];
		  }
		  else
		  {
			var ret = [];
			for(var i in obj)
			{
			  var prepend = patharr.slice();
			  var subpaths = descendparse(obj[i], prepend.concat(i));
			  for(var j=0,l=subpaths.length; j < l; j++)
			  {
				ret.push(subpaths[j]);
			  }
			}
			return ret;
		  }
		}
	
		var ret = [];
		var keys = lo_.keys(products);
		lo_.each(keys, function(v){
			var langcode = v;
			var parsed = descendparse(products[v]);
			var lang = parsed.shift()[1].slice(1);
			// console.log("parsed:");
			// console.log(parsed);
			var interpreted = lo_(parsed).map(function(v){
				var ret;
				if(!(v instanceof Array === true && v.length >= 1))
				  {return false;}
				ret = {};
				//shift off "products"
				v.shift();
				if(v[0] === "course")
				{
				  ret.family = "course";
				  ret.cat = langcode;
				  ret.lvl = v[1];
				  ret.sku = v[4] && v[4].slice(1);
				  ret.media = v[2];
				  ret.promokey = (ret.media).toLowerCase() + (ret.lvl).toUpperCase();
				  return ret;
				}
				else if(v[0] === "totale")
				{
				  ret.family = "totale";
				  ret.cat = langcode;
				  ret.lvl = v[2];
				  ret.sku = v[4] && v[4].slice(1);
				  ret.media = v[1];
				  ret.promokey = ("TO" + ret.media.slice(0, 3)).toLowerCase() + (ret.lvl).toUpperCase();
				  return ret;
				}
				else
				{
				   return false;
				}
			})
			.compact()
			.valueOf();
			// console.log("interp");
			// console.log(interpreted);
			ret = ret.concat(interpreted);
		});

		return ret;
	}
	
	
	
	try {
		var products = JSON.parse( fs.readFileSync(pony_path) );
		
	}
	catch(e) {
		return Promise.reject('Product data did not Parse into JSON as expected');
	}
	
	//console.log(parseProducts(products));

	return Promise.resolve(parseProducts(products));
}