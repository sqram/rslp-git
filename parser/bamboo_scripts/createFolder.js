"use strict";
const fs = require('fs');
const path = require('path');

module.exports = function _createFolder(model){
	
	var promoname = model[1][2];
	//console.log(!promoname);
	if(!promoname){
		return Promise.reject('Needs name for path');
	}
	
	var workingpath = __dirname + path.sep + promoname;
	if(!fs.existsSync(workingpath)){
		fs.mkdirSync(workingpath);
	}
	if(!fs.existsSync(workingpath + path.sep + 'scripts')){
		fs.mkdirSync(workingpath + path.sep + 'scripts');
	}
	if(!fs.existsSync(workingpath + path.sep + 'styles')){
		fs.mkdirSync(workingpath + path.sep + 'styles');
	}
	if(!fs.existsSync(workingpath + path.sep + 'assets')){
		fs.mkdirSync(workingpath + path.sep + 'assets');
	}
	
	return Promise.resolve(workingpath);
}