"use strict";
const lo_ = require('lodash');
const basecart = 'https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/';

function getcartapi(cat,sku,pc) {

    if(!(cat && sku))
    {
        return false;
    }

    //switch(process.argv.slice(-1)[0])
    //var _base = basecart;
    //hispanic_store_view
    var _pc = !!pc ? '/?pc=' + pc : "";

    return (
        basecart
        + 'sku/' + sku + '/'
        + 'category_id/' + cat.toLowerCase()
        + _pc
    );
}

module.exports = function(arr){
	//console.log('received: ' + JSON.stringify(v));
	//console.log(v.length);
	let products = arr[2];
	let promo_model = arr[1][0];
	let sitewide_model = arr[0];
	let ret = lo_.reduce(products,function(agg,v){
		if(promo_model[v.promokey + 'Promo']){
			v.code = promo_model[v.promokey + 'Promo'];
			v.price = promo_model[v.promokey + 'Price'];
			v.cart = getcartapi(v.cat, v.sku, v.code);
			agg.push(v);
			return agg;
		}
		else if(sitewide_model[v.promokey + 'Promo']){
			v.code = sitewide_model[v.promokey + 'Promo'];
			v.price = sitewide_model[v.promokey + 'Price'];
			v.cart = getcartapi(v.cat, v.sku, v.code);
			agg.push(v);
			return agg;
		}
		else {
			return agg;
		}
	},[])
	return Promise.resolve([ret, arr[1]]);
}

/* content_model[1] at pos[0]
downloadS5Promo: 'amsterdam',
downloadS5Price: 199,
*/
/* sitewide_model[0]
downloadS5Promo: 'amsterdam',
downloadS5Price: 199,
*/
/* business_info[2]
family: 'course'
cat: 'fra'
lvl: 'S5'
sku: '96214'
promokey: 'downloadS5'
*/