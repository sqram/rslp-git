"use strict";

module.exports = function(v){
    
	var https = require('https');
	
	if(!v){
		return Promise.reject('Must be called with an object to normalize');
	}
	
	function resolveEntry(e){
		if(!e.id){
			return Promise.reject('Entry must have ID');
		}
		var endpoint = `https://cdn.contentful.com/spaces/celb27rot2s1/${ e.linkType == 'Asset' ? 'assets' : 'entries'}/${e.id}?access_token=701ff15332e0d10fb95c4032ea5a81afd8563b98e487c1188f53f390f5d6495d`;
		var p = new Promise(function(resolve, reject){
			var req = https.request(endpoint, function(res,err){
					!!err && reject('API Endpoint Request Failed: ' + endpoint);
					var body = '';
					res.on('data', function(chunk){ body += chunk; });
					res.on('end', function(){
						//console.log('API Endpoint returned: ' + endpoint);
						try {
							var resultJSON = JSON.parse(body);
							if('fields' in resultJSON){
								resolve(resultJSON.fields);
							}
							else {
								reject('JSON from entry endpoint ' + e.id + ' did not have "fields" as expected.');
							}
						}
						catch(e) {
							reject('Response did not parse to JSON as expected from provided endpoint: ' + endpoint);
						}
					});
				});
				req.end();
		});
		
		return p;
	}
	
	return Promise.all([resolveEntry(v.fields.promo[0].sys), resolveEntry(v.fields.crescendo[0].sys), Promise.resolve(v.fields.name), resolveEntry(v.fields.mastheadDesktop.sys), resolveEntry(v.fields.mastheadMobile.sys)]);
	
}

/* top level:
	https://cdn.contentful.com/spaces/celb27rot2s1/entries/1qH9CHaK2QcAcgCW2uaOWS?access_token=701ff15332e0d10fb95c4032ea5a81afd8563b98e487c1188f53f390f5d6495d
	{
		  "sys": {
			"space": {
			  "sys": {
				"type": "Link",
				"linkType": "Space",
				"id": "celb27rot2s1"
			  }
			},
			"id": "1qH9CHaK2QcAcgCW2uaOWS",
			"type": "Entry",
			"createdAt": "2016-07-27T20:13:04.277Z",
			"updatedAt": "2016-07-27T20:43:26.612Z",
			"revision": 2,
			"contentType": {
			  "sys": {
				"type": "Link",
				"linkType": "ContentType",
				"id": "page"
			  }
			},
			"locale": "en-US"
		  },
		  "fields": {
			"name": "umbrellabdi",
			"promo": [
			  {
				"sys": {
				  "type": "Link",
				  "linkType": "Entry",
				  "id": "5Pec3iBWYEiKkEwIk62KyG"
				}
			  }
			],
			"crescendo": [
			  {
				"sys": {
				  "type": "Link",
				  "linkType": "Entry",
				  "id": "59tLm66zYQKKoc08i8s4OY"
				}
			  }
			]
		  }
	}
	
	*/
	/*
	"fields": {
    "name": "2q_249",
    "tosub24Promo": "amsterdam_24M",
    "tosub24Price": 249,
    "tosub12Promo": "amsterdam_12M",
    "tosub12Price": 179,
    "tosub06Promo": "shanghai_6M",
    "tosub06Price": 119,
    "tosub03Promo": "athens_3M",
    "tosub03Price": 99,
    "tosub01Promo": "athens_1M",
    "tosub01Price": 20,
    "downloadS5Promo": "amsterdam_S5",
    "downloadS5Price": 249,
    "downloadS3Promo": "amsterdam_S3",
    "downloadS3Price": 199,
    "downloadS2Promo": "amsterdam_S2",
    "downloadS2Price": 159,
    "downloadL1Promo": "",
    "downloadL1Price": 124,
    "boxS5Promo": "amsterdam_S5",
    "boxS5Price": 249,
    "boxS3Promo": "amsterdam_S3",
    "boxS3Price": 199,
    "boxS2Promo": "amsterdam_S2",
    "boxS2Price": 159,
    "boxL1Promo": "",
    "boxL1Price": 124
    }
	*/
	