"use strict";
let fs = require('fs');
let lo_ = require('lodash');
let path = require('path');
const pathprop = Symbol('pathprop');

module.exports = function(parentpath){
	function getStreamArray(dirpath) {
		let pathArray = fs.readdirSync(dirpath, 'utf-8');
		return pathArray.map((v)=>{var ret = fs.createReadStream(dirpath + path.sep + v); ret[pathprop] = v; return ret;});
	}
	
	let HTML = fs.readFileSync(parentpath + path.sep + 'index.html','utf-8');
	let cssArray = getStreamArray(parentpath + path.sep + 'styles');
	let scriptArray = getStreamArray(parentpath + path.sep + 'scripts');
	let assetsArray = getStreamArray(parentpath + path.sep + 'assets');
	
	return Promise.resolve([HTML, cssArray, scriptArray, assetsArray, pathprop]);
}