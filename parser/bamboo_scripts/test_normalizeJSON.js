"use strict";

let normalizeJSON = require('./normalizeJSON.js');

var res = normalizeJSON({
  "sys": {
    "space": {
      "sys": {
        "type": "Link",
        "linkType": "Space",
        "id": "celb27rot2s1"
      }
    },
    "id": "1qH9CHaK2QcAcgCW2uaOWS",
    "type": "Entry",
    "createdAt": "2016-07-27T20:13:04.277Z",
    "updatedAt": "2016-07-28T19:09:10.688Z",
    "revision": 4,
    "contentType": {
      "sys": {
        "type": "Link",
        "linkType": "ContentType",
        "id": "page"
      }
    },
    "locale": "en-US"
  },
  "fields": {
    "name": "umbrellabdi",
    "mastheadDesktop": {
      "sys": {
        "type": "Link",
        "linkType": "Asset",
        "id": "3VEZXrZym48qcWQiek80Wc"
      }
    },
    "mastheadMobile": {
      "sys": {
        "type": "Link",
        "linkType": "Asset",
        "id": "3Spl9iIhcQW6EEomm64e6Q"
      }
    },
    "promo": [
      {
        "sys": {
          "type": "Link",
          "linkType": "Entry",
          "id": "5Pec3iBWYEiKkEwIk62KyG"
        }
      }
    ],
    "crescendo": [
      {
        "sys": {
          "type": "Link",
          "linkType": "Entry",
          "id": "59tLm66zYQKKoc08i8s4OY"
        }
      }
    ]
  }
});
	
res.then(v=>console.log(v));