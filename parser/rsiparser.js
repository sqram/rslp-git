var fs = require('fs');
var lo_ = require('lodash');
var path = require('path')
//npm install uglify-js@1.3.5
var jsp = require("uglify-js").parser;
var pro = require("uglify-js").uglify;
var clog = console.log;
var process = require('process')

/*
 * Try to have this in a path relative to deploy folder,
 * so it will work out of the box if someone tries to
 * to go through the deployment process on their machine
 * instead of the ashclp00 server
 */

var svnroot = path.dirname(__dirname)
var promospath = path.join(svnroot, "data", "promosets.json");
var prodinfopath = path.join(svnroot, "data", "product-info.json");
var pagesjsonpath = path.join(svnroot, "data", "pages.json");
var productspath = path.join(svnroot, "data", "pony.json");
var shellpath = path.join(__dirname, "rsishell.js");
var rsifolderpath = path.join(svnroot,"rsi");
//var queryinpath = svnroot + "queryops.json";

var basecart = 'https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/';

//var applypromodiscounts = require(path.join(__dirname,"applypromodiscounts.js"));


//remove when pages.json
// eval(fs.readFileSync(pagesjsonpath).toString());
var pages = JSON.parse( fs.readFileSync(pagesjsonpath) );

//var pages = JSON.parse( fs.readFileSync(pagesjsonpath) );
//console.log("Loaded pages.json...");
var qbpromos = JSON.parse( fs.readFileSync(promospath) );


//console.log("Loaded qbpromos...");
var qbprod = JSON.parse( fs.readFileSync(prodinfopath) );//.prodinfo;
//console.log("Loaded qb prod info...");
var products = JSON.parse( fs.readFileSync(productspath) );
//console.log("Loaded products...");
//var querytargets = JSON.parse(fs.readFileSync(queryinpath));
//console.log("Loaded query targets");
//transform 1st arg in each array to regexp:
//querytargets = lo_.map(querytargets, function(v){ v[0] = new RegExp(v[0]); return v; })

var arg = process.argv.slice(2,3);

function descendparse(obj, patharr) {
  patharr = patharr || [];
  if (typeof obj !== "object")
  {
    patharr.push("#" + obj.toString());
    return [patharr];
  }
  else
  {
    var ret = [];
    for(var i in obj)
    {
      var prepend = patharr.slice();
      var subpaths = descendparse(obj[i], prepend.concat(i));
      for(var j=0,l=subpaths.length; j < l; j++)
      {
        ret.push(subpaths[j]);
      }
    }
    return ret;
  }
}

function parseProducts(products) {
    var ret = [];
    var keys = lo_.keys(products);
    lo_.each(keys, function(v){
        var langcode = v;
        var parsed = descendparse(products[v]);
        var lang = parsed.shift()[1].slice(1);
        // console.log("parsed:");
        // console.log(parsed);
        var interpreted = lo_(parsed).map(function(v){
            var ret;
            if(!(v instanceof Array === true && v.length >= 1))
              {return false;}
            ret = {};
            //shift off "products"
            v.shift();
            if(v[0] === "course")
            {
              ret.family = "course";
              ret.cat = langcode;
              ret.lvl = v[1];
              ret.sku = v[4] && v[4].slice(1);
              ret.media = v[2];
              ret.promokey = (ret.media + "_" + ret.lvl).toUpperCase();
              return ret;
            }
            else if(v[0] === "totale")
            {
              ret.family = "totale";
              ret.cat = langcode;
              ret.lvl = v[2];
              ret.sku = v[4] && v[4].slice(1);
              ret.media = v[1];
              ret.promokey = ("TO" + ret.media.slice(0, 3) + "_" + ret.lvl).toUpperCase();
              return ret;
            }
            else
            {
               return false;
            }
        })
        .compact()
        .valueOf();
        // console.log("interp");
        // console.log(interpreted);
        ret = ret.concat(interpreted);
    });

    return ret;
}

function preparecode(cd) {

    try {
        var ast = jsp.parse(cd); // parse code and get the initial AST
    }
    catch(e) {
        console.log(e);
        return false;
    }
    ast = pro.ast_mangle(ast); // get a new AST with mangled names
    ast = pro.ast_squeeze(ast); // get an AST with compression optimizations
    var final_code = pro.gen_code(ast); // compressed code here

    return final_code;
}

function getcartapi(cat,sku,pc, market) {
    var carturls = {
        us: 'https://secure.rosettastone.com/us_en_store_view/checkout/cart/add/',
        es: 'https://secure.rosettastone.com/hispanic_store_view/checkout/cart/add/'
    }

    if (typeof carturls[market]  == 'undefined') {
        //console.log('-- cant build cart url. invalid market.-- ')
        //return
    } else {
        basecart = carturls[market]
    }

    if(!(cat && sku))
    {
        return false;
    }

    //switch(process.argv.slice(-1)[0])
    //var _base = basecart;
    //hispanic_store_view
    var _pc = pc ? '/?pc=' + pc : "";

    return (
        basecart
        + 'sku/' + sku + '/'
        + 'category_id/' + cat.toLowerCase()
        + _pc
    );
}

function processRSI(rsi, processRSI_cb) {

    console.log('Processing ' + rsi.name + '...');

    var rsidata = rsi



    //console.log(rsidata)
    if(typeof rsidata == 'undefined')
    {
        processRSI_cb("RSI does not exist.");
        return;
    }
    var promocode = rsidata.promoset || "";
    var promodata = qbpromos[promocode];
    var productsarray = parseProducts(products);

    var msrp;
    try {
        promoprice = promodata['BOX_S5']['price']
        msrp = promodata['BOX_S5']['qb_price']
    } catch (e) {
        if (e)
            throw ('[rsioffercompiler] promocode is not in promos.json: ', promocode)
    }

    if (promoprice && msrp) {
        if (rsidata.strikethrough.length != 2) {
            rsidata.strikethrough = [msrp, promoprice];
        }
    }

    productsarray = lo_.map(productsarray, function(v) {

        var prodinfo = (qbprod && qbprod[v.promokey.split("_")[0]]);
        prodinfo = prodinfo && prodinfo[v.promokey.split("_")[1]];
        v.desc = (!!prodinfo && prodinfo["qb_desc"]) || "";
        v.img = (!!prodinfo && prodinfo["qb_img"]) || "";
        v.name = (!!prodinfo && prodinfo["qb_name"]) || "";
        v.msrp = (!!prodinfo && prodinfo["qb_price"]) || "";

        v.code = (promodata && promodata[v.promokey] && promodata[v.promokey].code ) || "";
        v.price = (promodata && promodata[v.promokey] && promodata[v.promokey].price );
        v.language = products[v.cat] && products[v.cat].language;
        v.cart = getcartapi(v.cat, v.sku, v.code, rsi.metadata.market);
        //no price listed in promos; Panic and get it from 'sitewide'
        if(!v.price)
        {
            //console.log(v.promokey);
            //console.log(Object.keys(qbpromos["sitewide"]));

            v.price = (qbpromos["sitewide"][v.promokey] && qbpromos["sitewide"][v.promokey].price );
            v.code = (qbpromos["sitewide"][v.promokey] && qbpromos["sitewide"][v.promokey].code ) || v.code;
        }
        //As a last resort, suppress products with no price...
        if(!v.cart)
        {
            //no cart link for product: suppress and report
            //console.log("Suppressing: " + v.cat + v.promokey + ' ...no sku/cart');
            return false;
        }
        if(!v.price)
        {
            //no price for product: suppress and report
            //console.log("Suppressing: " + v.cat + '::' + v.promokey + ' ...no price');
            return false;
        }
        //If specified, take a further %age discount off the marked promo price.
        /*if (rsidata.header.discount) {
            v.price = Math.ceil(v.price * (1-rsidata.header.discount));
        }*/
        return v;
    });
    productsarray = lo_.compact(productsarray);

    var rsikvpairs = [];
    var rsikeys = Object.getOwnPropertyNames(rsidata);
    var k;
    while(rsikeys.length)
    {
        k = rsikeys[0];
        rsikvpairs.push([k,JSON.stringify(rsidata[k])]);
        rsikeys.shift();
    }



    rsikvpairs.push(["rsi", (rsi.name && "'" + rsi.name+ "'") || "'sitewide'"]);
    var rsiprops = lo_.map(rsikvpairs, function(v){
        return "RSI." + v[0] + " = " + v[1] + ";";
    });
    rsiprops = rsiprops.join("\n");
    var shelltext = fs.readFileSync(shellpath, "utf-8");
    var rsijs = shelltext.replace("####PRODUCTS####", JSON.stringify(productsarray)).replace("####RSIPROPERTIES####", rsiprops);

    rsijs = preparecode(rsijs);


    processRSI_cb(!rsijs, rsijs, rsi.name);

}

function RSIHandler(e,r,_rsicode){

        _rsicode = _rsicode || "sitewide";
        if(!e)
        {
            if(!fs.existsSync(path.join(rsifolderpath, _rsicode))) {
                fs.mkdirSync(path.join(rsifolderpath, _rsicode));
            }



            // Drop the model inside rsi/<pagename>model.js  and globals/models/<pagename>.js
            fs.writeFileSync(path.join(rsifolderpath, _rsicode, "model.js"), r);
            fs.writeFileSync(path.join(svnroot, 'deploy', 'globals', 'models', _rsicode + '.js'), r)
        }
        else
        {
            //console.log("Error processing RSI: " + e);

        }
}

//qbpromos = applypromodiscounts(qbpromos);


if ((arg && arg[0]) === "-a")
{
    //console.log("Compiling all RSIs...");
    var allrsi = pages.slice()
    while(allrsi.length)
    {
        processRSI(allrsi.pop(), RSIHandler);
    }
}
else
{
    var rsi = pages.filter(p => p.name == arg[0])[0]
    processRSI(rsi, RSIHandler);

    process.exit(0)

}
// var shelltxt = fs.readFileSync(shellpath, "utf-8");
// console.log(preparecode(shelltxt));


