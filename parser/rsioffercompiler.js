var fs = require('fs');
var lo_ = require('lodash');
var mkdirp = require('mkdirp');
var path = require('path')


var svnroot = path.join(path.dirname(__dirname))
var pagesjsonpath = path.join(svnroot, "data", "pages.json");
var campaignjsonpath = path.join(svnroot, "campaign.json");
var rsifolderpath = path.join(svnroot, "rsi");
var templatespath = path.join(svnroot, 'offertemplates');
var promospath = path.join(svnroot, 'data', 'promosets.json');
var prodinfopath = path.join(svnroot, "data", "product-info.json");
var prodinfo = JSON.parse( fs.readFileSync(prodinfopath) )
//var applypromodiscounts = require(path.join(__dirname,"applypromodiscounts.js"));

var onlyrsi = false;
var onlypagefamily = false;


// contents of pages & promos
var pages = JSON.parse( fs.readFileSync(pagesjsonpath) );
var promos = JSON.parse( fs.readFileSync(promospath) );
var msrp = prodinfo.DOWNLOAD.S5.qb_price;
promos = promos;


//=> [['nostrikemiddle', pathofnostrikemiddle],['strikeright',pathofnostrikeright]]]
var templatefolders =  lo_.map(fs.readdirSync(templatespath), function(v){
    return fs.lstatSync(path.join(templatespath, v)).isDirectory() && [v, path.join(templatespath, v)];
});



var obtemplates = lo_(templatefolders)
.map(function(v){
    if( fs.existsSync(path.join(v[1],"offer.html") ))
    {
        //lodashcompiledtemplate
        //console.log("Attempting to compile " + v[1] + "offer.html");
        try {
            return v.concat( lo_.template( fs.readFileSync( path.join(v[1],"offer.html"),"utf-8") ) );
        }
        catch(e) {
            //console.log('lodash template error:\n\n');
            //console.log(e);
        }
    }
    else
    {
        //console.log("Skipping " + v[1] + ' ... no /offer.html found.');
        return false;
    }
})
.compact()
.value();


//console.log(obtemplates);
//=> [['nostrikemiddle', pathofnostrikemiddle, TemplateFunction], ...]

//console.log(obtemplates[0][2]({mastheadimage: "myimage"}));
//=> template rendered with "myimage" in place of <%= mastheadimage %>

var templatesmap = lo_.reduce(obtemplates, function(a,v,k){

    a[v[0]] = [v[1],v[2]];
    return a;
}, {});
//console.log(templatesmap);
//=> {strikeright: [pathtostrikeright, strikerighttemplate]}

for(var rsilist = pages, currentrsi; currentrsi = rsilist.pop();)
{



  //look up what header to use
  var rsiObj = currentrsi





    var currentoffertype = rsiObj.mastheadtype;


    //console.log(currentoffertype);
    //=> 'strikeright'
    if(currentoffertype && !templatesmap[currentoffertype])
    {
      throw new Error("Error: rsi " + rsiObj.name + " expects offer type: " + currentoffertype + "  - but it's not defined.");
    }

    //get the rsi's offer html template
    var currentheaderhtmltemplate = currentoffertype && templatesmap[currentoffertype][1];

    if(!currentheaderhtmltemplate)
    {
      //console.log('Skipping ' + rsiObj.name + ' ...no html template found for this rsi\'s header.type');
      continue;
    }

    try {
      var currentofferhtml = currentheaderhtmltemplate(rsiObj);
    }
    catch(e)
    {
      //console.log('error on: ', rsiObj.name)
      throw new Error("Error rendering offer html: ", e);
      break;
    }

    //console.log(currentofferhtml);
    //=> lodash-rendered html

    //look up available [pagefamily dirs] for that offertype
    var currentoffertypepath = templatesmap[currentoffertype][0];

    var currentfamilydirs = lo_.filter(fs.readdirSync(currentoffertypepath), function(v){
      return !!fs.lstatSync(path.join(currentoffertypepath, v)).isDirectory()
    });

    //console.log(currentfamilydirs);
    //=> ['sbs','newlang', ...]

    //for every [pagefamily dir]

    for(var currentfamily; currentfamily = currentfamilydirs.pop();)
    {


      //=> path to css

      //get the css template for that offertype/pagefamily

      var csspath = path.join(currentoffertypepath, currentfamily,"offer.css");

      if ( fs.existsSync(csspath) )
      {
        //render the css
        var renderedcss = lo_.template(fs.readFileSync(csspath, "utf-8"), rsiObj);

        //console.log(currentofferhtml + '\n' + renderedcss);
        //=> all the html and css




        //=> the place to put the offerbox stuff

        var outputdir = path.join(rsifolderpath,currentrsi.name,'offerboxes',currentfamily);

        if(!fs.existsSync(outputdir))
        {
          mkdirp.sync(outputdir);
        }
        //console.log("Writing offer html + styles to: " + outputdir + "offer.txt" );

        fs.writeFileSync(path.join(outputdir, "offer.txt"), currentofferhtml + '<style>' + renderedcss + '</style>');
      }
    }
}

