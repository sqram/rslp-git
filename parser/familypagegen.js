var fs = require("fs");
var fse = require('fs-extra')
var lo_ = require('lodash');
// var jsp = require("uglify-js").parser;
// var pro = require("uglify-js").uglify;
var path = require('path')


var svnroot = path.dirname(__dirname)
var templatespath = path.join(svnroot, "templates");
var rsipath = path.join(svnroot, "rsi");
var outpath = path.join(svnroot, "deploy");
var pagesjsonpath = path.join(svnroot, "data", "pages.json");
var pages = JSON.parse( fs.readFileSync(pagesjsonpath) );
var headerFile = path.join(svnroot,"parser", "header.txt")
var promospath = path.join(svnroot, "data", "promosets.json");
var promos = JSON.parse( fs.readFileSync(promospath) );


function readandreturn(filepath) {

    if(fs.existsSync(filepath)) {
        return fs.readFileSync(filepath, "utf-8");
    }
    else {
        return false;
    }
}


function gettoplevelhtmlfiles(dir) {

    var ret = [];
    if(fs.existsSync(dir) && fs.lstatSync(dir).isDirectory())
    {

        return lo_(fs.readdirSync(dir))
            .map(function(v){
                return fs.lstatSync(path.join(dir,v)).isFile() && v.match(/\.html/gi) ? [v,dir + v] : false;
            })
            .map(function(v){
                return v[0] == "index.html" ? false : v;
            })
            .compact()
            .value();

    }
    //console.error("Warning: directory not found - " + dir);
    return ret;
}

/*function shallowcopydir(dirsrc, dirtarget) {
    //console.log(dirsrc);
    console.log(dirtarget);
    if(fs.existsSync(dirsrc))
    {
        if(!fs.existsSync(dirtarget)) {
            fs.mkdirSync(dirtarget);
        }
        var srcfiles = fs.readdirSync(dirsrc);
        lo_.each(srcfiles, function(v){
            //console.log(v);
            var srcpath = path.join(dirsrc,  v);
            if(fs.lstatSync(srcpath).isDirectory()) {
                //do nothing
            } else {
                var filecontents = fs.readFileSync(srcpath);
                fs.writeFileSync(path.join(dirtarget, v), filecontents);
            }

        });
    }
    else
    {
        return null;
    }
}*/

var rsidirs = fs.readdirSync(rsipath);
//console.log(rsidirs);
//rsidirs => ['aaa','peacock','valentines',...]
var familydirs = fs.readdirSync(templatespath);
//console.log(familydirs);
//familydirs => ['newlang','sbs','mobilelp'...]

var n = lo_.map(rsidirs, function(v){ return [v,readandreturn(path.join(rsipath, v, 'model.js'))] });
//n => [['peacock',(contents of peacock's model.js)],['somersi',(contents of somersi's model.js)]]

var m = lo_.map(familydirs, function(v){ return [v,readandreturn(path.join(templatespath, v , 'index.html'))] });

//m => [['sbs',(contents of sbsindex.html)],['newlang',(contents of newlang.html)]]
/*var m = [
    ["newlang",readandreturn("C:\\Users\\bkimmel\\Documents\\rslp\\templates\\newlang\\index.html")],
    ["mobilelp",readandreturn("C:\\Users\\bkimmel\\Documents\\rslp\\templates\\mobilelp\\index.html")]
];*/


//reject elements with no source...
n = lo_.filter(n, function(v){ return !!v[1]; });
//m = ( process.argv.slice(2,3) && process.argv.slice(2,3) != "-his" ) ? lo_.filter(m, function(v){ return !!v[1] && v[0] == process.argv.slice(2,3); }) : lo_.filter(m, function(v){ return !!v[1]; });
//reject elements with no source...
m = lo_.filter(m, function(v){ return !!v[1]; });

//if there is an argument to the process and it's not -his...
if(process.argv.slice(2,3).length && process.argv.slice(2,3)[0] != "-his")
{
    //limit m to the designated pagefamily:
    m = lo_.filter(m, function(v){ return !!v[1] && v[0] == process.argv.slice(2,3); });
}

//make dirs for families that don't exist yet:

lo_.each(m, function(v){

    var tmpname = v[0];

    if(!fs.existsSync(path.join(outpath, tmpname))) {
        fs.mkdirSync(path.join(outpath,tmpname));
    }
});



//build headers object
var headersmap = lo_(n)
    .map(function(vn) {

        if(fs.existsSync(path.join(rsipath, vn[0],  "headers.txt")))
        {
            return [vn[0], fs.readFileSync(path.join(rsipath, vn[0], "headers.txt","utf-8"))];
        }
        return false;
    })
    .compact()
    /*
    .tap(function(v){
        console.log(v);
        //=> [['peacock','<title>200 off</title><meta ogImage:"something.jpg">'],['peacockbii','<title>only $299</title><meta>...']
    })
    */
    .zipObject()
    .valueOf();

//m = lo_.filter(m, function(v){ return v[0] == "sbs"; });
//n = lo_.filter(n, function(v){ return v[0] == "accentcbd"; });
//=> for testing, to constrain m to only sbs...

//console.log('Page Families:');
lo_(m).each(function(v){ /*console.log(v[0]); */});
//console.log('rsis:');
lo_(n).each(function(v){ /*console.log(v[0]);*/ });

lo_.each(m, function(v){
    var tmpname = v[0];
    //tmpname => the pagefamily ('sbs','newlang',etc...)
    //console.log('Deploying ' + tmpname + '...');
    var baseindex = v[1];
    //baseindex => <html>... with markers

    var mdir = path.join(outpath, tmpname)
    //mdir => "destination/sbs/"

    // copy js/css/assets from template/sbs to deploy/sbs/


    //shallowcopydir(path.join(templatespath, tmpname, 'js'), path.join(mdir, 'js'))
    //shallowcopydir(path.join(templatespath, tmpname, 'css'), path.join(mdir, 'css'))
    //shallowcopydir(path.join(templatespath, tmpname, 'assets'), path.join(mdir, 'assets'))

    fse.copy(path.join(templatespath, tmpname, 'js'), path.join(mdir, 'js'))
    fse.copy(path.join(templatespath, tmpname, 'css'), path.join(mdir, 'css'))
    fse.copy(path.join(templatespath, tmpname, 'assets'), path.join(mdir, 'assets'))




    //make all the necessary rsi dirs if they don't exist
    lo_.each(n, function(vn){

        var rsiname = vn[0];

        if(!fs.existsSync(path.join(mdir, rsiname))) {
            //e.g. if /sbs/rsiname/ does not exist, create it...
            fs.mkdirSync(path.join(mdir, rsiname));
        }
    });

    //make index.html file and place for every /rsi
    lo_.each(n, function(vn){
        //console.log('\t' + vn[0]);
        var rsiname = vn[0];
        var rsidir = path.join(mdir, rsiname)
        var deployment = v[1].replace(/<!--##MODEL##-->([\s\S])*<!--\/##MODEL##-->/igm, '<script type="text/javascript">' + vn[1] + '</script><!--'+ rsiname +'-->');
        //console.log(templatespath + tmpname + envslash + 'scripts' + envslash + 'concatall.js');
        if(fs.existsSync(path.join(templatespath, tmpname, 'scripts', 'concatall.js')))
        {
            //console.log('\t\tinjecting compressed scripts...');
            deployment = deployment.replace(/<!--##BODYSCRIPTS##-->([\s\S])*<!--\/##BODYSCRIPTS##-->/igm, '<script src="./scripts/concatall.js"></script>');
        }
        /*if(!!headersmap[rsiname])
        {
            //console.log('\t\tinjecting title/meta tags...');
            deployment = deployment.replace(/<!--##HEADERS##-->([\s\S])*<!--\/##HEADERS##-->/igm, headersmap[rsiname]);
        }*/

        // if there's a header.txt, fill it in
        if(fs.existsSync(headerFile))
        {
            //console.log('\t\tinjecting title/meta tags...');
            var headerhtml = fs.readFileSync(headerFile, 'utf-8')
            				 .replace(/sbs\/sitewide/ig, tmpname+'/'+rsiname);

            var msrp, price;
            try{
            	msrp = promos.prodinfo.DOWNLOAD.S5.qb_price;
				price = promos['promos'][rsiname]['DOWNLOAD_S5']['price'];
			}
			catch(e){}

			if(msrp && price){
				headerhtml = headerhtml.replace(/259/g,msrp)
									   .replace(/229/g,price);
			}

            deployment = deployment.replace(/<!--##HEADSTUFF##-->([\s\S])*<!--\/##HEADSTUFF##-->/ig, headerhtml);
        }
        var offerpath = path.join(rsipath, rsiname, 'offerboxes', tmpname, 'offer.txt')
        //console.log(offerpath);
        if(fs.existsSync(offerpath))
        {
            //console.log('\t\tinjecting masthead/offerbox');
            var offerboxhtml = fs.readFileSync(offerpath, 'utf-8');
            deployment = deployment.replace(/<!--##OFFERDIV##-->([\s\S])*<!--\/##OFFERDIV##-->/igm, offerboxhtml);
        }
        else
        {
            if( deployment.match(/<!--##OFFERDIV##-->([\s\S])*<!--\/##OFFERDIV##-->/igm) )
            {
                //throw new Error('No "offer.txt" found at: ' + offerpath + ' ... this file is required.');
                //console.log('ERROR:\nExpected RSI at ' + offerpath + ' and found none...');
            }
        }
        // take crescendo node out of this rsi in pages.json and put inside this dir
        /*if (typeof pages[rsiname]['crescendo'] != 'undefined') {
            var crescendo = JSON.stringify(pages[rsiname]['crescendo'])
            fs.writeFileSync(rsidir + envslash + 'crescendo.json', crescendo)
        }*/

        (function(nd) {
            fs.writeFileSync(path.join(rsidir , "index.html"), nd)
        })(deployment);

    });


    //transfer scripts for every /rsi
    lo_.each(n, function(vn){
        var pthsrc = path.join(templatespath, tmpname, 'scripts')
        //console.log(pathsrc);
        //=> templates/tablet70/scripts/
        var pthtarget = path.join(mdir, vn[0], 'scripts')
        //console.log(pthtarget);
        //=> deploy/sbstablet70/peacock/scripts/
        fse.copy(pthsrc, pthtarget)
        //shallowcopydir(pthsrc, pthtarget);


    });


    //transfer /*.html to every branch
    var indices = gettoplevelhtmlfiles(path.join(templatespath, tmpname))
    //console.log(indices);
    //=> [['extrathing.html','fullpath/to/extrathing.html'],['otherthing.html','fullpath/to/otherthing.html']]

    lo_.each(indices, function(vi){

        lo_.each(n, function(vn){
            fs.writeFileSync( path.join(mdir, vn[0], vi[0], fs.readFileSync(vi[1]) ))
        });

    });

});


